package ke.co.hoji.android.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import ke.co.hoji.BuildConfig;
import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.data.Progress;
import ke.co.hoji.core.data.PageInfo;
import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.http.UploadRequest;
import ke.co.hoji.core.data.http.UploadResponse;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.service.model.HttpException;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.core.service.model.RecordService;

import static ke.co.hoji.core.data.Constants.Api;
import static ke.co.hoji.core.data.Constants.PropertyKeys;
import static ke.co.hoji.core.data.Constants.Server;
import static ke.co.hoji.core.data.model.MainRecord.Stage;

/**
 * Created by gitahi on 13/07/15.
 */
public class UploadService extends IntentService {

    private AndroidHojiContext androidHojiContext;
    private HttpService httpService;
    private PropertyService propertyService;

    private boolean busy = false;
    private int batchNumber;

    /**
     * Whether or not to show upload progress. This is necessary so that we do not show upload
     * progress when this service is running automatically in the background.
     */
    private boolean showUploadProgress = false;

    public UploadService() {
        super("UploadIntentService");
    }

    @Override
    public void onCreate() {
        this.androidHojiContext = (AndroidHojiContext) getApplication();
        this.httpService = androidHojiContext.getHttpService();
        this.propertyService = androidHojiContext.getModelServiceManager().getPropertyService();
        super.onCreate();
    }

    /**
     * This override allows the user to "reset" the {@link #showUploadProgress} variable by clicking
     * on the upload button when a background upload is already running. This way, they can start
     * seeing upload progress.
     */
    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        this.showUploadProgress = intent.getBooleanExtra(Constants.SHOW_UPLOAD_PROGRESS, false);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        this.showUploadProgress = intent.getBooleanExtra(Constants.SHOW_UPLOAD_PROGRESS, false);
        broadcastProgress(Progress.Status.BUSY, getResources().getString(R.string.counting_records));
        int totalEligible = countRecords();
        if (totalEligible < 1) {
            broadcastProgress(Progress.Status.ABORTED,
                    getResources().getString(R.string.nothing_to_send));
            return;
        }
        int totalUploaded = 0;
        int pageSize = Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(this)
                .getString(SettingsActivity.PAGE_SIZE, String.valueOf(PageInfo.PAGE_SIZE)));
        final int noOfBatches = (totalEligible + pageSize - 1) / pageSize;
        batchNumber = 1;
        UploadResponse response = null;
        while (true) {
            busy = true;
            List<MainRecord> mainRecords = null;
            try {
                mainRecords = fetchRecords(pageSize);
            } catch (IOException ex) {
                broadcastProgress(Progress.Status.ABORTED, getResources()
                        .getString(R.string.unable_to_find_image_file));
                return;
            }
            if (mainRecords != null && !mainRecords.isEmpty()) {
                broadcastProgress(Progress.Status.BUSY, getResources().getString
                        (
                                R.string.sending_batch_no_x_of_y,
                                batchNumber,
                                noOfBatches
                        ));//publish progress here in case this is running faster than the Runnable
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        while (busy) {
                            if (batchNumber <= noOfBatches) {
                                broadcastProgress(Progress.Status.BUSY, getResources().getString
                                        (
                                                R.string.sending_batch_no_x_of_y,
                                                batchNumber,
                                                noOfBatches
                                        ));//publish progress here in case actual progress is
                                // running slower than this Runnable
                            }
                            try {
                                Thread.sleep(500);
                            } catch (Exception ignored) {
                            }
                        }
                    }
                };
                new Thread(runnable).start();
                try {
                    UploadRequest request = new UploadRequest(mainRecords,
                            androidHojiContext.getSurvey().getId());
                    request.setAppVersionCode(BuildConfig.VERSION_CODE);
                    response = httpService.sendAsJson
                            (
                                    ke.co.hoji.android.data.Constants.SERVER_URL + Api.DATA_UPLOAD,
                                    propertyService.getValue(Server.USERNAME),
                                    propertyService.getValue(Server.PASSWORD),
                                    request,
                                    UploadResponse.class
                            );
                    int batchUploaded = androidHojiContext.getDtoServiceManager().getRecordService()
                            .updateStatuses(response.getResults());
                    totalUploaded += batchUploaded;
                } catch (HttpException ex) {
                    busy = false;
                    if (ex.getCause() instanceof JsonSyntaxException) {
                        PackageManager manager = getPackageManager();
                        String versionName = "";
                        try {
                            PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
                            versionName = info.versionName;
                        } catch (PackageManager.NameNotFoundException e) {
                            //do nothing
                        }
                        broadcastProgress(Progress.Status.ABORTED, getResources()
                                .getString(R.string.sending_no_longer_supported, versionName));
                    } else {
                        broadcastProgress(Progress.Status.ABORTED, httpService.interpretHttpExceptionCode(ex.getCode(), ex));
                    }
                    return;
                }
                batchNumber++;
                if (batchNumber > noOfBatches) {
                    break;
                }
            } else {
                break;
            }
        }
        busy = false;
        propertyService.setProperty
                (
                        new Property(PropertyKeys.LAST_UPLOAD, String.valueOf(new Date().getTime()))
                );
        String message = "";
        if (response.getMessage() != null && !response.getMessage().isEmpty()) {
            message = " (" + response.getMessage() + ")";
        }
        broadcastProgress(Progress.Status.DONE, getResources().getQuantityString
                (
                        R.plurals.n_records_sent, totalUploaded, totalUploaded
                ) + message);
    }

    private int countRecords() {
        RecordSearch recordSearch = new RecordSearch();
        recordSearch.setStage(Stage.COMPLETE);
        RecordService recordService = androidHojiContext.getModelServiceManager().getRecordService();
        return recordService.count(recordSearch);
    }

    private List<MainRecord> fetchRecords(int limit) throws IOException {
        RecordSearch recordSearch = new RecordSearch();
        recordSearch.setStage(Stage.COMPLETE);
        return androidHojiContext.getDtoServiceManager().getRecordService()
                .findMainRecords(recordSearch, limit);
    }

    private void broadcastProgress(int status, String message) {
        Intent intent = new Intent(Constants.BROADCAST_ACTION);
        intent.putExtra(Constants.EXTENDED_DATA_STATUS, new Progress(status, message));
        intent.putExtra(Constants.SHOW_UPLOAD_PROGRESS, showUploadProgress);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
