package ke.co.hoji.android.data;

import android.location.Location;

/**
 * A convenience wrapper around the {@link Location} object, mainly so we can attach the address.
 * <p/>
 * Created by gitahi on 13/08/15.
 */
public class LocationWrapper {

    /**
     * The location object.
     */
    private final Location location;

    /**
     * The location address e.g. street address.
     */
    private final String address;

    public LocationWrapper(Location location, String address) {
        this.location = location;
        this.address = address;
    }

    public Location getLocation() {
        return location;
    }

    public String getAddress() {
        return address;
    }

    public Double getLatitude() {
        return location.getLatitude();
    }

    public Double getLongitude() {
        return location.getLongitude();
    }

    public Double getAltitude() {
        return location.getAltitude();
    }

    public Double getAccuracy() {
        return (double) location.getAccuracy();
    }

    public Long getTime() {
        return location.getTime();
    }
}
