package ke.co.hoji.android.helper;

import android.content.res.Resources;

/**
 * A UI component used to run downloads.
 * <p/>
 * Created by gitahi on 12/01/16.
 */
public interface DownloadingComponent {

    /**
     * Called when the download process succeeds.
     */
    void downloadSucceeded();

    /**
     * Called when the download process fails.
     *
     * @param message a message describing the failure, if available.
     */
    void downloadFailed(String message);

    /**
     * Called when the download process fails.
     *
     * @param message         a message describing the failure, if available.
     * @param nothingNewFound true if donload failed because there was nothing new and false otherwise.
     */
    void downloadFailed(String message, boolean nothingNewFound);

    /**
     * @return resources.
     */
    Resources getResources();

    /**
     * Dismiss the progress dialog
     */
    void dismissProgressDialog();

    /**
     * Check if this component is still alive or it has been destroyed.
     *
     * @return false if this component has been destroyed and true otherwise.
     */
    boolean isAlive();
}
