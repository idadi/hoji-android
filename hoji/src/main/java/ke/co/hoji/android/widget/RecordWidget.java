package ke.co.hoji.android.widget;

import android.content.Context;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.android.adapter.RecordArrayAdapter1;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.service.model.RecordService;

/**
 * Created by gitahi on 14/11/14.
 */
public class RecordWidget extends AndroidWidget {

    private ListView listView;

    private List<Record> records;
    private Record record;

    public RecordWidget(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.single_choice_widget;
    }

    @Override
    public void loadViews() {
        listView = findViewById(R.id.listView);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                record = records.get(position);
                raiseResponseChangedEvent();
            }
        });
    }

    @Override
    public void load() {
        boolean testMode = PreferenceManager.getDefaultSharedPreferences(getCurrentContext())
                .getBoolean(SettingsActivity.TEST_MODE, Boolean.FALSE);
        RecordService recordService = hojiContext.getModelServiceManager().getRecordService();
        records = recordService.findRecords(getLiveField(), testMode ? 1 : 0);
        RecordArrayAdapter1 adapter = new RecordArrayAdapter1(
                getCurrentContext(),
                R.layout.simple_list_item_single_choice,
                records,
                editable
        );
        listView.setAdapter(adapter);
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null) {
            record = (Record) response.getActualValue();
            int position = ((ArrayAdapter) listView.getAdapter()).getPosition(record);
            listView.setItemChecked(position, true);
            listView.smoothScrollToPosition(position);
        }
    }

    @Override
    public Object getValue() {
        return record;
    }

    @Override
    public void focus() {
        listView.requestFocus();
    }

    @Override
    public void prepare() {
        listView.setAdapter(null);
    }

    @Override
    public void wipe() {
        record = null;
        listView.clearChoices();
        listView.requestLayout();
    }

    @Override
    public View getPrincipalView() {
        return listView;
    }
}
