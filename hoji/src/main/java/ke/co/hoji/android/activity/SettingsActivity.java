package ke.co.hoji.android.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;

/**
 * Created by gitahi on 12/05/15.
 */
public class SettingsActivity extends AppCompatActivity {

    public static final String TEST_MODE = "test_mode";
    public static final String AUTO_UPLOAD = "auto_upload";
    public static final String AUTO_NAVIGATE = "auto_navigate";
    public static final String FLASH_NAVIGATE = "flash_navigate";
    public static final String AUTO_POP_KEYBOARD = "auto_pop_keyboard";
    public static final String SHOW_FIELD_NUMBERS_IN_EDIT_MODE = "show_field_numbers_in_edit_mode";
    public static final String SHOW_FIELD_NUMBERS_IN_VIEW_MODE = "show_field_numbers_in_view_mode";
    public static final String DATE_TIME_FORMAT = "date_time_format";
    public static final String PROJECT_LANGUAGE = "project_language";
    public static final String BATTERY_SAVER = "battery_saver";
    public static final String PAGE_SIZE = "page_size";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    public void projectDeleteCompleted() {
        AndroidHojiContext hojiContext = (AndroidHojiContext) this.getApplication();
        hojiContext.setSurvey(null);
        Intent intent = new Intent(this, SurveysActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}