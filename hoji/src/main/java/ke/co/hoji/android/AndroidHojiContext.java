package ke.co.hoji.android;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.acra.ACRA;
import org.acra.ErrorReporter;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.script.ScriptEngineManager;

import ke.co.hoji.BuildConfig;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.data.LocationWrapper;
import ke.co.hoji.android.helper.AndroidBase64Encoder;
import ke.co.hoji.android.helper.ImageLocation;
import ke.co.hoji.android.helper.ImageReceiver;
import ke.co.hoji.android.listener.OnRecordSelectedListener;
import ke.co.hoji.android.service.HttpService;
import ke.co.hoji.android.service.JsonService;
import ke.co.hoji.android.service.LocationService;
import ke.co.hoji.android.task.SetLiveFieldsTask;
import ke.co.hoji.android.widget.AndroidWidgetLoader;
import ke.co.hoji.android.widget.AndroidWidgetRenderer;
import ke.co.hoji.android.widget.helper.LocationComponent;
import ke.co.hoji.core.DtoServiceManager;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.ResourceManager;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.calculation.CalculationEvaluator;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Language;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.MainRecord;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.response.helper.Base64Encoder;
import ke.co.hoji.core.rule.RuleEvaluator;
import ke.co.hoji.core.service.model.LanguageService;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.core.service.model.RecordService;
import ke.co.hoji.core.service.model.TranslationService;
import ke.co.hoji.core.widget.WidgetManager;

import static ke.co.hoji.core.data.Constants.Acra;
import static ke.co.hoji.core.data.Constants.Device;
import static ke.co.hoji.core.data.Constants.Server;

/**
 * The android implementation of the {@link HojiContext}.
 */

@ReportsCrashes
        (
                formUri = Acra.URL,
                reportType = HttpSender.Type.JSON,
                httpMethod = HttpSender.Method.POST,
                formUriBasicAuthLogin = Acra.USERNAME,
                formUriBasicAuthPassword = Acra.PASSWORD,
                customReportContent =
                        {
                                ReportField.APP_VERSION_CODE,
                                ReportField.APP_VERSION_NAME,
                                ReportField.ANDROID_VERSION,
                                ReportField.PACKAGE_NAME,
                                ReportField.REPORT_ID,
                                ReportField.BUILD,
                                ReportField.STACK_TRACE,
                                ReportField.LOGCAT,
                                ReportField.CUSTOM_DATA
                        },
                mode = ReportingInteractionMode.SILENT
        )
public final class AndroidHojiContext extends Application implements HojiContext {

    private ModelServiceManager modelServiceManager;
    private DtoServiceManager dtoServiceManager;
    private WidgetManager widgetManager;
    private RuleEvaluator ruleEvaluator;
    private CalculationEvaluator calculationEvaluator;
    private Base64Encoder base64Encoder;
    private HttpService httpService;
    private LocationService locationService;

    private Survey survey;
    private Form form;
    private Record record;
    private LocationWrapper locationWrapper;
    private Integer userId;
    private String userCode;
    private String username;
    private String nameOfUser;
    private Integer ownerId;
    private String ownerName;
    private Boolean ownerVerified;
    private String tenantName;
    private String tenantCode;
    private String deviceId;

    /**
     * Temporary storage for the {@link ImageLocation} used in {@link ke.co.hoji.android.activity.DataEntryActivity}
     * because the underlying {@link Uri} is not Serializable and therefore its state cannot be
     * saved normally.
     */
    private ImageLocation imageLocation;

    /**
     * Temporary storage for the imageReceiver used in {@link ke.co.hoji.android.activity.DataEntryActivity}
     * because is not Serializable and therefore its state cannot be save normally.
     */
    private ImageReceiver imageReceiver;

    private boolean acraInitialized = false;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        if (BuildConfig.DEBUG) {
            acraInitialized = false;
        } else {
//            ACRA.init(AndroidHojiContext.this);
            acraInitialized = false;
        }
    }

    public ModelServiceManager getModelServiceManager() {
        if (modelServiceManager == null) {
            init();
        }
        return modelServiceManager;
    }

    @Override
    public DtoServiceManager getDtoServiceManager() {
        if (dtoServiceManager == null) {
            init();
        }
        return dtoServiceManager;
    }

    public WidgetManager getWidgetManager() {
        return widgetManager;
    }

    public RuleEvaluator getRuleEvaluator() {
        ruleEvaluator.setCurrentRecord(getRecord());
        return ruleEvaluator;
    }

    @Override
    public CalculationEvaluator getCalculationEvaluator() {
        if (calculationEvaluator == null) {
            calculationEvaluator = new CalculationEvaluator(new ScriptEngineManager().getEngineByName("rhino"));
        }
        return calculationEvaluator;
    }

    @Override
    public Base64Encoder getBase64Encoder() {
        if (base64Encoder == null) {
            base64Encoder = new AndroidBase64Encoder();
        }
        return base64Encoder;
    }

    public Survey getSurvey() {
        if (survey == null) {
            survey = getModelServiceManager().getConfigService().getSurvey();
        }
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
        setForm((Form) record.getFieldParent());
    }

    public LocationWrapper getLocationWrapper() {
        return locationWrapper;
    }

    public void setLocationWrapper(LocationWrapper locationWrapper) {
        if (locationWrapper != null) {
            this.locationWrapper = locationWrapper;
        }
    }

    @Override
    public String getDeviceIdentifier() {
        if (deviceId == null) {
            PropertyService propertyService = getModelServiceManager().getPropertyService();
            deviceId = propertyService.getValue(Device.ID);
            if (deviceId == null) {
                deviceId = generateDeviceId();
                propertyService.setProperty(new Property(Device.ID, deviceId));
            }
        }
        return deviceId;
    }

    @Override
    public Integer getUserId() {
        if (userId == null) {
            PropertyService propertyService = getModelServiceManager().getPropertyService();
            userId = Utils.parseInteger(propertyService.getValue(Server.USER_ID));
        }
        return userId;
    }

    @Override
    public String getUserCode() {
        if (userCode == null) {
            PropertyService propertyService = getModelServiceManager().getPropertyService();
            userCode = propertyService.getValue(Server.USER_CODE);
        }
        return userCode;
    }

    @Override
    public String getUsername() {
        if (username == null) {
            PropertyService propertyService = getModelServiceManager().getPropertyService();
            username = propertyService.getValue(Server.USERNAME);
        }
        return username;
    }

    @Override
    public String getNameOfUser() {
        if (nameOfUser == null) {
            PropertyService propertyService = getModelServiceManager().getPropertyService();
            nameOfUser = propertyService.getValue(Server.NAME);
        }
        return nameOfUser;
    }

    @Override
    public Integer getOwnerId() {
        if (ownerId == null) {
            PropertyService propertyService = getModelServiceManager().getPropertyService();
            ownerId = Utils.parseInteger(propertyService.getValue(Server.OWNER_ID));
        }
        return ownerId;
    }

    @Override
    public String getOwnerName() {
        if (ownerName == null) {
            PropertyService propertyService = getModelServiceManager().getPropertyService();
            ownerName = propertyService.getValue(Server.OWNER_NAME);
        }
        return ownerName;
    }

    @Override
    public boolean getOwnerVerified() {
        if (ownerVerified == null) {
            PropertyService propertyService = getModelServiceManager().getPropertyService();
            ownerVerified = Boolean.valueOf(propertyService.getValue(Server.OWNER_VERIFIED));
        }
        return ownerVerified;
    }

    @Override
    public String getTenantName() {
        if (tenantName == null) {
            PropertyService propertyService = getModelServiceManager().getPropertyService();
            tenantName = propertyService.getValue(Server.TENANT_NAME);
        }
        return tenantName;
    }

    @Override
    public String getTenantCode() {
        if (tenantCode == null) {
            PropertyService propertyService = getModelServiceManager().getPropertyService();
            tenantCode = propertyService.getValue(Server.TENANT_CODE);
        }
        return tenantCode;
    }

    @Override
    public Integer getLanguageId() {
        return Integer.valueOf(
                PreferenceManager.getDefaultSharedPreferences(
                        this
                ).getString(SettingsActivity.PROJECT_LANGUAGE, Constants.DEFAULT_LANGUAGE_ID));
    }

    @Override
    public String getType() {
        return HojiContext.ANDROID_CONTEXT;
    }

    @Override
    public String appVersion() {
        PackageManager packageManager = getPackageManager();
        try {
            PackageInfo info = packageManager.getPackageInfo(getPackageName(), 0);
            return info.versionName;
        } catch (PackageManager.NameNotFoundException ex) {
            return "Unknown";
        }
    }

    /**
     * Clear context data so it refreshes when it is next needed.
     */
    public void clearContext() {
        deviceId = null;
        username = null;
        userId = null;
        userCode = null;
        nameOfUser = null;
        tenantCode = null;
        tenantName = null;
        ownerVerified = null;
        ownerName = null;
        ownerId = null;
        getModelServiceManager().getPropertyService().clearCache();
        resetLanguageTranslation();
    }

    public HttpService getHttpService() {
        return httpService;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        widgetManager = new WidgetManager(new AndroidWidgetLoader(), new AndroidWidgetRenderer());
        ruleEvaluator = new RuleEvaluator();
        ResourceManager.setHojiContext(this);
        ResourceManager.setModelServiceManager(modelServiceManager);
        locationService = new LocationService(this);
        httpService = new HttpService(this, new JsonService());
        boolean batterySaver = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(SettingsActivity.BATTERY_SAVER, Boolean.FALSE);
        if (batterySaver) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
        }
    }

    private void init() {
        Map<String, Object> params = new HashMap<>();
        params.put(Constants.CONTEXT, this);
        modelServiceManager = new SqliteModelServiceManager();
        modelServiceManager.setParameters(params);
        dtoServiceManager = new SqliteDtoServiceManager(this);
        dtoServiceManager.setParameters(params);
        FirebaseMessaging.getInstance().subscribeToTopic(Constants.PROJECT_UPDATE_TOPIC)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            // Do nothing
                        }
                    }
                });
    }

    public void permitLocationService(boolean permit) {
        locationService.setPermitted(permit);
    }

    public boolean isLocationEnabled() {
        return locationService.isLocationEnabled();
    }

    public void connectLocationService(Activity activity) {
        locationService.connect(activity);
    }

    public void startLocationService(LocationComponent locationComponent, int priority) {
        locationService.addLocationComponent(locationComponent);
        locationService.start(priority);
    }

    public void pauseLocationService(LocationComponent locationComponent) {
        locationComponent.setReadyToUse(false);
        locationService.pause();
    }

    public void disconnectLocationService() {
        locationService.disconnect();
    }

    public void setUpAcraUserAndProjectData() {
        if (acraInitialized) {
            ErrorReporter errorReporter = ACRA.getErrorReporter();
            errorReporter.putCustomData("USER_ID", String.valueOf(getUserId()));
            errorReporter.putCustomData("USER_CODE", getUserCode());
            errorReporter.putCustomData("USER_EMAIL", getUsername());
            errorReporter.putCustomData("USER_NAME", getNameOfUser());
            errorReporter.putCustomData("TENANT_CODE", getTenantCode());
            errorReporter.putCustomData("TENANT_NAME", getTenantName());
            errorReporter.putCustomData("OWNER_ID", String.valueOf(getOwnerId()));
            errorReporter.putCustomData("OWNER_NAME", getOwnerName());
            errorReporter.putCustomData("OWNER_VERIFIED", String.valueOf(getOwnerVerified()));
            errorReporter.putCustomData("DEVICE_ID", getDeviceIdentifier());
            Survey survey = getSurvey();
            if (survey != null) {
                errorReporter.putCustomData("PROJECT_ID", String.valueOf(survey.getId()));
                errorReporter.putCustomData("PROJECT_CODE", survey.getCode());
                errorReporter.putCustomData("PROJECT_NAME", survey.getName());
                errorReporter.putCustomData("PROJECT_ACCESS", String.valueOf(survey.getAccess()));
            }
        }
    }

    public void setUpAcraFormData() {
        if (acraInitialized) {
            Form form = getForm();
            if (form != null) {
                ErrorReporter errorReporter = ACRA.getErrorReporter();
                errorReporter.putCustomData("FORM_ID", String.valueOf(form.getId()));
                errorReporter.putCustomData("FORM_NAME", form.getName());
            }
        }
    }

    public void setUpAcraRecordData() {
        if (acraInitialized) {
            Record record = getRecord();
            if (record != null) {
                ErrorReporter errorReporter = ACRA.getErrorReporter();
                errorReporter.putCustomData("RECORD_UUID", record.getUuid());
                errorReporter.putCustomData("RECORD_CAPTION", record.getCaption());
            }
        }
    }

    public void setUpAcraLocationData() {
        if (acraInitialized) {
            LocationWrapper locationWrapper = getLocationWrapper();
            if (locationWrapper != null) {
                Location location = getLocationWrapper().getLocation();
                if (location != null) {
                    ErrorReporter errorReporter = ACRA.getErrorReporter();
                    errorReporter.putCustomData("LOCATION_LATITUDE", String.valueOf(location.getLatitude()));
                    errorReporter.putCustomData("LOCATION_LONGITUDE", String.valueOf(location.getLongitude()));
                    errorReporter.putCustomData("LOCATION_ACCURACY", String.valueOf(location.getAccuracy()));
                    errorReporter.putCustomData("LOCATION_ALTITUDE", String.valueOf(location.getAltitude()));
                    errorReporter.putCustomData("LOCATION_BEARING", String.valueOf(location.getBearing()));
                    errorReporter.putCustomData("LOCATION_SPEED", String.valueOf(location.getSpeed()));
                    errorReporter.putCustomData("LOCATION_PROVIDER", String.valueOf(location.getProvider()));
                    errorReporter.putCustomData("LOCATION_TIME", Utils.formatDateTime(new Date(location.getTime())));
                    errorReporter.putCustomData("LOCATION_ADDRESS", getLocationWrapper().getAddress());
                }
            }
        }
    }

    public void reset() {
        survey = null;
        record = null;
        modelServiceManager.clearCache();
    }

    public void logout() {
        modelServiceManager.getPropertyService().setProperties(new Property(Server.PASSWORD, ""));
        clearContext();
    }

    public ImageLocation getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(ImageLocation imageLocation) {
        this.imageLocation = imageLocation;
    }

    public ImageReceiver getImageReceiver() {
        return imageReceiver;
    }

    public void setImageReceiver(ImageReceiver imageReceiver) {
        this.imageReceiver = imageReceiver;
    }


    /**
     * Exactly like {@link #deflateRecord(Record)} but operates on a list of {@link Record}s.
     */
    public List<Record> deflateRecords(List<Record> records) {
        List<Record> deflatedRecords = new ArrayList<>();
        for (Record record : records) {
            deflatedRecords.add(deflateRecord(record));
        }
        return deflatedRecords;
    }

    /**
     * This method exists to get around the 1MiB memory limit that Android imposes on
     * {@link android.os.Bundle} size.
     * <p>
     * It deflates a {@link Record} by removing any superfluous parts and allows us to save
     * only a light version of it in the Bundle.
     *
     * @param inflatedRecord a full-sized record.
     * @return a deflated version of the record.
     */
    public Record deflateRecord(Record inflatedRecord) {
        if (inflatedRecord == null) {
            return null;
        }
        Record deflatedRecord = new Record(
                deflateForm((Form) inflatedRecord.getFieldParent()),
                inflatedRecord.isMain(),
                inflatedRecord.isTest(),
                inflatedRecord.getCaption()
        );
        deflatedRecord.setUuid(inflatedRecord.getUuid());
        deflatedRecord.setDateCreated(inflatedRecord.getDateCreated());
        deflatedRecord.setDetail(deflateMainRecord((MainRecord) inflatedRecord.getDetail(), deflatedRecord));
        return deflatedRecord;
    }

    /**
     * This method is the opposite of {@link #deflateRecord(Record)}. Once a deflated record
     * is restored from the saved instance or passed to the next activity, it needs to be re-inflated.
     * <p>
     *
     * @param deflatedRecord a deflated record.
     * @return an inflated record.
     */
    public Record inflateRecord(Record deflatedRecord) {
        Record inflatedRecord = retrieveRecord(deflatedRecord);
        if (inflatedRecord.getLiveFields().isEmpty()) {
            getModelServiceManager().getRecordService().setLiveFields(inflatedRecord);
        }
        return inflatedRecord;
    }

    /**
     * The equivalent of {@link #inflateRecord(Record)} but using a background task.
     *
     * @param deflatedRecord           a deflated record
     * @param onRecordSelectedListener the callback to notify once inflation is complete.
     * @param progressDialog           a ProgressDialog for showing progress.
     * @param activity                 the Activity calling this method.
     */
    public void inflateRecord(
            Record deflatedRecord,
            OnRecordSelectedListener onRecordSelectedListener,
            ProgressDialog progressDialog,
            Activity activity
    ) {
        Record inflatedRecord = retrieveRecord(deflatedRecord);
        if (inflatedRecord.getLiveFields().isEmpty()) {
            new SetLiveFieldsTask(inflatedRecord, onRecordSelectedListener, progressDialog, activity).execute();
        } else {
            onRecordSelectedListener.onRecordSelected(inflatedRecord);
        }
    }

    /**
     * This method exists to get around the 1MiB memory limit that Android imposes on
     * {@link android.os.Bundle} size.
     * <p>
     * It deflates a {@link Form} by removing any superfluous parts and allows us to save
     * only a light version of it in the Bundle.
     *
     * @param inflatedForm a full-sized Form.
     * @return a deflated version of the Form.
     */
    public Form deflateForm(Form inflatedForm) {
        Form deflatedForm = new Form();
        deflatedForm.setId(inflatedForm.getId());
        deflatedForm.setName(inflatedForm.getName());
        return deflatedForm;
    }

    /**
     * This method is the opposite of {@link #deflateForm(Form)}. Once a deflated form
     * is restored from the saved instance or passed to the next activity, it needs to be re-inflated.
     * <p>
     *
     * @param deflatedForm a deflated Form.
     * @return an inflated Form.
     */
    public Form inflateForm(Form deflatedForm) {
        Survey survey = getSurvey();
        for (Form form : survey.getForms()) {
            if (form.equals(deflatedForm)) {
                return form;
            }
        }
        return null;
    }

    /**
     * This method exists to get around the 1MiB memory limit that Android imposes on
     * {@link android.os.Bundle} size.
     * <p>
     * It deflates a {@link LiveField} by removing any superfluous parts and allows us to save
     * only a light version of it in the Bundle.
     *
     * @param inflatedLiveField a full-sized LiveField.
     * @return a deflated version of the LiveField.
     */
    public LiveField deflateLiveField(LiveField inflatedLiveField) {
        LiveField deflatedLiveField = null;
        if (inflatedLiveField != null) {
            Field deflatedField = new Field(inflatedLiveField.getField().getId());
            deflatedLiveField = new LiveField(deflatedField);
            deflatedLiveField.setUuid(inflatedLiveField.getUuid());
            deflatedLiveField.setDateCreated(inflatedLiveField.getDateCreated());
            deflatedLiveField.setDateUpdated(inflatedLiveField.getDateUpdated());
        }
        return deflatedLiveField;
    }

    /**
     * This method is the opposite of {@link #deflateLiveField(LiveField)}. Once a deflated LiveField
     * is restored from the saved instance or passed to the next activity, it needs to be re-inflated.
     * <p>
     *
     * @param deflatedLiveField a deflated LiveField.
     * @return an inflated LiveField.
     */
    public LiveField inflateLiveField(LiveField deflatedLiveField, Record record) {
        LiveField inflatedLiveField = null;
        if (deflatedLiveField != null) {
            List<LiveField> liveFields = record.getLiveFields();
            Field inflatedField = null;
            if (liveFields.contains(deflatedLiveField)) {
                int n = liveFields.size() - 1;
                for (int i = n; i >= 0; i--) {
                    LiveField lf = liveFields.get(i);
                    if (lf.equals(deflatedLiveField)) {
                        inflatedField = lf.getField();
                        inflatedField.setResponse(lf.getResponse());
                        break;
                    }
                }
            } else {
                List<Field> fields = record.getFieldParent().getChildren();
                int n = fields.size() - 1;
                for (int i = n; i >= 0; i--) {
                    inflatedField = fields.get(i);
                    if (inflatedField.equals(deflatedLiveField.getField())) {
                        break;
                    }
                }
            }
            inflatedLiveField = new LiveField(inflatedField);
            inflatedField.setResponse(inflatedField.getResponse());
            inflatedLiveField.setUuid(deflatedLiveField.getUuid());
            inflatedLiveField.setResponse(deflatedLiveField.getResponse());
            inflatedLiveField.setDateCreated(deflatedLiveField.getDateCreated());
            inflatedLiveField.setDateUpdated(deflatedLiveField.getDateUpdated());
        }
        return inflatedLiveField;
    }

    /*
     * This method starts by calling getSurvey, to make sure that the config is loaded. The config
     * may not be loaded if Android had previously killed the app to recover memory.
     */
    private Record retrieveRecord(Record deflatedRecord) {
        getSurvey();
        RecordService recordService = getModelServiceManager().getRecordService();
        Record record = recordService.findRecordByUuid(deflatedRecord.getUuid(), new Field());
        MainRecord mainRecord = (MainRecord) record.getDetail();
        mainRecord.setStage(((MainRecord) deflatedRecord.getDetail()).getStage());
        return record;
    }

    private MainRecord deflateMainRecord(MainRecord mainRecord, Record deflatedRecord) {
        MainRecord deflatedMainRecord = new MainRecord(deflatedRecord);
        deflatedMainRecord.setStartLatitude(mainRecord.getStartLatitude());
        deflatedMainRecord.setStartLongitude(mainRecord.getStartLongitude());
        deflatedMainRecord.setStartAddress(mainRecord.getStartAddress());
        deflatedMainRecord.setStage(mainRecord.getStage());
        return deflatedMainRecord;
    }

    private void resetLanguageTranslation() {
        TranslationService translationService = modelServiceManager.getTranslationService();
        LanguageService languageService = modelServiceManager.getLanguageService();
        translationService.clearCache();
        List<Language> languages = languageService.findAllLanguages();
        Integer languageId = getLanguageId();
        boolean currentLanguageExists = false;
        for (Language language : languages) {
            if (language.getId().equals(languageId)) {
                currentLanguageExists = true;
                break;
            }
        }
        if (!currentLanguageExists) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(SettingsActivity.PROJECT_LANGUAGE, Constants.DEFAULT_LANGUAGE_ID);
            editor.commit();
        }
    }

    private String generateDeviceId() {
        return UUID.randomUUID().toString();
    }
}
