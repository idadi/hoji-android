package ke.co.hoji.android.listener;

import ke.co.hoji.core.data.RecordSearch;

/**
 * Defines a callback for reacting to a {@link RecordSearch} updated event.
 * <p/>
 * Created by gitahi on 25/05/15.
 */
public interface OnRecordSearchUpdatedListener {

    /**
     * Called when a {@link RecordSearch} is updated.
     *
     * @param recordSearch the updated {@link RecordSearch}.
     */
    void onRecordSearchUpdated(RecordSearch recordSearch);
}
