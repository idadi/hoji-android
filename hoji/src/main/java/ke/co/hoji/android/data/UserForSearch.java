package ke.co.hoji.android.data;

/**
 * Created by gitahi on 11/17/16.
 */
public enum UserForSearch {

    ME(1, "Me"),
    ANYONE(2, "Anyone");

    private final int value;
    private final String label;

    UserForSearch(int value, String label) {
        this.value = value;
        this.label = label;
    }

    public int getValue() {
        return value;
    }

    private String getLabel() {
        return label;
    }

    public static UserForSearch getByValue(int value) {
        switch (value) {
            case 1:
                return UserForSearch.ME;
            case 2:
                return UserForSearch.ANYONE;
            default:
                return null;
        }
    }

    @Override
    public String toString() {
        return getLabel();
    }
}
