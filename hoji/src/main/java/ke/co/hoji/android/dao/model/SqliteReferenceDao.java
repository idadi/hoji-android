package ke.co.hoji.android.dao.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.ReferenceLevel;
import ke.co.hoji.core.data.model.Reference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by gitahi on 11/06/15.
 */
public class SqliteReferenceDao {

    private final SqliteSqlExecutor se;

    public SqliteReferenceDao(SqliteSqlExecutor se) {
        this.se = se;
    }

    public List<Reference> select(Map<String, Object> params) {
        List<Reference> references = new ArrayList<>();
        StringBuilder whereClause = new StringBuilder();
        for (String column : params.keySet()) {
            if (whereClause.length() > 0) {
                whereClause.append("AND ");
            }
            whereClause.append(column).append(" = ?");
        }
        String select = "SELECT id, level_0, level_1, level_2, level_3, level_4, level_5, level_6 "
                + "FROM reference "
                + "WHERE " + whereClause.toString();
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                Reference reference = new Reference
                        (
                                cursor.getInt(cursor.getColumnIndex("id")),
                                getReferenceLevels()
                        );
                for (ReferenceLevel referenceLevel : reference.getReferenceLevels()) {
                    reference.addValue(referenceLevel.getLevel(),
                            cursor.getString(cursor.getColumnIndex(referenceLevel.getLevelColumn())));
                }
                references.add(reference);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteReferenceDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return references;
    }

    public List<ReferenceLevel> getReferenceLevels() {
        List<ReferenceLevel> referenceLevels = new ArrayList<ReferenceLevel>();
        ReferenceLevel referenceLevel;
        String select = "SELECT id, name, enabled, level, level_column, displayable, input_type, rulable, missing_action "
                + "FROM reference_level ORDER BY level";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, se.createParameterMap());
            while (cursor.moveToNext()) {
                Integer id = cursor.getInt(cursor.getColumnIndex("id"));
                referenceLevel = se.get(id, ReferenceLevel.class);
                if (referenceLevel == null) {
                    referenceLevel = new ReferenceLevel
                            (
                                    id,
                                    cursor.getString(cursor.getColumnIndex("name")),
                                    cursor.getInt(cursor.getColumnIndex("enabled")) > 0,
                                    cursor.getInt(cursor.getColumnIndex("level")),
                                    cursor.getString(cursor.getColumnIndex("level_column")),
                                    cursor.getInt(cursor.getColumnIndex("displayable")) > 0,
                                    cursor.getInt(cursor.getColumnIndex("input_type")),
                                    cursor.getInt(cursor.getColumnIndex("rulable")) > 0,
                                    cursor.getInt(cursor.getColumnIndex("missing_action"))
                            );
                    se.put(referenceLevel);
                }
                referenceLevels.add(referenceLevel);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteReferenceDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return referenceLevels;
    }
}
