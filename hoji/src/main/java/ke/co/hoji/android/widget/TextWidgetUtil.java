package ke.co.hoji.android.widget;

import android.text.InputType;
import android.widget.EditText;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.TextInputType;

/**
 * Created by gitahi on 14/08/15.
 */
class TextWidgetUtil {

    public static void setInputType(EditText editText, String tag) {
        if (tag != null) {
            Integer inputType = Utils.parseInteger(tag);
            if (inputType == null) {
                inputType = TextInputType.NONE;
            }
            setInputType(editText, inputType);
        }
    }

    public static void setInputType(EditText editText, int inputType) {
        switch (inputType) {
            case TextInputType.TEXT:
                editText.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
            case TextInputType.TEXT_ALL_CAPS:
                editText.setInputType(InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                break;
            case TextInputType.TEXT_CAP_WORDS:
                editText.setInputType(InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
                break;
            case TextInputType.TEXT_SENTENCE:
                editText.setInputType(InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                break;
            case TextInputType.TEXT_MULTILINE:
                editText.setInputType(InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                break;
            case TextInputType.TEXT_EMAIL:
                editText.setInputType(InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;
            case TextInputType.TEXT_PASSWORD:
                editText.setInputType(InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
            case TextInputType.NUMBER:
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case TextInputType.NUMBER_SIGNED:
                editText.setInputType(InputType.TYPE_CLASS_NUMBER
                        | InputType.TYPE_NUMBER_FLAG_SIGNED);
                break;
            case TextInputType.NUMBER_PHONE:
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case TextInputType.NUMBER_PASSWORD:
                editText.setInputType(InputType.TYPE_CLASS_NUMBER
                        | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                break;
            case TextInputType.DECIMAL:
                editText.setInputType(InputType.TYPE_CLASS_NUMBER
                        | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                break;
            case TextInputType.DECIMAL_SIGNED:
                editText.setInputType(InputType.TYPE_CLASS_NUMBER
                        | InputType.TYPE_NUMBER_FLAG_DECIMAL
                        | InputType.TYPE_NUMBER_FLAG_SIGNED);
                break;
            case TextInputType.DATE:
                editText.setInputType(InputType.TYPE_CLASS_DATETIME
                        | InputType.TYPE_DATETIME_VARIATION_DATE);
                break;
            case TextInputType.TIME:
                editText.setInputType(InputType.TYPE_CLASS_DATETIME
                        | InputType.TYPE_DATETIME_VARIATION_TIME);
                break;
            case TextInputType.DATE_TIME:
                editText.setInputType(InputType.TYPE_CLASS_DATETIME);
                break;
            default:
                editText.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
        }
    }

    /**
     * Set cursor at the end of any text there may be, and then request focus.
     *
     * @param editText the {@link EditText} on which to set focus.
     */
    public static void setFocus(EditText editText, boolean editable) {
        String existingText = editText.getText().toString();
        editText.setText("");
        editText.append(existingText);
        editText.requestFocus();
        editText.setEnabled(editable);
    }
}
