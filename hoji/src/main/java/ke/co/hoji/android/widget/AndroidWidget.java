package ke.co.hoji.android.widget;

import android.content.Context;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.PatternSyntaxException;

import ke.co.hoji.R;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.StringInterpolator;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.calculation.CalculationEvaluator;
import ke.co.hoji.core.calculation.CalculationException;
import ke.co.hoji.core.calculation.CalculationUtils;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.service.model.RecordService;
import ke.co.hoji.core.widget.Navigable;
import ke.co.hoji.core.widget.ResponseChangedListener;
import ke.co.hoji.core.widget.ResponseChangingListener;
import ke.co.hoji.core.widget.Widget;
import ke.co.hoji.core.widget.WidgetEvent;

/**
 * The base class for all @{@link Widget}s for the Android platform.
 * <p>
 * Created by gitahi on 25/10/14.
 */
public abstract class AndroidWidget extends LinearLayout implements Widget {

    /**
     * The current @{@link Context} of this Widget. Needs to be refreshed routinely because
     * {@link Widget}s are created only once and then cached for reuse. To get the correct
     * {@link android.app.Activity} hosting a Widget, always call {@link #getCurrentContext()}
     * instead of {@link #getContext()} which may return a stale Activity.
     */
    private Context currentContext;

    /**
     * The @{@link LiveField} currently being rendered by this Widget.
     */
    protected LiveField liveField;

    /**
     * The @{@link ResponseChangedListener} for this Widget.
     */
    protected ResponseChangedListener responseChangedListener;

    /**
     * The @{@link ResponseChangingListener} for this Widget.
     */
    protected ResponseChangingListener responseChangingListener;

    /**
     * The @{@link WidgetEvent.EventCause} that currently applies to this Widget.
     */
    protected WidgetEvent.EventCause eventCause;

    /**
     * The parent of this Widget if this Widget is rendered as a child of another Widget.
     */
    protected Widget parent;

    /**
     * The @{@link Navigable} onto which this Widget is currently rendered.
     */
    protected Navigable navigable;

    /**
     * The @{@link HojiContext} associated with this Widget.
     */
    protected HojiContext hojiContext;

    /**
     * Whether or not this AndroidWidget is editable.
     */
    protected boolean editable = true;

    /**
     * The {@link StringInterpolator} to use for interpolating piped values.
     */
    private final StringInterpolator stringInterpolator = new StringInterpolator();

    public AndroidWidget(Context context) {
        super(context);
    }

    public void setHojiContext(HojiContext hojiContext) {
        this.hojiContext = hojiContext;
    }

    /**
     * Inflate this Widget's layout.
     *
     * @param layoutInflater the @{@link LayoutInflater} to use for inflation.
     */
    public void inflateLayout(LayoutInflater layoutInflater) {
        layoutInflater.inflate(getLayout(), this);
    }

    /**
     * @return the id of the layout file for this Widget.
     */
    protected abstract int getLayout();

    /**
     * Load View components for this Widget i.e. the components that make up this compound component.
     */
    public void loadViews() {
    }

    @Override
    public LiveField getLiveField() {
        return liveField;
    }

    @Override
    public final void setLiveField(LiveField liveField) {
        this.liveField = liveField;
    }

    @Override
    public void load() {
    }

    @Override
    public void ready() {
    }

    @Override
    public void prepare() {
    }

    @Override
    public Response readResponse() {
        return Response.create(hojiContext, getLiveField(), getValue(), true);
    }

    @Override
    public void writeResponse() {
    }

    @Override
    public Object getValue() {
        return null;
    }

    @Override
    public String getResetText() {
        return getCurrentContext().getResources().getString(R.string.reset_action);
    }

    @Override
    public boolean isResettable() {
        return true;
    }

    @Override
    public final boolean reset() {
        wipe();
        Field field = getLiveField().getField();
        String defaultValue = field.getDefaultValue();
        if (defaultValue != null && !defaultValue.isEmpty()) {
            String pipedDefaultValue = stringInterpolator.interpolate(
                    defaultValue,
                    hojiContext.getRecord(),
                    StringInterpolator.InterpolationType.DEFAULT_VALUE
            );
            if (defaultValue.equals(pipedDefaultValue)) {
                setDefaultValue(defaultValue);
                return false;
            } else {
                return setDefaultValue(pipedDefaultValue);
            }
        }
        return false;
    }

    @Override
    public boolean setDefaultValue(String defaultValue) {
        return false;
    }

    @Override
    public void wipe() {
    }

    @Override
    public void focus() {
    }

    @Override
    public ValidationResult validate(Record record) {
        if (performValidation()) {
            boolean empty = isEmpty();
            {
                int missingAction = getLiveField().getField().getMissingAction();
                if (missingAction != Field.ValidationAction.NONE) {
                    if (empty) {
                        if (missingAction == Field.ValidationAction.WARN) {
                            return ValidationResult.warn(getCurrentContext().getResources().getString(R.string.no_response));
                        } else if (missingAction == Field.ValidationAction.STOP) {
                            return ValidationResult.stop(getCurrentContext().getResources().getString(R.string.no_response));
                        }
                    }
                }

            }
            if (!empty) {
                {
                    Field.ValidationValue regex = getRegex(record);
                    if (regex != null) {
                        int regexAction = regex.getAction();
                        String match = (String) regex.getValue();
                        if (!StringUtils.isBlank(match)) {
                            String value = readResponse().ruleString();
                            if (value != null) {
                                try {
                                    if (!value.matches(match)) {
                                        if (regexAction == Field.ValidationAction.WARN) {
                                            return ValidationResult.warn(getCurrentContext().getResources().getString(R.string.response_wrong_format));
                                        } else if (regexAction == Field.ValidationAction.STOP) {
                                            return ValidationResult.stop(getCurrentContext().getResources().getString(R.string.response_wrong_format));
                                        }
                                    }
                                } catch (PatternSyntaxException ex) {
                                    return ValidationResult.stop(getCurrentContext().getResources().getString(R.string.bad_regex));
                                }
                            }
                        }
                    }
                }
                {
                    Field.ValidationValue min = getMin(record);
                    if (min != null) {
                        int minAction = min.getAction();
                        Comparable value = getComparable();
                        if (value != null) {
                            int compareTo = value.compareTo(min.getValue());
                            if (compareTo < 0) {
                                if (minAction == Field.ValidationAction.WARN) {
                                    return ValidationResult.warn(getCurrentContext().getResources().getString(R.string.response_below_min, min.getValue()));
                                } else if (minAction == Field.ValidationAction.STOP) {
                                    return ValidationResult.stop(getCurrentContext().getResources().getString(R.string.response_below_min, min.getValue()));
                                }
                            }
                        }
                    }
                }
                {
                    Field.ValidationValue max = getMax(record);
                    if (max != null) {
                        int maxAction = max.getAction();
                        Comparable value = getComparable();
                        if (value != null) {
                            int compareTo = value.compareTo(max.getValue());
                            if (compareTo > 0) {
                                if (maxAction == Field.ValidationAction.WARN) {
                                    return ValidationResult.warn(getCurrentContext().getResources().getString(R.string.response_exceeds_max, max.getValue()));
                                } else if (maxAction == Field.ValidationAction.STOP) {
                                    return ValidationResult.stop(getCurrentContext().getResources().getString(R.string.response_exceeds_max, max.getValue()));
                                }
                            }
                        }
                    }
                }
                {
                    Field field = getLiveField().getField();
                    Integer uniqueness = field.getUniqueness();
                    if (uniqueness != null && uniqueness != Field.Uniqueness.NONE.getValue()) {
                        boolean testMode = PreferenceManager.getDefaultSharedPreferences(getCurrentContext())
                                .getBoolean(SettingsActivity.TEST_MODE, Boolean.FALSE);
                        RecordService recordService = getHojiContext().getModelServiceManager().getRecordService();
                        Response response = readResponse();
                        if (uniqueness == Field.Uniqueness.COMPOUND.getValue()) {
                            List<Field> compoundUniqueFields = record.getFieldParent().getCompoundUniqueFields();
                            Field lastCompoundUniqueField = compoundUniqueFields.get(compoundUniqueFields.size() - 1);
                            if (field.equals(lastCompoundUniqueField)) {
                                LinkedHashSet<LiveField> liveFields = new LinkedHashSet<>(record.getLiveFields());
                                LiveField current = new LiveField(field);
                                current.setResponse(response);
                                liveFields.add(current);
                                String compoundKey = "";
                                List<String> compoundKeyTokens = new ArrayList<>();
                                for (LiveField lf : liveFields) {
                                    if (lf.getField().getUniqueness() == Field.Uniqueness.COMPOUND.getValue()) {
                                        Object storageValue = lf.getResponse().toStorageValue();
                                        if (storageValue != null) {
                                            compoundKey += storageValue.toString();
                                            compoundKeyTokens.add(lf.getResponse().displayString());
                                        }
                                    }
                                }
                                boolean isUnique = true;
                                if (!"".equals(compoundKey)) {
                                    isUnique = recordService.isCompoundKeyUnique(
                                            record,
                                            hojiContext.getRecord(),
                                            compoundKey,
                                            testMode
                                    );
                                }
                                if (!isUnique) {
                                    String prettyCompoundKey = "'" + Utils.string(compoundKeyTokens, " - ", false, "", false) + "'";
                                    return ValidationResult.stop(getCurrentContext().getResources().getString(R.string.response_not_unique, prettyCompoundKey));
                                }
                            }
                        } else if (uniqueness == Field.Uniqueness.SIMPLE.getValue()) {
                            if (response.getActualValue() != null) {
                                String simpleKey = response.toStorageValue().toString();
                                boolean isUnique = recordService.isSimpleKeyUnique(
                                        record,
                                        hojiContext.getRecord(),
                                        getLiveField().getField(),
                                        simpleKey,
                                        testMode
                                );
                                if (!isUnique) {
                                    String prettySimpleKey = "'" + response.displayString() + "'";
                                    return ValidationResult.stop(getCurrentContext().getResources().getString(R.string.response_not_unique, prettySimpleKey));
                                }
                            }
                        }
                    }
                }
                {
                    if (getLiveField().getField().hasScriptFunction(Field.ScriptFunctions.VALIDATE)) {
                        ModelServiceManager modelServiceManager = hojiContext.getModelServiceManager();
                        Survey currentSurvey = hojiContext.getSurvey();
                        CalculationUtils calculationUtils = new CalculationUtils(
                                currentSurvey.getId(),
                                modelServiceManager,
                                record.getLiveFields(),
                                liveField.getField(),
                                new Date(record.getDateCreated())
                        );
                        liveField.getField().setResponse(readResponse());
                        CalculationEvaluator calculationEvaluator = hojiContext.getCalculationEvaluator();
                        try {
                            Object calculationResult = calculationEvaluator.evaluateScript(
                                    getLiveField().getField().getValueScript(),
                                    Field.ScriptFunctions.VALIDATE,
                                    calculationUtils,
                                    liveField.getField()
                            );
                            if (calculationResult != null
                                    && calculationResult instanceof ValidationResult
                                    && ((ValidationResult) calculationResult).getSignal() < ValidationResult.ERROR) {
                                return (ValidationResult) calculationResult;
                            } else {
                                String errorMessage = "";
                                if (calculationResult == null) {
                                    errorMessage = "Validate function returned null!";
                                } else if (calculationResult instanceof ValidationResult == false) {
                                    errorMessage = "Validate function returned the wrong type of object!";
                                } else {
                                    ValidationResult validationResult = (ValidationResult) calculationResult;
                                    if (validationResult.getSignal() != ValidationResult.OKAY
                                            || validationResult.getSignal() != ValidationResult.WARN
                                            || validationResult.getSignal() != ValidationResult.ERROR) {
                                        errorMessage = "Validate function returned an unsupported signal!";
                                    }
                                }
                                throw new CalculationException(
                                        errorMessage,
                                        new IllegalStateException(),
                                        getLiveField().getField().getValueScript());
                            }
                        } catch (CalculationException ex) {
                            navigable.handleCalculationException(ex);
                            return ValidationResult.error(ex.getMessage());
                        }
                    }
                }
            }
        }
        return ValidationResult.okay();
    }

    @Override
    public boolean isEmpty() {
        return getValue() == null;
    }

    @Override
    public Comparable getComparable() {
        Object actualValue = readResponse().getActualValue();
        if (actualValue instanceof Comparable) {
            return (Comparable) actualValue;
        }
        return null;
    }

    @Override
    public Comparable getMissingComparable() {
        return getComparable();
    }

    @Override
    public Comparable getMissingComparable(String stringValue) {
        return null;
    }

    @Override
    public Field.ValidationValue getMin(Record record) {
        Field.ValidationValue genericValidationValue = getGenericValidationValue(
                getLiveField().getField().getMinValue(),
                record
        );
        if (genericValidationValue != null) {
            Long min = Utils.parseLong(String.valueOf(genericValidationValue.getValue()));
            if (min != null) {
                return new Field.ValidationValue(min, genericValidationValue.getAction());
            }
        }
        return null;
    }

    @Override
    public Field.ValidationValue getMax(Record record) {
        Field.ValidationValue genericValidationValue = getGenericValidationValue(
                getLiveField().getField().getMaxValue(),
                record
        );
        if (genericValidationValue != null) {
            Long max = Utils.parseLong(String.valueOf(genericValidationValue.getValue()));
            if (max != null) {
                return new Field.ValidationValue(max, genericValidationValue.getAction());
            }
        }
        return null;
    }

    @Override
    public Field.ValidationValue getRegex(Record record) {
        Field.ValidationValue genericValidationValue = getGenericValidationValue(
                getLiveField().getField().getRegexValue(),
                record
        );
        if (genericValidationValue != null) {
            String regex = String.valueOf(genericValidationValue.getValue());
            if (regex != null) {
                return new Field.ValidationValue(regex, genericValidationValue.getAction());
            }
        }
        return null;
    }

    @Override
    public void setResponseChangedListener(ResponseChangedListener responseChangedListener) {
        this.responseChangedListener = responseChangedListener;
    }

    @Override
    public void setResponseChangingListener(ResponseChangingListener responseChangingListener) {
        this.responseChangingListener = responseChangingListener;
    }

    @Override
    public ResponseChangedListener getResponseChangedListener() {
        return responseChangedListener;
    }

    @Override
    public ResponseChangingListener getResponseChangingListener() {
        return responseChangingListener;
    }

    @Override
    public WidgetEvent.EventCause getEventCause() {
        if (eventCause == null) {
            setEventCause(WidgetEvent.EventCause.USER);
        }
        return eventCause;
    }

    @Override
    public void setEventCause(WidgetEvent.EventCause eventCause) {
        this.eventCause = eventCause;
    }

    @Override
    public Widget getParentWidget() {
        return parent;
    }

    @Override
    public void setParentWidget(Widget parent) {
        this.parent = parent;
    }

    @Override
    public boolean isResuming() {
        return false;
    }

    @Override
    public void rendered() {

    }

    @Override
    public boolean isEditable() {
        return editable;
    }

    @Override
    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    @Override
    public boolean needsKeyboard() {
        return false;
    }

    @Override
    public void setNavigable(Navigable navigable) {
        this.navigable = navigable;
    }

    @Override
    public Navigable getNavigable() {
        return navigable;
    }

    @Override
    public HojiContext getHojiContext() {
        return hojiContext;
    }

    protected Field.ValidationValue getGenericValidationValue(String valueString, Record record) {
        if (!StringUtils.isBlank(valueString)) {
            int action = Field.ValidationAction.STOP;
            if (Utils.isOptionalValidationValue(valueString)) {
                action = Field.ValidationAction.WARN;
                valueString = Utils.extractSubstantiveValidationValue(valueString);
            }
            valueString = stringInterpolator.interpolate(
                    valueString,
                    record,
                    StringInterpolator.InterpolationType.VALIDATION_VALUE
            );
            return new Field.ValidationValue(valueString, action);
        }
        return null;
    }

    /**
     * Gets the principal View of this AndroidWidget. The principal view is the most important View
     * in the AndroidWidget. It is typically the View to which focus is set when the AndroidWidget
     * is initialized.
     * <p>
     * The {@link AndroidWidgetRenderer} uses the View returned by this method to launch the soft
     * keyboard if necessary.
     *
     * @return the principal View.
     */
    public abstract View getPrincipalView();

    public final Context getCurrentContext() {
        return currentContext;
    }

    public final void setCurrentContext(Context currentContext) {
        this.currentContext = currentContext;
    }

    /**
     * Called once this {@link AndroidWidget} is un-rendered i.e. the next or previous Widget
     * has been rendered and this one is no longer in view.
     */
    public void unrendered() {
    }

    /*
     * Check if validation should be performed for this Widget.
     */
    protected boolean performValidation() {
        if (getLiveField().getField().getType().isLabel()) {
            return false;
        } else {
            String missingValue = getLiveField().getField().getMissingValue();
            if (!StringUtils.isBlank(missingValue)) {
                List<String> missingValueTokens = Utils.tokenizeString(missingValue, ",", true);
                Comparable value = getMissingComparable();
                if (value != null) {
                    for (String missingValueToken : missingValueTokens) {
                        Comparable missing = getMissingComparable(missingValueToken);
                        if (missing != null && value.compareTo(missing) == 0) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * A convenience method for raising a response changing {@link WidgetEvent}.
     */
    protected final void raiseResponseChangingEvent() {
        if (getEventCause() == WidgetEvent.EventCause.USER) {
            getResponseChangingListener().responseChanging(new WidgetEvent(this, getEventCause()));
        }
    }

    /**
     * A convenience method for raising a response changed {@link WidgetEvent}. Before raising the
     * event, this method clears the cache of {@link Response}s held for {@link Response}
     * inheritance. The idea behind this is that if the user explicitly changes the value of a
     * {@link Response}, he wants to take control and Response inheritance should be "temporarily
     * disabled".
     */
    protected final void raiseResponseChangedEvent() {
        if (getEventCause() == WidgetEvent.EventCause.USER) {
            Field field = liveField.getField();
            if (field.isResponseInheriting()) {
                hojiContext.getWidgetManager().clearResponseCache(field);
            }
            getResponseChangedListener().responseChanged(new WidgetEvent(this, getEventCause()));
        }
    }
}
