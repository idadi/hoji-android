package ke.co.hoji.android.service.dto;

import java.util.List;

import ke.co.hoji.android.dao.dto.SqliteReferenceDao;
import ke.co.hoji.core.data.dto.Reference;
import ke.co.hoji.core.service.dto.ReferenceService;

public class ReferenceServiceImpl implements ReferenceService {

    private final SqliteReferenceDao referenceDao;

    public ReferenceServiceImpl(SqliteReferenceDao referenceDao) {
        this.referenceDao = referenceDao;
    }

    @Override
    public void saveReferences(List<Reference> references) {
        referenceDao.saveReferences(references);
    }

}
