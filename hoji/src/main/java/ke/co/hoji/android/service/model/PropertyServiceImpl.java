package ke.co.hoji.android.service.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import ke.co.hoji.android.dao.model.SqlitePropertyDao;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.service.model.PropertyService;

public class PropertyServiceImpl implements PropertyService {

    private final SqlitePropertyDao propertyDao;

    public PropertyServiceImpl(SqlitePropertyDao propertyDao) {
        this.propertyDao = propertyDao;
    }

    @Override
    public Property getProperty(String propertyKey, Integer surveyId) {
        Property property;
        if (surveyId != null) {
            property = propertyDao.getProperty(propertyKey, surveyId);
        } else {
            property = propertyDao.getProperty(propertyKey);
        }
        return property;
    }

    @Override
    public List<Property> getProperties(Integer surveyId) {
        return propertyDao.getProperties(surveyId);
    }

    @Override
    public void setProperty(Property property) {
        if (property == null) {
            throw new IllegalArgumentException("property should not be null");
        }

        propertyDao.saveProperty(property);
    }

    @Override
    public void setProperties(Property... properties) {
        setProperties(Arrays.asList(properties));
    }

    @Override
    public void setProperties(Collection<Property> properties) {
        if (properties == null) {
            throw new IllegalArgumentException("props should not be null");
        }
        if (properties.size() == 0) {
            throw new IllegalArgumentException("props should not be an empty collection");
        }

        propertyDao.saveProperties(properties);
    }

    @Override
    public boolean deleteProperty(String propertyKey, Integer surveyId) {
        return propertyDao.deleteProperty(propertyKey, surveyId);
    }

    @Override
    public String getValue(String propertyKey, Integer surveyId) {
        Property property = getProperty(propertyKey, surveyId);
        if (property != null && property.getValue() != null) {
            return property.getValue();
        }
        return null;
    }

    @Override
    public String getValue(String propertyKey) {
        return getValue(propertyKey, null);
    }

    @Override
    public boolean hasValue(String propertyKey, Integer surveyId) {
        return getValue(propertyKey, surveyId) != null;
    }

    @Override
    public boolean hasValue(String propertyKey) {
        return getValue(propertyKey) != null;
    }

    @Override
    public void clearCache() {

    }

}
