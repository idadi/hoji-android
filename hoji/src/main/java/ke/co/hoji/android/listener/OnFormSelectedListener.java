package ke.co.hoji.android.listener;

import ke.co.hoji.core.data.model.Form;

/**
 * Defines a callback for reacting to a {@link Form} selection event.
 * <p/>
 * Created by gitahi on 17/05/15.
 */
public interface OnFormSelectedListener {

    /**
     * Called when a {@link Form} is selected.
     *
     * @param form the {@link Form} selected.
     */
    void onFormSelected(Form form);
}
