package ke.co.hoji.android.helper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import ke.co.hoji.R;

/**
 * Creates a lettered GMail-style icon.
 * <p>
 * Created by gitahi on 17/04/18.
 */
public class LetterIconCreator {

    public Drawable createIcon(Context context, String displayName, String key) {
        final Resources res = context.getResources();
        final LetterTileProvider tileProvider = new LetterTileProvider(context);
        final int tileSize = res.getDimensionPixelSize(R.dimen.letter_tile_size);
        final Bitmap letterTile = tileProvider.getLetterTile(displayName, key, tileSize, tileSize);
        return new BitmapDrawable(res, letterTile);
    }
}
