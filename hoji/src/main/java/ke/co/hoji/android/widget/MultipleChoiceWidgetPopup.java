package ke.co.hoji.android.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.EditText;

import java.util.LinkedHashMap;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.service.model.TranslationService;
import ke.co.hoji.core.widget.WidgetUtils;

/**
 * Created by gitahi on 16/05/17.
 */
public class MultipleChoiceWidgetPopup extends AndroidWidget {

    private EditText editText;
    private List<Choice> choices;
    private LinkedHashMap<Choice, Boolean> choiceMap;

    public MultipleChoiceWidgetPopup(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.popup_multiple_choice_widget;
    }

    @Override
    public void loadViews() {
        editText = findViewById(R.id.multipleChoiceEditText);
        editText.setKeyListener(null);
        editText.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                showMultipleChoicePickerDialog();
            }
        });
    }

    @Override
    public void load() {
        Record record = getNavigable().getNavigator() != null ? getNavigable().getNavigator().getRecord() : null;
        choices = WidgetUtils.getChoices(
                getLiveField().getField(),
                record,
                getHojiContext().getRecord(),
                getHojiContext().getModelServiceManager().getRecordService()
        );
    }

    private void showMultipleChoicePickerDialog() {
        final String[] items = getOptions();
        final boolean[] states = getCheckedStates();
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getCurrentContext());
        builder.setMultiChoiceItems(items, states, new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialogInterface, int item, boolean state) {
            }
        });
        builder.setPositiveButton(
                MultipleChoiceWidgetPopup.this.getResources().getString(R.string.ok_action),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SparseBooleanArray sparseBooleanArray = ((AlertDialog) dialog)
                                .getListView().getCheckedItemPositions();
                        choiceMap = new LinkedHashMap<>();
                        for (int position = 0; position < choices.size(); position++) {
                            choiceMap.put(choices.get(position), sparseBooleanArray.get(position));
                        }
                        editText.setText(readResponse().displayString());
                        raiseResponseChangedEvent();
                    }
                });
        builder.setNegativeButton(
                MultipleChoiceWidgetPopup.this.getResources().getString(R.string.cancel_action),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null) {
            setChoiceMap((LinkedHashMap<Choice, Boolean>) response.getActualValue(), false);
        }
    }

    @Override
    public Object getValue() {
        return choiceMap;
    }

    @Override
    public void focus() {
        editText.requestFocus();
    }

    @Override
    public void wipe() {
        setChoiceMap(null, false);
    }

    @Override
    public boolean isEmpty() {
        if (choiceMap != null) {
            Response response = readResponse();
            if (response != null) {
                return ((Long) response.comparableFromActualValue() == 0);
            }
        }
        return super.isEmpty();
    }

    @Override
    public Comparable getComparable() {
        return MultipleChoiceWidgetUtil.getComparable(
                (LinkedHashMap<Choice, Boolean>) readResponse().getActualValue()
        );
    }

    @Override
    public View getPrincipalView() {
        return editText;
    }

    @Override
    public ValidationResult validate(Record record) {
        Choice accompaniedLoner = MultipleChoiceWidgetUtil.getAccompaniedLoner(
                (LinkedHashMap<Choice, Boolean>) readResponse().getActualValue()
        );
        if (accompaniedLoner != null) {
            return ValidationResult.stop(getCurrentContext().getResources()
                    .getString(R.string.loner_not_alone, accompaniedLoner.getName()));
        }
        return super.validate(record);
    }

    private void setChoiceMap(LinkedHashMap<Choice, Boolean> choiceMap, boolean go) {
        this.choiceMap = choiceMap;
        if (this.choiceMap != null) {
            editText.setText(readResponse().displayString());
        } else {
            editText.setText(R.string.tap_to_select);
        }
        focus();
        if (go) {
            raiseResponseChangedEvent();
        }
    }

    private String[] getOptions() {
        AndroidHojiContext hojiContext = (AndroidHojiContext) getContext().getApplicationContext();
        TranslationService translationService = hojiContext.getModelServiceManager().getTranslationService();
        Integer languageId = hojiContext.getLanguageId();
        String[] options = new String[choices.size()];
        int i = 0;
        for (Choice choice : choices) {
            options[i] = translationService.translate(
                    choice,
                    languageId,
                    Choice.TranslatableComponent.NAME,
                    choice.getName()
            );
            i++;
        }
        return options;
    }

    private boolean[] getCheckedStates() {
        boolean[] checkedStates = new boolean[choices.size()];
        int i = 0;
        if (choiceMap != null) {
            for (Choice choice : choiceMap.keySet()) {
                Boolean checked = choiceMap.get(choice);
                checkedStates[choices.indexOf(choice)] = checked != null ? checked : false;
                i++;
            }
        } else {
            for (i = 0; i < choices.size(); i++) {
                checkedStates[i] = false;
                i++;
            }
        }
        return checkedStates;
    }
}
