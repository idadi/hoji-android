package ke.co.hoji.android.listener;

import ke.co.hoji.android.data.LocationWrapper;
import ke.co.hoji.core.data.model.Form;

/**
 * Defines callbacks for notifying changes to GPS status.
 * <p/>
 *
 * @author gitahi on 21/03/17.
 */
public interface OnGpsStatusChangedListener {

    /**
     * Called when a satisfactory GPS status is achieved. What constitutes a satisfactory GPS status
     * is defined for every {@link Form} by specifying values for {@link Form#gpsCapture} and
     * {@link Form#minGpsAccuracy}.
     *
     * @param locationWrapper an object containing the satisfactory location data.
     */
    void onGpsReady(LocationWrapper locationWrapper);

    /**
     * Called when a satisfactory GPS status could not be achieved after a specified number of
     * attempts. The number of attempts is specified for every form by setting the value for
     * {@link Form#noOfGpsAttempts}
     *
     * @param locationWrapper an object containing the unsatisfactory location data.
     */
    void onGpsAborted(LocationWrapper locationWrapper);
}
