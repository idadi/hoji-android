package ke.co.hoji.android.listener;

import java.util.List;

import ke.co.hoji.core.data.dto.LiteRecord;
import ke.co.hoji.core.data.model.Form;

/**
 * Defines a callback for reacting to {@link LiteRecord}s being returned for import from the server.
 * <p/>
 * Created by gitahi on 17/11/16.
 */
public interface OnLiteRecordsFoundListener {

    /**
     * Called when a {@link LiteRecord}s are found.
     *
     * @param liteRecords the {@link LiteRecord}s found.
     * @param form        the {@link Form} under which the LiteRecords belong.
     */
    void onLiteRecordsFound(List<LiteRecord> liteRecords, Form form);
}
