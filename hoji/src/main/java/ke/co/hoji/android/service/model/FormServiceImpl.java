package ke.co.hoji.android.service.model;

import java.util.List;

import ke.co.hoji.android.dao.model.SqliteFormDao;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.FormService;

public class FormServiceImpl implements FormService {

    private final SqliteFormDao formDao;

    public FormServiceImpl(SqliteFormDao formDao) {
        this.formDao = formDao;
    }

    @Override
    public Form getFormById(Integer formId) {
        return formDao.getFormById(formId);
    }

    @Override
    public List<Form> getFormsBySurvey(Survey survey) {
        return formDao.getFormBySurvey(survey.getId());
    }

    @Override
    public List<Form> getFormsBySurvey(Survey survey, boolean enabled) {
        return formDao.getFormBySurvey(survey.getId(), enabled);
    }

    @Override
    public void saveForm(Form form) {

    }

    @Override
    public List<Form> modifyForms(List<Form> list) {
        return null;
    }

    @Override
    public void deleteForm(Integer integer) {

    }

    @Override
    public void autoNumber(List<Field> list) {

    }

    @Override
    public void enableForm(Form form, boolean b) {

    }

    @Override
    public void updateOrdinal(List<Form> list) {

    }

    @Override
    public int computeCredits(Form form) {
        return 0;
    }

}
