package ke.co.hoji.android;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by gitahi on 16/04/15.
 */
public class HojiSQLiteAssetHelper extends SQLiteAssetHelper {

    /**
     * True if data needs to be ported to a new format and false otherwise. This is may be necessary if the underlying
     * data storage format has changed and legacy data needs to be ported. Inspired by issue #938.
     */
    private static boolean portData = false;

    private static boolean fetchData;

    private static final String DATABASE_NAME = "hoji.db";
    private static final int DATABASE_VERSION = 38;

    public HojiSQLiteAssetHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
        if (oldVersion < 22) {
            portData = true;
        }
        if (oldVersion < 32) {
            fetchData = true;
        }
    }

    public static boolean isPortData() {
        return portData;
    }

    public static void setPortData(boolean portData) {
        HojiSQLiteAssetHelper.portData = portData;
    }

    public static boolean isFetchData() {
        return fetchData;
    }

    public static void setFetchData(boolean fetchData) {
        HojiSQLiteAssetHelper.fetchData = fetchData;
    }
}
