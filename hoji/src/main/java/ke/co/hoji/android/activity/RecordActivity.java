package ke.co.hoji.android.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.io.Serializable;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.fragment.RecordFragment;
import ke.co.hoji.android.listener.OnRecordSearchUpdatedListener;
import ke.co.hoji.android.listener.OnRecordSelectedListener;
import ke.co.hoji.android.listener.OnRecordsLoadedListener;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.model.Record;

/**
 * Created by gitahi on 12/05/15.
 */
public class RecordActivity extends AppCompatActivity implements
        OnRecordSelectedListener, OnRecordsLoadedListener, OnRecordSearchUpdatedListener {

    private List<Record> records;
    private RecordSearch recordSearch;

    private RecordFragment recordFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_activity);
        recordFragment = (RecordFragment) getSupportFragmentManager()
                .findFragmentById(R.id.recordFragment);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        List<Record> records;
        RecordSearch recordSearch;
        if (savedInstanceState != null) {
            recordSearch = (RecordSearch) savedInstanceState.getSerializable(Constants.RECORD_SEARCH);
            records = (List<Record>) savedInstanceState.getSerializable(Constants.DEFLATED_RECORDS);
        } else {
            Intent intent = getIntent();
            records = (List<Record>) intent.getSerializableExtra(Constants.DEFLATED_RECORDS);
            recordSearch = (RecordSearch) intent.getSerializableExtra(Constants.RECORD_SEARCH);
        }
        onRecordsLoaded(records, "", recordSearch);
        refresh();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        refresh();
    }

    private void refresh() {
        Record record = ((HojiContext) getApplication()).getRecord();
        if (record != null) {
            if (records.contains(record)) {
                int index = records.indexOf(record);
                records.set(index, record);
            }
            onRecordsLoaded(records, "");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        List<Record> deflatedRecords = ((AndroidHojiContext) getApplication()).deflateRecords(records);
        outState.putSerializable(Constants.DEFLATED_RECORDS, (Serializable) deflatedRecords);
        outState.putSerializable(Constants.RECORD_SEARCH, recordSearch);
    }

    public void onRecordSelected(Record record) {
        Intent intent = new Intent(RecordActivity.this, LiveFieldActivity.class);
        intent.putExtra(Constants.DEFLATED_RECORD, ((AndroidHojiContext) getApplication()).deflateRecord(record));
        startActivity(intent);
    }

    public void onRecordsLoaded(List<Record> records, String searchTaskId) {
        onRecordsLoaded(records, searchTaskId, null);
    }

    public void onRecordsLoaded(List<Record> records, String searchTaskId, RecordSearch recordSearch) {
        this.records = records;
        recordFragment.refresh(this.records);
        if (recordSearch != null) {
            onRecordSearchUpdated(recordSearch);
            this.recordSearch = recordSearch;
        }
        this.setTitle(getResources().getString
                (
                        R.string.records_x_to_y_of_z,
                        this.recordSearch.getPageInfo().getFirstItemNo(),
                        this.recordSearch.getPageInfo().getLastItemNo(),
                        this.recordSearch.getPageInfo().getTotalItemCount()
                )
        );
        recordFragment.setRecordSearch(this.recordSearch);
    }

    public void onRecordSearchUpdated(RecordSearch recordSearch) {
        this.recordSearch = recordSearch;
    }
}