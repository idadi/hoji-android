package ke.co.hoji.android.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import ke.co.hoji.R;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.TextInputType;
import ke.co.hoji.core.response.Response;

/**
 * Created by gitahi on 14/11/14.
 */
public class NumberWidget extends AndroidWidget {

    private EditText editText;

    public NumberWidget(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.number_widget;
    }

    @Override
    public void loadViews() {
        editText = findViewById(R.id.editText);
        editText.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                raiseResponseChangingEvent();
            }
        });
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT) {
                    raiseResponseChangedEvent();
                }
                return false;
            }
        });
    }

    @Override
    public boolean setDefaultValue(String defaultValue) {
        Long defaultLong = Utils.parseLong(defaultValue);
        if (defaultLong != null) {
            editText.setText(String.valueOf(defaultLong));
            return true;
        }
        return false;
    }

    @Override
    public void wipe() {
        editText.setText("");
        int defaultInputType = TextInputType.NUMBER;
        Integer requestedInputType = Utils.parseInteger(getLiveField().getField().getTag());
        if (requestedInputType != null
                && requestedInputType == TextInputType.NUMBER_SIGNED) {
            TextWidgetUtil.setInputType(editText, requestedInputType);
        } else {
            TextWidgetUtil.setInputType(editText, defaultInputType);
        }
    }

    @Override
    public boolean needsKeyboard() {
        return true;
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null && response.getActualValue() != null) {
            editText.setText(response.getActualValue().toString());
        }
    }

    @Override
    public Object getValue() {
        String text = editText.getText().toString();
        if (text.length() > 0) {
            return Utils.parseLong(text);
        }
        return null;
    }

    @Override
    public void focus() {
        TextWidgetUtil.setFocus(editText, isEditable());
    }

    @Override
    public Comparable getComparable() {
        return (Long) readResponse().getActualValue();
    }

    @Override
    public Comparable getMissingComparable(String stringValue) {
        return Utils.parseLong(stringValue);
    }

    @Override
    public View getPrincipalView() {
        return editText;
    }
}
