package ke.co.hoji.android.dao.dto;

import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.NavigableList;
import ke.co.hoji.core.data.ReferenceLevel;
import ke.co.hoji.core.data.dto.Reference;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by gitahi on 28/08/15.
 */
public class SqliteReferenceDao {

    private final SqliteSqlExecutor se;

    public SqliteReferenceDao(SqliteSqlExecutor se) {
        this.se = se;
    }

    public void saveReferences(List<Reference> references) {
        se.beginTransaction();
        try {
            Reference first = new NavigableList<>(references).first();
            List<ReferenceLevel> referenceLevels = first.getReferenceLevels();

            for (ReferenceLevel referenceLevel : referenceLevels) {
                if (!referenceExists(first.getId())) {
                    insertReferenceLevel(referenceLevel);
                } else {
                    updateReferenceLevel(referenceLevel);
                }
            }

            for (Reference reference : references) {
                if (!referenceExists(reference.getId())) {
                    insertReference(reference);
                } else {
                    updateReference(reference);
                }
            }
            se.setTransactionSuccessful();
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteReferenceDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex.getMessage());
        } finally {
            se.endTransaction();
        }
    }

    private void insertReference(Reference reference) {
        Map<String, Object> params = se.createParameterMap();
        params.put("id", reference.getId());
        for (ReferenceLevel referenceLevel : reference.getReferenceLevels()) {
            params.put(referenceLevel.getLevelColumn(), reference.getValues().get(referenceLevel.getLevel()));
        }
        se.insert("reference", params);
    }


    private void updateReference(Reference reference) {
        Map<String, Object> params = se.createParameterMap();
        Map<String, Object> whereParams = se.createParameterMap("id", reference.getId());
        for (ReferenceLevel referenceLevel : reference.getReferenceLevels()) {
            params.put(referenceLevel.getLevelColumn(), reference.getValues().get(referenceLevel.getLevel()));
        }
        se.update("reference", params, "id = ?", whereParams);
    }

    private void insertReferenceLevel(ReferenceLevel referenceLevel) {
        Map<String, Object> params = se.createParameterMap();
        params.put("id", referenceLevel.getId());
        params.put("name", referenceLevel.getName());
        params.put("level", referenceLevel.getLevel());
        params.put("level_column", referenceLevel.getLevelColumn());
        params.put("displayable", referenceLevel.isDisplayable());
        params.put("input_type", referenceLevel.getInputType());
        params.put("rulable", referenceLevel.isRulable());
        params.put("missing_action", referenceLevel.getMissingAction());
        se.insert("reference_level", params);
    }


    private void updateReferenceLevel(ReferenceLevel referenceLevel) {
        Map<String, Object> params = se.createParameterMap();
        Map<String, Object> whereParams = se.createParameterMap("id", referenceLevel.getId());
        params.put("name", referenceLevel.getName());
        params.put("level", referenceLevel.getLevel());
        params.put("level_column", referenceLevel.getLevelColumn());
        params.put("displayable", referenceLevel.isDisplayable());
        params.put("input_type", referenceLevel.getInputType());
        params.put("rulable", referenceLevel.isRulable());
        params.put("missing_action", referenceLevel.getMissingAction());
        se.update("reference_level", params, "id = ?", whereParams);
    }

    private boolean referenceExists(int referenceId) {
        String select = "SELECT COUNT(id) FROM reference WHERE id = ?";
        return  se.exists(select, referenceId);
    }
}
