package ke.co.hoji.android.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.io.Serializable;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.listener.OnRecordsLoadedListener;
import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.model.Record;

/**
 * Created by gitahi on 17/04/18.
 */
public class SearchActivity extends AppCompatActivity implements OnRecordsLoadedListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    public void onRecordsLoaded(List<Record> records, String searchId) {
        onRecordsLoaded(records, searchId, null);
    }

    public void onRecordsLoaded(List<Record> records, String searchId, RecordSearch recordSearch) {
        List<Record> deflatedRecords = ((AndroidHojiContext) getApplication()).deflateRecords(records);
        Intent intent = new Intent(SearchActivity.this, RecordActivity.class);
        intent.putExtra(Constants.DEFLATED_RECORDS, (Serializable) deflatedRecords);
        intent.putExtra(Constants.RECORD_SEARCH, recordSearch);
        startActivity(intent);
    }
}