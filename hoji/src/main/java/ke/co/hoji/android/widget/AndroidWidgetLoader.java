package ke.co.hoji.android.widget;

import android.content.Context;
import android.view.LayoutInflater;

import ke.co.hoji.android.data.Constants;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.widget.Widget;
import ke.co.hoji.core.widget.WidgetLoader;

import java.lang.reflect.Constructor;
import java.util.Map;

/**
 * Created by gitahi on 12/11/14.
 */
public class AndroidWidgetLoader implements WidgetLoader {

    private int id = Integer.MIN_VALUE;

    @Override
    public Widget loadWidget(LiveField liveField, Map<String, Object> params) {
        Field field = liveField.getField();
        FieldType fieldType = field.getType();
        String className = fieldType.getWidgetClassByFieldParent(field.getParent());
        Context context = (Context) params.get(Constants.CONTEXT);
        try {
            Class cls = Class.forName(className);
            Constructor<AndroidWidget> cons = cls.getConstructor(Context.class);
            AndroidWidget widget = cons.newInstance(context);
            widget.setCurrentContext(context);
            widget.setLiveField(liveField);
            widget.setHojiContext((HojiContext) context.getApplicationContext());
            widget.inflateLayout(LayoutInflater.from(context));
            widget.loadViews();
            widget.setId(id);
            id++;
            return widget;
        } catch (Exception ex) {
            throw new RuntimeException("Could not load widget with class name: " + className, ex);
        }
    }

    @Override
    public void refreshWidget(Widget widget, Map<String, Object> params) {
        Context context = (Context) params.get(Constants.CONTEXT);
        ((AndroidWidget) widget).setCurrentContext(context);
    }
}
