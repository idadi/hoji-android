package ke.co.hoji.android.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.listener.OnRecordSelectedListener;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.service.model.RecordService;

/**
 * Created by gitahi on 2/7/18.
 */
public class SetLiveFieldsTask extends AsyncTask<Void, Void, Void> {

    private final Record record;
    private final OnRecordSelectedListener onRecordSelectedListener;
    private final ProgressDialog progressDialog;
    private final Activity activity;

    public SetLiveFieldsTask(
            Record record,
            OnRecordSelectedListener onRecordSelectedListener,
            ProgressDialog progressDialog,
            Activity activity
    ) {
        this.record = record;
        this.onRecordSelectedListener = onRecordSelectedListener;
        this.progressDialog = progressDialog;
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        if (progressDialog != null) {
            progressDialog.setMessage(activity.getResources().getText(R.string.please_wait));
            progressDialog.show();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        AndroidHojiContext hojiContext = (AndroidHojiContext) activity.getApplication();
        hojiContext.setRecord(record);
        hojiContext.setUpAcraRecordData();
        RecordService recordService = hojiContext.getModelServiceManager().getRecordService();
        recordService.setLiveFields(record);
        onRecordSelectedListener.onRecordSelected(record);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (activity != null) {
            if (activity.isFinishing()) {
                return;
            }
        }
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
