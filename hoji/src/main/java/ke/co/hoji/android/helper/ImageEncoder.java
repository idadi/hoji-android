package ke.co.hoji.android.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.core.data.Constants;

/**
 * A utility class for encoding and decoding images. On Hoji, images are saved to the file system
 * and only the path is placed in the database. In addition, images are always converted to Base64
 * string representation for transfer across the network.
 * <p>
 * This class supports convenient conversions between these different representations as necessary.
 */
public class ImageEncoder {

    private AndroidHojiContext androidHojiContext;

    public ImageEncoder(AndroidHojiContext androidHojiContext) {
        this.androidHojiContext = androidHojiContext;
    }

    /**
     * Reads an image from the file system and converts it to a Base64 string. The image size in
     * pixels may be scaled down.
     *
     * @param filePath         the path where the image file is stored.
     * @param maxWidthOrHeight the longest dimension of the image (either height or width) in pixels.
     * @param quality          the image quality to compress for (a value between 1% for really low
     *                         quality and small file size to 100% for best quality and large file
     *                         size).
     * @return the Base64 string representation of the image.
     */
    public String fromFileToBase64(String filePath, Integer maxWidthOrHeight, Integer quality) throws IOException {
        ByteArrayOutputStream baos = null;
        try {
            baos = new ByteArrayOutputStream();
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            if (bitmap == null) {
                throw new IOException("Image file could not be read!");
            }
            Bitmap scaled;
            if (maxWidthOrHeight == null) {
                maxWidthOrHeight = Constants.ImageManagement.MAX_DIMENSION;
            }
            scaled = scaleBitmap(bitmap, maxWidthOrHeight);
            scaled = ImageRotator.rotateImage(scaled, filePath);
            if (quality == null) {
                quality = Constants.ImageManagement.DEFAULT_QUALITY;
            }
            scaled.compress(Bitmap.CompressFormat.JPEG, quality, baos);
            byte[] bytes = baos.toByteArray();
            return androidHojiContext.getBase64Encoder().encodeToBase64String(bytes);
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException e) {
                    // Do nothing
                }
            }
        }
    }

    /**
     * Takes a Base64 string representation of an image, decodes it and and saves it to file.
     *
     * @param base64String  the Base64 string representation of the image.
     * @param directoryPath the directory path, within the root image directory where the file
     *                      should be created.
     * @param fileName      the name to assign to the image file.
     * @return the file path to the image file that's saved in the file system.
     */
    public String fromBase64ToFile(String base64String, String directoryPath, String fileName) {
        byte[] bytes = androidHojiContext.getBase64Encoder().decodeToByteArray(base64String);
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        File imageFile = null;
        try {
            imageFile = createImageFile(directoryPath, fileName);
            FileOutputStream out = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String path = imageFile.getAbsolutePath();
        return path;
    }

    /**
     * Creates (or returns if it exists) a {@link File} for storing an image.
     *
     * @param directoryPath the directory path, within the root image directory where the file
     *                      should be created.
     * @param fileName      the name of the image file.
     * @return the file for storing an image.
     */
    public File createImageFile(String directoryPath, String fileName) throws IOException {
        String rootPath = androidHojiContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath();
        String fullPath = rootPath + File.separator + directoryPath;
        File imageFolder = new File(fullPath);
        boolean folderExists = true;
        if (!imageFolder.exists()) {
            folderExists = imageFolder.mkdirs();
        }
        if (folderExists) {
            return File.createTempFile(fileName, ".jpg", imageFolder);
        }
        return null;
    }

    private Bitmap scaleBitmap(Bitmap bitmap, int maxWidthOrHeight) {
        int originalHeight = bitmap.getHeight();
        int originalWidth = bitmap.getWidth();
        int longestDimension = originalHeight > originalWidth ? originalHeight : originalWidth;
        Bitmap scaled;
        if (longestDimension > maxWidthOrHeight) {
            double reductionRation = (double) maxWidthOrHeight / (double) longestDimension;
            int newHeight = (int) (reductionRation * (double) originalHeight);
            int newWidth = (int) (reductionRation * (double) originalWidth);
            scaled = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
        } else {
            scaled = bitmap;
        }
        return scaled;
    }
}
