package ke.co.hoji.android.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.activity.RecordActivity;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.android.listener.OnRecordsLoadedListener;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.data.PageInfo;
import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.service.model.RecordService;

/**
 * Created by gitahi on 22/05/15.
 */
public class SearchTask extends AsyncTask<Void, Void, Boolean> {

    private List<Record> records;

    /**
     * A unique id for a running instance of this class. Consumers may optionally decide to keep
     * track of running tasks to ensure they avoid repeating the same work unnecessarily.
     */
    private final String searchTaskId;
    private final RecordSearch recordSearch;
    private final Activity activity;
    private final OnRecordsLoadedListener onRecordsLoadedListener;
    private final boolean infiniteScrollMode;

    private final ProgressDialog progressDialog;

    public SearchTask(
            String searchTaskId,
            RecordSearch recordSearch,
            Activity activity,
            OnRecordsLoadedListener onRecordsLoadedListener,
            boolean infiniteScrollMode
    ) {
        this(searchTaskId, recordSearch, activity, onRecordsLoadedListener, infiniteScrollMode, null);
    }

    public SearchTask(
            String searchTaskId,
            RecordSearch recordSearch,
            Activity activity,
            OnRecordsLoadedListener onRecordsLoadedListener,
            boolean infiniteScrollMode,
            ProgressDialog progressDialog
    ) {
        this.searchTaskId = searchTaskId;
        this.recordSearch = recordSearch;
        this.activity = activity;
        this.onRecordsLoadedListener = onRecordsLoadedListener;
        this.infiniteScrollMode = infiniteScrollMode;
        this.progressDialog = progressDialog;
    }

    @Override
    protected void onPreExecute() {
        if (progressDialog != null) {
            progressDialog.setMessage(activity.getResources().getText(R.string.please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        ModelServiceManager modelServiceManager = ((HojiContext) activity.getApplication()).getModelServiceManager();
        RecordService recordService = modelServiceManager.getRecordService();
        int itemCount = recordService.count(recordSearch);
        if (itemCount <= 0) {
            //If we are in infinite scroll mode and the data-set has no records, we call the observer
            //with an empty list. Otherwise we do not call the observer.
            if (infiniteScrollMode && recordSearch.getTimeStamp() == 0L) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onRecordsLoadedListener.onRecordsLoaded(new ArrayList<Record>(), searchTaskId, recordSearch);
                    }
                });
            }
            return false;
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        Integer maxPageSize = PageInfo.PAGE_SIZE;
        if (!infiniteScrollMode) {
            maxPageSize = Integer.valueOf(sharedPreferences.getString(SettingsActivity.PAGE_SIZE,
                    String.valueOf(PageInfo.PAGE_SIZE)));
        }
        PageInfo pageInfo;
        if (recordSearch.getTimeStamp() == 0) {
            records = recordService.search(recordSearch, maxPageSize);
            pageInfo = new PageInfo(itemCount, maxPageSize);
            recordSearch.setPageInfo(pageInfo);
        } else {
            records = recordService.search(recordSearch, maxPageSize);
            pageInfo = recordSearch.getPageInfo();
        }
        if (!recordSearch.isReverse()) {
            pageInfo.next(records.size());
        } else {
            pageInfo.previous(records.size());
        }
        activity.runOnUiThread(new Runnable() {
            public void run() {
                onRecordsLoadedListener.onRecordsLoaded(records, searchTaskId, recordSearch);
            }
        });
        return true;
    }

    @Override
    protected void onPostExecute(Boolean recordsFound) {
        if (!infiniteScrollMode && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (!infiniteScrollMode && !recordsFound) {
            if (onRecordsLoadedListener instanceof RecordActivity) {
                ((Activity) onRecordsLoadedListener).finish();
            } else {
                Toast toast = Toast.makeText
                        (
                                activity,
                                activity.getResources().getText(R.string.nothing_found),
                                Toast.LENGTH_SHORT
                        );
                toast.show();
            }
        }
    }
}
