package ke.co.hoji.android.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import java.util.List;

import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.TranslationService;

public class FormArrayAdapter extends ArrayAdapter<Form> {

    public FormArrayAdapter(Context context, int resource, List<Form> forms) {
        super(context, resource, forms);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AndroidHojiContext androidHojiContext = (AndroidHojiContext) getContext().getApplicationContext();
        TranslationService translationService = androidHojiContext.getModelServiceManager().getTranslationService();
        Integer languageId = androidHojiContext.getLanguageId();
        Form form = getItem(position);
        TextView textView = (TextView) super.getView(position, convertView, parent);
        textView.setText(Html.fromHtml(translationService.translate(
                form,
                languageId,
                Form.TranslatableComponent.NAME,
                form.getName()))
        );
        return textView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        AndroidHojiContext androidHojiContext = (AndroidHojiContext) getContext().getApplicationContext();
        TranslationService translationService = androidHojiContext.getModelServiceManager().getTranslationService();
        Integer languageId = androidHojiContext.getLanguageId();
        Form form = getItem(position);
        CheckedTextView checkedTextView = (CheckedTextView) super.getDropDownView(position, convertView, parent);
        checkedTextView.setText(Html.fromHtml(translationService.translate(
                form,
                languageId,
                Form.TranslatableComponent.NAME,
                form.getName()))
        );
        return checkedTextView;
    }
}
