package ke.co.hoji.android.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import ke.co.hoji.R;
import ke.co.hoji.core.response.Response;

/**
 * Created by gitahi on 14/11/14.
 */
public class TextWidget extends AndroidWidget {

    private EditText editText;

    public TextWidget(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.text_widget;
    }

    @Override
    public void loadViews() {
        editText = findViewById(R.id.editText);
        editText.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                raiseResponseChangingEvent();
            }
        });
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT) {
                    raiseResponseChangedEvent();
                }
                return false;
            }
        });
    }

    @Override
    public void wipe() {
        editText.setText("");
        TextWidgetUtil.setInputType(editText, getLiveField().getField().getTag());
    }

    @Override
    public boolean setDefaultValue(String defaultValue) {
        editText.setText(defaultValue);
        return true;
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null) {
            editText.setText((String) response.getActualValue());
        }
    }

    @Override
    public Object getValue() {
        String text = editText.getText().toString();
        return !"".equals(text) ? text : null;
    }

    @Override
    public void focus() {
        TextWidgetUtil.setFocus(editText, isEditable());
    }

    @Override
    public boolean needsKeyboard() {
        return true;
    }

    @Override
    public boolean isEmpty() {
        return editText.getText().toString().trim().length() < 1;
    }

    @Override
    public Comparable getComparable() {
        return (long) editText.getText().toString().length();
    }

    @Override
    public Comparable getMissingComparable() {
        return readResponse().displayString();
    }

    public Comparable getMissingComparable(String stringValue) {
        return stringValue;
    }

    @Override
    public View getPrincipalView() {
        return editText;
    }
}
