package ke.co.hoji.android.adapter;

import android.content.Context;
import android.preference.PreferenceManager;
import androidx.core.content.ContextCompat;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.android.helper.UiUtils;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.MainRecord;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.service.model.TranslationService;

/**
 * Created by gitahi on 12/05/15.
 */
public class RecordArrayAdapter2 extends ArrayAdapter<Record> {

    private final AndroidHojiContext androidHojiContext;

    public RecordArrayAdapter2(Context context, List<Record> records) {
        super(context, 0, records);
        androidHojiContext = (AndroidHojiContext) context.getApplicationContext();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TranslationService translationService = androidHojiContext.getModelServiceManager().getTranslationService();
        Integer languageId = androidHojiContext.getLanguageId();

        Record record = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.record_row, parent, false);
        }
        TextView recordCaption = convertView.findViewById(R.id.recordCaptionTextView);
        TextView recordForm = convertView.findViewById(R.id.formTextView);
        TextView recordDate = convertView.findViewById(R.id.recordDateTextView);
        TextView recordLocation = convertView.findViewById(R.id.recordLocationTextView);
        ImageView recordIcon = convertView.findViewById(R.id.recordIconImageView);

        Form form = (Form) record.getFieldParent();
        MainRecord mainRecord = (MainRecord) record.getDetail();
        recordCaption.setText(record.getCaption() != null ? record.getCaption() : getTranslatedFormName(form, languageId, translationService));
        recordForm.setText(Html.fromHtml(getTranslatedFormName(form, languageId, translationService)));
        int dateFormat = Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(getContext())
                .getString(SettingsActivity.DATE_TIME_FORMAT, "1"));
        Date dateCreated = new Date(mainRecord.getRecord().getDateCreated());
        String dateCreatedString;
        if (dateFormat == 2) {
            dateCreatedString = Utils.formatDate(dateCreated);
        } else if (dateFormat == 3) {
            dateCreatedString = Utils.formatTime(dateCreated);
        } else if (dateFormat == 4) {
            dateCreatedString = Utils.formatDateTime(dateCreated);
        } else {
            dateCreatedString = new PrettyTime().format(dateCreated);
        }
        recordDate.setText(dateCreatedString);
        recordLocation.setText(UiUtils.getLocation(mainRecord));
        setRecordIcon(record, recordIcon);
        return convertView;
    }

    private void setRecordIcon(Record record, ImageView recordIcon) {
        MainRecord mainRecord = (MainRecord) record.getDetail();
        switch (mainRecord.getStage()) {
            case MainRecord.Stage.DRAFT:
                recordIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.draft));
                break;
            case MainRecord.Stage.COMPLETE:
                recordIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.complete));
                break;
            case MainRecord.Stage.UPLOADED:
                recordIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.uploaded));
                break;
            default:
                recordIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.draft));
                break;
        }
    }

    private String getTranslatedFormName(Form form, Integer languageId, TranslationService translationService) {
        return translationService.translate(form, languageId, Form.TranslatableComponent.NAME, form.getName());
    }
}
