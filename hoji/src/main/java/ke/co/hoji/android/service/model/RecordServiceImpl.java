package ke.co.hoji.android.service.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import ke.co.hoji.android.dao.model.SqliteRecordDao;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ResourceManager;
import ke.co.hoji.core.data.GpsLocation;
import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.MainRecord;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.RecordResponse;
import ke.co.hoji.core.service.model.RecordService;

public class RecordServiceImpl implements RecordService {

    private final SqliteRecordDao recordDao;

    private final HojiContext hojiContext;

    public RecordServiceImpl(SqliteRecordDao recordDao) {
        this.recordDao = recordDao;
        this.hojiContext = ResourceManager.getHojiContext();
    }

    @Override
    public boolean isSimpleKeyUnique(Record record, Record mainRecord, Field field, Object uniqueValue, boolean testMode) {
        int count = recordDao.countSimpleKeyInstances(record, mainRecord, field, uniqueValue, testMode);
        return count == 0;
    }

    @Override
    public boolean isCompoundKeyUnique(Record record, Record mainRecord, String uniqueValue, boolean testMode) {
        int count = recordDao.countCompoundKeyInstances(record, mainRecord, uniqueValue, testMode);
        return count == 0;
    }

    @Override
    public List<Record> findRecords(LiveField liveField, int testMode) {
        Record record = hojiContext.getRecord();
        if (liveField.getField().getMainRecordField() == null) {
            List<Integer> recordFormIds = new ArrayList<>();
            if (liveField.getField().getRecordForms() != null && !liveField.getField().getRecordForms().isEmpty()) {
                for (Form recordForm : liveField.getField().getRecordForms()) {
                    recordFormIds.add(recordForm.getId());
                }
            }
            List<Record> records = new ArrayList<>();
            if (liveField.getField().hasFilterRules()) {
                List<Record> candidates = recordDao.findMainRecords(recordFormIds, testMode > 0, SqliteRecordDao.FetchMode.FULL);
                for (Record candidate : candidates) {
                    if (hojiContext.getRuleEvaluator().evaluateRules(liveField.getField().getFilterRules(),
                            candidate, null)) {
                        records.add(candidate);
                    }
                }
            } else {
                records = recordDao.findMainRecords(recordFormIds, testMode > 0, SqliteRecordDao.FetchMode.EXTRA_LITE);
            }
            return records;
        } else {
            if (record != null) {
                Record mainRecord = null;
                LiveField mainRecordLiveField = record.getLiveField(liveField.getField().getMainRecordField());
                if (mainRecordLiveField != null) {
                    mainRecord = ((RecordResponse) mainRecordLiveField.getResponse()).getActualValue();
                }
                if (mainRecord != null) {
                    Field matrixRecordField = liveField.getField().getMatrixRecordField();
                    if (matrixRecordField != null) {
                        List<Record> records = new ArrayList<>();
                        if (liveField.getField().hasFilterRules()) {
                            List<Record> candidates = recordDao.findMatrixRecords(mainRecord.getUuid(), matrixRecordField.getId(), SqliteRecordDao.FetchMode.FULL, mainRecord);
                            for (Record candidate : candidates) {
                                if (hojiContext.getRuleEvaluator().evaluateRules(liveField.getField().getFilterRules(),
                                        candidate, null)) {
                                    records.add(candidate);
                                }
                            }
                        } else {
                            records = recordDao.findMatrixRecords(mainRecord.getUuid(), matrixRecordField.getId(), SqliteRecordDao.FetchMode.EXTRA_LITE, mainRecord);
                        }
                        Collections.sort(records, new Comparator<Record>() {
                            @Override
                            public int compare(Record one, Record another) {
                                return one.toString().compareTo(another.toString());
                            }
                        });
                        return records;
                    }
                }
            }
        }
        return new ArrayList<>();
    }

    @Override
    public Record findRecordByUuid(String uuid, Field field) {
        Record record;
        if (field.getMainRecordField() != null && field.getMatrixRecordField() != null) {
            Record mainRecord = hojiContext != null ? hojiContext.getRecord() : null;
            record = recordDao.findMatrixRecordByUuid(
                    uuid,
                    SqliteRecordDao.FetchMode.EXTRA_LITE,
                    mainRecord,
                    field.getMatrixRecordField()
            );
        } else {
            record = recordDao.findMainRecordByUuuid(uuid, SqliteRecordDao.FetchMode.EXTRA_LITE);
        }
        return record;
    }

    @Override
    public List<Record> search(RecordSearch recordSearch, int limit) {
        return recordDao.findMainRecords(recordSearch, SqliteRecordDao.FetchMode.EXTRA_LITE, limit);
    }

    @Override
    public void save(Record record) {
        if (record.getUuid() == null) {
            if (record.isMain()) {
                MainRecord mainRecord = (MainRecord) record.getDetail();
                mainRecord.setUserId(hojiContext.getUserId());
                mainRecord.setDeviceId(hojiContext.getDeviceIdentifier());
                Form form = (Form) record.getFieldParent();
                if (form.getGpsCapture() != Form.GpsCapture.NEVER) {
                    GpsLocation gpsLocation = GpsLocation.getInstance();
                    if (gpsLocation != null) {
                        mainRecord.setStartLatitude(gpsLocation.getLatitude());
                        mainRecord.setStartLongitude(gpsLocation.getLongitude());
                        mainRecord.setStartAccuracy(gpsLocation.getAccuracy());
                        mainRecord.setStartAge(gpsLocation.getAge());
                        mainRecord.setStartAddress(gpsLocation.getAddress());
                    }
                }
            }
            recordDao.insert(record);
        } else {
            recordDao.update(record);
        }
    }

    @Override
    public void updateStage(MainRecord mainRecord, boolean updateDateCompleted) {
        Form form = (Form) mainRecord.getRecord().getFieldParent();
        if (form.getGpsCapture() != Form.GpsCapture.NEVER) {
            GpsLocation gpsLocation = GpsLocation.getInstance();
            if (gpsLocation != null) {
                mainRecord.setEndLatitude(gpsLocation.getLatitude());
                mainRecord.setEndLongitude(gpsLocation.getLongitude());
                mainRecord.setEndAccuracy(gpsLocation.getAccuracy());
                mainRecord.setEndAge(gpsLocation.getAge());
                mainRecord.setEndAddress(gpsLocation.getAddress());
            }
        }
        recordDao.updateStage(mainRecord, updateDateCompleted);
    }

    @Override
    public int count(RecordSearch recordSearch) {
        return recordDao.countRecords(recordSearch);
    }

    @Override
    public void setLiveFields(Record record) {
        recordDao.setLiveFields(record);
    }

    @Override
    public int clear(List<LiveField> liveFields, Record record) {
        return recordDao.clearViolated(liveFields, record);
    }

    @Override
    public void delete(Record record) {
        recordDao.delete(record);
    }

    @Override
    public String getImageData(String uuid) {
        return recordDao.getImageData(uuid);
    }

    @Override
    public void portDataIfNecessary() {
        recordDao.portDataIfNecessary();
    }

    @Override
    public Set<Integer> getUsedChoiceIds(Field field, Record record, Record mainRecord) {
        return recordDao.getUsedChoiceIds(field, record, mainRecord);
    }

}
