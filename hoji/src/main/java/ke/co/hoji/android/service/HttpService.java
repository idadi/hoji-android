package ke.co.hoji.android.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.squareup.okhttp.Credentials;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.core.service.model.HttpException;
import okio.BufferedSink;

/**
 * Created by gitahi on 23/08/15.
 */
public class HttpService {

    private final AndroidHojiContext androidHojiContext;
    private final JsonService jsonService;

    public HttpService(AndroidHojiContext androidHojiContext, JsonService jsonService) {
        this.androidHojiContext = androidHojiContext;
        this.jsonService = jsonService;
    }

    public <T> T sendAsJson(String address, String username, String password, final Object data,
                            Type responseType) throws HttpException {
        if (isConnected()) {
            int code = HttpException.Code.ERROR;
            try {
                RequestBody body = new RequestBody() {
                    @Override
                    public MediaType contentType() {
                        return MediaType.parse("application/json; charset=utf-8");
                    }

                    @Override
                    public void writeTo(BufferedSink sink) throws IOException {
                        OutputStreamWriter writer = null;
                        try {
                            writer = new OutputStreamWriter(sink.outputStream());
                            jsonService.toJson(data, writer);
                        } finally {
                            if (writer != null) {
                                writer.close();
                            }
                        }
                    }
                };
                Request.Builder builder = new Request.Builder()
                        .url(address)
                        .post(body);
                if (username != null && password != null) {
                    builder.addHeader("Authorization", Credentials.basic(username, password));
                }
                Request request = builder.build();
                OkHttpClient okHttpClient = new OkHttpClient();
                okHttpClient.setConnectTimeout(20, TimeUnit.SECONDS);
                okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
                okHttpClient.setWriteTimeout(60, TimeUnit.SECONDS);
                Response response = okHttpClient.newCall(request).execute();
                code = response.code();
                if (code == 200) {
                    if (response.headers().get("content-type").contains("application/json")) {
                        T obj = jsonService.fromJson(response.body().string(), responseType);
                        return obj;
                    }
                    //Throw UNAUTHORIZED exception because we got redirected to the sign-in page.
                    code = HttpException.Code.UNAUTHORIZED;
                    throw new Exception();
                } else if (code == 451) {
                    throw new Exception();
                }
            } catch (Exception ex) {
                if (!(ex instanceof HttpException)) {
                    throw new HttpException(ex, code != 200 ? code : HttpException.Code.ERROR);
                }
            }
            throw new HttpException(code);
        } else {
            throw new HttpException(HttpException.Code.NO_CONNECTION);
        }
    }

    public String interpretHttpExceptionCode(HttpException ex) {
        return interpretHttpExceptionCode(ex.getCode(), ex);
    }

    public String interpretHttpExceptionCode(int code, Exception ex) {
        String message;
        switch (code) {
            case HttpException.Code.BAD_REQUEST:
                message = androidHojiContext.getResources().getString(R.string.bad_request);
                break;
            case HttpException.Code.UNAUTHORIZED:
                message = androidHojiContext.getResources().getString(R.string.unauthorized);
                break;
            case HttpException.Code.FORBIDDEN:
                message = androidHojiContext.getResources().getString(R.string.forbidden);
                break;
            case HttpException.Code.METHOD_NOT_ALLOWED:
                message = androidHojiContext.getResources().getString(R.string.method_not_allowed);
                break;
            case HttpException.Code.GATEWAY_TIMEOUT:
                message = androidHojiContext.getResources().getString(R.string.gateway_timeout);
                break;
            case HttpException.Code.INTERNAL_SERVER_ERROR:
                message = androidHojiContext.getResources().getString(R.string.internal_server_error);
                break;
            case HttpException.Code.NO_CONNECTION:
                message = androidHojiContext.getResources().getString(R.string.no_connection);
                break;
            case HttpException.Code.NOT_FOUND:
                message = androidHojiContext.getResources().getString(R.string.not_found);
                break;
            case HttpException.Code.SERVICE_UNAVAILABLE:
                message = androidHojiContext.getResources().getString(R.string.service_unavailable);
                break;
            case HttpException.Code.ACCOUNT_SUSPENDED:
                message = androidHojiContext.getResources().getString(R.string.account_suspended);
                break;
            default:
                message = ex.getLocalizedMessage();
                String[] tokens = message.split(":");
                if (tokens.length > 1) {
                    message = tokens[1].trim();
                    String firstChar = message.substring(0, 1);
                    String firstCharUpper = firstChar.toUpperCase();
                    message = firstCharUpper + message.substring(1);
                }
                break;
        }
        return message;
    }

    private boolean isConnected() {
        boolean status = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)
                androidHojiContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getNetworkInfo(0);
        if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
            status = true;
        } else {
            netInfo = connectivityManager.getNetworkInfo(1);
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED)
                status = true;
        }
        return status;
    }
}
