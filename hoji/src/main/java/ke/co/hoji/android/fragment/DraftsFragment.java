package ke.co.hoji.android.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.activity.HomeActivity;
import ke.co.hoji.core.data.model.MainRecord;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.service.model.RecordService;

/**
 * Created by gitahi on 15/04/18.
 */
public class DraftsFragment extends RecordFolderFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.draft_records_actions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clearButton:
                if (records.isEmpty()) {
                    Toast toast = Toast.makeText
                            (
                                    getActivity(),
                                    getActivity().getResources().getText(R.string.nothing_to_clear),
                                    Toast.LENGTH_SHORT
                            );
                    toast.show();
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getResources().getString(R.string.clear_drafts_title))
                            .setMessage(getResources().getQuantityString(R.plurals.clear_drafts_message, records.size(), records.size()))
                            .setPositiveButton(R.string.yes_action, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    AndroidHojiContext hojiContext = (AndroidHojiContext) getActivity().getApplication();
                                    RecordService recordService = hojiContext.getModelServiceManager().getRecordService();
                                    for (Record record : DraftsFragment.this.records) {
                                        recordService.delete(record);
                                    }
                                    DraftsFragment.this.records.clear();
                                    refreshRecords();
                                    ((HomeActivity) getActivity()).showBadges();
                                }
                            })
                            .setNegativeButton(R.string.no_action, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //Do nothing
                                }
                            })
                            .create()
                            .show();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public int getStage() {
        return MainRecord.Stage.DRAFT;
    }
}