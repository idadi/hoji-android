package ke.co.hoji.android.service.model;

import java.util.List;

import ke.co.hoji.android.dao.model.SqliteReferenceDao;
import ke.co.hoji.core.data.NavigableList;
import ke.co.hoji.core.data.ReferenceLevel;
import ke.co.hoji.core.data.model.Reference;
import ke.co.hoji.core.service.model.ReferenceService;

public class ReferenceServiceImpl implements ReferenceService {

    private final SqliteReferenceDao referenceDao;

    public ReferenceServiceImpl(SqliteReferenceDao referenceDao) {
        this.referenceDao = referenceDao;
    }

    @Override
    public Reference search(Reference reference) {
        List<Reference> references = referenceDao.select(reference.translateToMap());
        if (!references.isEmpty()) {
            return new NavigableList<>(references).first();
        }
        return null;
    }

    @Override
    public List<ReferenceLevel> getReferenceLevels() {
        return referenceDao.getReferenceLevels();
    }

}
