package ke.co.hoji.android;

import android.content.Context;

import ke.co.hoji.android.dao.model.SqliteChoiceDao;
import ke.co.hoji.android.dao.model.SqliteChoiceGroupDao;
import ke.co.hoji.android.dao.model.SqliteFieldDao;
import ke.co.hoji.android.dao.model.SqliteFormDao;
import ke.co.hoji.android.dao.model.SqliteLanguageDao;
import ke.co.hoji.android.dao.model.SqlitePropertyDao;
import ke.co.hoji.android.dao.model.SqliteRecordDao;
import ke.co.hoji.android.dao.model.SqliteReferenceDao;
import ke.co.hoji.android.dao.model.SqliteSurveyDao;
import ke.co.hoji.android.dao.model.SqliteTranslationDao;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.service.model.FieldServiceImpl;
import ke.co.hoji.android.service.model.FormServiceImpl;
import ke.co.hoji.android.service.model.LanguageServiceImpl;
import ke.co.hoji.android.service.model.PropertyServiceImpl;
import ke.co.hoji.android.service.model.RecordServiceImpl;
import ke.co.hoji.android.service.model.ReferenceServiceImpl;
import ke.co.hoji.android.service.model.SurveyServiceImpl;
import ke.co.hoji.android.service.model.TranslationServiceImpl;
import ke.co.hoji.core.AbstractServiceManager;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.core.service.model.LanguageService;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.core.service.model.RecordService;
import ke.co.hoji.core.service.model.ReferenceService;
import ke.co.hoji.core.service.model.SurveyService;
import ke.co.hoji.core.service.model.TranslationService;

/**
 * Created by gitahi on 15/04/15.
 */
class SqliteModelServiceManager extends AbstractServiceManager implements ModelServiceManager {

    private ConfigService configService;
    private RecordService recordService;
    private ReferenceService referenceService;
    private PropertyService propertyService;
    private LanguageService languageService;
    private TranslationService translationService;
    private final SqliteSqlExecutor sqlExecutor;

    public SqliteModelServiceManager() {
        this.sqlExecutor = SqliteSqlExecutor.getInstance();
    }

    @Override
    public ConfigService getConfigService() {
        if (configService == null) {
            setContextFromParameters();
            SurveyService surveyService = new SurveyServiceImpl(new SqliteSurveyDao(sqlExecutor));
            FormService formService = new FormServiceImpl(formDao());
            FieldService fieldService = new FieldServiceImpl(fieldDao());

            configService = new ConfigService(surveyService, formService, fieldService);
        }
        return configService;
    }

    @Override
    public RecordService getRecordService() {
        if (recordService == null) {
            setContextFromParameters();
            SqliteRecordDao sqliteRecordDao = new SqliteRecordDao(sqlExecutor, formDao());
            recordService = new RecordServiceImpl(sqliteRecordDao);
        }
        return recordService;
    }

    @Override
    public ReferenceService getReferenceService() {
        if (referenceService == null) {
            setContextFromParameters();
            referenceService = new ReferenceServiceImpl(new SqliteReferenceDao(sqlExecutor));
        }
        return referenceService;
    }

    @Override
    public PropertyService getPropertyService() {
        if (propertyService == null) {
            setContextFromParameters();
            propertyService = new PropertyServiceImpl(new SqlitePropertyDao(sqlExecutor));
        }
        return propertyService;
    }

    @Override
    public LanguageService getLanguageService() {
        if (languageService == null) {
            setContextFromParameters();
            languageService = new LanguageServiceImpl(languageDao());
        }
        return languageService;
    }

    @Override
    public TranslationService getTranslationService() {
        if (translationService == null) {
            setContextFromParameters();
            translationService = new TranslationServiceImpl(translationDao());
        }
        return translationService;
    }

    @Override
    public void clearCache() {
        sqlExecutor.clearCache();
    }

    private void setContextFromParameters() {
        Context context = (Context) getParameters().get(Constants.CONTEXT);
        sqlExecutor.setSqliteAssetHelper(new HojiSQLiteAssetHelper(context));
    }

    private SqliteFormDao formDao() {
        return new SqliteFormDao(sqlExecutor);
    }

    private SqliteFieldDao fieldDao() {
        return new SqliteFieldDao(sqlExecutor, choiceGroupDao());
    }

    private SqliteChoiceGroupDao choiceGroupDao() {
        return new SqliteChoiceGroupDao(sqlExecutor, choiceDao());
    }

    private SqliteChoiceDao choiceDao() {
        return new SqliteChoiceDao(sqlExecutor);
    }

    private SqliteLanguageDao languageDao() {
        return new SqliteLanguageDao(sqlExecutor);
    }

    private SqliteTranslationDao translationDao() {
        return new SqliteTranslationDao(sqlExecutor);
    }

}
