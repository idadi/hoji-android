package ke.co.hoji.android.widget;

import android.content.Context;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.FragmentActivity;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import java.util.Date;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;

/**
 * Created by gitahi on 14/11/14.
 */
public class DateWidget extends AndroidWidget implements DateComponent {

    private EditText editText;
    private Date date;

    public DateWidget(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.date_widget;
    }

    @Override
    public void loadViews() {
        editText = findViewById(R.id.dateEditText);
        editText.setKeyListener(null);
        editText.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                showDatePickerDialog();
            }
        });
    }

    public void setDate(Date date) {
        setDate(date, false);
    }

    public void setDate(Date date, boolean go) {
        this.date = date;
        if (this.date != null) {
            editText.setText(Utils.formatDate(date));
        } else {
            editText.setText(R.string.tap_to_enter);
        }
        focus();
        if (go) {
            raiseResponseChangedEvent();
        }
    }

    public Date getDate() {
        return date;
    }

    private void showDatePickerDialog() {
        Date min = Utils.parseIsoDate(liveField.getField().getMinValue());
        Date max = Utils.parseIsoDate(liveField.getField().getMaxValue());
        DatePickerFragment datePickerFragment = new DatePickerFragment(min, max);
        datePickerFragment.setDateComponent(this);
        datePickerFragment.show(((FragmentActivity) getCurrentContext()).getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null) {
            setDate((Date) response.getActualValue(), false);
        }
    }

    @Override
    public Object getValue() {
        return date;
    }

    @Override
    public void focus() {
        TextWidgetUtil.setFocus(editText, isEditable());
    }

    @Override
    public void wipe() {
        setDate(null, false);
    }

    @Override
    public boolean setDefaultValue(String defaultValue) {
        Date defaultDate = Utils.parseIsoDate(defaultValue);
        if (defaultDate != null) {
            setDate(defaultDate);
            return true;
        }
        return false;
    }

    @Override
    public Comparable getComparable() {
        if (date != null) {
            return new LocalDate(date);
        }
        return null;
    }

    @Override
    public Comparable getMissingComparable(String stringValue) {
        return Utils.parseLocalDate(stringValue);
    }

    @Override
    public Field.ValidationValue getMin(Record record) {
        Field.ValidationValue genericValidationValue = getGenericValidationValue(
                getLiveField().getField().getMinValue(),
                record
        );
        if (genericValidationValue != null) {
            String dateString = String.valueOf(genericValidationValue.getValue());
            LocalDate min;
            if (Constants.Command.TODAY.equals(dateString)) {
                min = new LocalDate(new Date());
            } else {
                min = Utils.parseLocalDate(String.valueOf(genericValidationValue.getValue()));
            }
            if (min != null) {
                return new Field.ValidationValue(min, genericValidationValue.getAction());
            }
        }
        return null;
    }

    @Override
    public Field.ValidationValue getMax(Record record) {
        Field.ValidationValue genericValidationValue = getGenericValidationValue(
                getLiveField().getField().getMaxValue(),
                record
        );
        if (genericValidationValue != null) {
            String dateString = String.valueOf(genericValidationValue.getValue());
            LocalDate max;
            if (Constants.Command.TODAY.equals(dateString)) {
                max = new LocalDate(new Date());
            } else {
                max = Utils.parseLocalDate(String.valueOf(genericValidationValue.getValue()));
            }
            if (max != null) {
                return new Field.ValidationValue(max, genericValidationValue.getAction());
            }
        }
        return null;
    }

    @Override
    protected boolean performValidation() {
        String missingValue = getLiveField().getField().getMissingValue();
        if (!StringUtils.isBlank(missingValue)) {
            List<String> missingValueTokens = Utils.tokenizeString(missingValue, ",", true);
            LocalDate value = (LocalDate) getMissingComparable();
            if (value != null) {
                for (String missingValueToken : missingValueTokens) {
                    Comparable missing;
                    if (missingValue != null && missingValue.length() == 4) {
                        missing = Utils.parseInteger(missingValue);
                        if (missing != null && Integer.valueOf(value.getYear()).compareTo((Integer) missing) == 0) {
                            return false;
                        }
                    } else {
                        missing = getMissingComparable(missingValueToken);
                        if (missing != null && value.compareTo((LocalDate) missing) == 0) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    @Override
    public View getPrincipalView() {
        return editText;
    }
}
