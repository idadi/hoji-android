package ke.co.hoji.android.listener;

import java.util.List;

import ke.co.hoji.core.data.dto.MainRecord;

/**
 * Defines a callback for reacting to a list of {@link MainRecord}s being imported.
 * <p/>
 * Created by gitahi on 18/11/16.
 */
public interface OnMainRecordsImportedListener {

    /**
     * Called when a list of {@link MainRecord}s is imported.
     *
     * @param mainRecords the {@link MainRecord}s imported.
     */
    void onMainRecordsImported(List<MainRecord> mainRecords);
}
