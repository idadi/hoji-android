package ke.co.hoji.android.widget;

import android.content.Context;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.FragmentActivity;

import org.joda.time.LocalTime;

import java.util.Date;

import ke.co.hoji.R;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;

/**
 * Created by gitahi on 14/11/14.
 */
public class TimeWidget extends AndroidWidget implements DateComponent {

    private EditText editText;
    private Date date;

    public TimeWidget(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.time_widget;
    }

    @Override
    public void loadViews() {
        editText = findViewById(R.id.timeEditText);
        editText.setKeyListener(null);
        editText.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                showDatePickerDialog();
            }
        });
    }

    public void setDate(Date date) {
        setDate(date, false);
    }

    public void setDate(Date date, boolean go) {
        this.date = date;
        if (this.date != null) {
            editText.setText(Utils.formatTime(date));
        } else {
            editText.setText(R.string.tap_to_enter);
        }
        focus();
        if (go) {
            raiseResponseChangedEvent();
        }
    }

    public Date getDate() {
        return date;
    }

    private void showDatePickerDialog() {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setDateComponent(this);
        timePickerFragment.show(((FragmentActivity) getCurrentContext()).getSupportFragmentManager(), "timePicker");
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null) {
            setDate((Date) response.getActualValue(), false);
        }
    }

    @Override
    public Object getValue() {
        return date;
    }

    @Override
    public void focus() {
        TextWidgetUtil.setFocus(editText, isEditable());
    }

    @Override
    public void wipe() {
        setDate(null, false);
    }

    @Override
    public boolean setDefaultValue(String defaultValue) {
        Date defaultDate = Utils.parseIsoDateTime(defaultValue);
        if (defaultDate != null) {
            setDate(defaultDate);
            return true;
        }
        return false;
    }

    @Override
    public Comparable getComparable() {
        if (date != null) {
            return new LocalTime(date);
        }
        return null;
    }

    @Override
    public Comparable getMissingComparable(String stringValue) {
        return Utils.parseLocalTime(stringValue);
    }

    @Override
    public Field.ValidationValue getMin(Record record) {
        Field.ValidationValue genericValidationValue = getGenericValidationValue(
                getLiveField().getField().getMinValue(),
                record
        );
        if (genericValidationValue != null) {
            String dateString = String.valueOf(genericValidationValue.getValue());
            LocalTime min;
            if (Constants.Command.NOW.equals(dateString)) {
                min = new LocalTime(date);
            } else {
                min = Utils.parseLocalTime(String.valueOf(genericValidationValue.getValue()));
            }
            if (min != null) {
                return new Field.ValidationValue(min, genericValidationValue.getAction());
            }
        }
        return null;
    }

    @Override
    public Field.ValidationValue getMax(Record record) {
        Field.ValidationValue genericValidationValue = getGenericValidationValue(
                getLiveField().getField().getMaxValue(),
                record
        );
        if (genericValidationValue != null) {
            String dateString = String.valueOf(genericValidationValue.getValue());
            LocalTime max;
            if (Constants.Command.NOW.equals(dateString)) {
                max = new LocalTime(date);
            } else {
                max = Utils.parseLocalTime(String.valueOf(genericValidationValue.getValue()));
            }
            if (max != null) {
                return new Field.ValidationValue(max, genericValidationValue.getAction());
            }
        }
        return null;
    }

    @Override
    public View getPrincipalView() {
        return editText;
    }
}
