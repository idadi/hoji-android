package ke.co.hoji.android.listener;

import ke.co.hoji.core.data.model.LiveField;

/**
 * Defines a callback for reacting to a {@link LiveField} selection event.
 * <p/>
 * Created by gitahi on 08/05/15.
 */
public interface OnLiveFieldSelectedListener {

    /**
     * Called when a {@link LiveField} is selected.
     *
     * @param liveField the {@link LiveField} selected.
     */
    void onLiveFieldSelected(LiveField liveField);
}
