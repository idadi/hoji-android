package ke.co.hoji.android.dao.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.model.Property;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by gitahi on 29/06/15.
 */
public class SqlitePropertyDao {

    private final SqliteSqlExecutor se;

    public SqlitePropertyDao(SqliteSqlExecutor se) {
        this.se = se;
    }

    public Property getProperty(String key) {
        Property property = null;
        String select = "SELECT key, value, survey_id FROM property WHERE key = ?";
        Map<String, Object> params = se.createParameterMap("key", key);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                property = new Property
                        (
                                cursor.getString(cursor.getColumnIndex("key"))
                        );
                property.setValue(cursor.getString(cursor.getColumnIndex("value")));
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqlitePropertyDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return property;
    }

    public Property getProperty(String key, int surveyId) {
        Property property = null;
        String select = "SELECT key, value, survey_id FROM property WHERE key = ? AND survey_id = ?";
        Map<String, Object> params = se.createParameterMap();
        params.put("key", key);
        params.put("survey_id", surveyId);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                property = new Property
                        (
                                cursor.getString(cursor.getColumnIndex("key"))
                        );
                property.setValue(cursor.getString(cursor.getColumnIndex("value")));
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqlitePropertyDao.class.getName()).log(Level.SEVERE,
                    null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return property;
    }

    public List<Property> getProperties(int surveyId) {
        List<Property> properties = new ArrayList<>();
        String select = "SELECT `key`, `value`, survey_id FROM property WHERE survey_id = ?";
        Map<String, Object> params = se.createParameterMap("survey_id", surveyId);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                Property property = new Property
                        (
                                cursor.getString(cursor.getColumnIndex("key"))
                        );
                property.setValue(cursor.getString(cursor.getColumnIndex("value")));
                properties.add(property);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqlitePropertyDao.class.getName()).log(Level.SEVERE,
                    null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return properties;
    }

    public Property saveProperty(Property property) throws DaoException {
        Integer surveyId = property.getSurvey() != null ? property.getSurvey().getId() : null;
        if (!propertyExists(property.getKey(), surveyId)) {
            insert(property, null);
        } else {
            update(property, null);
        }
        return property;
    }

    public List<Property> saveProperties(Collection<Property> properties) throws DaoException {
        List<Property> inserted = new ArrayList<>();
        se.beginTransaction();
        try {
            for (Property property : properties) {
                Integer surveyId = property.getSurvey() != null ? property.getSurvey().getId() : null;
                if (!propertyExists(property.getKey(), surveyId)) {
                    inserted.add(insert(property, surveyId));
                } else {
                    inserted.add(update(property, surveyId));
                }
            }
            se.setTransactionSuccessful();
        } catch (SQLiteException ex) {
            Logger.getLogger(SqlitePropertyDao.class.getName()).log(Level.SEVERE,
                null, ex);
            throw new DaoException(ex.getMessage());
        } finally {
            se.endTransaction();
        }
        return inserted;
    }

    public boolean deleteProperty(String key, Integer surveyId) {
        return false;
    }

    private Property insert(Property property, Integer surveyId) throws DaoException {
        Map<String, Object> params = se.createParameterMap();
        params.put("key", property.getKey());
        params.put("value", property.getValue());
        params.put("survey_id", surveyId);
        se.insert("property", params);
        return property;
    }

    private Property update(Property property, Integer surveyId) throws DaoException {
        Map<String, Object> whereParams = se.createParameterMap("key", property.getKey());
        Map<String, Object> params = se.createParameterMap();
        params.put("value", property.getValue());
        params.put("survey_id", surveyId);
        se.update("property", params, "key" + " = ?", whereParams);
        return property;
    }

    private boolean propertyExists(String key, Integer surveyId) {
        boolean exists = false;
        Map<String, Object> params = se.createParameterMap();
        params.put("key", key);
        String where = "WHERE key = ?";
        if (surveyId != null) {
            params.put("survey_id", surveyId);
            where += " AND survey_id = ?";
        }
        String select = "SELECT COUNT(`key`) FROM property " + where;
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                exists = cursor.getInt(0) == 1;
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqlitePropertyDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return exists;
    }
}
