package ke.co.hoji.android.activity;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.tabs.TabLayout;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.data.LaunchType;
import ke.co.hoji.android.data.Progress;
import ke.co.hoji.android.fragment.OutboxFragment;
import ke.co.hoji.android.fragment.SentFragment;
import ke.co.hoji.android.task.DownloadTask;
import ke.co.hoji.android.helper.DownloadingComponent;
import ke.co.hoji.android.manager.FeedbackManager;
import ke.co.hoji.android.helper.HomePagerAdapter;
import ke.co.hoji.android.listener.OnFormSelectedListener;
import ke.co.hoji.android.listener.OnRecordSelectedListener;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.MainRecord;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.core.service.model.RecordService;
import ke.co.hoji.android.HojiSQLiteAssetHelper;

/**
 * Created by gitahi on 01/09/15.
 */
public class HomeActivity
        extends
        AppCompatActivity
        implements
        OnFormSelectedListener,
        OnRecordSelectedListener,
        DownloadingComponent {

    private List<Form> forms;

    private ProgressDialog progressDialog;

    private FeedbackManager feedbackManager;

    private MenuItem feedbackMenuItem;

    private TabLayout tabLayout;

    private BadgeDrawable draftsBadge;
    private BadgeDrawable completeBadge;
    private BadgeDrawable uploadedBadge;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ViewPager viewPager = findViewById(R.id.viewpager);

        HomePagerAdapter adapter = new HomePagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(adapter);

        tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        draftsBadge = tabLayout.getTabAt(1).getOrCreateBadge();
        completeBadge = tabLayout.getTabAt(2).getOrCreateBadge();
        uploadedBadge = tabLayout.getTabAt(3).getOrCreateBadge();
        draftsBadge.setBackgroundColor(getResources().getColor(R.color.red));
        completeBadge.setBackgroundColor(getResources().getColor(R.color.white));
        uploadedBadge.setBackgroundColor(getResources().getColor(R.color.accent));
        draftsBadge.setBadgeTextColor(getResources().getColor(R.color.white));
        completeBadge.setBadgeTextColor(getResources().getColor(R.color.primary));
        uploadedBadge.setBadgeTextColor(getResources().getColor(R.color.white));


        ModelServiceManager modelServiceManager = ((HojiContext) getApplication()).getModelServiceManager();
        feedbackManager = new FeedbackManager(
                modelServiceManager.getPropertyService(),
                modelServiceManager.getRecordService()
        );
        RecordService recordService = modelServiceManager.getRecordService();
        recordService.portDataIfNecessary();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (forms == null) {
            forms = getConfiguredForms();
        }
        progressDialog = new ProgressDialog(this);
        AndroidHojiContext hojiContext = (AndroidHojiContext) getApplication();
        PropertyService propertyService = hojiContext.getModelServiceManager().getPropertyService();
        hojiContext.setUpAcraUserAndProjectData();
        IntentFilter intentFilter = new IntentFilter(Constants.BROADCAST_ACTION);
        UploadBroadcastReceiver uploadProgressReceiver = new UploadBroadcastReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(uploadProgressReceiver, intentFilter);
        showBadges();

        if (HojiSQLiteAssetHelper.isFetchData()) {
            propertyService.setProperty(new Property(Constants.FIRE_UPDATE, Boolean.TRUE.toString()));
        }

        String propertyValue = propertyService.getValue(Constants.FIRE_UPDATE);
        if (propertyValue != null && Boolean.valueOf(propertyValue)) {
            update((AndroidHojiContext) getApplication());
        }
    }

    @Override
    protected void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_activity_actions, menu);
        feedbackMenuItem = menu.findItem(R.id.feedbackButton);
        if (!feedbackManager.isFeedbackEligible(true)) {
            feedbackMenuItem.setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        AndroidHojiContext hojiContext = (AndroidHojiContext) getApplication();
        switch (item.getItemId()) {
            case R.id.searchButton:
                intent = new Intent(HomeActivity.this, SearchActivity.class);
                startActivity(intent);
                return true;
            case R.id.settingsButton:
                intent = new Intent(HomeActivity.this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.updateButton:
                update(hojiContext);
                return true;
            case R.id.importButton:
                intent = new Intent(HomeActivity.this, ImportActivity.class);
                startActivity(intent);
                return true;
            case R.id.aboutButton:
                intent = new Intent(HomeActivity.this, AboutActivity.class);
                startActivity(intent);
                return true;
            case R.id.feedbackButton:
                feedbackManager.requestFeedback(this, hojiContext.getNameOfUser());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFormSelected(Form form) {
        if (form.getFields().isEmpty()) {
            Toast toast = Toast.makeText(this,
                    getResources().getText(R.string.form_has_no_fields), Toast.LENGTH_SHORT);
            toast.show();
            return;
        }
        Intent intent = new Intent(HomeActivity.this, DataEntryActivity.class);
        intent.putExtra(Constants.LAUNCH_TYPE, LaunchType.NEW);
        intent.putExtra(Constants.DEFLATED_FORM, ((AndroidHojiContext) getApplication()).deflateForm(form));
        startActivity(intent);
    }

    @Override
    public void onRecordSelected(Record record) {
        Intent intent = new Intent(HomeActivity.this, LiveFieldActivity.class);
        intent.putExtra(Constants.DEFLATED_RECORD, ((AndroidHojiContext) getApplication()).deflateRecord(record));
        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        try {
            super.onSaveInstanceState(outState);
        } catch (Exception ex) {
            //Do nothing. This covers the Samsung Galaxy Tab P5100 running Android 4.0.3 which
            //throws an NPE here.
        }
    }

    @Override
    public void downloadSucceeded() {
        markUpdateAsProcessed();
        AndroidHojiContext hojiContext = (AndroidHojiContext) getApplication();
        hojiContext.reset();
        Intent intent = getIntent();
        finish();
        startActivity(intent);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText
                        (
                                HomeActivity.this, getResources().getString(R.string.update_completed),
                                Toast.LENGTH_SHORT
                        );
                toast.show();
            }
        });
    }

    @Override
    public void downloadFailed(final String message) {
        downloadFailed(message, false);
    }

    @Override
    public void downloadFailed(final String message, boolean nothingNewFound) {
        if (nothingNewFound) {
            markUpdateAsProcessed();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText
                        (
                                HomeActivity.this, message,
                                Toast.LENGTH_SHORT
                        );
                toast.show();
            }
        });
    }

    @Override
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public boolean isAlive() {
        return !this.isFinishing();
    }

    public void hideFeedbackMenuItem() {
        if (feedbackMenuItem != null) {
            feedbackMenuItem.setVisible(false);
        }
    }

    public void showFeedbackMenuItem() {
        if (feedbackMenuItem != null) {
            feedbackMenuItem.setVisible(true);
        }
    }

    public List<Form> getForms() {
        return forms;
    }

    public void showBadges() {
        ModelServiceManager modelServiceManager = ((HojiContext) getApplication()).getModelServiceManager();
        RecordService recordService = modelServiceManager.getRecordService();
        boolean testMode = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(SettingsActivity.TEST_MODE, Boolean.FALSE);
        RecordSearch recordSearch = new RecordSearch();
        recordSearch.setTestMode(testMode);
        {
            recordSearch.setStage(MainRecord.Stage.DRAFT);
            int itemCount = recordService.count(recordSearch);
            if (itemCount > 0) {
                draftsBadge.setNumber(itemCount);
                draftsBadge.setVisible(true);
            } else {
                draftsBadge.setVisible(false);
            }
        }
        {
            recordSearch.setStage(MainRecord.Stage.COMPLETE);
            int itemCount = recordService.count(recordSearch);
            if (itemCount > 0) {
                completeBadge.setNumber(itemCount);
                completeBadge.setVisible(true);
            } else {
                completeBadge.setVisible(false);
            }
        }
        {
            recordSearch.setStage(MainRecord.Stage.UPLOADED);
            int itemCount = recordService.count(recordSearch);
            if (itemCount > 0) {
                uploadedBadge.setNumber(itemCount);
                uploadedBadge.setVisible(true);
            } else {
                uploadedBadge.setVisible(false);
            }
        }
    }

    private List<Form> getConfiguredForms() {
        Survey survey = ((HojiContext) getApplication()).getSurvey();
        if (survey != null) {
            forms = survey.getForms();
        } else {
            forms = new ArrayList<>();
        }
        return forms;
    }

    private String getFragmentTag(int pos) {
        return "android:switcher:" + R.id.viewpager + ":" + pos;
    }

    private void update(AndroidHojiContext hojiContext) {
        Survey s = hojiContext.getSurvey();
        ke.co.hoji.core.data.dto.Survey survey = new ke.co.hoji.core.data.dto.Survey();
        survey.setId(s.getId());
        survey.setCode(s.getCode());
        survey.setName(s.getName());
        DownloadTask downloadTask = new DownloadTask
                (
                        HomeActivity.this,
                        survey,
                        true,
                        hojiContext.getHttpService(),
                        hojiContext.getModelServiceManager().getPropertyService(),
                        hojiContext.getDtoServiceManager().getSurveyService(),
                        hojiContext.getDtoServiceManager().getFormService(),
                        hojiContext.getDtoServiceManager().getFieldService(),
                        hojiContext.getDtoServiceManager().getReferenceService(),
                        hojiContext.getDtoServiceManager().getLanguageService(),
                        hojiContext.getDtoServiceManager().getTranslationService(),
                        progressDialog
                );
        downloadTask.execute();
        hojiContext.clearContext();
    }

    private void markUpdateAsProcessed() {
        AndroidHojiContext hojiContext = (AndroidHojiContext) getApplication();
        hojiContext.getModelServiceManager().getPropertyService().setProperty(
                new Property(ke.co.hoji.android.data.Constants.FIRE_UPDATE, Boolean.FALSE.toString())
        );
        HojiSQLiteAssetHelper.setFetchData(false);
        NotificationManager notificationManager
                = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(Constants.PROJECT_UPDATE_NOTIFICATION_ID);
    }

    private class UploadBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean showUploadProgress = intent.getBooleanExtra(Constants.SHOW_UPLOAD_PROGRESS, false);
            OutboxFragment outboxFragment = (OutboxFragment) getSupportFragmentManager()
                    .findFragmentByTag(getFragmentTag(2));
            SentFragment sentFragments = (SentFragment) getSupportFragmentManager()
                    .findFragmentByTag(getFragmentTag(3));
            Progress progress = (Progress) intent.getSerializableExtra(Constants.EXTENDED_DATA_STATUS);
            if (outboxFragment != null && outboxFragment.isVisible() && showUploadProgress) {
                outboxFragment.reportUploadProgress(progress);
            }
            if (progress.getStatus() == Progress.Status.DONE
                    || progress.getStatus() == Progress.Status.ABORTED) {
                if (outboxFragment != null) {
                    outboxFragment.loadRecords(0L);
                }
                if (sentFragments != null) {
                    sentFragments.loadRecords(0L);
                }
            }
            showBadges();
        }
    }
}