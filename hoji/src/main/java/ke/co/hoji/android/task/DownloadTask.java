package ke.co.hoji.android.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ke.co.hoji.BuildConfig;
import ke.co.hoji.R;
import ke.co.hoji.android.helper.DownloadingComponent;
import ke.co.hoji.android.service.HttpService;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.dto.Field;
import ke.co.hoji.core.data.dto.Form;
import ke.co.hoji.core.data.dto.Language;
import ke.co.hoji.core.data.dto.Reference;
import ke.co.hoji.core.data.dto.Survey;
import ke.co.hoji.core.data.dto.Translation;
import ke.co.hoji.core.data.http.FieldsRequest;
import ke.co.hoji.core.data.http.FieldsResponse;
import ke.co.hoji.core.data.http.FormsRequest;
import ke.co.hoji.core.data.http.FormsResponse;
import ke.co.hoji.core.data.http.LanguagesRequest;
import ke.co.hoji.core.data.http.LanguagesResponse;
import ke.co.hoji.core.data.http.PropertiesRequest;
import ke.co.hoji.core.data.http.PropertiesResponse;
import ke.co.hoji.core.data.http.ReferencesRequest;
import ke.co.hoji.core.data.http.ReferencesResponse;
import ke.co.hoji.core.data.http.SurveyRequest;
import ke.co.hoji.core.data.http.SurveyResponse;
import ke.co.hoji.core.data.http.TranslationsRequest;
import ke.co.hoji.core.data.http.TranslationsResponse;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.service.dto.FieldService;
import ke.co.hoji.core.service.dto.FormService;
import ke.co.hoji.core.service.dto.LanguageService;
import ke.co.hoji.core.service.dto.ReferenceService;
import ke.co.hoji.core.service.dto.SurveyService;
import ke.co.hoji.core.service.model.HttpException;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.core.service.dto.TranslationService;

/**
 * Created by gitahi on 11/01/16.
 */
public class DownloadTask extends AsyncTask<Void, String, Void> {

    private final DownloadingComponent downloadingComponent;
    private final Survey survey;
    private final boolean updating;
    private final HttpService httpService;
    private final PropertyService propertyService;
    private final SurveyService surveyService;
    private final FormService formService;
    private final FieldService fieldService;
    private final ReferenceService referenceService;
    private final LanguageService languageService;
    private final TranslationService translationService;
    private final ProgressDialog progressDialog;

    private final Map<String, String> cache = new HashMap<>();

    public DownloadTask
            (
                    DownloadingComponent downloadingComponent,
                    Survey survey,
                    boolean updating,
                    HttpService httpService,
                    PropertyService propertyService,
                    SurveyService surveyService,
                    FormService formService,
                    FieldService fieldService,
                    ReferenceService referenceService,
                    LanguageService languageService,
                    TranslationService translationService,
                    ProgressDialog progressDialog
            ) {
        this.downloadingComponent = downloadingComponent;
        this.survey = survey;
        this.updating = updating;
        this.httpService = httpService;
        this.propertyService = propertyService;
        this.surveyService = surveyService;
        this.formService = formService;
        this.fieldService = fieldService;
        this.referenceService = referenceService;
        this.languageService = languageService;
        this.translationService = translationService;
        this.progressDialog = progressDialog;
    }

    @Override
    protected void onPreExecute() {
        if (progressDialog != null) {
            progressDialog.setMessage(downloadingComponent.getResources().getString(R.string.please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        Survey survey = downloadSurvey();
        if (survey == null) {
            return null;
        }
        List<Form> forms = downloadForms(survey);
        if (forms == null || forms.isEmpty()) {
            return null;
        } else {
            boolean success = downloadFields(forms);
            if (!success) {
                return null;
            }
            success = downloadReferences(survey);
            if (!success) {
                return null;
            }
            success = downloadProperties(survey);
            if (!success) {
                return null;
            }
            List<Language> languages = downloadLanguages(survey);
            if (languages != null && !languages.isEmpty()) {
                success = downloadTranslations(languages);
                if (!success) {
                    return null;
                }
            }
        }
        downloadingComponent.downloadSucceeded();
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        if (progressDialog != null && progressDialog.isShowing()) {
            if (values != null) {
                String message = values[0];
                if (message != null) {
                    progressDialog.setMessage(message);
                }
            }
        }
    }

    @Override
    protected void onPostExecute(Void result) {
        if (!downloadingComponent.isAlive()) {
            return;
        }
        downloadingComponent.dismissProgressDialog();
    }

    private Survey downloadSurvey() {
        Survey ret = null;
        if (!updating) {
            publishProgress(downloadingComponent.getResources()
                    .getString(R.string.downloading_survey, survey.getName()));
        } else {
            publishProgress(downloadingComponent.getResources()
                    .getString(R.string.checking_for_updates, survey.getName()));
        }
        SurveyRequest request = new SurveyRequest(survey.getId());
        request.setAppVersionCode(BuildConfig.VERSION_CODE);
        try {
            SurveyResponse response = httpService.sendAsJson
                    (
                            ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.SURVEY_DOWNLOAD,
                            getServerUsername(),
                            getServerPassword(),
                            request,
                            SurveyResponse.class
                    );
            if (response.getCode() == SurveyResponse.Code.SURVEY_NOT_FOUND) {
                downloadingComponent.downloadFailed(downloadingComponent.getResources()
                        .getString(R.string.survey_not_found, survey.getName()));
            } else {
                switch (response.getCode()) {
                    case SurveyResponse.Code.SURVEY_NOT_ENABLED:
                        downloadingComponent.downloadFailed(downloadingComponent.getResources()
                                .getString(R.string.survey_not_enabled, survey.getName()));
                        break;
                    case SurveyResponse.Code.SURVEY_NOT_PUBLISHED:
                        downloadingComponent.downloadFailed(downloadingComponent.getResources()
                                .getString(R.string.survey_not_published, survey.getName()));
                        break;
                    case SurveyResponse.Code.APP_VERSION_TOO_OLD:
                        downloadingComponent.downloadFailed(downloadingComponent.getResources()
                                .getString(R.string.app_version_too_old));
                        break;
                    case SurveyResponse.Code.SURVEY_FOUND:
                        ret = response.getSurvey();
                        publishProgress(downloadingComponent.getResources()
                                .getString(R.string.saving_survey));
                        surveyService.saveSurvey(ret);
                        propertyService.setProperties(
                                new Property(Constants.Server.OWNER_ID, Integer.toString(response.getOwnerId())),
                                new Property(Constants.Server.OWNER_NAME, response.getOwnerName()),
                                new Property(Constants.Server.OWNER_VERIFIED, Boolean.toString(response.isOwnerVerified()))
                        );
                        publishProgress(downloadingComponent.getResources()
                                .getString(R.string.survey_downloaded, survey.getName()));
                        break;
                }
            }
        } catch (HttpException ex) {
            reportFailure(ex);
        }
        return ret;
    }

    private List<Form> downloadForms(Survey survey) {
        List<Form> forms = new ArrayList<>();
        publishProgress(downloadingComponent.getResources()
                .getString(R.string.downloading_forms));
        FormsRequest request = new FormsRequest(survey.getId());
        try {
            FormsResponse response = httpService.sendAsJson
                    (
                            ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.FORMS_DOWNLOAD,
                            getServerUsername(),
                            getServerPassword(),
                            request,
                            FormsResponse.class
                    );
            forms = response.getForms();
            if (forms != null && !forms.isEmpty()) {
                int updated = 0;
                int added = 0;
                if (updating) {
                    for (Form form : forms) {
                        Form existing = formService.getFormById(form.getId());
                        if (existing != null) {
                            if (form.getMinorVersion() > existing.getMinorVersion()
                                    || form.getMajorVersion() > existing.getMajorVersion()) {
                                updated++;
                            }
                        } else {
                            added++;
                        }
                    }
                }
                if (!updating) {
                    publishProgress(downloadingComponent.getResources()
                            .getString(R.string.saving_forms));
                    formService.saveForms(forms);
                    publishProgress(downloadingComponent.getResources()
                            .getString(R.string.forms_downloaded, forms.size()));
                } else {
                    int total = updated + added;
                    if (total > 0) {
                        publishProgress(downloadingComponent.getResources()
                                .getString(R.string.saving_forms));
                        formService.saveForms(forms);
                        publishProgress(downloadingComponent.getResources()
                                .getQuantityString(R.plurals.forms_updated, total, total));
                    } else {
                        downloadingComponent.downloadFailed(downloadingComponent.getResources()
                                .getString(R.string.no_updates_found), true);
                        forms.clear();
                    }
                }
            } else {
                downloadingComponent.downloadFailed(downloadingComponent.getResources()
                        .getString(R.string.forms_not_found, survey.getName()));
            }
        } catch (HttpException ex) {
            reportFailure(ex);
        }
        return forms;
    }

    private boolean downloadFields(List<Form> forms) {
        List<Field> allFields = new ArrayList<>();
        if (forms != null && !forms.isEmpty()) {
            for (Form form : forms) {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.downloading_fields, form.getName()));
                FieldsRequest request = new FieldsRequest(form.getId());
                try {
                    FieldsResponse response = httpService.sendAsJson
                            (
                                    ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.FIELDS_DOWNLOAD,
                                    getServerUsername(),
                                    getServerPassword(),
                                    request,
                                    FieldsResponse.class
                            );
                    List<Field> fields = response.getFields();
                    if (fields != null && !fields.isEmpty()) {
                        allFields.addAll(fields);
                    }
                } catch (HttpException ex) {
                    if (ex.getCode() == HttpException.Code.METHOD_NOT_ALLOWED) {
                        reportFailure(ex);
                    } else {
                        reportFailure(ex);
                    }
                    return false;
                }
            }
            if (!allFields.isEmpty()) {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.saving_fields));
                fieldService.saveFields(allFields);
                if (!updating) {
                    publishProgress(downloadingComponent.getResources()
                            .getString(R.string.fields_downloaded));
                } else {
                    publishProgress(downloadingComponent.getResources()
                            .getString(R.string.fields_updated));
                }
            }
        }
        return true;
    }

    private boolean downloadReferences(Survey survey) {
        List<Reference> references;
        publishProgress(downloadingComponent.getResources()
                .getString(R.string.downloading_references, survey.getName()));
        Integer surveyId = survey.getId();
        ReferencesRequest request = new ReferencesRequest(surveyId);
        try {
            ReferencesResponse response = httpService.sendAsJson
                    (
                            ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.REFERENCES_DOWNLOAD,
                            getServerUsername(),
                            getServerPassword(),
                            request,
                            ReferencesResponse.class
                    );
            references = response.getReferences();
            if (references != null && !references.isEmpty()) {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.saving_references));
                referenceService.saveReferences(references);
            }
            if (!updating) {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.references_downloaded));
            } else {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.references_updated));
            }
        } catch (HttpException ex) {
            reportFailure(ex);
            return false;
        }
        return true;
    }

    private boolean downloadProperties(Survey survey) {
        List<Property> properties;
        publishProgress(downloadingComponent.getResources()
                .getString(R.string.downloading_properties, survey.getName()));
        Integer surveyId = survey.getId();
        PropertiesRequest request = new PropertiesRequest(surveyId);
        try {
            PropertiesResponse response = httpService.sendAsJson
                    (
                            ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.PROPERTIES_DOWNLOAD,
                            getServerUsername(),
                            getServerPassword(),
                            request,
                            PropertiesResponse.class
                    );
            properties = response.getProperties();
            if (properties != null && !properties.isEmpty()) {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.saving_properties));
                //TODO: Remove this hack, which sets the survey on each property so that server-side
                //TODO: properties can be saved with a survey_id and hence cascade deletes.
                ke.co.hoji.core.data.model.Survey s = new ke.co.hoji.core.data.model.Survey();
                s.setId(surveyId);
                for (Property property : properties) {
                    property.setSurvey(s);
                }
                propertyService.setProperties(properties);
            }
            if (!updating) {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.properties_downloaded));
            } else {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.properties_updated));
            }
        } catch (HttpException ex) {
            reportFailure(ex);
            return false;
        }
        return true;
    }

    private List<Language> downloadLanguages(Survey survey) {
        List<Language> languages = new ArrayList<>();
        publishProgress(downloadingComponent.getResources()
                .getString(R.string.downloading_languages, survey.getName()));
        Integer surveyId = survey.getId();
        LanguagesRequest request = new LanguagesRequest(surveyId);
        try {
            LanguagesResponse response = httpService.sendAsJson
                    (
                            ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.LANGUAGES_DOWNLOAD,
                            getServerUsername(),
                            getServerPassword(),
                            request,
                            LanguagesResponse.class
                    );
            languages = response.getLanguages();
            if (languages != null && !languages.isEmpty()) {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.saving_languages));
                languageService.saveLanguages(languages);
            }
            if (!updating) {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.languages_downloaded));
            } else {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.languages_updated));
            }
        } catch (HttpException ex) {
            reportFailure(ex);
            return languages;
        }
        return languages;
    }

    private boolean downloadTranslations(List<Language> languages) {
        List<Translation> translations;
        publishProgress(downloadingComponent.getResources()
                .getString(R.string.downloading_translations, survey.getName()));
        TranslationsRequest request = new TranslationsRequest(languages);
        try {
            TranslationsResponse response = httpService.sendAsJson
                    (
                            ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.TRANSLATIONS_DOWNLOAD,
                            getServerUsername(),
                            getServerPassword(),
                            request,
                            TranslationsResponse.class
                    );
            translations = response.getTranslations();
            if (translations != null && !translations.isEmpty()) {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.saving_translations));
                translationService.saveTranslations(translations);
            }
            if (!updating) {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.translations_downloaded));
            } else {
                publishProgress(downloadingComponent.getResources()
                        .getString(R.string.translations_updated));
            }
        } catch (HttpException ex) {
            reportFailure(ex);
            return false;
        }
        return true;
    }

    /**
     * @return the server username.
     */
    private String getServerUsername() {
        return getValue(Constants.Server.USERNAME);
    }

    /**
     * @return the server password.
     */
    private String getServerPassword() {
        return getValue(Constants.Server.PASSWORD);
    }

    private String getValue(String key) {
        String value = cache.get(key);
        if (value == null) {
            value = propertyService.getValue(key);
            cache.put(key, value);
        }
        return value;
    }

    private void reportFailure(HttpException ex) {
        if (!updating) {
            surveyService.deleteSurvey(survey.getId());
        }
        downloadingComponent.downloadFailed(httpService.interpretHttpExceptionCode(ex));
    }
}
