package ke.co.hoji.android.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.android.helper.UiUtils;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.service.model.TranslationService;

/**
 * Created by gitahi on 12/05/15.
 */
public class LiveFieldArrayAdapter extends ArrayAdapter<LiveField> {

    private final Record record;

    public LiveFieldArrayAdapter(Context context, Record record) {
        super(context, 0, record.getLiveFields());
        this.record = record;
    }

    @Override
    public View getView(int position, View liveFieldView, ViewGroup parent) {
        AndroidHojiContext hojiContext = (AndroidHojiContext) getContext().getApplicationContext();
        TranslationService translationService = hojiContext.getModelServiceManager().getTranslationService();
        Integer languageId = hojiContext.getLanguageId();
        LiveField liveField = getItem(position);
        if (liveFieldView == null) {
            liveFieldView = LayoutInflater.from(getContext()).inflate(R.layout.live_field_row, parent, false);
        }
        TextView fieldDescriptionTextView = liveFieldView.findViewById(R.id.fieldDescriptionTextView);
        TextView responseTextView = liveFieldView.findViewById(R.id.responseTextView);
        View emptyResponseUnderlineView = liveFieldView.findViewById(R.id.emptyResponseUnderlineView);
        if (!liveField.isAnswered(true)) {
            emptyResponseUnderlineView.setVisibility(View.VISIBLE);
        } else {
            emptyResponseUnderlineView.setVisibility(View.GONE);
        }
        String nameOfUser = hojiContext.getNameOfUser();
        String fieldText = translationService.translate(
                liveField.getField(),
                languageId,
                Field.TranslatableComponent.DESCRIPTION,
                liveField.getField().getDescription()
        );
        if (liveField.getField().getType().getDataType() == FieldType.DataType.NULL) {
            responseTextView.setText(UiUtils.getText(translationService.translate(
                    liveField.getField(),
                    languageId,
                    Field.TranslatableComponent.INSTRUCTIONS,
                    liveField.getField().getInstructions()
            ), record, nameOfUser));
        } else {
            if (showFieldNumbersInViewMode(fieldDescriptionTextView.getContext())) {
                fieldText = "<b>" + liveField.getField().getName() + ". </b>" + fieldText;
            }
            responseTextView.setText(liveField.getResponse().displayString());
        }
        fieldDescriptionTextView.setText(UiUtils.getText(fieldText, record, nameOfUser));
        UiUtils.format(liveField.getField(), fieldDescriptionTextView);
        return liveFieldView;
    }

    private boolean showFieldNumbersInViewMode(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsActivity.SHOW_FIELD_NUMBERS_IN_VIEW_MODE, Boolean.TRUE);
    }
}
