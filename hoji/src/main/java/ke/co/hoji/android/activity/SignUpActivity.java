package ke.co.hoji.android.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ke.co.hoji.BuildConfig;
import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.service.HttpService;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.http.SignUpRequest;
import ke.co.hoji.core.data.http.SignUpResponse;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.service.model.HttpException;

public class SignUpActivity extends AppCompatActivity {

    private EditText nameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private Button signUpButton;
    private ProgressDialog progressDialog;

    private AndroidHojiContext androidHojiContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_activity);
        nameEditText = findViewById(R.id.nameEditText);
        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        signUpButton = findViewById(R.id.signUpButton);
        TextView signInTextView = findViewById(R.id.signInTextView);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });
        signInTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setResult(InitialActivity.RESULT_SWITCH, null);
                finish();
            }
        });
        androidHojiContext = (AndroidHojiContext) getApplication();

        TextView termsAgreementText = findViewById(R.id.termsAgreementText);
        termsAgreementText.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    protected void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        setResult(InitialActivity.RESULT_CANCEL, null);
        finish();
    }

    private void signUp() {
        if (!validate()) {
            return;
        }
        signUpButton.setEnabled(false);
        progressDialog = new ProgressDialog(SignUpActivity.this);
        SignUpTask signUpTask = new SignUpTask
                (
                        androidHojiContext,
                        nameEditText.getText().toString(),
                        emailEditText.getText().toString(),
                        passwordEditText.getText().toString(),
                        progressDialog
                );
        signUpTask.execute();
    }


    private boolean validate() {
        boolean valid = true;
        String name = nameEditText.getText().toString();
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        if (name.isEmpty()) {
            nameEditText.setError(getResources().getString(R.string.cannot_be_empty));
            nameEditText.requestFocus();
            valid = false;
        } else {
            nameEditText.setError(null);
        }
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailEditText.setError(getResources().getString(R.string.invalid_email));
            emailEditText.requestFocus();
            valid = false;
        } else {
            emailEditText.setError(null);
        }
        if (password.isEmpty() || password.length() < Constants.Server.MIN_PASSWORD_LENGTH) {
            passwordEditText.setError(getResources().getString(R.string.must_be_at_least_n_characters,
                    Constants.Server.MIN_PASSWORD_LENGTH));
            passwordEditText.requestFocus();
            valid = false;
        } else {
            passwordEditText.setError(null);
        }
        return valid;
    }

    private void signUpSucceeded() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                signUpButton.setEnabled(true);
                setResult(InitialActivity.RESULT_SUCCESS, null);
                finish();
            }
        });
    }

    private void signUpFailed(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText
                        (
                                getBaseContext(),
                                message,
                                Toast.LENGTH_LONG
                        ).show();
                passwordEditText.requestFocus();
                signUpButton.setEnabled(true);
            }
        });
    }

    private void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private class SignUpTask extends AsyncTask<Void, Void, Void> {

        private final AndroidHojiContext androidHojiContext;
        private final String name;
        private final String username;
        private final String password;
        private final ProgressDialog progressDialog;

        private SignUpTask
                (
                        AndroidHojiContext androidHojiContext,
                        String name,
                        String username,
                        String password,
                        ProgressDialog progressDialog
                ) {
            this.androidHojiContext = androidHojiContext;
            this.name = name;
            this.username = username;
            this.password = password;
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            if (progressDialog != null) {
                progressDialog.setMessage(getResources().getText(R.string.signing_up));
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpService httpService = androidHojiContext.getHttpService();
            SignUpRequest request = new SignUpRequest(name, username, password, BuildConfig.VERSION_CODE);
            SignUpResponse response;
            try {
                response = httpService.sendAsJson
                        (
                                ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.SIGN_UP,
                                null,
                                null,
                                request,
                                SignUpResponse.class
                        );
                if (response.getStatusCode() == SignUpResponse.StatusCode.SUCCEEDED) {
                    androidHojiContext.getModelServiceManager().getPropertyService().setProperties
                            (
                                    new Property(Constants.Server.USER_ID, response.getUserId().toString()),
                                    new Property(Constants.Server.USER_CODE, response.getUserCode()),
                                    new Property(Constants.Server.TENANT_CODE, response.getUserCode()),
                                    new Property(Constants.Server.TENANT_NAME, name),
                                    new Property(Constants.Server.NAME, name),
                                    new Property(Constants.Server.USERNAME, username),
                                    new Property(Constants.Server.PASSWORD, password)
                            );
                    signUpSucceeded();
                } else if (response.getStatusCode() == SignUpResponse.StatusCode.EXISTS) {
                    signUpFailed(getResources().getString(R.string.account_exists, username));
                } else if (response.getStatusCode() == SignUpResponse.StatusCode.FAILED) {
                    signUpFailed(getResources().getString(R.string.sign_up_failed));
                }
            } catch (HttpException ex) {
                signUpFailed(httpService.interpretHttpExceptionCode(ex));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (SignUpActivity.this.isFinishing()) {
                return;
            }
            dismissProgressDialog();
        }
    }
}
