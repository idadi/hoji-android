package ke.co.hoji.android.helper;

import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import ke.co.hoji.core.StringInterpolator;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.MainRecord;
import ke.co.hoji.core.data.model.Record;

/**
 * Created by gitahi on 28/05/15.
 */
public final class UiUtils {

    private static final StringInterpolator STRING_INTERPOLATOR = new StringInterpolator();

    public static void clear(TextView... textViews) {
        for (TextView textView : textViews) {
            textView.setText("");
        }
    }

    public static void hideOrShow(TextView... textViews) {
        for (TextView textView : textViews) {
            if (textView.length() == 0) {
                textView.setVisibility(View.GONE);
            } else {
                textView.setVisibility(View.VISIBLE);
            }
        }
    }

    public static void format(Field field, TextView... textViews) {
        for (TextView textView : textViews) {
            if (field.getType().getDataType() == FieldType.DataType.NULL) {
                textView.setGravity(Gravity.CENTER);
            } else {
                textView.setGravity(Gravity.LEFT);
            }
        }
    }

    public static void scrollToTop(TextView... textViews) {
        for (TextView textView : textViews) {
            textView.scrollTo(0, 0);
        }
    }

    public static Spanned getText(String input, Record record, String nameOfUser) {
        String output = "";
        if (input != null) {
            output = input.replace(Constants.Command.USERNAME, nameOfUser);
            output = STRING_INTERPOLATOR.interpolate(
                    output,
                    record,
                    StringInterpolator.InterpolationType.LABEL_VALUE
            );
        }
        return Html.fromHtml(output);
    }

    public static String getLocation(MainRecord mainRecord) {
        String location = mainRecord.getStartAddress();
        if ((location == null || location.isEmpty()) && mainRecord.getStage() != MainRecord.Stage.DRAFT) {
            location = mainRecord.getEndAddress();
        }
        if ((location == null || location.isEmpty())
                && mainRecord.getStartLatitude() != null
                && mainRecord.getStartLatitude() != 0.0
                && mainRecord.getStartLongitude() != null
                && mainRecord.getStartLongitude() != 0.0
        ) {
            location = Utils.roundDouble(mainRecord.getStartLatitude()) + ", "
                    + Utils.roundDouble(mainRecord.getStartLongitude());
        }
        if (((location == null || location.isEmpty()) && mainRecord.getStage() != MainRecord.Stage.DRAFT)
                && mainRecord.getEndLatitude() != null
                && mainRecord.getEndLatitude() != 0.0
                && mainRecord.getEndLongitude() != null
                && mainRecord.getEndLatitude() != 0.0) {
            location = Utils.roundDouble(mainRecord.getEndLatitude())
                    + ", " + Utils.roundDouble(mainRecord.getEndLongitude());
        }
        return location != null ? location : "";
    }
}
