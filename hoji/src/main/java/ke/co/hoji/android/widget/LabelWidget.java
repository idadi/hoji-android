package ke.co.hoji.android.widget;

import android.content.Context;
import android.view.View;

import ke.co.hoji.R;

/**
 * Created by gitahi on 27/05/15.
 */
public class LabelWidget extends AndroidWidget {

    public LabelWidget(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.label_widget;
    }

    public boolean isResettable() {
        return false;
    }

    @Override
    public Object getValue() {
        return null;
    }

    @Override
    public View getPrincipalView() {
        return null;
    }
}
