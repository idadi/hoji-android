package ke.co.hoji.android.helper;

/**
 * Receives an image once it is takenby the @{@link ImageCapturer}.
 * <p>
 * Created by gitahi on 12/06/15.
 */
public interface ImageReceiver {

    /**
     * Receive an image.
     *
     * @param hojiBitmap the image.
     */
    void receiveImage(HojiBitmap hojiBitmap);
}
