package ke.co.hoji.android.adapter;

import android.content.Context;
import android.preference.PreferenceManager;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.LiteRecord;

/**
 * Created by gitahi on 17/11/16.
 */
public class LiteRecordArrayAdapter extends ArrayAdapter<LiteRecord> {

    public LiteRecordArrayAdapter(Context context, List<LiteRecord> records) {
        super(context, 0, records);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LiteRecord liteRecord = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.lite_record_row, parent, false);
        }
        TextView recordCaption = convertView.findViewById(R.id.recordCaptionTextView);
        TextView searchTerms = convertView.findViewById(R.id.searchTermsTextView);
        TextView recordDate = convertView.findViewById(R.id.recordDateTextView);
        ImageView importIcon = convertView.findViewById(R.id.importIconImageView);

        recordCaption.setText(liteRecord.getCaption() != null ? liteRecord.getCaption() : liteRecord.getUuid());
        searchTerms.setText(prepareSearchTerms(liteRecord));
        int dateFormat = Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(getContext())
                .getString(SettingsActivity.DATE_TIME_FORMAT, "1"));
        Date dateCreated = new Date(liteRecord.getDateCreated());
        String dateCreatedString;
        if (dateFormat == 2) {
            dateCreatedString = Utils.formatDate(dateCreated);
        } else if (dateFormat == 3) {
            dateCreatedString = Utils.formatTime(dateCreated);
        } else if (dateFormat == 4) {
            dateCreatedString = Utils.formatDateTime(dateCreated);
        } else {
            dateCreatedString = new PrettyTime().format(dateCreated);
        }
        recordDate.setText(dateCreatedString);
        if (liteRecord.getStatus() == LiteRecord.Status.NEW) {
            importIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.import_new));
        } else if (liteRecord.getStatus() == LiteRecord.Status.EXISTING) {
            importIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.import_existing));
        } else if (liteRecord.getStatus() == LiteRecord.Status.IMPORTED) {
            importIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.done));
        }
        return convertView;
    }

    private String prepareSearchTerms(LiteRecord liteRecord) {
        List<String> searchTermTokens = Utils.tokenizeString(liteRecord.getSearchTerms(), "\\|", true, true);
        List<String> captionTokens = Utils.tokenizeString(liteRecord.getCaption(), " ", true, true);
        List<String> searchTermTokensToUse = new ArrayList<>();
        for (String searchTermToken : searchTermTokens) {
            if (!captionTokens.contains(searchTermToken)) {
                int len = searchTermToken.length();
                boolean isUuid = len == 36 && searchTermToken.contains("-");
                if (!isUuid) {
                    searchTermTokensToUse.add(searchTermToken);
                }
            }
        }
        return Utils.string(searchTermTokensToUse, " ", "", false);
    }
}
