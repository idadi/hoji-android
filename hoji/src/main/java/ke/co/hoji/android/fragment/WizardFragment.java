package ke.co.hoji.android.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.activity.DataEntryActivity;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.android.data.LaunchType;
import ke.co.hoji.android.listener.OnWidgetRenderedListener;
import ke.co.hoji.android.listener.OnLiveFieldSavedListener;
import ke.co.hoji.android.widget.AndroidNavigableHelper;
import ke.co.hoji.android.widget.FieldLabel;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.calculation.CalculationException;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.MainRecord;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.RecordDetail;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.core.service.model.RecordService;
import ke.co.hoji.core.widget.Navigable;
import ke.co.hoji.core.widget.Navigator;
import ke.co.hoji.core.widget.Widget;
import ke.co.hoji.core.widget.WidgetEvent;

/**
 * Created by gitahi on 25/10/14.
 */
public class WizardFragment extends Fragment implements Navigable<ViewGroup, FieldLabel> {

    private Navigator navigator;
    private AndroidNavigableHelper navigableHelper;

    private ViewGroup widgetPane;
    private FieldLabel fieldLabel;

    private OnLiveFieldSavedListener onLiveFieldSavedListener;
    private OnWidgetRenderedListener onWidgetRenderedListener;

    private DataEntryActivity dataEntryActivity;

    private HojiContext hojiContext;

    private boolean navigating = false;

    private boolean atRecordEnd = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hojiContext = (HojiContext) getActivity().getApplication();
        navigableHelper = new AndroidNavigableHelper(this, hojiContext.getWidgetManager());
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.wizard_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        widgetPane = getView().findViewById(R.id.widgetPane);
        fieldLabel = getView().findViewById(R.id.fieldLabel);
        dataEntryActivity = (DataEntryActivity) getActivity();
        onLiveFieldSavedListener = dataEntryActivity;
        onWidgetRenderedListener = dataEntryActivity;
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.wizard_fragment_actions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem resetMenuItem = menu.findItem(R.id.resetButton);
        Widget active = getActiveWidget();
        if (active != null) {
            boolean resettable = active.isResettable();
            resetMenuItem.setEnabled(resettable);
            if (resettable) {
                resetMenuItem.setTitle(active.getResetText());
            }
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.viewButton:
                List<LiveField> liveFields = hojiContext.getRecord().getLiveFields();
                if (liveFields.isEmpty()) {
                    Toast toast = Toast.makeText(getActivity(),
                            getResources().getText(R.string.nothing_to_view), Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    LiveFieldFragment liveFieldFragment = new LiveFieldFragment();
                    FragmentTransaction transaction = dataEntryActivity.getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, liveFieldFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    InputMethodManager imm = (InputMethodManager) dataEntryActivity
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(((View) getActiveWidget()).getWindowToken(), 0);
                    TextView testModeTextView = dataEntryActivity.getTestModeTextView();
                    testModeTextView.setPadding(20, 10, 20, 0);
                    ProgressBar formProgressBar = dataEntryActivity.getFormProgressBar();
                    int topPadding = 20;
                    if (testModeTextView.getVisibility() == View.VISIBLE) {
                        topPadding = 0;
                    }
                    formProgressBar.setPadding(20, topPadding, 20, 20);
                }
                return true;
            case R.id.firstButton:
                first();
                return true;
            case R.id.lastButton:
                last();
                return true;
            case R.id.resetButton:
                resetWidget();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void refresh() {
        if (dataEntryActivity.getLaunchType() == LaunchType.NEW) {
            loadForm();
        } else if (dataEntryActivity.getLaunchType() == LaunchType.OPEN
                || dataEntryActivity.getLaunchType() == LaunchType.RESUME) {
            loadRecord();
        }
    }

    private void loadForm() {
        AndroidHojiContext androidHojiContext = (AndroidHojiContext) hojiContext;
        if (dataEntryActivity.getForm() != null) {
            hojiContext.setForm(dataEntryActivity.getForm());
            androidHojiContext.setUpAcraFormData();
            FieldParent parent = getFieldParent();
            if (parent != null) {
                List<Field> children = parent.getChildren();
                if (children != null && !children.isEmpty()) {
                    boolean testMode = PreferenceManager.getDefaultSharedPreferences(getActivity())
                            .getBoolean(SettingsActivity.TEST_MODE, Boolean.FALSE);
                    Record record = new Record(parent, isMain(), testMode);
                    RecordService recordService = hojiContext.getModelServiceManager().getRecordService();
                    hojiContext.setRecord(record);
                    androidHojiContext.setUpAcraRecordData();
                    navigator = new Navigator(hojiContext.getWidgetManager(), hojiContext.getRuleEvaluator(), recordService, record);
                    navigableHelper.first();
                }
            }
        }
    }

    private void loadRecord() {
        if (dataEntryActivity.getRecord() == null) {
            return;
        }
        RecordService recordService = hojiContext.getModelServiceManager().getRecordService();
        hojiContext.setRecord(dataEntryActivity.getRecord());
        ((AndroidHojiContext) hojiContext).setUpAcraFormData();
        navigator = new Navigator(hojiContext.getWidgetManager(), hojiContext.getRuleEvaluator(), recordService, dataEntryActivity.getRecord());
        navigableHelper.navigateTo(dataEntryActivity.getLiveField().getField());
    }

    @Override
    public Response getResponse() {
        return navigableHelper.readResponse();
    }

    @Override
    public ValidationResult validateWidget() {
        return navigableHelper.validate();
    }

    @Override
    public boolean isDirty() {
        return navigableHelper.isDirty();
    }

    @Override
    public void setDirty(boolean dirty) {
        navigableHelper.setDirty(dirty);
    }

    public boolean isAtRecordEnd() {
        return atRecordEnd;
    }

    @Override
    public void processGps(Form form) {
        if (dataEntryActivity != null) {
            dataEntryActivity.setReadyToUse(true);
        }
        AndroidHojiContext androidHojiContext = (AndroidHojiContext) dataEntryActivity.getApplicationContext();
        int gpsCapture = form.getGpsCapture();
        if (gpsCapture == Form.GpsCapture.NEVER) {
            dataEntryActivity.onGpsReady(null);
            return;
        }
        boolean gpsIsMandatory = (gpsCapture == Form.GpsCapture.ALWAYS);
        if (!atRecordEnd) {
            if (!androidHojiContext.isLocationEnabled()) {
                String formName = getResources().getString(R.string.form_for_location_request);
                if (gpsIsMandatory) {
                    dataEntryActivity.enableLocationMandatory(formName);
                } else {
                    dataEntryActivity.enableLocationNonMandatory(formName);
                }
                return;
            }
        }
        dataEntryActivity.updateLocation(androidHojiContext.getLocationWrapper(), true);
    }

    public void proceedWithNavigation() {
        navigableHelper.next(Navigator.Interruption.EXECUTE, Navigator.Interruption.CHECK, Navigator.Interruption.EXECUTE);
    }

    public void abortNavigation() {
        navigableHelper.next(Navigator.Interruption.ABORT, Navigator.Interruption.CHECK, Navigator.Interruption.EXECUTE);
    }

    @Override
    public void processSkipViolation() {
        navigableHelper.processSkipViolation(getActivity());
    }

    @Override
    public void processValidation() {
        navigableHelper.processValidation(getActivity());
    }

    @Override
    public void clearedViolatedFields(int cleared) {
        Toast toast = Toast.makeText(getActivity(),
                getResources().getQuantityString(R.plurals.n_fields_deleted,
                        cleared, cleared), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void processRecordEnd(final Record record, final Navigator navigator) {
        List<Field> unAnsweredFields = getUnAnsweredFields(record);
        int noUnAnswered = unAnsweredFields.size();
        if (noUnAnswered > 0) {
            Field firstUnAnsweredField = unAnsweredFields.get(0);
            Context context = this.getActivity();
            Resources resources = context.getResources();
            final Field fuf = firstUnAnsweredField;
            PropertyService propertyService = hojiContext.getModelServiceManager().getPropertyService();
            Property property = propertyService.getProperty(
                    Constants.Server.FIX_BLANKS,
                    hojiContext.getSurvey().getId()
            );
            boolean fix = false;
            if (property != null && !StringUtils.isBlank(property.getValue())) {
                fix = Boolean.valueOf(property.getValue());
            }
            if (fix) {
                new AlertDialog.Builder(context)
                        .setTitle(resources.getQuantityString(R.plurals.n_blank_fields_detected, noUnAnswered, noUnAnswered))
                        .setMessage(context.getResources().getQuantityString(R.plurals.fix_n_blank_fields, noUnAnswered, noUnAnswered))
                        .setPositiveButton(R.string.fix_action, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                navigableHelper.navigateTo(fuf);
                                return;
                            }
                        })
                        .create()
                        .show();
            } else {
                new AlertDialog.Builder(context)
                        .setTitle(resources.getQuantityString(R.plurals.n_blank_fields_detected, noUnAnswered, noUnAnswered))
                        .setMessage(resources.getQuantityString(R.plurals.fix_or_ignore_n_blank_fields, noUnAnswered, noUnAnswered))
                        .setPositiveButton(R.string.fix_action, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                navigableHelper.navigateTo(fuf);
                                return;
                            }
                        })
                        .setNegativeButton(R.string.ignore_action, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finalizeRecord(record, navigator);
                            }
                        })
                        .create()
                        .show();
            }
        } else {
            finalizeRecord(record, navigator);
        }
    }

    @Override
    public boolean isMain() {
        return true;
    }

    @Override
    public FieldParent getFieldParent() {
        return dataEntryActivity.getForm();
    }

    @Override
    public Navigator getNavigator() {
        return navigator;
    }

    @Override
    public void setNavigator(Navigator navigator) {
        this.navigator = navigator;
    }

    @Override
    public Navigable getParentNavigable() {
        return null;
    }

    @Override
    public void setParentNavigable(Navigable navigable) {
    }

    @Override
    public ViewGroup getWidgetContainer() {
        return widgetPane;
    }

    @Override
    public FieldLabel getFieldLabel() {
        return fieldLabel;
    }

    @Override
    public Widget getActiveWidget() {
        return navigableHelper.getActiveWidget();
    }

    @Override
    public void setActiveWidget(Widget widget) {
        navigableHelper.setActiveWidget(widget);
    }

    @Override
    public void setResetText(String resetText) {
    }

    @Override
    public boolean isNavigating() {
        return navigating;
    }

    @Override
    public void setNavigating(boolean navigating) {
        this.navigating = navigating;
    }

    @Override
    public void first() {
        navigableHelper.first();
    }

    @Override
    public void previous() {
        navigableHelper.previous();
    }

    @Override
    public void next() {
        ((AndroidHojiContext) hojiContext).setUpAcraRecordData();
        navigableHelper.next();
    }

    @Override
    public void last() {
        navigableHelper.last();
    }

    @Override
    public void resetWidget() {
        navigableHelper.resetWidget();
    }

    @Override
    public RecordDetail getDetail() {
        RecordDetail recordDetail = navigator.getRecord().getDetail();
        if (recordDetail == null) {
            MainRecord mainRecord = new MainRecord(navigator.getRecord());
            mainRecord.setAppVersion(hojiContext.appVersion());
            recordDetail = mainRecord;
        }
        return recordDetail;
    }

    @Override
    public void responseChanged(WidgetEvent widgetEvent) {
        navigableHelper.responseChanged(widgetEvent);
    }

    @Override
    public void responseChanging(WidgetEvent widgetEvent) {
        navigableHelper.responseChanging(widgetEvent);
    }

    @Override
    public Map<String, Object> getParameters() {
        return navigableHelper.getParameters(getActivity());
    }

    @Override
    public void rendered(Widget widget, Record record) {
        setNavigating(false);
        onWidgetRenderedListener.onWidgetRendered(widget, record);
        this.getActivity().invalidateOptionsMenu();
    }

    @Override
    public void saved(LiveField liveField, Record record) {
        onLiveFieldSavedListener.onLiveFieldSaved(liveField, record);
    }

    @Override
    public boolean autoNavigate() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(dataEntryActivity);
        return sharedPreferences.getBoolean(SettingsActivity.AUTO_NAVIGATE, Boolean.TRUE);
    }

    @Override
    public boolean flashNavigate() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(dataEntryActivity);
        return sharedPreferences.getBoolean(SettingsActivity.FLASH_NAVIGATE, Boolean.FALSE);
    }

    @Override
    public void handleCalculationException(CalculationException ex) {
        new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.calculation_error_title))
                .setMessage(getString(R.string.the_calculation) + "\n\n"
                        + ex.getCalculationScript() + "\n\n"
                        + getString(R.string.calculation_failed_with) + "\n\n"
                        + ex.getMessage() + "\n\n"
                        + getString(R.string.calculation_contact_administrator))
                .setPositiveButton(R.string.ok_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Do nothing
                    }
                })
                .create()
                .show();
    }

    private List<Field> getUnAnsweredFields(Record record) {
        List<Field> unAnsweredFields = new ArrayList<>();
        for (LiveField liveField : record.getLiveFields()) {
            if (!liveField.getResponse().isAnswered()) {
                unAnsweredFields.add(liveField.getField());
            }
        }
        return unAnsweredFields;
    }

    private void finalizeRecord(Record record, Navigator navigator) {
        navigator.finalizeRecord(record);
        atRecordEnd = true;
        processGps((Form) record.getFieldParent());
    }
}