package ke.co.hoji.android.dao.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.model.Language;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SqliteLanguageDao {

    private final SqliteSqlExecutor se;

    public SqliteLanguageDao(SqliteSqlExecutor se) {
        this.se = se;
    }

    public List<Language> getAllLanguages() {
        List<Language> languages = new ArrayList<>();
        String select = "SELECT id, name, survey_id FROM language";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, se.createParameterMap());
            while (cursor.moveToNext()) {
                Language language = new Language
                    (
                        cursor.getInt(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("name"))
                    );
                languages.add(language);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteLanguageDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, se.createParameterMap());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return languages;
    }
}
