package ke.co.hoji.android.dao.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.ConfigObjectCache;
import ke.co.hoji.core.data.model.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by gitahi on 15/04/15.
 */
public class SqliteFieldDao {

    private final SqliteSqlExecutor se;

    private final SqliteChoiceGroupDao choiceGroupDao;

    public SqliteFieldDao(SqliteSqlExecutor se, SqliteChoiceGroupDao choiceGroupDao) {
        this.se = se;
        this.choiceGroupDao = choiceGroupDao;
    }

    public Field getFieldById(int fieldId) {
        Field field = null;
        String select = "SELECT "
                + "id, "
                + "name, "
                + "`column`, "
                + "description, "
                + "ordinal, "
                + "instructions, "
                + "field_type_id, "
                + "form_id, "
                + "enabled, "
                + "captioning, "
                + "searchable, "
                + "filterable, "
                + "output_type, "
                + "tag, "
                + "calculated, "
                + "value_script, "
                + "enable_if_null, "
                + "validation_script, "
                + "default_value, "
                + "missing_value, "
                + "missing_action, "
                + "min_value, "
                + "min_action, "
                + "max_value, "
                + "max_action, "
                + "regex_value, "
                + "regex_action, "
                + "uniqueness, "
                + "unique_action, "
                + "choice_group_id, "
                + "choice_filter_field_id, "
                + "parent_id, "
                + "pipe_source_id, "
                + "reference_field_id, "
                + "main_record_field_id, "
                + "matrix_record_field_id, "
                + "response_inheriting "
                + "FROM field "
                + "WHERE id = ? ";
        Map<String, Object> params = se.createParameterMap("id", fieldId);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                field = new FieldRowMapper().mapRow(cursor);
                se.put(field);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteFieldDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return field;
    }

    public List<Field> getFields(int formId) {
        List<Field> fields = new ArrayList<>();
        String select = "SELECT "
            + "id, "
            + "name, "
            + "`column`, "
            + "description, "
            + "ordinal, "
            + "instructions, "
            + "field_type_id, "
            + "form_id, "
            + "enabled, "
            + "captioning, "
            + "searchable, "
            + "filterable, "
            + "output_type, "
            + "tag, "
            + "calculated, "
            + "value_script, "
            + "enable_if_null, "
            + "validation_script, "
            + "default_value, "
            + "missing_value, "
            + "missing_action, "
            + "min_value, "
            + "min_action, "
            + "max_value, "
            + "max_action, "
            + "regex_value, "
            + "regex_action, "
            + "uniqueness, "
            + "unique_action, "
            + "choice_group_id, "
            + "choice_filter_field_id, "
            + "parent_id, "
            + "pipe_source_id, "
            + "reference_field_id, "
            + "main_record_field_id, "
            + "matrix_record_field_id, "
            + "response_inheriting "
            + "FROM field "
            + "WHERE form_id = ? AND parent_id = 0 "
            + "ORDER BY ordinal";
        Map<String, Object> params = se.createParameterMap("form_id", formId);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                Field field = new FieldRowMapper().mapRow(cursor);
                fields.add(field);
                se.put(field);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteFieldDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return fields;
    }

    public List<Field> getChildren(int parentId) {
        List<Field> fields = new ArrayList<>();
        String select = "SELECT "
            + "id, "
            + "name, "
            + "`column`, "
            + "description, "
            + "ordinal, "
            + "instructions, "
            + "field_type_id, "
            + "form_id, "
            + "enabled, "
            + "captioning, "
            + "searchable, "
            + "filterable, "
            + "output_type, "
            + "tag, "
            + "calculated, "
            + "value_script, "
            + "enable_if_null, "
            + "validation_script, "
            + "default_value, "
            + "missing_value, "
            + "missing_action, "
            + "min_value, "
            + "min_action, "
            + "max_value, "
            + "max_action, "
            + "regex_value, "
            + "regex_action, "
            + "uniqueness, "
            + "unique_action, "
            + "choice_group_id, "
            + "choice_filter_field_id, "
            + "parent_id, "
            + "pipe_source_id, "
            + "reference_field_id, "
            + "main_record_field_id, "
            + "matrix_record_field_id, "
            + "response_inheriting "
            + "FROM "
            + "field "
            + "WHERE parent_id = ? "
            + "ORDER BY ordinal";
        Map<String, Object> params = se.createParameterMap("parent_id", parentId);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                Field field = new FieldRowMapper().mapRow(cursor);
                fields.add(field);
                se.put(field);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteFieldDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return fields;
    }

    public List<Rule> getRules(Field owner, Integer type, ConfigObjectCache<Field> configCache) throws DaoException {
        List<Rule> rules = new ArrayList<>();
        Map<String, Object> params = se.createParameterMap();
        params.put("owner_id", owner.getId());
        params.put("type", type);
        String select = "SELECT id, type, ordinal, owner_id, operator_descriptor_id, value, target_id, combiner, grouper "
            + "FROM rule "
            + "WHERE owner_id = ? AND type = ?"
            + "ORDER BY ordinal";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                Rule rule = new Rule
                    (
                        cursor.getInt(cursor.getColumnIndex("id")),
                        new BigDecimal(cursor.getDouble(cursor.getColumnIndex("ordinal"))),
                        cursor.getString(cursor.getColumnIndex("value")),
                        cursor.getInt(cursor.getColumnIndex("type")),
                        cursor.getInt(cursor.getColumnIndex("grouper"))
                    );
                rule.setOwner(owner);
                rule.setTarget(configCache.get(cursor.getInt(cursor.getColumnIndex("target_id"))));
                rule.setOperatorDescriptor(getOperatorDescriptor(cursor.getInt(cursor.getColumnIndex(
                    "operator_descriptor_id"))));
                rule.setCombiner(cursor.getInt(cursor.getColumnIndex("combiner")));
                rules.add(rule);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteFieldDao.class.getName()).log(Level.SEVERE,
                null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return rules;
    }

    private OperatorDescriptor getOperatorDescriptor(Integer id) {
        OperatorDescriptor operatorDescriptor = se.get(id, OperatorDescriptor.class);
        if (operatorDescriptor == null) {
            Map<String, Object> params = se.createParameterMap("id", id);
            String select = "SELECT id, name, class FROM operator_descriptor WHERE id = ?";
            Cursor cursor = null;
            try {
                cursor = se.rawQuery(select, params);
                if (cursor.moveToNext()) {
                    operatorDescriptor = new OperatorDescriptor
                        (
                            cursor.getInt(cursor.getColumnIndex("id")),
                            cursor.getString(cursor.getColumnIndex("name")),
                            cursor.getString(cursor.getColumnIndex("class"))
                        );
                    se.put(operatorDescriptor);
                }
            } catch (SQLiteException ex) {
                Logger.getLogger(SqliteFieldDao.class.getName()).log(Level.SEVERE,
                    null, ex);
                throw new DaoException(ex, select);
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return operatorDescriptor;
    }

    private FieldType getFieldType(Integer id) {
        FieldType fieldType = se.get(id, FieldType.class);
        if (fieldType == null) {
            Map<String, Object> params = se.createParameterMap("id", id);
            String select = "SELECT id, code, name, ordinal, data_type, `group`, widget_class, matrix_widget_class, response_class " +
                "FROM field_type  " +
                "WHERE id = ? ";
            Cursor cursor = null;
            try {
                cursor = se.rawQuery(select, params);
                if (cursor.moveToNext()) {
                    fieldType = new FieldType
                        (
                            cursor.getInt(cursor.getColumnIndex("id")),
                            cursor.getString(cursor.getColumnIndex("name")),
                            cursor.getString(cursor.getColumnIndex("code"))
                        );
                    fieldType.setDataType(cursor.getInt(cursor.getColumnIndex("data_type")));
                    fieldType.setWidgetClass((cursor.getString(cursor.getColumnIndex("widget_class"))));
                    fieldType.setMatrixWidgetClass((cursor.getString(cursor.getColumnIndex("matrix_widget_class"))));
                    fieldType.setResponseClass((cursor.getString(cursor.getColumnIndex("response_class"))));
                    se.put(fieldType);
                }
            } catch (SQLiteException ex) {
                Logger.getLogger(SqliteFieldDao.class.getName()).log(Level.SEVERE,
                    null, ex);
                throw new DaoException(ex, select);
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return fieldType;
    }

    private List<Form> getRecordForms(Field field) {
        List<Form> forms = new ArrayList<>();
        Map<String, Object> params = se.createParameterMap("field_id", field.getId());
        String select = "SELECT field_id, form_id FROM field_record_form WHERE field_id = ?;";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                Integer formId = cursor.getInt(cursor.getColumnIndex("form_id"));
                Form form = se.get(formId, Form.class);
                if (form != null) {
                    forms.add(form);
                }
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteFieldDao.class.getName()).log(Level.SEVERE,
                null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return forms;
    }

    private class FieldRowMapper {

        Field mapRow(Cursor cursor) {
            Field field = new Field
                    (
                            cursor.getInt(cursor.getColumnIndex("id")),
                            cursor.getString(cursor.getColumnIndex("name")),
                            cursor.getInt(cursor.getColumnIndex("enabled")) > 0,
                            cursor.getString(cursor.getColumnIndex("description")),
                            new BigDecimal(cursor.getDouble(cursor.getColumnIndex("ordinal"))),
                            cursor.getInt(cursor.getColumnIndex("captioning")) > 0,
                            cursor.getInt(cursor.getColumnIndex("searchable")) > 0,
                            cursor.getInt(cursor.getColumnIndex("filterable")) > 0,
                            cursor.getInt(cursor.getColumnIndex("output_type")),
                            cursor.getInt(cursor.getColumnIndex("missing_action"))
                    );
            field.setInstructions(cursor.getString(cursor.getColumnIndex("instructions")));
            field.setTag(cursor.getString(cursor.getColumnIndex("tag")));
            field.setCalculated(cursor.getInt(cursor.getColumnIndex("calculated")));
            field.setValueScript(cursor.getString(cursor.getColumnIndex("value_script")));
            field.setDefaultValue(cursor.getString(cursor.getColumnIndex("default_value")));
            field.setMissingValue(cursor.getString(cursor.getColumnIndex("missing_value")));
            field.setMinValue(cursor.getString(cursor.getColumnIndex("min_value")));
            field.setMaxValue(cursor.getString(cursor.getColumnIndex("max_value")));
            field.setRegexValue(cursor.getString(cursor.getColumnIndex("regex_value")));
            field.setUniqueness(cursor.getInt(cursor.getColumnIndex("uniqueness")));
            field.setType(getFieldType(cursor.getInt(cursor.getColumnIndex("field_type_id"))));
            field.setChoiceFilterField(se.get(cursor.getInt(cursor.getColumnIndex("choice_filter_field_id")),
                    Field.class));
            field.setReferenceField(se.get(cursor.getInt(cursor.getColumnIndex("reference_field_id")),
                    Field.class));
            field.setMainRecordField(se.get(cursor.getInt(cursor.getColumnIndex("main_record_field_id")),
                    Field.class));
            field.setMatrixRecordField(se.get(cursor.getInt(cursor.getColumnIndex("matrix_record_field_id")),
                    Field.class));
            if (field.getType().isChoice()) {
                field.setChoiceGroup(
                        choiceGroupDao.getChoiceGroupById(
                                cursor.getInt(cursor.getColumnIndex("choice_group_id"))));
            }
            if (field.getType().isRecord()) {
                field.setRecordForms(getRecordForms(field));
            }
            field.setResponseInheriting(cursor.getInt(cursor.getColumnIndex("response_inheriting")) > 0);
            return field;
        }

    }
}
