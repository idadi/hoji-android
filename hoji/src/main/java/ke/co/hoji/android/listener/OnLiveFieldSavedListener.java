package ke.co.hoji.android.listener;

import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;

/**
 * Defines a callback for reacting to a {@link LiveField} saved event.
 * <p/>
 * Created by gitahi on 26/05/15.
 */
public interface OnLiveFieldSavedListener {

    /**
     * Called when a {@link LiveField} is saved.
     *
     * @param liveField the {@link LiveField} saved.
     * @param record    the {@link Record} associated with the saved liveField.
     */
    void onLiveFieldSaved(LiveField liveField, Record record);
}
