package ke.co.hoji.android.service.model;

import java.util.ArrayList;
import java.util.List;

import ke.co.hoji.android.dao.model.SqliteFieldDao;
import ke.co.hoji.core.data.ConfigObjectCache;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.OperatorDescriptor;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.service.model.FieldService;

public class FieldServiceImpl implements FieldService {

    private final SqliteFieldDao fieldDao;

    public FieldServiceImpl(SqliteFieldDao fieldDao) {
        this.fieldDao = fieldDao;
    }

    @Override
    public List<Field> getFields(Form form) {
        return fieldDao.getFields(form.getId());
    }

    @Override
    public List<Field> getAllFields(Form form) {
        List<Field> fields = new ArrayList<>();
        for (Field field : getFields(form)) {
            fields.add(field);
            if (field.isMatrix()) {
                List<Field> children = getChildren(field);
                field.setChildren(children);
                for (Field child : children) {
                    child.setParent(field);
                    fields.add(child);
                }
            }
        }
        return fields;
    }

    @Override
    public List<Field> getParentFields(Form form) {
        List<Field> fields = new ArrayList<>();
        for (Field field : getFields(form)) {
            if (field.isMatrix()) {
                fields.add(field);
            }
        }
        return fields;
    }

    @Override
    public List<Field> getReferenceFields(Form form) {
        List<Field> fields = new ArrayList<>();
        for (Field field : getFields(form)) {
            if (field.getType().getCode().equals(FieldType.Code.REFERENCE)) {
                fields.add(field);
            }
        }
        return fields;
    }

    @Override
    public List<Rule> getForwardSkips(Field owner, ConfigObjectCache<Field> fieldCache) {
        return fieldDao.getRules(owner, Rule.Type.FORWARD_SKIP, fieldCache);
    }

    @Override
    public List<Rule> getBackwardSkips(Field owner, ConfigObjectCache<Field> fieldCache) {
        return fieldDao.getRules(owner, Rule.Type.BACKWARD_SKIP, fieldCache);
    }

    @Override
    public List<Rule> getFilterRules(Field owner, ConfigObjectCache<Field> fieldCache) {
        return fieldDao.getRules(owner, Rule.Type.FILTER_RULE, fieldCache);
    }

    @Override
    public List<Field> getChildren(Field field) {
        return fieldDao.getChildren(field.getId());
    }

    @Override
    public Field getFieldById(Integer fieldId) {
        return fieldDao.getFieldById(fieldId);
    }

    @Override
    public void deleteField(Integer integer) {
    }

    @Override
    public Field createField(Field field) {
        return null;
    }

    @Override
    public Field modifyField(Field field) {
        return null;
    }

    @Override
    public List<Field> modifyFields(List<Field> list) {
        return null;
    }

    @Override
    public void enableField(Field field, boolean b) {

    }

    @Override
    public List<FieldType> getAllFieldTypes() {
        return null;
    }

    @Override
    public List<OperatorDescriptor> getAllOperatorDescriptors() {
        return null;
    }

    @Override
    public FieldType getFieldTypeById(Integer integer) {
        return null;
    }

    @Override
    public OperatorDescriptor getOperatorDescriptorById(Integer integer) {
        return null;
    }

    @Override
    public void deleteRule(Integer integer) {

    }

    @Override
    public void deleteRule(Rule rule) {

    }

    @Override
    public Rule createRule(Rule rule) {
        return null;
    }

    @Override
    public Rule modifyRule(Rule rule) {
        return null;
    }

    @Override
    public List<Rule> getRules(Field field, Integer integer) {
        return null;
    }

    @Override
    public Rule getRuleById(Field field, Integer integer) {
        return null;
    }

    @Override
    public void setQuestionNumber(Field field, String s) {

    }

    @Override
    public void updateOrdinal(List<Field> list) {

    }

    @Override
    public void updateRuleOrdinal(List<Rule> list) {

    }

    @Override
    public Field getFieldFromDto(ke.co.hoji.core.data.dto.Field field) {
        return null;
    }

}
