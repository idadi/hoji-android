package ke.co.hoji.android.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ke.co.hoji.R;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.android.adapter.LiveFieldArrayAdapter;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.calculation.CalculationException;
import ke.co.hoji.core.data.NavigableList;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.MatrixRecord;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.RecordDetail;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.service.model.RecordService;
import ke.co.hoji.core.widget.Navigable;
import ke.co.hoji.core.widget.Navigator;
import ke.co.hoji.core.widget.Widget;
import ke.co.hoji.core.widget.WidgetEvent;

/**
 * A widget for rendering Matrix questions.
 * <p/>
 * Created by gitahi on 17/11/14.
 */
public class MatrixWidget extends AndroidWidget implements Navigable<ViewGroup, FieldLabel> {

    private static class ShowMode {
        private static final int FIRST = 1;
        private static final int LAST = 2;
        private static final int CURRENT = 3;
    }

    /**
     * Contains cached {@link ke.co.hoji.core.widget.Navigator}s.
     */
    private final Map<Record, Navigator> navigatorCache = new HashMap<>();
    /**
     * The {@link ke.co.hoji.core.widget.Navigator} of the currently active {@link MatrixRecord}.
     */
    private Navigator navigator;
    /**
     * The {@link ke.co.hoji.core.widget.Navigable} containing this {@link ke.co.hoji.core.widget.Navigable}.
     */
    private Navigable parent;

    /**
     * The currently active {@link MatrixRecord}
     */
    private MatrixRecord activeMatrixRecord;
    private ViewGroup widgetPane;
    private FieldLabel fieldLabel;
    private ListView listView;
    private ImageButton newButton;
    private ImageButton previousButton;
    private ImageButton nextButton;
    private ImageButton resetButton;
    private ArrayAdapter<MatrixRecord> adapter;
    private List<MatrixRecord> matrixRecords;
    private AndroidNavigableHelper navigableHelper;
    private boolean navigating = false;

    public MatrixWidget(Context context) {
        super(context);
    }

    @Override
    public void setHojiContext(HojiContext hojiContext) {
        super.setHojiContext(hojiContext);
        navigableHelper = new AndroidNavigableHelper(this, hojiContext.getWidgetManager());
    }

    @Override
    protected int getLayout() {
        return R.layout.matrix_widget;
    }

    @Override
    public void loadViews() {
        matrixRecords = new ArrayList<>();
        fieldLabel = findViewById(R.id.fieldLabel);
        widgetPane = findViewById(R.id.widgetPane);
        newButton = findViewById(R.id.newButton);
        previousButton = findViewById(R.id.previousButton);
        nextButton = findViewById(R.id.nextButton);
        resetButton = findViewById(R.id.resetButton);
        listView = findViewById(R.id.listView);
        adapter = new ArrayAdapter<>(this.getContext(),
                R.layout.matrix_record_activated, matrixRecords);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long rowId) {
                MatrixRecord selected = (MatrixRecord) listView.getItemAtPosition(position);
                setActiveMatrixRecord(selected, ShowMode.CURRENT);
                if (selected.getRecord().getUuid() != null) {
                    showLiveFieldPickerDialog(selected.getRecord());
                }
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                MatrixRecord selected = (MatrixRecord) listView.getItemAtPosition(position);
                setActiveMatrixRecord(selected, ShowMode.CURRENT);
                if (selected.getRecord().getUuid() != null) {
                    deleteMatrixRecord(selected);
                }
                return true;
            }
        });
        addButtonListeners();
    }

    public RecordDetail getDetail() {
        RecordDetail recordDetail = navigator.getRecord().getDetail();
        if (recordDetail == null) {
            recordDetail = new MatrixRecord(navigator.getRecord(), hojiContext.getRecord());
        }
        return recordDetail;
    }

    @Override
    public boolean isResuming() {
        Widget activeWidget = getActiveWidget();
        if (activeWidget != null) {
            return activeWidget.isResuming();
        }
        return super.isResuming();
    }

    @Override
    public void ready() {
        if (!isResuming()) {
            newRecord();
        }
    }

    @Override
    public void prepare() {
        matrixRecords.clear();
        adapter.notifyDataSetChanged();
        fieldLabel.clear();
        widgetPane.removeAllViews();
        navigatorCache.clear();
    }

    @Override
    public void focus() {
        newButton.requestFocus();
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null) {
            List<MatrixRecord> matrixRecords = (List<MatrixRecord>) response.getActualValue();
            if (matrixRecords != null) {
                this.matrixRecords.addAll(matrixRecords);
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public Object getValue() {
        List<MatrixRecord> matrixRecords = new ArrayList<>();
        for (MatrixRecord matrixRecord : this.matrixRecords) {
            if (matrixRecord.getRecord().getUuid() != null) {
                matrixRecords.add(matrixRecord);
            }
        }
        return matrixRecords;
    }

    @Override
    public boolean isEmpty() {
        Object value = getValue();
        if (value == null) {
            return true;
        } else {
            List<MatrixRecord> matrixRecords = (List<MatrixRecord>) value;
            return matrixRecords.isEmpty();
        }
    }

    @Override
    public Comparable getComparable() {
        if (matrixRecords != null && !matrixRecords.isEmpty()) {
            if (matrixRecords.size() == 1) {
                Record first = new NavigableList<>(matrixRecords).first().getRecord();
                if (first.getUuid() == null) {
                    return 0L;
                } else {
                    return 1L;
                }
            } else {
                Record last = new NavigableList<>(matrixRecords).last().getRecord();
                if (last.getUuid() == null) {
                    return (long) (matrixRecords.size() - 1);
                } else {
                    return (long) matrixRecords.size();
                }
            }
        }
        return 0L;
    }

    @Override
    public ValidationResult validate(Record record) {
        ValidationResult validationResult = super.validate(record);
        if (validationResult.getSignal() != ValidationResult.OKAY) {
            return validationResult;
        }
        validationResult = ValidationResult.okay();
        for (MatrixRecord matrixRecord : matrixRecords) {
            Record r = matrixRecord.getRecord();
            if (!r.getLiveFields().isEmpty()) {
                Navigator navigator = getNavigator(r);
                if (!navigator.isRecordComplete(getHojiContext())) {
                    validationResult = ValidationResult.stop(getCurrentContext().getResources().getString(
                            R.string.incomplete_matrix_record,
                            r.getCaption()
                    ));
                    setActiveMatrixRecord(matrixRecord, ShowMode.LAST);
                    break;
                }
            }
        }
        return validationResult;
    }

    @Override
    public void setDirty(boolean dirty) {
        navigableHelper.setDirty(dirty);
    }

    @Override
    public boolean isDirty() {
        return navigableHelper.isDirty();
    }

    @Override
    public void processGps(Form form) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void processSkipViolation() {
        navigableHelper.processSkipViolation(getCurrentContext());
    }

    @Override
    public void processValidation() {
        navigableHelper.processValidation(getCurrentContext());
    }

    @Override
    public void clearedViolatedFields(int cleared) {
        Toast toast = Toast.makeText(getContext(),
                getResources().getQuantityString(R.plurals.n_fields_deleted,
                        cleared, cleared), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void processRecordEnd(Record record, Navigator navigator) {
        Toast toast = Toast.makeText(getContext(),
                getResources().getText(R.string.the_end),
                Toast.LENGTH_SHORT
        );
        toast.show();
    }

    @Override
    public Response getResponse() {
        return navigableHelper.readResponse();
    }

    @Override
    public ValidationResult validateWidget() {
        return navigableHelper.validate();
    }

    public boolean isMain() {
        return false;
    }

    @Override
    public FieldParent getFieldParent() {
        return getLiveField().getField();
    }

    @Override
    public Navigator getNavigator() {
        return navigator;
    }

    @Override
    public void setNavigator(Navigator navigator) {
        this.navigator = navigator;
    }

    @Override
    public Navigable getParentNavigable() {
        return parent;
    }

    @Override
    public void setParentNavigable(Navigable parent) {
        this.parent = parent;
    }

    @Override
    public ViewGroup getWidgetContainer() {
        return widgetPane;
    }

    @Override
    public FieldLabel getFieldLabel() {
        return fieldLabel;
    }

    @Override
    public Widget getActiveWidget() {
        return navigableHelper.getActiveWidget();
    }

    @Override
    public void setActiveWidget(Widget widget) {
        navigableHelper.setActiveWidget(widget);
    }

    @Override
    public void setResetText(String resetText) {
    }

    @Override
    public boolean isNavigating() {
        return navigating;
    }

    @Override
    public void setNavigating(boolean navigating) {
        this.navigating = navigating;
    }

    @Override
    public void first() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void previous() {
        navigableHelper.previous();
    }

    @Override
    public void next() {
        navigableHelper.next();
    }

    @Override
    public void last() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void resetWidget() {
        navigableHelper.resetWidget();
    }

    @Override
    public Map<String, Object> getParameters() {
        return navigableHelper.getParameters(getCurrentContext());
    }

    @Override
    public void responseChanged(WidgetEvent widgetEvent) {
        navigableHelper.responseChanged(widgetEvent);
    }

    @Override
    public void responseChanging(WidgetEvent widgetEvent) {
        navigableHelper.responseChanging(widgetEvent);
    }

    @Override
    public void rendered(Widget widget, Record record) {
        setNavigating(false);
    }

    @Override
    public void rendered() {
        Widget widget = getActiveWidget();
        if (widget != null) {
            widget.rendered();
        }
    }

    public void saved(LiveField liveField, Record record) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean autoNavigate() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getCurrentContext());
        return sharedPreferences.getBoolean(SettingsActivity.AUTO_NAVIGATE, Boolean.TRUE);
    }

    @Override
    public boolean flashNavigate() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getCurrentContext());
        return sharedPreferences.getBoolean(SettingsActivity.FLASH_NAVIGATE, Boolean.FALSE);
    }

    @Override
    public View getPrincipalView() {
        return widgetPane;
    }

    @Override
    public void handleCalculationException(CalculationException ex) {
        new AlertDialog.Builder(getCurrentContext())
                .setTitle(getResources().getString(R.string.calculation_error_title))
                .setMessage(getResources().getString(R.string.the_calculation) + "\n\n"
                        + ex.getCalculationScript() + "\n\n"
                        + getResources().getString(R.string.calculation_failed_with) + "\n\n"
                        + ex.getMessage() + "\n\n"
                        + getResources().getString(R.string.calculation_contact_administrator))
                .setPositiveButton(R.string.ok_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Do nothing
                    }
                })
                .create()
                .show();
    }

    private void showLiveFieldPickerDialog(final Record record) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getCurrentContext());
        builder.setTitle(record.getCaption());
        builder.setAdapter(new LiveFieldArrayAdapter(getCurrentContext(), record),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int position) {
                        if (position > -1) {
                            LiveField selected = record.getLiveFields().get(position);
                            navigableHelper.navigateTo(selected.getField());
                        }
                    }
                }
        );
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void deleteMatrixRecord(MatrixRecord matrixRecord) {
        final Record selectedRecord = matrixRecord.getRecord();
        final String recordName = selectedRecord.getCaption() != null
                && !selectedRecord.getCaption().equals("")
                ? selectedRecord.getCaption()
                : getResources().getString(R.string.this_record);
        new AlertDialog.Builder(MatrixWidget.this.getCurrentContext())
                .setTitle(R.string.delete_record_title)
                .setMessage(getResources().getString(R.string.delete_record_message, recordName))
                .setPositiveButton(R.string.yes_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        delete();
                    }
                })
                .setNegativeButton(R.string.no_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do nothing
                    }
                })
                .create()
                .show();
    }

    private void addButtonListeners() {
        newButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                newRecord();
            }
        });
        previousButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                previous();
            }
        });
        nextButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                next();
            }
        });
        resetButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                resetWidget();
            }
        });
    }

    private void newRecord() {
        MatrixRecord matrixRecord = null;
        if (!matrixRecords.isEmpty()) {
            MatrixRecord last = new NavigableList<>(matrixRecords).last();
            if (last.getRecord().getUuid() == null) {
                matrixRecord = last;
            }
        }
        if (matrixRecord == null) {
            boolean testMode = PreferenceManager.getDefaultSharedPreferences(getCurrentContext())
                    .getBoolean(SettingsActivity.TEST_MODE, Boolean.FALSE);
            matrixRecord = new MatrixRecord(new Record(getFieldParent(), false, testMode), hojiContext.getRecord());
            matrixRecords.add(matrixRecord);
            adapter.notifyDataSetChanged();
        }
        setActiveMatrixRecord(matrixRecord, ShowMode.FIRST);
    }

    private void setActiveMatrixRecord(MatrixRecord activeMatrixRecord, int showMode) {
        this.activeMatrixRecord = activeMatrixRecord;
        Navigator navigator = getNavigator(this.activeMatrixRecord.getRecord());
        setNavigator(navigator);
        if (showMode == ShowMode.FIRST) {
            navigableHelper.first();
        } else if (showMode == ShowMode.LAST) {
            navigableHelper.last();
        } else if (showMode == ShowMode.CURRENT) {
            Field toShow = getToShow(this.activeMatrixRecord.getRecord());
            if (toShow != null) {
                navigableHelper.navigateTo(toShow);
            } else {
                navigableHelper.first();
            }
        }
        int position = matrixRecords.indexOf(this.activeMatrixRecord);
        listView.setItemChecked(position, true);
        listView.smoothScrollToPosition(position);
    }

    private Field getToShow(Record record) {
        Field toShow = null;
        Widget activeWidget = getActiveWidget();
        if (activeWidget != null) {
            toShow = activeWidget.getLiveField().getField();
            if (!record.contains(new LiveField(toShow))) {
                List<LiveField> liveFields = record.getLiveFields();
                for (int i = liveFields.size() - 1; i >= 0; i--) {
                    LiveField liveField = liveFields.get(i);
                    if (liveField.getField().getOrdinal().compareTo(toShow.getOrdinal()) < 0) {
                        toShow = liveField.getField();
                        break;
                    }
                }
            }
        }
        return toShow;
    }

    private Navigator getNavigator(Record record) {
        Navigator navigator = navigatorCache.get(record);
        if (navigator == null) {
            RecordService recordService = hojiContext.getModelServiceManager().getRecordService();
            navigator = new Navigator(hojiContext.getWidgetManager(), hojiContext.getRuleEvaluator(), recordService, record);
            navigatorCache.put(record, navigator);
        }
        return navigator;
    }

    private void delete() {
        navigableHelper.deleteRecord(activeMatrixRecord.getRecord());
        matrixRecords.remove(activeMatrixRecord);
        adapter.notifyDataSetChanged();
        liveField.setResponse(readResponse());
        getActiveWidget().reset();
    }
}
