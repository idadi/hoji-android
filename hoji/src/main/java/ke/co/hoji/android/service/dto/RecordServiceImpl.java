package ke.co.hoji.android.service.dto;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import ke.co.hoji.android.dao.dto.SqliteRecordDao;
import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.dto.LiteRecord;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.http.UploadResult;
import ke.co.hoji.core.service.dto.RecordService;

public class RecordServiceImpl implements RecordService {

    private final SqliteRecordDao recordDao;

    public RecordServiceImpl(SqliteRecordDao recordDao) {
        this.recordDao = recordDao;
    }

    @Override
    public List<MainRecord> findMainRecords(RecordSearch recordSearch, int limit) throws IOException {
        return recordDao.getMainRecordsByStage(recordSearch.getStage(), limit);
    }

    @Override
    public void saveMainRecords(List<MainRecord> mainRecords) {
        recordDao.saveMainRecords(mainRecords);
    }

    @Override
    public void flagNonNewLiteRecords(List<LiteRecord> liteRecords) {
        for (LiteRecord liteRecord : liteRecords) {
            Long dateUpdated = recordDao.getDateUpdated(liteRecord.getUuid());
            if (dateUpdated == null) {
                liteRecord.setStatus(LiteRecord.Status.NEW);
            } else {
                if (dateUpdated.equals(liteRecord.getDateUpdated())) {
                    liteRecord.setStatus(LiteRecord.Status.IMPORTED);
                } else {
                    liteRecord.setStatus(LiteRecord.Status.EXISTING);
                }
            }
        }
    }

    @Override
    public int updateStatuses(Map<String, UploadResult> results) {
        return recordDao.updateStatuses(results);
    }

    @Override
    public void undoUpdateStatuses() {
        recordDao.undoUpdateStatuses();
    }

}
