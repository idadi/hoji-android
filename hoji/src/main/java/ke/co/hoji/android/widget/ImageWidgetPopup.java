package ke.co.hoji.android.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;

import ke.co.hoji.R;
import ke.co.hoji.android.helper.HojiBitmap;
import ke.co.hoji.android.helper.ImageCapturer;
import ke.co.hoji.android.helper.ImageReceiver;
import ke.co.hoji.android.helper.ImageRotator;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.response.helper.Image;

public class ImageWidgetPopup extends AndroidWidget implements ImageReceiver {

    private ImageView thumbnail;
    private Button takeButton;
    private boolean resuming = false;
    private String filePath = null;
    private boolean justTakenAnImage = false;
    private HojiBitmap hojiBitmap = null;

    public ImageWidgetPopup(Context context) {
        super(context);
    }

    @Override
    public void receiveImage(HojiBitmap hojiBitmap) {
        justTakenAnImage = true;
        showImage(hojiBitmap);
    }

    @Override
    protected int getLayout() {
        return R.layout.popup_image_widget;
    }

    @Override
    public void loadViews() {
        thumbnail = findViewById(R.id.thumbnail);
        thumbnail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageDialog();
            }
        });
        takeButton = findViewById(R.id.takeButton);
        takeButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                takeImage();
            }
        });
    }

    @Override
    public void wipe() {
        filePath = null;
        thumbnail.setImageDrawable(null);
        hojiBitmap = null;
    }

    @Override
    public void focus() {
        if (getCurrentContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            takeButton.setText(getResources().getText(R.string.take_picture_action));
            takeButton.setEnabled(true);
        } else {
            takeButton.setText(getResources().getText(R.string.no_camera_action));
            takeButton.setEnabled(false);
        }
        takeButton.requestFocus();
    }

    @Override
    public boolean isResuming() {
        return resuming;
    }

    @Override
    public void rendered() {
        resuming = false;
        if (justTakenAnImage) {
            raiseResponseChangedEvent();
        }
        justTakenAnImage = false;
    }

    @Override
    public Object getValue() {
        return filePath;
    }

    @Override
    public boolean isEmpty() {
        return filePath == null;
    }

    @Override
    public Response readResponse() {
        return Response.create(hojiContext, getLiveField(), getValue(), false);
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null) {
            Image value = (Image) response.getActualValue();
            if (value != null) {
                //Load the image from file at this point to avoid clogging memory
                if (value.getFilePath() != null) {
                    value = (Image) Response.create(
                            hojiContext,
                            getLiveField(),
                            value.getFilePath(),
                            false
                    ).getActualValue();
                }
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inMutable = true;
                Bitmap image = BitmapFactory.decodeByteArray(
                        value.getData(),
                        0,
                        value.getData().length,
                        options
                );
                showImage(new HojiBitmap(image, value.getFilePath()));
            } else {
                filePath = null;
            }
        }
    }

    @Override
    public View getPrincipalView() {
        return thumbnail;
    }

    private void takeImage() {
        raiseResponseChangingEvent();
        resuming = true;
        ImageCapturer imageCapturer = (ImageCapturer) getCurrentContext();
        String directoryPath = hojiContext.getRecord().getFieldParent().getId() + File.separator
                + liveField.getField().getId();
        String fileName = hojiContext.getRecord().getUuid();
        imageCapturer.startCapture(this, directoryPath, fileName);
    }

    private void showImage(HojiBitmap hojiBitmap) {
        if (hojiBitmap != null) {
            Bitmap rotated = ImageRotator.rotateImage(
                    hojiBitmap.getBitmap(),
                    hojiBitmap.getFilePath()
            );
            thumbnail.setImageBitmap(rotated);
            filePath = hojiBitmap.getFilePath();
            this.hojiBitmap = new HojiBitmap(rotated, filePath);
        }
    }

    private void showImageDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getCurrentContext());
        ImageView popupImageView = new ImageView(this.getCurrentContext());
        popupImageView.setImageBitmap(hojiBitmap.getBitmap());
        builder.setView(popupImageView);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
