package ke.co.hoji.android.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import ke.co.hoji.R;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.android.adapter.FormArrayAdapter;
import ke.co.hoji.android.task.SearchTask;
import ke.co.hoji.android.listener.OnRecordsLoadedListener;
import ke.co.hoji.android.widget.DateComponent;
import ke.co.hoji.android.widget.DatePickerFragment;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Period;
import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.MainRecord;

/**
 * Created by gitahi on 08/05/15.
 */
public class SearchFragment extends Fragment implements DateComponent {

    private boolean setFrom = true;
    private Date fromDate;
    private Date toDate;

    private MaterialSpinner formSpinner;
    private MaterialSpinner periodSpinner;
    private EditText fromEditText;
    private EditText toEditText;
    private MaterialSpinner stageSpinner;
    private EditText captionEditText;
    private Button searchButton;
    private LinearLayout dateLinearLayout;

    private DatePickerFragment datePickerFragment;

    private ProgressDialog progressDialog;

    private void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
        if (this.fromDate != null) {
            fromEditText.setText(Utils.formatDate(this.fromDate));
        } else {
            fromEditText.setText("");
        }
    }

    private void setToDate(Date toDate) {
        this.toDate = toDate;
        if (this.toDate != null) {
            toEditText.setText(Utils.formatDate(this.toDate));
        } else {
            toEditText.setText("");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        formSpinner = getView().findViewById(R.id.formSpinner);
        periodSpinner = getView().findViewById(R.id.periodSpinner);
        fromEditText = getView().findViewById(R.id.fromEditText);
        toEditText = getView().findViewById(R.id.toEditText);
        setFromDate(new Date());
        setToDate(new Date());
        captionEditText = getView().findViewById(R.id.captionEditText);
        stageSpinner = getView().findViewById(R.id.stageSpinner);
        searchButton = getView().findViewById(R.id.searchButton);
        dateLinearLayout = getView().findViewById(R.id.dateLinearLayout);
        dateLinearLayout.setVisibility(LinearLayout.GONE);
        addListeners();
        loadForms();
        loadPeriods();
        loadStages();
        formSpinner.requestFocus();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.search_fragment, container, false);
    }


    @Override
    public void onResume() {
        super.onResume();
        progressDialog = new ProgressDialog(this.getActivity());
    }


    @Override
    public void onPause() {
        super.onPause();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        if (datePickerFragment != null) {
            datePickerFragment.dismiss();
        }
    }

    private void addListeners() {
        periodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Object selected = periodSpinner.getSelectedItem();
                if (selected instanceof Period && ((Period) selected).getName().equals(Period.BETWEEN)) {
                    dateLinearLayout.setVisibility(LinearLayout.VISIBLE);
                    fromEditText.requestFocus();
                } else {
                    dateLinearLayout.setVisibility(LinearLayout.GONE);
                    stageSpinner.requestFocus();
                }
            }

            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
        searchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                search();
            }
        });
        fromEditText.setKeyListener(null);
        fromEditText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                setFrom = true;
                showDatePickerDialog();
            }
        });
        toEditText.setKeyListener(null);
        toEditText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                setFrom = false;
                showDatePickerDialog();
            }
        });
    }

    private void loadForms() {
        List<Form> forms = new ArrayList<>(((HojiContext) getActivity().getApplication()).getSurvey().getForms());
        FormArrayAdapter adapter = new FormArrayAdapter(
                getActivity(),
                android.R.layout.simple_spinner_item,
                forms
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        formSpinner.setAdapter(adapter);
    }

    private void loadPeriods() {
        List<Period> periods = new ArrayList<>();
        periods.add(new Period(Period.TODAY));
        periods.add(new Period(Period.YESTERDAY));
        periods.add(new Period(Period.THIS_WEEK));
        periods.add(new Period(Period.LAST_WEEK));
        periods.add(new Period(Period.THIS_MONTH));
        periods.add(new Period(Period.LAST_MONTH));
        periods.add(new Period(Period.BETWEEN));
        ArrayAdapter<Period> adapter = new ArrayAdapter<>
                (
                        getActivity(),
                        android.R.layout.simple_spinner_item,
                        periods
                );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        periodSpinner.setAdapter(adapter);
    }

    private void loadStages() {
        List<MainRecord.Stage> stages = new ArrayList<>();
        stages.add(new MainRecord.Stage(getResources().getString(R.string.drafts), MainRecord.Stage.DRAFT));
        stages.add(new MainRecord.Stage(getResources().getString(R.string.outbox), MainRecord.Stage.COMPLETE));
        stages.add(new MainRecord.Stage(getResources().getString(R.string.sent), MainRecord.Stage.UPLOADED));
        ArrayAdapter<MainRecord.Stage> adapter = new ArrayAdapter<>
                (
                        getActivity(),
                        android.R.layout.simple_spinner_item,
                        stages
                );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stageSpinner.setAdapter(adapter);
    }

    private void search() {
        Form form = formSpinner.getSelectedItem() instanceof Form ?
                (Form) formSpinner.getSelectedItem() : null;
        Period period = periodSpinner.getSelectedItem() instanceof Period ?
                (Period) periodSpinner.getSelectedItem() : null;
        MainRecord.Stage stage = stageSpinner.getSelectedItem() instanceof MainRecord.Stage ?
                (MainRecord.Stage) stageSpinner.getSelectedItem() : null;
        boolean testMode = PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getBoolean(SettingsActivity.TEST_MODE, Boolean.FALSE);
        RecordSearch recordSearch = new RecordSearch();
        recordSearch.setForm(form);
        if (period != null) {
            if (period.getName().equals(Period.BETWEEN)) {
                recordSearch.setFrom(fromDate);
                recordSearch.setTo(toDate);
            } else {
                recordSearch.setFrom(period.getFrom());
                recordSearch.setTo(period.getTo());
            }
        }
        recordSearch.setStage(stage != null ? stage.getValue() : null);
        recordSearch.setCaption(captionEditText.getText().toString());
        recordSearch.setTimeStamp(0L);
        recordSearch.setReverse(false);
        recordSearch.setTestMode(testMode);
        String searchTaskId = recordSearch.getStage() + "" + recordSearch.getTimeStamp();
        new SearchTask(searchTaskId, recordSearch, this.getActivity(),
                (OnRecordsLoadedListener) getActivity(), false, progressDialog).execute();
    }

    private void showDatePickerDialog() {
        datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDateComponent(this);
        datePickerFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }

    public void setDate(Date date) {
        if (setFrom) {
            setFromDate(date);
        } else {
            setToDate(date);
        }
    }

    public Date getDate() {
        if (setFrom) {
            return fromDate;
        } else {
            return toDate;
        }
    }
}