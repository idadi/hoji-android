package ke.co.hoji.android.helper;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.IOException;

/**
 * Corrects the orientation of images taken using phone cameras that automatically rotate portrait
 * images to landscape.
 */
public final class ImageRotator {

    /**
     * Check if an image has been erroneously rotated by the phone camera and set it right.
     *
     * @param input    an image returned by the camera app that may have been erroneously rotated.
     * @param filePath the file path to the image.
     * @return the properly oriented image.
     */
    public static Bitmap rotateImage(Bitmap input, String filePath) {
        try {
            ExifInterface exifInterface = new ExifInterface(filePath);
            int orientation = exifInterface.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED
            );
            float rotationAngle = 0;
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotationAngle = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotationAngle = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotationAngle = 270;
                    break;
            }
            if (rotationAngle != 0) {
                Matrix matrix = new Matrix();
                matrix.postRotate(rotationAngle);
                Bitmap rotated = Bitmap.createBitmap(
                        input,
                        0,
                        0,
                        input.getWidth(),
                        input.getHeight(),
                        matrix,
                        true
                );
                input.recycle();
                return rotated;
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return input;
    }
}
