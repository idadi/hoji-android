package ke.co.hoji.android.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

/**
 * Created by gitahi on 23/08/15.
 */
public class JsonService {

    private final Gson gson;

    public JsonService() {
        this.gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();
    }

    public String toJson(Object object) {
        return gson.toJson(object);
    }

    /**
     * Writes directly to network, avoiding consuming memory by holding onto the Json string.
     * @param object, the object to serialise to Json.
     * @param writer, the output stream to write the Json to.
     */
    public void toJson(Object object, Appendable writer) {
        gson.toJson(object, writer);
    }

    public <T> T fromJson(String json, Type type) {
        return gson.fromJson(json, type);
    }
}
