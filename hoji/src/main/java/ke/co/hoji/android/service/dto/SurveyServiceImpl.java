package ke.co.hoji.android.service.dto;

import ke.co.hoji.android.dao.dto.SqliteSurveyDao;
import ke.co.hoji.core.data.dto.Survey;
import ke.co.hoji.core.service.dto.SurveyService;

public class SurveyServiceImpl implements SurveyService {

    private final SqliteSurveyDao surveyDao;

    public SurveyServiceImpl(SqliteSurveyDao surveyDao) {
        this.surveyDao = surveyDao;
    }

    @Override
    public void saveSurvey(Survey survey) {
        surveyDao.saveSurvey(survey);
    }

    @Override
    public Survey getSurveyByCode(String code) {
        return null;
    }

    @Override
    public void deleteSurvey(Integer surveyId) {
        surveyDao.deleteSurvey(surveyId);
    }

}
