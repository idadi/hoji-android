package ke.co.hoji.android.widget;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;

import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.widget.Navigable;
import ke.co.hoji.core.widget.NavigableHelper;
import ke.co.hoji.core.widget.Widget;
import ke.co.hoji.core.widget.WidgetEvent;
import ke.co.hoji.core.widget.WidgetManager;
import ke.co.hoji.core.widget.WidgetRenderer;

/**
 * Created by gitahi on 12/11/14.
 */
public class AndroidWidgetRenderer implements WidgetRenderer {

    /**
     * How long the transition animation should last if navigating normally.
     */
    private static final int TRANSITIONING_ANIMATION_DURATION = 200;

    /**
     * How long the animation should last when we're merely staying on the same widget e.g. if
     * our Activity is resuming.
     */
    private static final int NON_TRANSITIONING_ANIMATION_DURATION = 0;

    /**
     * Constant for outgoing animation i.e. for the widget going out of view.
     */
    private static final int OUTGOING_ANIMATION = 1;

    /**
     * Constant for incoming animation i.e. for the widget coming into view.
     */
    private static final int INCOMING_ANIMATION = 2;

    @Override
    public void render
            (
                    WidgetManager widgetManager,
                    Widget widget,
                    Navigable navigable,
                    NavigableHelper.Direction direction,
                    boolean transitioning,
                    Record record
            ) {
        AndroidWidget incomingWidget = (AndroidWidget) widget;
        AndroidWidget outgoingWidget = (AndroidWidget) navigable.getActiveWidget();
        FieldLabel fieldLabel = (FieldLabel) navigable.getFieldLabel();
        ViewGroup widgetPane = (ViewGroup) navigable.getWidgetContainer();
        Context currentContext = incomingWidget.getCurrentContext();
        if (currentContext instanceof Activity) {
            if (outgoingWidget != null) {
                if (!incomingWidget.isEditable() || !incomingWidget.needsKeyboard()) {
                    InputMethodManager imm = (InputMethodManager) currentContext
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(outgoingWidget.getWindowToken(), 0);
                }
            }
        }
        SwipeAnimationListener swipeAnimationListener = new SwipeAnimationListener
                (
                        widgetManager,
                        navigable,
                        fieldLabel,
                        widgetPane,
                        direction,
                        incomingWidget,
                        outgoingWidget,
                        transitioning,
                        record
                );
        if (outgoingWidget != null) {
            TranslateAnimation outgoingAnimation = getAnimation(OUTGOING_ANIMATION, widgetPane, direction, transitioning);
            outgoingAnimation.setAnimationListener(swipeAnimationListener);
            fieldLabel.startAnimation(outgoingAnimation);
            outgoingWidget.startAnimation(outgoingAnimation);
        } else {
            swipeAnimationListener.onAnimationEnd(null);
        }
    }

    private TranslateAnimation getAnimation(
            int type,
            ViewGroup widgetPane,
            NavigableHelper.Direction direction,
            boolean transitioning
    ) {
        TranslateAnimation animation = null;
        if (type == OUTGOING_ANIMATION) {
            if (direction == NavigableHelper.Direction.FORWARD) {
                animation = new TranslateAnimation(0, -widgetPane.getWidth(), 0, 0);
            } else if (direction == NavigableHelper.Direction.BACKWARD) {
                animation = new TranslateAnimation(0, widgetPane.getWidth(), 0, 0);
            }
        } else if (type == INCOMING_ANIMATION) {
            if (direction == NavigableHelper.Direction.FORWARD) {
                animation = new TranslateAnimation(widgetPane.getWidth(), 0, 0, 0);
            } else if (direction == NavigableHelper.Direction.BACKWARD) {
                animation = new TranslateAnimation(-widgetPane.getWidth(), 0, 0, 0);
            }
            animation.setFillAfter(true);
        }
        if (transitioning) {
            animation.setDuration(TRANSITIONING_ANIMATION_DURATION);
        } else {
            animation.setDuration(NON_TRANSITIONING_ANIMATION_DURATION);
        }
        return animation;
    }

    private boolean autoPopKeyboard(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsActivity.AUTO_POP_KEYBOARD, Boolean.TRUE);
    }

    private class SwipeAnimationListener implements Animation.AnimationListener {

        private final WidgetManager widgetManager;
        private final Navigable navigable;
        private final FieldLabel fieldLabel;
        private final ViewGroup widgetPane;
        private final NavigableHelper.Direction direction;
        private final AndroidWidget incomingWidget;
        private final AndroidWidget outgoingWidget;
        private final boolean transitioning;
        private final Record record;

        SwipeAnimationListener(
                WidgetManager widgetManager,
                Navigable navigable,
                FieldLabel fieldLabel,
                ViewGroup widgetPane,
                NavigableHelper.Direction direction,
                AndroidWidget incomingWidget,
                AndroidWidget outgoingWidget,
                boolean transitioning,
                Record record
        ) {
            this.widgetManager = widgetManager;
            this.navigable = navigable;
            this.fieldLabel = fieldLabel;
            this.widgetPane = widgetPane;
            this.direction = direction;
            this.incomingWidget = incomingWidget;
            this.outgoingWidget = outgoingWidget;
            this.transitioning = transitioning;
            this.record = record;
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            widgetPane.removeAllViews();
            ViewParent parent = incomingWidget.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(incomingWidget);
            }
            widgetPane.addView(incomingWidget);
            TranslateAnimation incomingAnimation = getAnimation(INCOMING_ANIMATION, widgetPane, direction, transitioning);
            fieldLabel.startAnimation(incomingAnimation);
            incomingWidget.startAnimation(incomingAnimation);
            Field field = incomingWidget.getLiveField().getField();
            fieldLabel.setField(field, record, incomingWidget.getHojiContext().getNameOfUser());
            Context currentContext = incomingWidget.getCurrentContext();
            if (currentContext instanceof Activity) {
                if (incomingWidget.isEditable() && incomingWidget.needsKeyboard()) {
                    if (direction == NavigableHelper.Direction.FORWARD) {
                        if (autoPopKeyboard(currentContext)) {
                            InputMethodManager imm = (InputMethodManager) currentContext
                                    .getSystemService(Context.INPUT_METHOD_SERVICE);
                            if (incomingWidget.getPrincipalView().requestFocus()) {
                                imm.showSoftInput(incomingWidget.getPrincipalView(), InputMethodManager.SHOW_IMPLICIT);
                            }
                        }
                    } else {
                        InputMethodManager imm = (InputMethodManager) currentContext
                                .getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(incomingWidget.getWindowToken(), 0);
                    }
                }
            }
            boolean flashNavigable = widgetManager.primeWidget(navigable, incomingWidget);
            navigable.rendered(incomingWidget, navigable.getNavigator().getRecord());
            incomingWidget.rendered();
            if (outgoingWidget != null) {
                outgoingWidget.unrendered();
            }
            if (flashNavigable && transitioning && navigable.flashNavigate()) {
                incomingWidget.getResponseChangedListener().responseChanged(
                        new WidgetEvent(incomingWidget, WidgetEvent.EventCause.SIMULATION)
                );
            }
        }

        @Override
        public void onAnimationStart(Animation animation) {
            //do nothing
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            //do nothing
        }
    }
}
