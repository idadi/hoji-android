package ke.co.hoji.android.widget;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TimePicker;

import androidx.fragment.app.DialogFragment;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by gitahi on 02/03/17.
 */
public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    private DateComponent dateComponent;

    public void setDateComponent(DateComponent dateComponent) {
        this.dateComponent = dateComponent;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        if (dateComponent.getDate() != null) {
            calendar.setTime(dateComponent.getDate());
        }
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        return new TimePickerDialog(getActivity(), this, hour, minute, true);
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
        Date date = getTimeFromPicker(timePicker);
        if (dateComponent instanceof TimeWidget) {
            TimeWidget timeWidget = (TimeWidget) dateComponent;
            timeWidget.setDate(date, true);
        } else {
            dateComponent.setDate(date);
        }
    }

    private Date getTimeFromPicker(TimePicker timePicker) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH),
                timePicker.getCurrentHour(),
                timePicker.getCurrentMinute(),
                0
        );
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
