package ke.co.hoji.android.widget;

import java.util.Date;

/**
 * An arbitrary user interface component that can collect time information
 * i.e date, time or datetime.
 * <p/>
 * Created by gitahi on 15/05/15.
 */
public interface DateComponent {

    /**
     * Set the date.
     *
     * @param date the date to set
     */
    void setDate(Date date);

    /**
     * Get the date.
     *
     * @return the date
     */
    Date getDate();
}
