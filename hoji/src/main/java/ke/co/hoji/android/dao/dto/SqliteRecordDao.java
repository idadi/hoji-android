package ke.co.hoji.android.dao.dto;

import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.android.helper.ImageEncoder;
import ke.co.hoji.core.ResourceManager;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.dto.Record;
import ke.co.hoji.core.data.http.UploadResult;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static ke.co.hoji.core.data.model.MainRecord.Stage;

/**
 * Created by gitahi on 15/09/15.
 */
public class SqliteRecordDao {

    private final SqliteSqlExecutor se;
    private final ke.co.hoji.android.dao.model.SqliteFieldDao fieldDao;
    private final ImageEncoder imageEncoder;

    public SqliteRecordDao(
            SqliteSqlExecutor se,
            ke.co.hoji.android.dao.model.SqliteFieldDao fieldDao,
            ImageEncoder imageEncoder
    ) {
        this.se = se;
        this.fieldDao = fieldDao;
        this.imageEncoder = imageEncoder;
    }

    public List<MainRecord> getMainRecordsByStage(int stage, int limit) throws IOException {
        List<MainRecord> mainRecords = new ArrayList<>();
        String select = "SELECT "
                + "record_uuid, "
                + "form_id, "
                + "caption, "
                + "test, "
                + "search_terms, "
                + "unique_value, "
                + "stage, "
                + "version, "
                + "date_created, "
                + "date_updated, "
                + "date_completed, "
                + "date_uploaded, "
                + "user_id, "
                + "device_id, "
                + "start_accuracy, "
                + "start_address, "
                + "start_age, "
                + "start_latitude, "
                + "start_longitude, "
                + "end_accuracy, "
                + "end_address, "
                + "end_age, "
                + "end_latitude, "
                + "end_longitude, "
                + "form_version, "
                + "app_version, "
                + "id "
                + "FROM record INNER JOIN main_record ON record.uuid = main_record.record_uuid "
                + "WHERE stage = ? "
                + "LIMIT ?";
        Map<String, Object> params = se.createParameterMap();
        params.put("stage", stage);
        params.put("limit", limit);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                MainRecord mainRecord = new MainRecord();
                mainRecord.setUuid(cursor.getString(cursor.getColumnIndex("record_uuid")));
                mainRecord.setCaption(cursor.getString(cursor.getColumnIndex("caption")));
                mainRecord.setSearchTerms(cursor.getString(cursor.getColumnIndex("search_terms")));
                mainRecord.setUniqueValue(cursor.getString(cursor.getColumnIndex("unique_value")));
                mainRecord.setDateCreated(cursor.getLong(cursor.getColumnIndex("date_created")));
                mainRecord.setDateUpdated(cursor.getLong(cursor.getColumnIndex("date_updated")));
                mainRecord.setMain(true);
                mainRecord.setTest(cursor.getInt(cursor.getColumnIndex("test")) > 0);
                mainRecord.setFormId(cursor.getInt(cursor.getColumnIndex("form_id")));
                mainRecord.setStage(cursor.getInt(cursor.getColumnIndex("stage")));
                mainRecord.setDateCompleted(cursor.getLong(cursor.getColumnIndex("date_completed")));
                mainRecord.setDateUploaded(cursor.getLong(cursor.getColumnIndex("date_uploaded")));
                mainRecord.setVersion(cursor.getInt(cursor.getColumnIndex("version")));
                mainRecord.setUserId(cursor.getInt(cursor.getColumnIndex("user_id")));
                mainRecord.setDeviceId(cursor.getString(cursor.getColumnIndex("device_id")));
                mainRecord.setStartLatitude(cursor.getDouble(cursor.getColumnIndex("start_latitude")));
                mainRecord.setStartLongitude(cursor.getDouble(cursor.getColumnIndex("start_longitude")));
                mainRecord.setStartAccuracy(cursor.getDouble(cursor.getColumnIndex("start_accuracy")));
                mainRecord.setStartAge(cursor.getLong(cursor.getColumnIndex("start_age")));
                mainRecord.setStartAddress(cursor.getString(cursor.getColumnIndex("start_address")));
                mainRecord.setEndLatitude(cursor.getDouble(cursor.getColumnIndex("end_latitude")));
                mainRecord.setEndLongitude(cursor.getDouble(cursor.getColumnIndex("end_longitude")));
                mainRecord.setEndAccuracy(cursor.getDouble(cursor.getColumnIndex("end_accuracy")));
                mainRecord.setEndAge(cursor.getLong(cursor.getColumnIndex("end_age")));
                mainRecord.setEndAddress(cursor.getString(cursor.getColumnIndex("end_address")));
                mainRecord.setFormVersion(cursor.getString(cursor.getColumnIndex("form_version")));
                mainRecord.setAppVersion(cursor.getString(cursor.getColumnIndex("app_version")));
                mainRecord.setLiveFields(getLiveFields(mainRecord));
                mainRecord.setMatrixRecords(getMatrixRecords(mainRecord));
                mainRecord.setId(cursor.getString(cursor.getColumnIndex("id")));
                mainRecords.add(mainRecord);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE,
                    null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return mainRecords;
    }

    public void saveMainRecords(List<MainRecord> mainRecords) {
        se.beginTransaction();
        try {
            for (MainRecord mainRecord : mainRecords) {
                saveMainRecord(mainRecord);
            }
            se.setTransactionSuccessful();
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE,
                    null, ex);
            throw new DaoException(ex.getMessage());
        } finally {
            se.endTransaction();
        }
    }

    private void saveMainRecord(MainRecord mainRecord) {
        insertRecord(mainRecord);
        insertMainOrMatrixRecord(mainRecord);
        for (LiveField liveField : mainRecord.getLiveFields()) {
            insertLiveField(liveField, mainRecord, mainRecord);
        }
        if (mainRecord.getMatrixRecords() != null) {
            for (MatrixRecord matrixRecord : mainRecord.getMatrixRecords()) {
                saveMatrixRecord(matrixRecord, mainRecord);
            }
        }
    }

    private void saveMatrixRecord(MatrixRecord matrixRecord, MainRecord mainRecord) {
        insertRecord(matrixRecord);
        insertMainOrMatrixRecord(matrixRecord);
        for (LiveField liveField : matrixRecord.getLiveFields()) {
            insertLiveField(liveField, matrixRecord, mainRecord);
        }
    }

    private void insertRecord(Record record) {
        {
            Map<String, Object> whereParams = se.createParameterMap("uuid", record.getUuid());
            se.delete("record", "uuid = ?", whereParams);
        }
        Map<String, Object> params = se.createParameterMap();
        params.put("uuid", record.getUuid());
        params.put("main", record.isMain());
        params.put("test", record.isTest());
        params.put("caption", record.getCaption());
        params.put("search_terms", record.getSearchTerms());
        params.put("unique_value", record.getUniqueValue());
        params.put("date_created", record.getDateCreated());
        params.put("date_updated", record.getDateUpdated());
        se.insert("record", params);
    }

    private void insertMainOrMatrixRecord(Record record) {
        Map<String, Object> params = se.createParameterMap();
        params.put("record_uuid", record.getUuid());
        if (record.getClass().equals(MainRecord.class)) {
            MainRecord mainRecord = (MainRecord) record;
            params.put("form_id", mainRecord.getFormId());
            params.put("stage", mainRecord.getStage());
            params.put("user_id", mainRecord.getUserId());
            params.put("device_id", mainRecord.getDeviceId());
            params.put("date_completed", mainRecord.getDateCompleted());
            params.put("date_uploaded", mainRecord.getDateUploaded());
            params.put("version", (mainRecord.getVersion()));
            params.put("start_latitude", mainRecord.getStartLatitude());
            params.put("start_longitude", mainRecord.getStartLongitude());
            params.put("start_accuracy", mainRecord.getStartAccuracy());
            params.put("start_age", mainRecord.getStartAge());
            params.put("start_address", mainRecord.getStartAddress());
            params.put("end_latitude", mainRecord.getEndLatitude());
            params.put("end_longitude", mainRecord.getEndLongitude());
            params.put("end_accuracy", mainRecord.getEndAccuracy());
            params.put("end_age", mainRecord.getEndAge());
            params.put("end_address", mainRecord.getEndAddress());
            params.put("form_version", mainRecord.getFormVersion());
            params.put("app_version", mainRecord.getAppVersion());
            se.insert("main_record", params);
        } else if (record.getClass().equals(MatrixRecord.class)) {
            MatrixRecord matrixRecord = (MatrixRecord) record;
            params.put("main_record_uuid", matrixRecord.getMainRecordUuid());
            params.put("field_id", matrixRecord.getFieldId());
            try {
                se.insert("matrix_record", params);
            } catch (SQLiteConstraintException ex) {
                // Do nothing because the field causing constraint violation simply doesn't exist (has since been
                // deleted).
            }
        }
    }

    private void insertLiveField(LiveField liveField, Record record, MainRecord mainRecord) {
        Map<String, Object> params = se.createParameterMap();
        params.put("field_id", liveField.getFieldId());
        params.put("record_uuid", record.getUuid());
        params.put("date_created", liveField.getDateCreated());
        params.put("date_updated", liveField.getDateUpdated());
        params.put("uuid", liveField.getUuid());
        Field field = fieldDao.getFieldById(liveField.getFieldId());
        Object value;
        if (field.getType().isImage()) {
            String base64 = (String) liveField.getValue();
            String directoryPath = mainRecord.getFormId() + File.separator + liveField.getFieldId();
            String fileName = record.getUuid();
            value = imageEncoder.fromBase64ToFile(base64, directoryPath, fileName);
        } else {
            value = liveField.getValue();
        }
        params.put("value", value);
        try {
            se.insert("live_field", params);
        } catch (SQLiteConstraintException ex) {
            // Do nothing because the field causing constraint violation simply doesn't exist (has since been
            // deleted).
        }
    }

    public Long getDateUpdated(String uuid) {
        Long dateUpdated = null;
        String select = "SELECT date_updated FROM record WHERE uuid = ?";
        Map<String, Object> params = se.createParameterMap("uuid", uuid);
        Cursor cursor = se.rawQuery(select, params);
        if (cursor.moveToNext()) {
            dateUpdated = cursor.getLong(cursor.getColumnIndex("date_updated"));
        }
        return dateUpdated;
    }

    public int updateStatuses(Map<String, UploadResult> results) {
        int count = 0;
        List<MainRecord> mainRecords = getCompletedMainRecords(results);
        for (MainRecord mainRecord : mainRecords) {
            UploadResult uploadResult = results.get(mainRecord.getUuid());
            if (mainRecord.getStage() == Stage.COMPLETE
                    && uploadResult != null
                    && uploadResult.getCode() == UploadResult.SUCCESS) {
                mainRecord.setId(uploadResult.getId());
                mainRecord.setDateUploaded(uploadResult.getDateUploaded());
                mainRecord.setStage(Stage.UPLOADED);
                Map<String, Object> params = se.createParameterMap();
                params.put("version", (mainRecord.getVersion() + 1));
                params.put("stage", mainRecord.getStage());
                params.put("date_uploaded", mainRecord.getDateUploaded());
                params.put("id", mainRecord.getId());
                Map<String, Object> whereParams = se.createParameterMap("record_uuid", mainRecord.getUuid());
                se.update("main_record", params, "record_uuid = ?", whereParams);
                ke.co.hoji.core.data.model.Record currentRecord = ResourceManager.getHojiContext().getRecord();
                if (currentRecord != null) {
                    if (mainRecord.getUuid().equals(currentRecord.getUuid())) {
                        ke.co.hoji.core.data.model.MainRecord currentMainRecord
                                = (ke.co.hoji.core.data.model.MainRecord) currentRecord.getDetail();
                        currentMainRecord.setStage(mainRecord.getStage());
                    }
                }
                count++;
            }
        }
        return count;
    }

    public void undoUpdateStatuses() {
        Map<String, Object> params = se.createParameterMap();
        params.put("stage", Stage.COMPLETE);
        params.put("id", null);
        Map<String, Object> whereParams = se.createParameterMap("stage", Stage.UPLOADED);
        se.update("main_record", params, "stage = ?", whereParams);
    }

    private List<MainRecord> getCompletedMainRecords(Map<String, UploadResult> results) {
        List<MainRecord> mainRecords = new ArrayList<>();
        Map<String, Object> params = se.createParameterMap();
        StringBuilder idsPlaceholder = new StringBuilder();
        String[] uuids = results.keySet().toArray(new String[0]);
        for (int index = 0; index < uuids.length; index++) {
            if (idsPlaceholder.length() > 0) {
                idsPlaceholder.append(", ");
            }
            idsPlaceholder.append("?");
            params.put("uuid_" + index, uuids[index]);
        }
        String select = "SELECT record_uuid, stage, version  FROM main_record WHERE record_uuid IN ("
                + idsPlaceholder.toString() + ")";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                MainRecord mainRecord = new MainRecord();
                mainRecord.setUuid(cursor.getString(cursor.getColumnIndex("record_uuid")));
                mainRecord.setVersion(cursor.getInt(cursor.getColumnIndex("version")));
                mainRecord.setStage(cursor.getInt(cursor.getColumnIndex("stage")));
                mainRecords.add(mainRecord);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE,
                    null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return mainRecords;
    }

    private List<MatrixRecord> getMatrixRecords(MainRecord mainRecord) throws IOException {
        List<MatrixRecord> matrixRecords = new ArrayList<>();
        Map<String, Object> params = se.createParameterMap("main_record_uuid", mainRecord.getUuid());
        String select = "SELECT record_uuid, field_id, caption, search_terms, unique_value, date_created, date_updated "
                + "FROM record INNER JOIN matrix_record ON record.uuid = matrix_record.record_uuid "
                + "WHERE main_record_uuid = ? "
                + "ORDER BY date_created";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                MatrixRecord matrixRecord = new MatrixRecord();
                matrixRecord.setUuid(cursor.getString(cursor.getColumnIndex("record_uuid")));
                matrixRecord.setCaption(cursor.getString(cursor.getColumnIndex("caption")));
                matrixRecord.setSearchTerms(cursor.getString(cursor.getColumnIndex("search_terms")));
                matrixRecord.setUniqueValue(cursor.getString(cursor.getColumnIndex("unique_value")));
                matrixRecord.setDateCreated(cursor.getLong(cursor.getColumnIndex("date_created")));
                matrixRecord.setDateUpdated(cursor.getLong(cursor.getColumnIndex("date_updated")));
                matrixRecord.setMain(false);
                matrixRecord.setTest(mainRecord.isTest());
                matrixRecord.setFieldId(cursor.getInt(cursor.getColumnIndex("field_id")));
                matrixRecord.setMainRecordUuid(mainRecord.getUuid());
                matrixRecord.setLiveFields(getLiveFields(matrixRecord));
                matrixRecords.add(matrixRecord);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return matrixRecords;
    }

    private List<LiveField> getLiveFields(Record record) throws IOException {
        List<LiveField> liveFields = new ArrayList<>();
        Map<String, Object> params = se.createParameterMap("record_uuid", record.getUuid());
        String select = "SELECT live_field.uuid, record_uuid, field_id, date_created, date_updated, value "
                + "FROM live_field INNER JOIN field ON live_field.field_id = field.id "
                + "WHERE record_uuid = ? "
                + "ORDER BY ordinal";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                LiveField liveField = new LiveField();
                liveField.setUuid(cursor.getString(cursor.getColumnIndex("uuid")));
                liveField.setFieldId(cursor.getInt(cursor.getColumnIndex("field_id")));
                liveField.setRecordUuid(record.getUuid());
                int columnIndex = cursor.getColumnIndex("value");
                Field field = fieldDao.getFieldById(liveField.getFieldId());
                int dataType = field.getType().getDataType();
                Object columnValue = null;
                if (!cursor.isNull(columnIndex)) {
                    boolean isBlob = (cursor.getType(columnIndex) == Cursor.FIELD_TYPE_BLOB);
                    switch (dataType) {
                        case FieldType.DataType.NULL:
                            columnValue = null;
                            break;
                        case FieldType.DataType.NUMBER:
                            columnValue = !isBlob ? cursor.getLong(columnIndex) : null;
                            break;
                        case FieldType.DataType.DECIMAL:
                            columnValue = !isBlob ? cursor.getDouble(columnIndex) : null;
                            break;
                        case FieldType.DataType.STRING:
                            columnValue = !isBlob ? cursor.getString(columnIndex) : null;
                            break;
                    }
                }
                Object value = columnValue;
                if (field.getType().isImage() && columnValue != null) {
                    value = imageEncoder.fromFileToBase64(
                            (String) columnValue,
                            Utils.parseInteger(field.getMaxValue()),
                            Utils.parseInteger(field.getMinValue())
                    );
                }
                liveField.setValue(value);
                liveField.setDateCreated(cursor.getLong(cursor.getColumnIndex("date_created")));
                liveField.setDateUpdated(cursor.getLong(cursor.getColumnIndex("date_updated")));
                liveFields.add(liveField);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE,
                    null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return liveFields;
    }
}
