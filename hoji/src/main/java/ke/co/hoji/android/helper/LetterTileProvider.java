package ke.co.hoji.android.helper;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;

import ke.co.hoji.R;

/**
 * Used to create a {@link android.graphics.Bitmap} that contains a letter used in the English
 * alphabet or digit, if there is no letter or digit available, a default image
 * is shown instead.
 * <p/>
 * Adapted from: http://stackoverflow.com/questions/23122088/colored-boxed-with-letters-a-la-gmail
 * <p/>
 * Created by gitahi on 11/09/15.
 */
class LetterTileProvider {


    /**
     * The number of available tile colors (see R.array.letter_tile_colors)
     */
    private static final int NUM_OF_TILE_COLORS = 8;

    /**
     * The {@link TextPaint} used to draw the letter onto the tile
     */
    private final TextPaint textPaint = new TextPaint();
    /**
     * The bounds that enclose the letter
     */
    private final Rect rect = new Rect();
    /**
     * The {@link Canvas} to draw on
     */
    private final Canvas canvas = new Canvas();
    /**
     * The first char of the name being displayed
     */
    private final char[] firstChar = new char[1];

    /**
     * The background colors of the tile
     */
    private final TypedArray colors;
    /**
     * The font size used to display the letter
     */
    private final int tileLetterFontSize;
    /**
     * The default image to display
     */
    private final Bitmap defaultBitmap;

    /**
     * Constructor for <code>LetterTileProvider</code>
     *
     * @param context The {@link Context} to use
     */
    public LetterTileProvider(Context context) {
        final Resources res = context.getResources();

        textPaint.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
        textPaint.setColor(Color.WHITE);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setAntiAlias(true);

        colors = res.obtainTypedArray(R.array.letter_tile_colors);
        tileLetterFontSize = res.getDimensionPixelSize(R.dimen.text_size_medium);

        defaultBitmap = BitmapFactory.decodeResource(res, R.drawable.hoji);
    }

    /**
     * @param displayName The name used to create the letter for the tile
     * @param key         The key used to generate the background color for the tile
     * @param width       The desired width of the tile
     * @param height      The desired height of the tile
     * @return A {@link Bitmap} that contains a letter used in the English
     * alphabet or digit, if there is no letter or digit available, a
     * default image is shown instead
     */
    public Bitmap getLetterTile(String displayName, String key, int width, int height) {
        final Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        final char firstChar = displayName.charAt(0);

        final Canvas c = canvas;
        c.setBitmap(bitmap);
        c.drawColor(pickColor(key));

        if (isEnglishLetterOrDigit(firstChar)) {
            this.firstChar[0] = Character.toUpperCase(firstChar);
            textPaint.setTextSize(tileLetterFontSize);
            textPaint.getTextBounds(this.firstChar, 0, 1, rect);
            c.drawText(this.firstChar, 0, 1, width / 2, height / 2
                    + (rect.bottom - rect.top) / 2, textPaint);
        } else {
            c.drawBitmap(defaultBitmap, 0, 0, null);
        }
        return bitmap;
    }

    /**
     * @param c The char to check
     * @return True if <code>c</code> is in the English alphabet or is a digit,
     * false otherwise
     */
    private static boolean isEnglishLetterOrDigit(char c) {
        return 'A' <= c && c <= 'Z' || 'a' <= c && c <= 'z' || '0' <= c && c <= '9';
    }

    /**
     * @param key The key used to generate the tile color
     * @return A new or previously chosen color for <code>key</code> used as the
     * tile background color
     */
    private int pickColor(String key) {
        // String.hashCode() is not supposed to change across java versions, so
        // this should guarantee the same key always maps to the same color
        final int color = Math.abs(key.hashCode()) % NUM_OF_TILE_COLORS;
        try {
            return colors.getColor(color, Color.BLACK);
        } finally {
            colors.recycle();
        }
    }
}
