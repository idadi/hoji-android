package ke.co.hoji.android.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.util.HashMap;
import java.util.Map;

import ke.co.hoji.R;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.widget.Navigable;
import ke.co.hoji.core.widget.NavigableHelper;
import ke.co.hoji.core.widget.Navigator;
import ke.co.hoji.core.widget.WidgetManager;

/**
 * An extension of {@link NavigableHelper} that provides Android-specific functionality.
 * <p/>
 * Created by gitahi on 27/07/15.
 */
public class AndroidNavigableHelper extends NavigableHelper {

    public AndroidNavigableHelper(Navigable navigable, WidgetManager widgetManager) {
        super(navigable, widgetManager);
    }

    public Map<String, Object> getParameters(Context context) {
        Map<String, Object> params = new HashMap<>();
        params.put(Constants.CONTEXT, context);
        return params;
    }

    public void processSkipViolation(Context context) {
        new AlertDialog.Builder(context)
                .setMessage(R.string.switch_branches_message)
                .setTitle(R.string.switch_branches_title)
                .setPositiveButton(R.string.yes_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        next(Navigator.Interruption.CHECK, Navigator.Interruption.EXECUTE, Navigator.Interruption.CHECK);
                    }
                })
                .setNegativeButton(R.string.no_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        next(Navigator.Interruption.CHECK, Navigator.Interruption.ABORT, Navigator.Interruption.CHECK);
                    }
                })
                .create()
                .show();
    }

    public void processValidation(Context context) {
        ValidationResult validationResult = validate();
        if (validationResult.getSignal() == ValidationResult.WARN) {
            new AlertDialog.Builder(context)
                    .setTitle(R.string.validation_error)
                    .setMessage(validationResult.getMessage()
                            + "\n\n" + context.getResources().getString(R.string.continue_anyway))
                    .setPositiveButton(R.string.yes_action, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.EXECUTE);
                        }
                    })
                    .setNegativeButton(R.string.no_action, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.ABORT);
                        }
                    })
                    .create()
                    .show();
        } else if (validationResult.getSignal() == ValidationResult.STOP) {
            new AlertDialog.Builder(context)
                    .setTitle(R.string.validation_error)
                    .setMessage(validationResult.getMessage())
                    .setPositiveButton(R.string.ok_action, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.ABORT);
                        }
                    })
                    .create()
                    .show();
        }
    }
}
