package ke.co.hoji.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.service.model.PropertyService;

/**
 * The application Splash screen.
 * <p/>
 * Created by gitahi on 06/05/15.
 */
public class SplashActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        AndroidHojiContext androidHojiContext = (AndroidHojiContext) getApplication();
        PropertyService propertyService = androidHojiContext.getModelServiceManager().getPropertyService();
        new SplashTask(this, propertyService).execute();
    }

    private class SplashTask extends AsyncTask<Void, Void, Void> {

        private final Activity activity;
        private final PropertyService propertyService;

        SplashTask(Activity activity, PropertyService propertyService) {
            this.activity = activity;
            this.propertyService = propertyService;
        }

        @Override
        protected Void doInBackground(Void... params) {
            signIn();
            return null;
        }

        private void signIn() {
            try {
                Property username = propertyService.getProperty(Constants.Server.USERNAME, null);
                Property password = propertyService.getProperty(Constants.Server.PASSWORD, null);
                if (username != null && password != null
                        && username.getValue() != null && password.getValue() != null
                        && !username.getValue().isEmpty() && !password.getValue().isEmpty()) {
                    Intent intent = new Intent(SplashActivity.this, SurveysActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, InitialActivity.class);
                    startActivity(intent);
                    finish();
                }
            } catch (Exception ex) {
                Toast.makeText(activity, activity.getResources().getText(R.string.outdated_app_version),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}