package ke.co.hoji.android.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import ke.co.hoji.R;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.data.Progress;
import ke.co.hoji.android.service.UploadService;
import ke.co.hoji.core.data.model.MainRecord;

/**
 * Created by gitahi on 15/04/18.
 */
public class OutboxFragment extends RecordFolderFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.outbox_records_actions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sendButton:
                if (records.isEmpty()) {
                    Toast toast = Toast.makeText
                            (
                                    getActivity(),
                                    getActivity().getResources().getText(R.string.nothing_to_send),
                                    Toast.LENGTH_SHORT
                            );
                    toast.show();
                } else {
                    Intent intent = new Intent(getActivity(), UploadService.class);
                    intent.putExtra(Constants.SHOW_UPLOAD_PROGRESS, true);
                    this.getActivity().startService(intent);
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public int getStage() {
        return MainRecord.Stage.COMPLETE;
    }

    public void reportUploadProgress(Progress progress) {
        if (progress.getStatus() == Progress.Status.BUSY) {
            if (progressDialog != null) {
                progressDialog.setMessage(progress.getMessage());
                progressDialog.setCancelable(true);
                if (!progressDialog.isShowing()) {
                    progressDialog.show();
                }
            }
        } else {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog.setCancelable(false);
            }
            Toast toast = Toast.makeText
                    (
                            getActivity(),
                            progress.getMessage(),
                            Toast.LENGTH_SHORT
                    );
            toast.show();
        }
    }
}