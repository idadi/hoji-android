package ke.co.hoji.android.widget;

import android.content.Context;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ke.co.hoji.R;
import ke.co.hoji.core.data.ReferenceLevel;
import ke.co.hoji.core.data.TextInputType;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Reference;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.service.model.ReferenceService;

/**
 * Created by gitahi on 11/06/15.
 */
public class ReferenceWidget extends AndroidWidget {

    private Reference reference = null;
    private List<ReferenceLevel> referenceLevels;

    private ViewGroup referenceLayout;
    private Map<Integer, ReferenceLabel> referenceLabels;

    private ScrollView scrollView;

    private ReferenceLabel firstReferenceLabel;

    public ReferenceWidget(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.reference_widget;
    }

    @Override
    public void loadViews() {
        ReferenceService referenceService = hojiContext.getModelServiceManager().getReferenceService();
        referenceLevels = referenceService.getReferenceLevels();
        referenceLayout = (LinearLayout) findViewById(R.id.referenceLayout);
        referenceLayout.removeAllViews();
        referenceLabels = new HashMap<>();
        for (ReferenceLevel referenceLevel : referenceLevels) {
            ReferenceLabel referenceLabel = new ReferenceLabel(getCurrentContext());
            if (firstReferenceLabel == null) {
                firstReferenceLabel = referenceLabel;
            }
            referenceLayout.addView(referenceLabel);
            referenceLabels.put(referenceLevel.getLevel(), referenceLabel);
        }
        scrollView = findViewById(R.id.referenceScrollView);
    }

    @Override
    public void load() {
        for (ReferenceLevel referenceLevel : referenceLevels) {
            ReferenceLabel label = referenceLabels.get(referenceLevel.getLevel());
            label.setLabel(referenceLevel.getName());
            label.configure(this, referenceLevel.getInputType());
        }
    }

    @Override
    public void wipe() {
        for (ReferenceLabel referenceLabel : referenceLabels.values()) {
            referenceLabel.clear();
        }
        reference = null;
    }

    @Override
    public boolean needsKeyboard() {
        return true;
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null) {
            reference = (Reference) response.getActualValue();
            show(reference);
        }
    }

    @Override
    public void focus() {
        scrollView.scrollTo(0, scrollView.getBottom());
        if (firstReferenceLabel != null) {
            firstReferenceLabel.focus();
        }
    }

    @Override
    public Object getValue() {
        return reference;
    }

    private void search() {
        Reference search = new Reference(null, referenceLevels);
        for (ReferenceLevel referenceLevel : referenceLevels) {
            if (referenceLevel.isEditable()) {
                String value = referenceLabels.get(referenceLevel.getLevel()).getValue();
                int missingAction = referenceLevel.getMissingAction();
                if (value.trim().length() < 1) {
                    if (missingAction == Field.ValidationAction.WARN) {
                        ValidationResult.warn(getCurrentContext().getResources().getString(R.string.no_response));
                        return;
                    } else if (missingAction == Field.ValidationAction.STOP) {
                        ValidationResult.stop(getCurrentContext().getResources().getString(R.string.no_response));
                        return;
                    }
                }
                if (value != null && !value.isEmpty()) {
                    search.addValue(referenceLevel.getLevel(), value);
                }
            }
        }
        ReferenceService referenceService = hojiContext.getModelServiceManager().getReferenceService();
        reference = referenceService.search(search);
        if (reference != null) {
            show(reference);
        } else {
            Toast toast = Toast.makeText(getCurrentContext(),
                    getCurrentContext().getResources()
                            .getText(R.string.nothing_found), Toast.LENGTH_SHORT);
            toast.show();
            for (ReferenceLabel referenceLabel : referenceLabels.values()) {
                if (referenceLabel.isReadOnly()) {
                    referenceLabel.clear();
                }
            }
        }
        raiseResponseChangingEvent();
    }

    private void show(Reference reference) {
        for (ReferenceLevel referenceLevel : referenceLevels) {
            referenceLabels.get(referenceLevel.getLevel())
                    .setValue(reference.getValues().get(referenceLevel.getLevel()));
        }
    }

    private class ReferenceLabel extends LinearLayout {


        private TextView textView;
        private EditText editText;
        private boolean readOnly = false;

        private ReferenceLabel(Context context) {
            super(context);
            LayoutInflater inflater = LayoutInflater.from(context);
            inflater.inflate(R.layout.reference_label, this);
            loadViews();
        }

        private void loadViews() {
            textView = findViewById(R.id.levelNameTextView);
            editText = findViewById(R.id.levelValueEditText);
        }

        private void clear() {
            setValue("");
        }

        private void setValue(String value) {
            editText.setText(value);
        }

        private String getValue() {
            return editText.getText().toString();
        }

        private void setLabel(String value) {
            textView.setText(value);
        }

        boolean isReadOnly() {
            return readOnly;
        }

        private void configure(final ReferenceWidget referenceWidget, Integer inputType) {
            editText.setTag(editText.getKeyListener());
            boolean editable = (inputType != TextInputType.NONE);
            if (editable) {
                readOnly = false;
                editText.setKeyListener((KeyListener) editText.getTag());
                TextWidgetUtil.setInputType(editText, inputType);
                textView.setTextColor(getResources().getColor(R.color.primary_text));
                editText.setTextColor(getResources().getColor(R.color.primary_text));
                editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE
                                || actionId == EditorInfo.IME_ACTION_NEXT) {
                            referenceWidget.search();
                        }
                        return false;
                    }
                });
            } else {
                readOnly = true;
                editText.setKeyListener(null);
                textView.setTextColor(getResources().getColor(R.color.secondary_text));
                editText.setTextColor(getResources().getColor(R.color.secondary_text));
                editText.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast toast = Toast.makeText
                                (
                                        referenceWidget.getCurrentContext(),
                                        getResources().getText(R.string.not_editable),
                                        Toast.LENGTH_SHORT
                                );
                        toast.show();
                    }
                });
            }
            editText.setFocusable(editable);
            editText.setFocusableInTouchMode(editable);
        }

        private void focus() {
            TextWidgetUtil.setFocus(editText, isEditable());
        }
    }

    @Override
    public View getPrincipalView() {
        return referenceLayout;
    }
}
