package ke.co.hoji.android;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import ke.co.hoji.core.data.*;
import ke.co.hoji.core.data.model.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by gitahi on 15/04/15.
 */
public class SqliteSqlExecutor {

    /**
     * Stores an instance of SqliteSqlExecutor. The same instance is returned across multiple calls to {@link
     * SqliteSqlExecutor#getInstance()}.
     */
    protected static SqliteSqlExecutor instance;

    private SQLiteAssetHelper sqliteAssetHelper;

    private SQLiteDatabase database;

    private final Cache cache = new Cache();

    /**
     * A private constructor for SqliteSqlExecutor.
     */
    private SqliteSqlExecutor() {
    }

    /**
     * Get an instance of SqliteSqlExecutor. If none exists a new one is created and returned. Otherwise the existing
     * one is returned.
     *
     * @return an instance of SqliteSqlExecutor
     */
    public static SqliteSqlExecutor getInstance() {
        if (instance == null) {
            instance = new SqliteSqlExecutor();
        }
        return instance;
    }

    public SQLiteAssetHelper getSqliteAssetHelper() {
        return sqliteAssetHelper;
    }

    public void setSqliteAssetHelper(SQLiteAssetHelper sqliteAssetHelper) {
        this.sqliteAssetHelper = sqliteAssetHelper;
    }

    public Map<String, Object> createParameterMap(String column, Object value) {
        if (column == null) {
            return null;
        }
        Map<String, Object> params = new LinkedHashMap<>();
        params.put(column, value);
        return params;
    }

    public Map<String, Object> createParameterMap() {
        return new LinkedHashMap<>();
    }

    /**
     * Gets a {@link ConfigObject} of the given type from the cache. See {@link #put(ConfigObject) }.
     *
     * @param <T>  any subclass of {@link ConfigObject}.
     * @param id   the integer id of the {@link ConfigObject}.
     * @param type the type of {@link ConfigObject}.
     *
     * @return the {@link ConfigObject} from the cache or null if none can be found.
     */
    public <T extends ConfigObject> T get(Integer id, Class<T> type) {
        return cache.get(id, type);
    }

    /**
     * Caches the {@link ConfigObject} passed. See {@link #get(java.lang.Integer, java.lang.Class) }.
     *
     * @param configObject the {@link ConfigObject} to cache.
     */
    public void put(ConfigObject configObject) {
        cache.put(configObject);
    }

    /**
     * Caches the list of {@link ConfigObject}s passed. See {@link #get(java.lang.Integer, java.lang.Class) }.
     *
     * @param configObjects the list of {@link ConfigObject}s to cache.
     */
    public <T extends ConfigObject> void putAll(List<T> configObjects) {
        cache.putAll(configObjects);
    }

    /**
     * Gets all cached {@link ConfigObject}s of a given type. See {@link #put(ConfigObject) } and {@link
     * #get(java.lang.Integer, java.lang.Class) }.
     *
     * @param <T>  any subclass of {@link ConfigObject}.
     * @param type the type of {@link ConfigObject}.
     *
     * @return all cached {@link ConfigObject}s of the given type
     */
    public <T extends ConfigObject> List<T> getAll(Class<T> type) {
        return cache.getAll(type);
    }

    /**
     * Removes a single cached {@link ConfigObject}
     */
    public void remove(ConfigObject configObject) {
        cache.remove(configObject);
    }

    /**
     * Clears the cache.
     */
    public void clearCache() {
        cache.clear();
    }

    private final class Cache {

        private final ConfigObjectCache<FieldType> fieldTypes = new ConfigObjectCache<>();
        private final ConfigObjectCache<Survey> surveys = new ConfigObjectCache<>();
        private final ConfigObjectCache<Choice> choices = new ConfigObjectCache<>();
        private final ConfigObjectCache<ChoiceGroup> choiceGroups = new ConfigObjectCache<>();
        private final ConfigObjectCache<Form> forms = new ConfigObjectCache<>();
        private final ConfigObjectCache<Field> fields = new ConfigObjectCache<>();
        private final ConfigObjectCache<OperatorDescriptor> operatorDescriptors = new ConfigObjectCache<>();
        private final ConfigObjectCache<ReferenceLevel> referenceLevels = new ConfigObjectCache<>();

        private <T extends ConfigObject> T get(Integer id, Class<T> type) {
            if (type == FieldType.class) {
                return type.cast(fieldTypes.get(id));
            } else if (type == Survey.class) {
                return type.cast(surveys.get(id));
            } else if (type == Choice.class) {
                return type.cast(choices.get(id));
            } else if (type == ChoiceGroup.class) {
                return type.cast(choiceGroups.get(id));
            } else if (type == Form.class) {
                return type.cast(forms.get(id));
            } else if (type == Field.class) {
                return type.cast(fields.get(id));
            } else if (type == OperatorDescriptor.class) {
                return type.cast(operatorDescriptors.get(id));
            } else if (type == ReferenceLevel.class) {
                return type.cast(referenceLevels.get(id));
            } else {
                cachingNotSupported(type);
            }
            return null;
        }

        private void put(ConfigObject configObject) {
            if (configObject.getClass() == FieldType.class) {
                fieldTypes.put((FieldType) configObject);
            } else if (configObject.getClass() == Survey.class) {
                surveys.put((Survey) configObject);
            } else if (configObject.getClass() == Choice.class) {
                choices.put((Choice) configObject);
            } else if (configObject.getClass() == ChoiceGroup.class) {
                choiceGroups.put((ChoiceGroup) configObject);
            } else if (configObject.getClass() == Form.class) {
                forms.put((Form) configObject);
            } else if (configObject.getClass() == Field.class) {
                fields.put((Field) configObject);
            } else if (configObject.getClass() == OperatorDescriptor.class) {
                operatorDescriptors.put((OperatorDescriptor) configObject);
            } else if (configObject.getClass() == ReferenceLevel.class) {
                referenceLevels.put((ReferenceLevel) configObject);
            } else {
                cachingNotSupported(configObject.getClass());
            }
        }

        private void remove(ConfigObject configObject) {
            if (configObject.getClass() == FieldType.class) {
                fieldTypes.remove((FieldType) configObject);
            } else if (configObject.getClass() == Survey.class) {
                surveys.remove((Survey) configObject);
            } else if (configObject.getClass() == Choice.class) {
                choices.remove((Choice) configObject);
            } else if (configObject.getClass() == ChoiceGroup.class) {
                choiceGroups.remove((ChoiceGroup) configObject);
            } else if (configObject.getClass() == Form.class) {
                forms.remove((Form) configObject);
            } else if (configObject.getClass() == Field.class) {
                fields.remove((Field) configObject);
            } else if (configObject.getClass() == OperatorDescriptor.class) {
                operatorDescriptors.remove((OperatorDescriptor) configObject);
            } else if (configObject.getClass() == ReferenceLevel.class) {
                referenceLevels.remove((ReferenceLevel) configObject);
            } else {
                cachingNotSupported(configObject.getClass());
            }
        }

        private <T extends ConfigObject> void putAll(List<T> configObjects) {
            for (ConfigObject configObject : configObjects) {
                put(configObject);
            }
        }

        private void cachingNotSupported(Class type) {
            throw new UnsupportedOperationException("Caching not supported for "
                + "objects of type: " + type.getName());
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            super.clone();
            throw new CloneNotSupportedException();
        }

        private <T extends ConfigObject> List<T> getAll(Class<T> type) {
            List<T> all = new ArrayList<>();
            if (type == FieldType.class) {
                all.addAll((List<T>) fieldTypes.values());
            } else if (type == Survey.class) {
                all.addAll((List<T>) surveys.values());
            } else if (type == Choice.class) {
                all.addAll((List<T>) choices.values());
            } else if (type == Form.class) {
                all.addAll((List<T>) forms.values());
            } else if (type == Field.class) {
                all.addAll((List<T>) fields.values());
            } else if (type == OperatorDescriptor.class) {
                all.addAll((List<T>) operatorDescriptors.values());
            } else if (type == ReferenceLevel.class) {
                all.addAll((List<T>) referenceLevels.values());
            } else {
                cachingNotSupported(type);
            }
            return all;
        }

        private void clear() {
            fieldTypes.clear();
            surveys.clear();
            choices.clear();
            choiceGroups.clear();
            forms.clear();
            fields.clear();
            operatorDescriptors.clear();
            referenceLevels.clear();
        }
    }

    public Cursor rawQuery(String query, Map<String, Object> params) {
        return getDatabase().rawQuery(query, createArrayValues(params));
    }

    public void insert(String table, Map<String, Object> values) {
        getDatabase().insertOrThrow(table, null, createContentValues(values));
    }

    public void update(String table, Map<String, Object> values, String whereClause, Map<String, Object> whereParams) {
        getDatabase().update(table, createContentValues(values), whereClause, createArrayValues(whereParams));
    }

    public boolean exists(String select, int referenceId) {
        boolean exists = false;
        Map<String, Object> params = createParameterMap("id", referenceId);
        Cursor cursor = null;
        try {
            cursor = getDatabase().rawQuery(select, createArrayValues(params));
            while (cursor.moveToNext()) {
                exists = cursor.getInt(0) == 1;
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteSqlExecutor.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return exists;
    }

    public void delete(String table, String whereClause, Map<String, Object> whereParams) {
        getDatabase().delete(table, whereClause, createArrayValues(whereParams));
    }

    public void beginTransaction() {
        getDatabase().beginTransaction();
    }

    public void setTransactionSuccessful() {
        getDatabase().setTransactionSuccessful();
    }

    public void endTransaction() {
        getDatabase().endTransaction();
    }

    private SQLiteDatabase getDatabase() {
        if (database == null || !database.isOpen()) {
            database = getSqliteAssetHelper().getWritableDatabase();
            database.execSQL("PRAGMA foreign_keys=ON;");
        }
        return database;
    }

    private String[] createArrayValues(Map<String, Object> params) {
        if (params == null) {
            return null;
        }
        List<String> stringValues = new ArrayList<String>();
        for (Object param : params.values()) {
            stringValues.add(param != null ? param.toString() : null);
        }
        return stringValues.toArray(new String[0]);
    }

    private ContentValues createContentValues(Map<String, Object> params) {
        ContentValues values = new ContentValues();
        for (String column : params.keySet()) {
            putValue(column, params.get(column), values);
        }
        return values;
    }

    private void putValue(String key, Object value, ContentValues values) {
        if (value == null) {
            values.putNull(key);
        } else {
            Object type = value.getClass();
            if (Byte.class.equals(type)) {
                values.put(key, (Byte) value);
            } else if (Integer.class.equals(type)) {
                values.put(key, (Integer) value);
            } else if (Float.class.equals(type)) {
                values.put(key, (Float) value);
            } else if (Short.class.equals(type)) {
                values.put(key, (Short) value);
            } else if (byte[].class.equals(type)) {
                values.put(key, (byte[]) value);
            } else if (String.class.equals(type)) {
                values.put(key, (String) value);
            } else if (Double.class.equals(type)) {
                values.put(key, (Double) value);
            } else if (Long.class.equals(type)) {
                values.put(key, (Long) value);
            } else if (BigDecimal.class.equals(type)) {
                values.put(key, ((BigDecimal) value).doubleValue());
            } else if (Boolean.class.equals(type)) {
                values.put(key, (Boolean) value);
            } else {
                throw new RuntimeException("Unsupported");
            }
        }
    }

    private void close() {
        getDatabase().close();
    }
}
