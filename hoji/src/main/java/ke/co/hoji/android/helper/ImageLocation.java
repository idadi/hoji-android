package ke.co.hoji.android.helper;

import android.net.Uri;

/**
 * Provides a way to manage image location. This class exists because there doesn't seem to be a way
 * to get the proper file path from the {@link Uri}.
 * <p>
 * The proper file path might point to a symlink like "/storage/emulated/0/" while the Uri returns
 * something like "/external_files/". Storing the later as the path in the database results in
 * FNEs.
 */
public final class ImageLocation {

    /**
     * The URI for the image file.
     */
    private final Uri uri;

    /**
     * The absolute path to the image file.
     */
    private final String path;

    public ImageLocation(Uri uri, String path) {
        this.uri = uri;
        this.path = path;
    }

    public Uri getUri() {
        return uri;
    }

    public String getPath() {
        return path;
    }
}
