package ke.co.hoji.android.widget;

import java.util.LinkedHashMap;

import ke.co.hoji.core.data.Choice;

/**
 * General purpose methods to support various varieties of multiple choice widgets.
 * <p>
 * Created by gitahi on 8/25/17.
 */

class MultipleChoiceWidgetUtil {

    /**
     * Count the number of checked cho
     *
     * @param choiceMap the map of all choices and their checked status.
     * @return the number of checked choices.
     */
    public static Comparable getComparable(LinkedHashMap<Choice, Boolean> choiceMap) {
        long comparable = 0;
        if (choiceMap != null) {
            for (Boolean value : choiceMap.values()) {
                if (value) {
                    comparable++;
                }
            }
        }
        return comparable;
    }

    /**
     * Get the last loner {@link Choice} that is checked, along with other choices as well.
     *
     * @param choiceMap the map of all choices and their checked status.
     * @return the accompanied choice or null.
     */
    public static Choice getAccompaniedLoner(LinkedHashMap<Choice, Boolean> choiceMap) {
        if (choiceMap != null) {
            int numberChecked = 0;
            Choice checkedLoner = null;
            for (Choice choice : choiceMap.keySet()) {
                if (choiceMap.get(choice)) {
                    numberChecked++;
                    if (choice.isLoner()) {
                        checkedLoner = choice;
                    }
                }
            }
            if (checkedLoner != null && numberChecked > 1) {
                return checkedLoner;
            }
        }
        return null;
    }
}
