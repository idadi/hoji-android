package ke.co.hoji.android.dao.model;

import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.Choice;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by gitahi on 15/04/15.
 */
public class SqliteChoiceDao {

    private final SqliteSqlExecutor se;

    public SqliteChoiceDao(SqliteSqlExecutor se) {
        this.se = se;
    }

    public Choice saveChoice(Choice choice) {
        saveChoices(Collections.singleton(choice));
        return choice;
    }

    public Set<Choice> saveChoices(Collection<Choice> choices) {
        se.beginTransaction();
        try {
            for (Choice choice : choices) {
                boolean exists = choiceExists(choice.getId());
                Map<String, Object> params = se.createParameterMap();
                if (!exists) {
                    params.put("id", choice.getId());
                }
                params.put("name", choice.getName());
                params.put("description", choice.getDescription());
                params.put("scale", choice.getScale());
                params.put("parent_id", choice.getParent() != null ? choice.getParent().getId() : null);
                params.put("loner", choice.isLoner());
                if (!exists) {
                    se.insert("choice", params);
                } else {
                    Map<String, Object> whereParams = se.createParameterMap("id", choice.getId());
                    se.update("choice", params, "id" + " = ?", whereParams);
                }
            }
            se.setTransactionSuccessful();
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteChoiceDao.class.getName()).log(Level.SEVERE,
                null, ex);
            throw new DaoException(ex.getMessage());
        } finally {
            se.endTransaction();
        }
        return new HashSet<>(choices);
    }

    private boolean choiceExists(int choiceId) {
        String select = "SELECT COUNT(id) FROM choice WHERE id = ?";
        return se.exists(select, choiceId);
    }
}
