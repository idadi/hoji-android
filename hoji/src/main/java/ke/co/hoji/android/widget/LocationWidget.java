package ke.co.hoji.android.widget;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.location.LocationRequest;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.activity.DataEntryActivity;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.data.LocationWrapper;
import ke.co.hoji.android.listener.OnGpsStatusChangedListener;
import ke.co.hoji.android.widget.helper.LocationComponent;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.response.helper.HojiLocation;
import ke.co.hoji.core.response.Response;

/**
 * A {@link ke.co.hoji.core.widget.Widget} for capturing location data..
 */
public class LocationWidget
        extends
        AndroidWidget
        implements
        LocationComponent,
        OnGpsStatusChangedListener {

    private EditText editText;
    private HojiLocation hojiLocation;

    private ProgressDialog progressDialog;
    private boolean readyToUse;

    /**
     * Allows this Widget to accept the last known location once it has obtained a location for
     * itself, but not before. This means that once it has obtained a location, the Widget will not
     * try to get a new fix if a good enough one is already available. However, this entire thing
     * resets when the Widget is navigated away from.
     */
    private boolean acceptsLastKnownLocation = false;

    public LocationWidget(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.location_widget;
    }

    @Override
    public void loadViews() {
        editText = findViewById(R.id.locationEditText);
        editText.setKeyListener(null);
        editText.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                captureLocation();
            }
        });
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null) {
            setHojiLocation((HojiLocation) response.getActualValue(), false);
        }
    }

    public void setHojiLocation(HojiLocation hojiLocation, boolean go) {
        this.hojiLocation = hojiLocation;
        if (this.hojiLocation != null) {
            if (hojiLocation.getAddress() != null) {
                editText.setText(hojiLocation.getAddress());
            } else {
                editText.setText(hojiLocation.getLongitude() + ", " + hojiLocation.getLatitude());
            }
        } else {
            editText.setText(getResources().getText(R.string.tap_for_location));
        }
        focus();
        if (go) {
            raiseResponseChangedEvent();
        }
    }

    @Override
    public Object getValue() {
        return hojiLocation;
    }

    @Override
    public void focus() {
        TextWidgetUtil.setFocus(editText, isEditable());
    }

    @Override
    public void wipe() {
        setHojiLocation(null, false);
    }

    @Override
    public Comparable getComparable() {
        if (hojiLocation != null) {
            return hojiLocation.getAccuracy();
        }
        return null;
    }

    @Override
    public Field.ValidationValue getMin(Record record) {
        return null;
    }

    @Override
    public Field.ValidationValue getMax(Record record) {
        return null;
    }

    @Override
    public View getPrincipalView() {
        return editText;
    }

    @Override
    public void unrendered() {
        acceptsLastKnownLocation = false;
    }

    @Override
    public boolean isReadyToUse() {
        return readyToUse;
    }

    @Override
    public void setReadyToUse(boolean readyToUse) {
        this.readyToUse = readyToUse;
    }

    @Override
    public boolean acceptsLastKnownLocation() {
        return acceptsLastKnownLocation;
    }

    @Override
    public void updateLocation(LocationWrapper locationWrapper, boolean lastKnown) {
        AndroidHojiContext androidHojiContext = (AndroidHojiContext) getCurrentContext().getApplicationContext();
        androidHojiContext.setLocationWrapper(locationWrapper);
        if (!lastKnown) {
            acceptsLastKnownLocation = true;
        }
        if (!isReadyToUse()) {
            return;
        }
        int gpsQuality = checkGpsQuality(locationWrapper);
        if (gpsQuality == GpsQuality.GOOD) {
            useLocation(locationWrapper);
        } else {
            showGpsProgressDialog(locationWrapper);
        }
    }

    @Override
    public void onGpsReady(LocationWrapper locationWrapper) {
        HojiLocation hojiLocation = null;
        if (locationWrapper != null) {
            hojiLocation = new HojiLocation(
                    locationWrapper.getLatitude(),
                    locationWrapper.getLongitude(),
                    locationWrapper.getAltitude(),
                    locationWrapper.getAccuracy(),
                    locationWrapper.getAddress()
            );
        }
        setHojiLocation(hojiLocation, true);
    }

    @Override
    public void onGpsAborted(LocationWrapper locationWrapper) {
    }

    public void useLocation(LocationWrapper locationWrapper) {
        AndroidHojiContext androidHojiContext = (AndroidHojiContext) getCurrentContext().getApplicationContext();
        dismissGpsProgressDialog();
        androidHojiContext.pauseLocationService(this);
        onGpsReady(locationWrapper);
    }

    @Override
    public ValidationResult validate(Record record) {
        boolean empty = isEmpty();
        {
            int missingAction = getLiveField().getField().getMissingAction();
            if (missingAction != Field.ValidationAction.NONE) {
                if (empty) {
                    if (missingAction == Field.ValidationAction.WARN) {
                        return ValidationResult.warn(getCurrentContext().getResources().getString(R.string.no_response));
                    } else if (missingAction == Field.ValidationAction.STOP) {
                        return ValidationResult.stop(getCurrentContext().getResources().getString(R.string.no_response));
                    }
                }
            }

        }
        if (!empty) {
            Field.ValidationValue max = getMin(record);
            if (max != null) {
                int maxAction = max.getAction();
                Comparable value = getComparable();
                if (value != null) {
                    int compareTo = value.compareTo(max.getValue());
                    if (compareTo < 0) {
                        if (maxAction == Field.ValidationAction.WARN) {
                            return ValidationResult.warn(getCurrentContext().getResources().getString(R.string.response_exceeds_max, max.getValue()));
                        } else if (maxAction == Field.ValidationAction.STOP) {
                            return ValidationResult.stop(getCurrentContext().getResources().getString(R.string.response_exceeds_max, max.getValue()));
                        }
                    }
                }
            }
        }
        return ValidationResult.okay();
    }

    private int checkGpsQuality(LocationWrapper locationWrapper) {
        if (locationWrapper == null) {
            return GpsQuality.NULL;
        }
        Integer minGpsAccuracy = getMinAccuracy();
        int gpsAccuracy = (int) Math.ceil(locationWrapper.getAccuracy());
        if (minGpsAccuracy >= 0 && gpsAccuracy > minGpsAccuracy) {
            return GpsQuality.INACCURATE;
        }
        Long gpsShelfLife = getMaxShelfLife();
        if (gpsShelfLife == null || gpsShelfLife < 1) {
            gpsShelfLife = DEFAULT_GPS_SHELF_LIFE;
        }
        gpsShelfLife = gpsShelfLife * 60 * 1000;
        long gpsAge = new Date().getTime() - locationWrapper.getTime();
        if (gpsAge > gpsShelfLife) {
            return GpsQuality.STALE;
        }
        return GpsQuality.GOOD;
    }

    private int getMinAccuracy() {
        int minAccuracy = -1;
        Integer minValue = Utils.parseInteger(liveField.getField().getMinValue());
        if (minValue != null) {
            minAccuracy = minValue;
        }
        return minAccuracy;
    }

    private long getMaxShelfLife() {
        long maxShelfLife = DEFAULT_GPS_SHELF_LIFE;
        Integer maxValue = Utils.parseInteger(liveField.getField().getMaxValue());
        if (maxValue != null) {
            maxShelfLife = maxValue;
        }
        return maxShelfLife;
    }

    private boolean isLocationMandatory() {
        boolean mandatory = true;
        if (getLiveField().getField().getMissingAction() != ValidationResult.STOP) {
            mandatory = false;
        } else {
            if (getMinAccuracy() == -1 || Field.ValidationAction.STOP != Field.ValidationAction.STOP) {
                mandatory = false;
            }
        }
        return mandatory;
    }

    private void showGpsProgressDialog(final LocationWrapper locationWrapper) {
        DataEntryActivity dataEntryActivity = (DataEntryActivity) getCurrentContext();
        progressDialog = dataEntryActivity.getLocationWidgetProgressDialog(
                isLocationMandatory(),
                locationWrapper,
                this
        );
        String title;
        String message = null;
        int gpsQuality = checkGpsQuality(locationWrapper);
        if (gpsQuality == GpsQuality.NULL) {
            title = getResources().getString(R.string.acquiring_location_title);
            message = getResources().getString(R.string.please_wait);
        } else {
            String locationAddress = locationWrapper.getAddress();
            int locationAccuracy = (int) Math.ceil(locationWrapper.getAccuracy());
            title = !StringUtils.isBlank(locationAddress)
                    ? locationAddress
                    : getResources().getString(R.string.improving_location_accuracy_title);
            if (gpsQuality == GpsQuality.STALE) {
                message = getResources().getString(R.string.stale_location_message);
            } else if (gpsQuality == GpsQuality.INACCURATE) {
                message = getResources().getString(
                        R.string.improving_location_accuracy_message,
                        locationAccuracy,
                        getMinAccuracy()
                );
            }
        }
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        if (!dataEntryActivity.isFinishing()) {
            if (!progressDialog.isShowing()) {
                try {
                    progressDialog.show();
                } catch (Exception ex) {
                    // Do nothing
                }
            }
        }
    }

    private void dismissGpsProgressDialog() {
        Activity activity = (Activity) getCurrentContext();
        if (!activity.isFinishing()) {
            if (progressDialog != null && progressDialog.isShowing()) {
                // Catch IAE just in case the activity was destroyed. This seems to be necessary
                // in spite checking isFinishing() above.
                try {
                    progressDialog.dismiss();
                } catch (IllegalArgumentException ex) {
                    // Do nothing
                }
            }
        }
    }

    private void captureLocation() {
        AndroidHojiContext androidHojiContext = (AndroidHojiContext) getCurrentContext().getApplicationContext();
        if (androidHojiContext.isLocationEnabled()) {
            this.setReadyToUse(true);
            int minAccuracy = getMinAccuracy();
            int priority = LocationRequest.PRIORITY_HIGH_ACCURACY;
            if (minAccuracy == -1 || minAccuracy > Constants.HIGH_ACCURACY_THRESHOLD) {
                priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
            }
            androidHojiContext.startLocationService(this, priority);
        } else {
            DataEntryActivity dataEntryActivity = (DataEntryActivity) getCurrentContext();
            String fieldName = getResources().getString(R.string.field_for_location_request);
            if (isLocationMandatory()) {
                dataEntryActivity.enableLocationMandatory(fieldName);
            } else {
                dataEntryActivity.enableLocationNonMandatory(fieldName);
            }
        }
    }
}
