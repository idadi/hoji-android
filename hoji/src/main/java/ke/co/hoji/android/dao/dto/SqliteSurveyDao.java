package ke.co.hoji.android.dao.dto;

import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.dto.Survey;

import java.util.Map;

/**
 * Created by gitahi on 27/08/15.
 */
public class SqliteSurveyDao {

    private final SqliteSqlExecutor se;

    public SqliteSurveyDao(SqliteSqlExecutor se) {
        this.se = se;
    }

    public Survey saveSurvey(Survey survey) {
        if (!surveyExists(survey.getId())) {
            return createSurvey(survey);
        } else {
            return modifySurvey(survey);
        }
    }

    private Survey createSurvey(Survey survey) {
        Map<String, Object> params = se.createParameterMap();
        params.put("id", survey.getId());
        params.put("code", survey.getCode());
        params.put("name", survey.getName());
        params.put("user_id", survey.getUserId());
        params.put("enabled", survey.isEnabled());
        params.put("access", survey.getAccess());
        params.put("free", survey.isFree());
        params.put("status", survey.getStatus());
        se.insert("survey", params);
        return survey;
    }

    private Survey modifySurvey(Survey survey) {
        Map<String, Object> params = se.createParameterMap();
        Map<String, Object> whereParams = se.createParameterMap("id", survey.getId());
        params.put("code", survey.getCode());
        params.put("name", survey.getName());
        params.put("user_id", survey.getUserId());
        params.put("enabled", survey.isEnabled());
        params.put("access", survey.getAccess());
        params.put("free", survey.isFree());
        params.put("status", survey.getStatus());
        se.update("survey", params, "id = ?", whereParams);
        return survey;
    }

    public void deleteSurvey(Integer surveyId) {
        Map<String, Object> whereParams = se.createParameterMap("id", surveyId);
        se.delete("survey", "id = ?", whereParams);
    }

    private boolean surveyExists(int surveyId) {
        String select = "SELECT count(id) FROM survey WHERE id = ?";
        return se.exists(select, surveyId);
    }

}
