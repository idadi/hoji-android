package ke.co.hoji.android.service;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import ke.co.hoji.android.data.Constants;
import ke.co.hoji.core.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by gitahi on 13/08/15.
 */
public class AddressService extends IntentService {

    private ResultReceiver resultReceiver;

    public AddressService() {
        super("AddressService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Location location = intent.getParcelableExtra(Constants.LOCATION_DATA);
        resultReceiver = intent.getParcelableExtra(Constants.RESULT_RECEIVER);
        List<Address> addresses = null;
        String TAG = "AddressService";
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation
                    (
                            location.getLatitude(),
                            location.getLongitude(),
                            1
                    );
        } catch (IOException ex) {
            Log.e(TAG, "Unable to retrieve address. Service unavailable.", ex);
        } catch (IllegalArgumentException ex) {
            Log.e(TAG, "Unable to retrieve address. Invalid values " + "(" + location.getLatitude() + ", "
                    + location.getLongitude() + ")", ex);
        }

        if (addresses == null || addresses.isEmpty()) {
            deliverAddressToReceiver(Constants.FAILURE, null, location);
        } else {
            Address address = addresses.get(0);
            ArrayList<String> addressTokens = new ArrayList<>();

            if (address.getMaxAddressLineIndex() > 0) {
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressTokens.add(address.getAddressLine(i));
                }
            } else {
                try {
                    addressTokens.add(address.getAddressLine(0));
                } catch (Exception ex) {
                    Log.e(TAG, "No address in address lines.", ex);
                }
            }

            int n = address.getMaxAddressLineIndex();
            for (int i = 0; i < n; i++) {
                addressTokens.add(address.getAddressLine(i));
            }
            deliverAddressToReceiver(Constants.SUCCESS, Utils.string(addressTokens), location);
        }
    }

    private void deliverAddressToReceiver(int resultCode, String message, Location location) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.RESULT_DATA, message);
        bundle.putParcelable(Constants.LOCATION_DATA, location);
        resultReceiver.send(resultCode, bundle);
    }
}
