package ke.co.hoji.android.service.model;

import ke.co.hoji.android.dao.model.SqliteTranslationDao;
import ke.co.hoji.core.data.Translatable;
import ke.co.hoji.core.data.model.Translation;
import ke.co.hoji.core.service.model.TranslationService;

public class TranslationServiceImpl implements TranslationService {

    private final SqliteTranslationDao translationDao;

    public TranslationServiceImpl(SqliteTranslationDao translationDao) {
        this.translationDao = translationDao;
    }

    @Override
    public String translate(Translatable translatable, Integer languageId, String component, String untranslated) {
        Translation translation = translationDao.findTranslation(translatable, languageId, component);
        return translation != null ? translation.getValue() : untranslated;
    }

    @Override
    public void clearCache() {

    }

}
