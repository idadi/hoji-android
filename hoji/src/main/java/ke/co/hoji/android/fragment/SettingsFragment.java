package ke.co.hoji.android.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatDelegate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.task.DeleteProjectTask;
import ke.co.hoji.android.widget.DeleteSurveyButton;
import ke.co.hoji.core.data.PageInfo;
import ke.co.hoji.core.data.model.Language;
import ke.co.hoji.core.service.model.LanguageService;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    /**
     * Map language names to laguage ids here so it's easy to render the name of the language
     * from the id.
     */
    private Map<String, String> languageMap = new HashMap<>();

    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        loadLanguages();
        setProjectLanguagePreference(getPreferenceScreen().getSharedPreferences());
        setDateTimeFormatPreference(getPreferenceScreen().getSharedPreferences());
        setPageSizePreference(getPreferenceScreen().getSharedPreferences());
        EditTextPreference pageSizePreference = (EditTextPreference)
                findPreference(SettingsActivity.PAGE_SIZE);
        pageSizePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (newValue != null && !"".equals(newValue)) {
                    int pageSize = Integer.valueOf(newValue.toString());
                    return pageSize > 0 && pageSize <= 100;
                }
                return false;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout view = (LinearLayout) super.onCreateView(inflater, container, savedInstanceState);
        DeleteSurveyButton deleteSurveyButton = new DeleteSurveyButton
                (getActivity(), R.layout.delete_survey_button);
        view.addView(deleteSurveyButton);
        deleteSurveyButton.getButton().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new AlertDialog.Builder(SettingsFragment.this.getActivity())
                        .setTitle(getResources().getString(R.string.delete_survey_title))
                        .setMessage(getResources().getString(R.string.delete_survey_message))
                        .setPositiveButton(R.string.yes_action, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                deleteSurvey();
                            }
                        })
                        .setNegativeButton(R.string.no_action, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //Do nothing
                            }
                        })
                        .create()
                        .show();
            }
        });
        return view;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SettingsActivity.PAGE_SIZE)) {
            setPageSizePreference(sharedPreferences);
        } else if (key.equals(SettingsActivity.TEST_MODE)) {
            setTestModePreference(sharedPreferences);
        } else if (key.equals(SettingsActivity.DATE_TIME_FORMAT)) {
            setDateTimeFormatPreference(sharedPreferences);
        } else if (key.equals(SettingsActivity.PROJECT_LANGUAGE)) {
            setProjectLanguagePreference(sharedPreferences);
        } else if (key.equals(SettingsActivity.BATTERY_SAVER)) {
            Preference bSaver = findPreference(SettingsActivity.BATTERY_SAVER);
            if (bSaver != null) {
                boolean tMode = sharedPreferences.getBoolean(SettingsActivity.BATTERY_SAVER, false);
                if (tMode) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                }
                AndroidHojiContext hojiContext = (AndroidHojiContext) getActivity().getApplication();
                hojiContext.getWidgetManager().clearWidgetCache();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        progressDialog = new ProgressDialog(this.getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        if (progressDialog != null
                && !getActivity().isFinishing()
                && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void setTestModePreference(SharedPreferences sharedPreferences) {
        Preference testMode = findPreference(SettingsActivity.TEST_MODE);
        if (testMode != null) {
            boolean tMode = sharedPreferences.getBoolean(SettingsActivity.TEST_MODE, false);
            String summary;
            String testModeMessageTitle;
            String testModeMessageContent;
            if (tMode) {
                summary = getResources().getString(R.string.test_mode_on_summary);
                testModeMessageTitle = getResources().getString(R.string.test_mode_on_message_title);
                testModeMessageContent = getResources().getString(R.string.test_mode_on_message_content);
            } else {
                summary = getResources().getString(R.string.test_mode_off_summary);
                testModeMessageTitle = getResources().getString(R.string.test_mode_off_message_title);
                testModeMessageContent = getResources().getString(R.string.test_mode_off_message_content);
            }
            testMode.setSummary(summary);
            new AlertDialog.Builder(SettingsFragment.this.getActivity())
                    .setTitle(testModeMessageTitle)
                    .setMessage(testModeMessageContent)
                    .setPositiveButton(R.string.ok_action, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //Do nothing;
                        }
                    })
                    .create()
                    .show();
        }
    }

    private void loadLanguages() {
        AndroidHojiContext androidHojiContext = (AndroidHojiContext) getActivity().getApplication();
        LanguageService languageService = androidHojiContext.getModelServiceManager().getLanguageService();
        List<Language> languages = new ArrayList<>();
        Language defaultLanguage = new Language(
                Integer.parseInt(Constants.DEFAULT_LANGUAGE_ID),
                getResources().getString(R.string.default_language)
        );
        languages.add(defaultLanguage);
        List<Language> configuredLanguages = languageService.findAllLanguages();
        Collections.sort(configuredLanguages);
        languages.addAll(configuredLanguages);
        String[] languageIds = new String[languages.size()];
        String[] languageNames = new String[languages.size()];
        int i = 0;
        for (Language language : languages) {
            String languageId = language.getId().toString();
            String languageName = language.getName();
            languageMap.put(languageId, languageName);
            languageIds[i] = languageId;
            languageNames[i] = languageName;
            i++;
        }
        ListPreference projectLanguage = (ListPreference) findPreference(SettingsActivity.PROJECT_LANGUAGE);
        projectLanguage.setEntries(languageNames);
        projectLanguage.setEntryValues(languageIds);
        projectLanguage.setDefaultValue(Constants.DEFAULT_LANGUAGE_ID);
    }

    private void setProjectLanguagePreference(SharedPreferences sharedPreferences) {
        Preference projectLanguage = findPreference(SettingsActivity.PROJECT_LANGUAGE);
        if (projectLanguage != null) {
            String language = sharedPreferences.getString(SettingsActivity.PROJECT_LANGUAGE, Constants.DEFAULT_LANGUAGE_ID);
            projectLanguage.setSummary(languageMap.get(language));
        }
    }

    private void setDateTimeFormatPreference(SharedPreferences sharedPreferences) {
        Preference dateTimeFormat = findPreference(SettingsActivity.DATE_TIME_FORMAT);
        if (dateTimeFormat != null) {
            String format = sharedPreferences.getString(SettingsActivity.DATE_TIME_FORMAT, "1");
            String summary = getResources().getString(R.string.date_time_format_duration);
            switch (format) {
                case "2":
                    summary = getResources().getString(R.string.date_time_format_date_only);
                    break;
                case "3":
                    summary = getResources().getString(R.string.date_time_format_time_only);
                    break;
                case "4":
                    summary = getResources().getString(R.string.date_time_format_date_and_time);
                    break;
            }
            dateTimeFormat.setSummary(summary);
        }
    }

    private void setPageSizePreference(SharedPreferences sharedPreferences) {
        Preference pageSize = findPreference(SettingsActivity.PAGE_SIZE);
        if (pageSize != null) {
            String noOfPages = sharedPreferences.getString(SettingsActivity.PAGE_SIZE,
                    String.valueOf(PageInfo.PAGE_SIZE));
            String summary = getResources().getQuantityString(R.plurals.page_size_summary,
                    Integer.valueOf(noOfPages), noOfPages);
            pageSize.setSummary(summary);
        }
    }

    private void deleteSurvey() {
        AndroidHojiContext hojiContext = (AndroidHojiContext) getActivity().getApplication();
        DeleteProjectTask deleteProjectTask = new DeleteProjectTask(
                hojiContext.getSurvey(),
                hojiContext.getModelServiceManager().getConfigService(),
                hojiContext.getWidgetManager(),
                (SettingsActivity) getActivity(),
                progressDialog
        );
        deleteProjectTask.execute();
    }
}
