package ke.co.hoji.android.widget.helper;

import ke.co.hoji.android.data.LocationWrapper;

/**
 * Any UI component responsible for acquiring GPS location and showing the progress thereof to the
 * user.
 */
public interface LocationComponent {

    /**
     * The quality of any location received by a LocationComponent.
     */
    class GpsQuality {

        /**
         * This location is good enough for our purposes and we can go ahead and use it. This is
         * based on whether the location is stale and/or inaccurate, and whether we actually care.
         * We don't care, for instance, if we are simply capturing GPS at the end of the record
         * since that's for QA only.
         */
        public static final int GOOD = 0;

        /**
         * This location is too old for our purposes. This is based on the period specified by
         * {@link #DEFAULT_GPS_SHELF_LIFE}.
         */
        public static final int STALE = 1;

        /**
         * This location does not meet the accuracy threshold specified in the current form.
         */
        public static final int INACCURATE = 2;

        /**
         * The location is simply null.
         */
        public static final int NULL = 3;

    }

    /**
     * The number of minutes after which location data is considered unusable by default.
     */
    long DEFAULT_GPS_SHELF_LIFE = 15L;//15 minutes

    /**
     * Checks if this component is currently ready to use location information or it is merely
     * listening for it with the intention of keeping a fresh copy around for future use.
     *
     * @return true if this component is currently ready to use location information and false
     * otherwise.
     */
    boolean isReadyToUse();

    /**
     * Sets whether or not this component is currently ready to use location information or it is
     * merely listening for it with the intention of keeping a fresh copy around for future use.
     *
     * @param readyToUse
     */
    void setReadyToUse(boolean readyToUse);

    /**
     * @return true if this LocationComponent accepts the last known location and false if
     */
    boolean acceptsLastKnownLocation();

    /**
     * The callback through which a LocationComponent is notified of the availability of new
     * location information.
     *
     * @param locationWrapper the {@link LocationWrapper} containing location information.
     * @param lastKnown       true if the update is the last known location and false if the
     *                        updated location is fresh.
     */
    void updateLocation(LocationWrapper locationWrapper, boolean lastKnown);
}
