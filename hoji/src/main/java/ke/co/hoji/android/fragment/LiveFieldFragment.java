package ke.co.hoji.android.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.ListFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.activity.DataEntryActivity;
import ke.co.hoji.android.activity.RecordContainer;
import ke.co.hoji.android.adapter.LiveFieldArrayAdapter;
import ke.co.hoji.android.listener.OnLiveFieldSelectedListener;
import ke.co.hoji.core.data.NavigableList;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;

/**
 * Created by gitahi on 08/05/15.
 */
public class LiveFieldFragment extends ListFragment {

    private final List<LiveField> liveFields = new ArrayList<>();
    private LiveFieldArrayAdapter adapter;
    private OnLiveFieldSelectedListener onLiveFieldSelectedListener;

    private RecordContainer recordContainer = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        LiveField selected = liveFields.get(position);
        onLiveFieldSelectedListener.onLiveFieldSelected(selected);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Activity activity = getActivity();
        onLiveFieldSelectedListener = (OnLiveFieldSelectedListener) activity;
        recordContainer = (RecordContainer) activity;
        adapter = new LiveFieldArrayAdapter(getContext(), recordContainer.getRecord());
        setListAdapter(adapter);
        refresh(null);
        final ViewConfiguration viewConfiguration = ViewConfiguration.get(LiveFieldFragment.this.getActivity());
        GestureDetector gestureDetector = new GestureDetector(LiveFieldFragment.this.getActivity(), new SwipeGestureDetector(
                viewConfiguration.getScaledMinimumFlingVelocity(),
                viewConfiguration.getScaledTouchSlop()
        ));
        View.OnTouchListener touchListener = new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        };
        getListView().setOnTouchListener(touchListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (recordContainer != null) {
            Record record = recordContainer.getRecord();
            if (record != null) {
                String title;
                if (record.getCaption() != null) {
                    title = record.getCaption();
                } else {
                    title = getResources().getText(R.string.live_field_activity_title).toString();
                }
                ((Activity) recordContainer).setTitle(title);
                refresh(record.getLiveFields());
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.live_field_fragment_actions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.editButton:
                LiveField selected = null;
                if (liveFields != null && !liveFields.isEmpty()) {
                    selected = new NavigableList<>(liveFields).last();
                }
                onLiveFieldSelectedListener.onLiveFieldSelected(selected);
                Activity activity = getActivity();
                if (activity instanceof DataEntryActivity) {
                    DataEntryActivity dataEntryActivity = (DataEntryActivity) activity;
                    TextView testModeTextView = dataEntryActivity.getTestModeTextView();
                    testModeTextView.setPadding(20, 10, 20, 0);
                    ProgressBar formProgressBar = dataEntryActivity.getFormProgressBar();
                    int topPadding = 20;
                    if (testModeTextView.getVisibility() == View.VISIBLE) {
                        topPadding = 0;
                    }
                    formProgressBar.setPadding(20, topPadding, 20, 0);
                }
                return true;
            case R.id.firstButton:
                autoScroll(0);
                return true;
            case R.id.lastButton:
                autoScroll(1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void refresh(List<LiveField> liveFields) {
        if (liveFields != null) {
            Collections.sort(liveFields);
            this.liveFields.clear();
            this.liveFields.addAll(liveFields);
        }
        adapter.notifyDataSetChanged();
    }

    private void autoScroll(final int where) {
        final ListView listView = getListView();
        if (listView.getCount() > 0) {
            listView.post(new Runnable() {
                public void run() {
                    int to = 0;
                    if (where != to) {
                        to = listView.getCount() - 1;
                    }
                    listView.setSelection(to);
                }
            });
        }
    }

    private class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {

        private final int swipeMinDistance;
        private final int swipeThresholdVelocity;

        public SwipeGestureDetector(int swipeMinDistance, int swipeThresholdVelocity) {
            this.swipeMinDistance = swipeMinDistance;
            this.swipeThresholdVelocity = swipeThresholdVelocity;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getX() - e2.getX()) > swipeMinDistance
                        && Math.abs(velocityX) > swipeThresholdVelocity) {
                    Toast.makeText(LiveFieldFragment.this.getActivity(), getString(R.string.tap_to_edit), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception ex) {
                //Do nothing
            }
            return false;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return super.onDown(e);
        }
    }
}