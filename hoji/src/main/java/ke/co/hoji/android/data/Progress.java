package ke.co.hoji.android.data;

import java.io.Serializable;

/**
 * Created by gitahi on 13/07/15.
 */
public final class Progress implements Serializable {

    public static class Status {

        public static final int BUSY = 1;
        public static final int DONE = 2;
        public static final int ABORTED = 3;
    }

    private final int status;
    private final String message;

    public Progress(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
