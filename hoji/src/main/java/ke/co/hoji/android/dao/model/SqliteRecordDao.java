package ke.co.hoji.android.dao.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.ResourceManager;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.model.*;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.MatrixResponse;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.service.model.ReferenceService;
import ke.co.hoji.android.HojiSQLiteAssetHelper;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by gitahi on 15/04/15.
 */
public class SqliteRecordDao {

    /**
     * Defines constants that describe how much information is fetched for a {@link Record}. This helps clients to
     * request only the information they need and no more, so as to save memory and disk access time.
     */
    public static class FetchMode {

        /**
         * Fetches the {@link Record} information, without its {@link RecordDetail}. Calling {@link Record#getDetail()}
         * on a Record fetched in this mode would return null. Record {@link LiveField}s are also excluded.
         */
        public static final int EXTRA_LITE = 1;

        /**
         * Fetches the {@link Record} information as well as its {@link RecordDetail}. Record {@link LiveField}s
         * excluded.
         */
        public static final int LITE = 2;

        /**
         * Fetches the entire {@link Record} information, including its {@link RecordDetail} and its {@link LiveField}s.
         * This mode is yet to be tested so it might cause problems.
         */
        public static final int FULL = 3;
    }

    private final SqliteSqlExecutor se;

    private SqliteFormDao formDao;

    public SqliteRecordDao(SqliteSqlExecutor se, SqliteFormDao formDao) {
        this.se = se;
        this.formDao = formDao;
    }

    public Record insert(Record record) {
        se.beginTransaction();
        try {
            insertRecord(record);
            insertDetail(record.getDetail());
            saveLiveField(record.getActiveLiveField(), record);
            se.setTransactionSuccessful();
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex.getMessage());
        } finally {
            se.endTransaction();
        }
        return record;
    }

    public Record update(Record record) {
        se.beginTransaction();
        try {
            updateRecord(record);
            updateDetail(record.getDetail());
            saveLiveField(record.getActiveLiveField(), record);
            se.setTransactionSuccessful();
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex.getMessage());
        } finally {
            se.endTransaction();
        }
        return record;
    }

    public void updateStage(MainRecord mainRecord, boolean updateDateCompleted) {
        Map<String, Object> whereParams = se.createParameterMap("uuid", mainRecord.getRecord().getUuid());
        Map<String, Object> params = se.createParameterMap();
        params.put("stage", mainRecord.getStage());
        if (mainRecord.getStage() == MainRecord.Stage.COMPLETE) {
            params.put("end_latitude", mainRecord.getEndLatitude());
            params.put("end_longitude", mainRecord.getEndLongitude());
            params.put("end_accuracy", mainRecord.getEndAccuracy());
            params.put("end_age", mainRecord.getEndAge());
            params.put("end_address", mainRecord.getEndAddress());
            if (updateDateCompleted) {
                mainRecord.setDateCompleted(new Date().getTime());
                params.put("date_completed", mainRecord.getDateCompleted());
            }
        }
        se.update("main_record", params, "record_uuid = ?", whereParams);
    }

    private void insertRecord(Record record) {
        record.setUuid(UUID.randomUUID().toString());
        if (("|uuid|").equals(record.getSearchTerms())) {
            record.setSearchTerms("|" + record.getUuid() + "|");
        }
        record.setDateCreated(new Date().getTime());
        record.setDateUpdated(new Date().getTime());
        Map<String, Object> params = se.createParameterMap();
        params.put("uuid", record.getUuid());
        params.put("main", record.isMain());
        params.put("test", record.isTest());
        params.put("caption", record.getCaption());
        params.put("search_terms", record.getSearchTerms());
        params.put("unique_value", record.getUniqueValue());
        params.put("date_created", record.getDateCreated());
        params.put("date_updated", record.getDateUpdated());
        se.insert("record", params);
    }

    private void insertDetail(RecordDetail detail) {
        Map<String, Object> params = se.createParameterMap();
        params.put("record_uuid", detail.getRecord().getUuid());
        if (detail.getClass().equals(MainRecord.class)) {
            MainRecord mainRecord = (MainRecord) detail;
            mainRecord.setVersion(0);
            params.put("version", mainRecord.getVersion());
            params.put("form_version", mainRecord.getFormVersion());
            params.put("app_version", mainRecord.getAppVersion());
            params.put("form_id", mainRecord.getRecord().getFieldParent().getId());
            params.put("user_id", mainRecord.getUserId());
            params.put("device_id", mainRecord.getDeviceId());
            params.put("stage", mainRecord.getStage());
            params.put("start_latitude", mainRecord.getStartLatitude());
            params.put("start_longitude", mainRecord.getStartLongitude());
            params.put("start_accuracy", mainRecord.getStartAccuracy());
            params.put("start_age", mainRecord.getStartAge());
            params.put("start_address", mainRecord.getStartAddress());
            se.insert("main_record", params);
        } else if (detail.getClass().equals(MatrixRecord.class)) {
            MatrixRecord matrixRecord = (MatrixRecord) detail;
            params.put("main_record_uuid", matrixRecord.getMainRecord().getUuid());
            params.put("field_id", matrixRecord.getRecord().getFieldParent().getId());
            se.insert("matrix_record", params);
        }
    }

    private void saveLiveField(LiveField liveField, Record record) {
        Map<String, Object> whereParams = null;
        Map<String, Object> params = se.createParameterMap();
        params.put("field_id", liveField.getField().getId());
        params.put("record_uuid", record.getUuid());
        MainRecord mainRecord;
        if (record.isMain()) {
            mainRecord = (MainRecord) record.getDetail();
        } else {
            mainRecord = (MainRecord) ((MatrixRecord) record.getDetail()).getMainRecord().getDetail();
        }
        String liveFieldUuid = liveField.getUuid();
        if (liveFieldUuid == null) {
            //Confirm that this LiveField truly does not already exist by checking in the database. This is to
            //capture any navigation system bugs that might cause the same LiveField to be submitted to the database
            //twice.
            liveFieldUuid = getLiveFieldUuid(liveField, record);
        }
        if (liveFieldUuid == null) {
            liveField.setUuid(UUID.randomUUID().toString());
            liveField.setDateCreated(new Date().getTime());
        } else {
            liveField.setUuid(liveFieldUuid);
            whereParams = se.createParameterMap("uuid", liveField.getUuid());
        }
        liveField.setDateUpdated(new Date().getTime());
        params.put("date_created", liveField.getDateCreated());
        params.put("date_updated", liveField.getDateUpdated());
        if (!record.isMain()) {
            LiveField parent = ((MatrixRecord) record.getDetail()).getMainRecord().getActiveLiveField();
            saveLiveField(parent, mainRecord.getRecord());
        }
        Object value = liveField.getResponse().toStorageValue();
        if (liveField.getResponse() instanceof MatrixResponse) {
            if (value != null) {
                value = ((List<MatrixRecord>) value).size() + " items";
            }
        }
        params.put("uuid", liveField.getUuid());
        params.put("value", value);
        boolean insert = liveFieldUuid == null;
        if (insert) {
            se.insert("live_field", params);
        } else {
            se.update("live_field", params, "uuid = ?", whereParams);
        }
    }

    private String getLiveFieldUuid(LiveField liveField, Record record) {
        Map<String, Object> params = se.createParameterMap();
        params.put("field_id", liveField.getField().getId());
        params.put("record_uuid", record.getUuid());
        String select = "SELECT uuid, field_id, record_uuid FROM live_field WHERE field_id = ? AND record_uuid = ?";
        String uuid = null;
        Cursor cursor = se.rawQuery(select, params);
        if (cursor.moveToNext()) {
            uuid = cursor.getString(cursor.getColumnIndex("uuid"));
        }
        return uuid;
    }

    private void updateRecord(Record record) {
        record.setDateUpdated(new Date().getTime());
        Map<String, Object> whereParams = se.createParameterMap("uuid", record.getUuid());
        Map<String, Object> params = se.createParameterMap();
        params.put("caption", record.getCaption());
        params.put("search_terms", record.getSearchTerms());
        params.put("unique_value", record.getUniqueValue());
        params.put("date_updated", record.getDateUpdated());
        se.update("record", params, "uuid = ?", whereParams);
    }

    private void updateDetail(RecordDetail detail) {
        Map<String, Object> whereParams = se.createParameterMap("record_uuid", detail.getRecord().getUuid());
        Map<String, Object> params = se.createParameterMap();
        if (detail.getClass().equals(MainRecord.class)) {
            MainRecord mainRecord = (MainRecord) detail;
            if (mainRecord.getStage() == MainRecord.Stage.UPLOADED) {
                mainRecord.setStage(MainRecord.Stage.COMPLETE);
            }
            params.put("form_id", mainRecord.getRecord().getFieldParent().getId());
            params.put("stage", mainRecord.getStage());
            se.update("main_record", params, "record_uuid = ?", whereParams);
        } else if (detail.getClass().equals(MatrixRecord.class)) {
            MatrixRecord matrixRecord = (MatrixRecord) detail;
            MainRecord mainRecord = (MainRecord) matrixRecord.getMainRecord().getDetail();
            if (mainRecord.getStage() == MainRecord.Stage.UPLOADED) {
                mainRecord.setStage(MainRecord.Stage.COMPLETE);
                updateStage(mainRecord, false);
            }
            params.put("main_record_uuid", matrixRecord.getMainRecord().getUuid());
            params.put("field_id", matrixRecord.getRecord().getFieldParent().getId());
            se.update("matrix_record", params, "record_uuid = ?", whereParams);
        }
    }

    public int clearViolated(List<LiveField> liveFields, Record record) {
        int cleared = -1;
        se.beginTransaction();
        try {
            for (LiveField liveField : liveFields) {
                Map<String, Object> whereParams = se.createParameterMap("uuid", liveField.getUuid());
                se.delete("live_field", "uuid = ?", whereParams);
                if (liveField.getField().isMatrix()) {
                    Map params = this.se.createParameterMap();
                    params.put("field_id", liveField.getField().getId());
                    params.put("main_record_uuid", record.getUuid());
                    se.delete("matrix_record", "field_id = ? AND main_record_uuid = ?", params);
                }
            }
            se.setTransactionSuccessful();
            cleared = liveFields.size();
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex.getMessage());
        } finally {
            se.endTransaction();
        }
        return cleared;
    }

    public void delete(Record record) {
        Map<String, Object> whereParams = se.createParameterMap("uuid", record.getUuid());
        se.delete("record", "uuid = ?", whereParams);
    }

    private List<LiveField> getLiveFields(Record record) {
        List<LiveField> liveFields = new ArrayList<>();
        List<Field> fields = new ArrayList<>(record.getFieldParent().getChildren());
        Map<String, Object> params = se.createParameterMap("record_uuid", record.getUuid());
        String select = "SELECT live_field.uuid, record_uuid, field_id, date_created, date_updated, value "
                + "FROM live_field "
                + "INNER JOIN field ON live_field.field_id = field.id "
                + "WHERE record_uuid = ? "
                + "ORDER BY ordinal ";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                Integer fieldId = cursor.getInt(cursor.getColumnIndex("field_id"));
                Field field = null;
                for (Field f : fields) {
                    if (f.getId().equals(fieldId)) {
                        field = f;
                        break;
                    }
                }
                if (field != null) {
                    fields.remove(field);
                    LiveField liveField = new LiveField(field);
                    liveField.setUuid(cursor.getString(cursor.getColumnIndex("uuid")));
                    liveField.setDateCreated(cursor.getLong(cursor.getColumnIndex("date_created")));
                    liveField.setDateUpdated(cursor.getLong(cursor.getColumnIndex("date_updated")));
                    if (liveField.getField().isMatrix()) {
                        liveField.setResponse(Response.create(ResourceManager.getHojiContext(), liveField, getMatrixRecords(record, liveField.getField()), false));
                    } else {
                        int columnIndex = cursor.getColumnIndex("value");
                        int dataType = liveField.getField().getType().getDataType();
                        Object value = null;
                        if (!cursor.isNull(columnIndex)) {
                            boolean isBlob = (cursor.getType(columnIndex) == Cursor.FIELD_TYPE_BLOB);
                            switch (dataType) {
                                case FieldType.DataType.NULL:
                                    value = null;
                                    break;
                                case FieldType.DataType.NUMBER:
                                    value = !isBlob ? cursor.getLong(columnIndex) : null;
                                    break;
                                case FieldType.DataType.DECIMAL:
                                    value = !isBlob ? cursor.getDouble(columnIndex) : null;
                                    break;
                                case FieldType.DataType.STRING:
                                    value = !isBlob ? cursor.getString(columnIndex) : null;
                                    break;
                            }
                        }
                        liveField.setResponse(Response.create(ResourceManager.getHojiContext(), liveField, value, false));
                    }
                    liveFields.add(liveField);
                }
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return liveFields;
    }

    private List<MatrixRecord> getMatrixRecords(Record mainRecord, Field matrixField) throws DaoException {
        List<MatrixRecord> matrixRecords = new ArrayList<MatrixRecord>();
        Map<String, Object> params = se.createParameterMap();
        params.put("main_record_uuid", mainRecord.getUuid());
        params.put("field_id", matrixField.getId());
        String select = "SELECT record_uuid, field_id, caption, search_terms, unique_value, date_created "
                + "FROM record "
                + "INNER JOIN matrix_record ON record.uuid = matrix_record.record_uuid "
                + "WHERE main_record_uuid = ? AND field_id = ? "
                + "ORDER BY date_created";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                Record record = new Record(matrixField, false, false);
                record.setUuid(cursor.getString(cursor.getColumnIndex("record_uuid")));
                record.setCaption(cursor.getString(cursor.getColumnIndex("caption")));
                record.setSearchTerms(cursor.getString(cursor.getColumnIndex("search_terms")));
                record.setUniqueValue(cursor.getString(cursor.getColumnIndex("unique_value")));
                record.setDateCreated(cursor.getLong(cursor.getColumnIndex("date_created")));
                MatrixRecord matrixRecord = new MatrixRecord(record, mainRecord);
                record.setLiveFields(getLiveFields(record));
                matrixRecords.add(matrixRecord);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return matrixRecords;
    }

    public Record findMainRecordByUuuid(String uuid, int fetchMode) {
        Record record = null;
        String select = "SELECT "
                + "record_uuid, "
                + "form_id, "
                + "caption, "
                + "test, "
                + "search_terms, "
                + "unique_value, "
                + "stage, "
                + "date_created, "
                + "date_updated, "
                + "date_completed, "
                + "date_uploaded, "
                + "user_id, "
                + "device_id, "
                + "start_accuracy, "
                + "start_address, "
                + "start_age, "
                + "start_latitude, "
                + "start_longitude, "
                + "end_accuracy, "
                + "end_address, "
                + "end_age, "
                + "end_latitude, "
                + "end_longitude, "
                + "form_version, "
                + "app_version, "
                + "id "
                + "FROM record INNER JOIN main_record ON record.uuid = main_record.record_uuid "
                + "WHERE record_uuid = ?";
        Map<String, Object> params = se.createParameterMap("uuid", uuid);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                record = new RecordRowMapper().mapRow(cursor, fetchMode);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return record;
    }

    public List<Record> findMainRecords(RecordSearch recordSearch, int fetchMode, int limit) {
        WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder(recordSearch);
        Map<String, Object> params = new LinkedHashMap<>(whereClauseBuilder.getParameters());
        params.put("limit", limit);
        String select = "SELECT "
                + "record_uuid, "
                + "form_id, "
                + "caption, "
                + "test, "
                + "search_terms, "
                + "unique_value, "
                + "stage, "
                + "date_created, "
                + "date_updated, "
                + "date_completed, "
                + "date_uploaded, "
                + "user_id, "
                + "device_id, "
                + "start_accuracy, "
                + "start_address, "
                + "start_age, "
                + "start_latitude, "
                + "start_longitude, "
                + "end_accuracy, "
                + "end_address, "
                + "end_age, "
                + "end_latitude, "
                + "end_longitude, "
                + "form_version, "
                + "app_version, "
                + "id "
                + "FROM record INNER JOIN main_record ON record.uuid = main_record.record_uuid "
                + whereClauseBuilder.getWhereClause()
                + "ORDER BY date_created " + (recordSearch.isReverse() ? "ASC " : "DESC ")
                + "LIMIT ?";
        List<Record> records = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                Record record = new RecordRowMapper().mapRow(cursor, fetchMode);
                records.add(record);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return records;
    }

    public List<Record> findMainRecords(List<Integer> formIds, boolean test, int fetchMode) {
        Map<String, Object> params = new LinkedHashMap<>();
        StringBuilder formIdsPlaceholder = new StringBuilder();
        for (Integer formId : formIds) {
            if (formIdsPlaceholder.length() > 0) {
                formIdsPlaceholder.append(", ");
            }
            params.put("form_id_" + formId, formId);
            formIdsPlaceholder.append("?");
        }
        params.put("test", test ? 1 : 0);
        String select = "SELECT "
                + "record_uuid, "
                + "form_id, "
                + "caption, "
                + "test, "
                + "search_terms, "
                + "unique_value, "
                + "stage, "
                + "date_created, "
                + "date_updated, "
                + "date_completed, "
                + "date_uploaded, "
                + "user_id, "
                + "device_id, "
                + "start_accuracy, "
                + "start_address, "
                + "start_age, "
                + "start_latitude, "
                + "start_longitude, "
                + "end_accuracy, "
                + "end_address, "
                + "end_age, "
                + "end_latitude, "
                + "end_longitude, "
                + "form_version, "
                + "app_version, "
                + "id "
                + "FROM record INNER JOIN main_record ON record.uuid = main_record.record_uuid "
                + "WHERE form_id IN (" + formIdsPlaceholder.toString() + ") AND test = ? "
                + "ORDER BY date_created DESC";
        List<Record> records = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                Record record = new RecordRowMapper().mapRow(cursor, fetchMode);
                records.add(record);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return records;
    }

    public String getImageData(String uuid) {
        String imageData = null;
        Map<String, Object> params = se.createParameterMap("uuid", uuid);
        String select = "SELECT value FROM live_field WHERE uuid = ?";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                int columnIndex = cursor.getColumnIndex("value");
                if (!cursor.isNull(columnIndex)) {
                    imageData = cursor.getString(columnIndex);
                }
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return imageData;
    }

    public Record findMatrixRecordByUuid(String uuid, int fetchMode, Record mainRecord, Field matrixRecordField) {
        Record record = null;
        String select = "SELECT record_uuid, field_id, caption, search_terms, unique_value, date_created, test "
                + "FROM record INNER JOIN matrix_record ON record.uuid = matrix_record.record_uuid "
                + "WHERE record_uuid = ? AND field_id = ? ";
        Map<String, Object> params = se.createParameterMap();
        params.put("uuid", uuid);
        params.put("field_id", matrixRecordField.getId());
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                boolean test = cursor.getInt(cursor.getColumnIndex("test")) > 1;
                record = new Record(se.get(cursor.getInt(cursor.getColumnIndex("field_id")),
                        Field.class), false, test);
                record.setUuid(cursor.getString(cursor.getColumnIndex("record_uuid")));
                record.setCaption(cursor.getString(cursor.getColumnIndex("caption")));
                record.setSearchTerms(cursor.getString(cursor.getColumnIndex("search_terms")));
                record.setUniqueValue(cursor.getString(cursor.getColumnIndex("unique_value")));
                record.setDateCreated(cursor.getLong(cursor.getColumnIndex("date_created")));
                if (fetchMode >= FetchMode.LITE) {
                    MatrixRecord matrixRecord = new MatrixRecord(record, mainRecord);
                    record.setDetail(matrixRecord);
                }
                if (fetchMode >= FetchMode.FULL) {
                    setLiveFields(record);
                }
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return record;
    }

    public List<Record> findMatrixRecords(String mainRecordUuid, int fieldId, int fetchMode, Record mainRecord) {
        List<Record> records = new ArrayList<>();
        String select = "SELECT "
                + "record_uuid, "
                + "field_id, "
                + "caption, search_terms, "
                + "unique_value, "
                + "date_created, "
                + "test "
                + "FROM record "
                + "INNER JOIN matrix_record ON record.uuid = matrix_record.record_uuid "
                + "WHERE main_record_uuid = ? AND field_id = ? "
                + "ORDER BY date_created DESC";
        Map<String, Object> params = se.createParameterMap();
        params.put("main_record_uuid", mainRecordUuid);
        params.put("field_id", fieldId);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                boolean test = cursor.getInt(cursor.getColumnIndex("test")) > 1;
                Record record = new Record(se.get(cursor.getInt(cursor.getColumnIndex("field_id")),
                        Field.class), false, test);
                record.setUuid(cursor.getString(cursor.getColumnIndex("record_uuid")));
                record.setCaption(cursor.getString(cursor.getColumnIndex("caption")));
                record.setSearchTerms(cursor.getString(cursor.getColumnIndex("search_terms")));
                record.setUniqueValue(cursor.getString(cursor.getColumnIndex("unique_value")));
                record.setDateCreated(cursor.getLong(cursor.getColumnIndex("date_created")));
                if (fetchMode >= FetchMode.LITE) {
                    MatrixRecord matrixRecord = new MatrixRecord(record, mainRecord);
                    record.setDetail(matrixRecord);
                }
                if (fetchMode >= FetchMode.FULL) {
                    setLiveFields(record);
                }
                records.add(record);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return records;
    }

    public void setLiveFields(Record record) {
        record.setLiveFields(getLiveFields(record));
    }

    public int countRecords(RecordSearch recordSearch) {
        WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder(recordSearch);
        String select = "SELECT COUNT(record_uuid) "
                + "FROM record INNER JOIN main_record ON record.uuid = main_record.record_uuid "
                + whereClauseBuilder.getWhereClause();
        return count(select, whereClauseBuilder.getParameters());
    }

    public int countSimpleKeyInstances(Record record, Record mainRecord, Field field, Object uniqueValue, boolean testMode) {
        String select;
        Map<String, Object> params = new LinkedHashMap<>();
        String mainRecordUuid = mainRecord.getUuid() != null ? mainRecord.getUuid() : "";
        if (record.isMain()) {
            select = "SELECT COUNT(*) "
                    + "FROM live_field l "
                    + "INNER JOIN record r ON l.record_uuid = r.uuid "
                    + "INNER JOIN main_record m ON r.uuid = m.record_uuid "
                    + "WHERE l.value = ? AND l.field_id = ? AND r.uuid != ? AND r.test = ?";
            params.put("value", uniqueValue);
            params.put("field_id", field.getId());
            params.put("record_uuid", mainRecordUuid);
            params.put("test", testMode ? 1 : 0);
        } else {
            select = "SELECT COUNT(*) "
                    + "FROM live_field l "
                    + "INNER JOIN record r ON l.record_uuid = r.uuid "
                    + "INNER JOIN matrix_record m ON r.uuid = m.record_uuid "
                    + "WHERE l.value = ? AND l.field_id = ? AND r.uuid != ? AND m.main_record_uuid = ? AND r.test = ?";
            params.put("value", uniqueValue);
            params.put("field_id", field.getId());
            params.put("record_uuid", record.getUuid() != null ? record.getUuid() : "");
            params.put("main_record_uuid", mainRecordUuid);
            params.put("test", testMode ? 1 : 0);
        }
        return count(select, params);
    }

    public int countCompoundKeyInstances(Record record, Record mainRecord, String uniqueValue, boolean testMode) {
        String select;
        Map<String, Object> params = se.createParameterMap();
        String mainRecordUuid = mainRecord.getUuid() != null ? mainRecord.getUuid() : "";
        if (record.isMain()) {
            select = "SELECT COUNT(*) "
                    + "FROM record r INNER JOIN main_record m ON r.uuid = m.record_uuid "
                    + "WHERE r.unique_value = ? AND r.uuid != ? AND r.test = ?";
            params.put("unique_value", uniqueValue);
            params.put("uuid", mainRecord.getUuid());
            params.put("test", testMode ? 1 : 0);
        } else {
            select = "SELECT COUNT(*) "
                    + "FROM record r INNER JOIN matrix_record m ON r.uuid = m.record_uuid "
                    + "WHERE r.unique_value = ? AND r.uuid != ? AND m.main_record_uuid = ? AND r.test = ?";
            params.put("unique_value", uniqueValue);
            params.put("uuid", record.getUuid() != null ? record.getUuid() : "");
            params.put("main_record_uuid", mainRecordUuid);
            params.put("test", testMode ? 1 : 0);
        }
        return count(select, params);
    }

    public Set<Integer> getUsedChoiceIds(Field field, Record record, Record mainRecord) {
        FieldParent fieldParent = record.getFieldParent();
        String query;
        Map<String, Object> params = se.createParameterMap();
        if (fieldParent instanceof Form) {
            query = "SELECT DISTINCT value "
                    + "FROM live_field "
                    + "INNER JOIN record ON live_field.record_uuid = record.uuid "
                    + "INNER JOIN main_record on record.uuid = main_record.record_uuid "
                    + "WHERE form_id = ? AND field_id = ? AND main_record.record_uuid != ?";
            params.put("form_id", fieldParent.getId());
        } else {
            String mainRecordUuid = mainRecord != null && mainRecord.getUuid() != null ? mainRecord.getUuid() : "";
            query = "SELECT DISTINCT value "
                    + "FROM live_field "
                    + "INNER JOIN record ON live_field.record_uuid = record.uuid "
                    + "INNER JOIN matrix_record on record.uuid = matrix_record.record_uuid "
                    + "WHERE matrix_record.field_id = ? AND matrix_record.main_record_uuid = ? "
                    + "AND live_field.field_id = ? AND matrix_record.record_uuid != ?";
            params.put("parent_field_id", fieldParent.getId());
            params.put("parent_main_record_uuid", mainRecordUuid);
        }
        String recordUuid = record.getUuid() != null ? record.getUuid() : "";
        params.put("field_id", field.getId());
        params.put("record_uuid", recordUuid);
        Cursor cursor = null;
        Set<Integer> usedChoiceIds = new HashSet<>();
        try {
            cursor = se.rawQuery(query, params);
            while (cursor.moveToNext()) {
                Integer value = cursor.getInt(cursor.getColumnIndex("value"));
                if (value != null) {
                    usedChoiceIds.add(value);
                }
            }
        } catch (SQLiteException ex) {
            throw new DaoException(ex, query);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return usedChoiceIds;
    }

    private int count(String query, Map<String, Object> params) {
        int count = 0;
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(query, params);
            if (cursor.moveToNext()) {
                count = cursor.getInt(0);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, query);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return count;
    }

    private Reference getReference(Integer id) {
        Reference reference = null;
        if (id != null) {
            ReferenceService referenceService = ResourceManager.getHojiContext().getModelServiceManager()
                    .getReferenceService();
            Reference search = new Reference(id.intValue(),
                    referenceService.getReferenceLevels());
            reference = referenceService.search(search);
        }
        return reference;
    }

    private Integer getLastFieldId(Record record) {
        Integer lastFieldId = null;
        String select = "SELECT field_id FROM live_field WHERE record_uuid = ? ORDER BY date_created DESC";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, se.createParameterMap("record_uuid", record.getUuid()));
            if (cursor.moveToNext()) {
                lastFieldId = cursor.getInt(cursor.getColumnIndex("field_id"));
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return lastFieldId;
    }

    /**
     * Ports legacy data to a new data format. See Issue #938 for a sample use case.
     * <p>
     * This method is a rudimentary hack, for now. Such data porting is not expected to be routine. But in future,
     * if this need arises again, this utility may be re-written as a series of "DataPorters" that execute one after
     * the other.
     */
    public void portDataIfNecessary() {
        if (!HojiSQLiteAssetHelper.isPortData()) {
            return;
        }
        portChoiceCodesToChoiceIdsInData();
        portChoiceCodesToChoiceIdsInRules();
        HojiSQLiteAssetHelper.setPortData(false);

    }

    private void portChoiceCodesToChoiceIdsInData() {
        Map<String, Integer> liveFieldValues = new HashMap<>();
        String select = "SELECT live_field.uuid, field.choice_group_id, live_field.value "
                + "FROM live_field "
                + "INNER JOIN field ON live_field.field_id = field.id "
                + "INNER JOIN field_type ON field.field_type_id = field_type.id "
                + "WHERE field.field_type_id = ?";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, se.createParameterMap("field_type_id", 1));
            while (cursor.moveToNext()) {
                Integer choiceGroupId = cursor.getInt(cursor.getColumnIndex("choice_group_id"));
                String liveFieldUuid = cursor.getString(cursor.getColumnIndex("uuid"));
                String choiceCodeValue = cursor.getString(cursor.getColumnIndex("value"));
                Integer choiceIdValue = getChoiceId(choiceGroupId, choiceCodeValue);
                liveFieldValues.put(liveFieldUuid, choiceIdValue);
            }
            saveChoiceIdValues(liveFieldValues);
        } catch (SQLiteException ex) {
            throw new DaoException(ex, select);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void portChoiceCodesToChoiceIdsInRules() {
        Map<String, Object> params = se.createParameterMap();
        String select = "SELECT rule.id, owner_id, target_id, type, value FROM rule";
        Map<Integer, String> ruleValues = new HashMap<>();
        Cursor cursor = null;
        try {
            Integer ruleId;
            Integer ownerId;
            Integer targetId;
            Integer ruleType;
            String value;
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                ruleId = cursor.getInt(cursor.getColumnIndex("id"));
                ownerId = cursor.getInt(cursor.getColumnIndex("owner_id"));
                targetId = cursor.getInt(cursor.getColumnIndex("target_id"));
                ruleType = cursor.getInt(cursor.getColumnIndex("type"));
                value = cursor.getString(cursor.getColumnIndex("value"));
                Integer fieldTypeId = null;
                Integer choiceGroupId = null;
                if (ruleType == 1) {
                    fieldTypeId = getFieldTypeId(ownerId);
                    choiceGroupId = getChoiceGroupId(ownerId);
                } else if (ruleType == 2) {
                    fieldTypeId = getFieldTypeId(targetId);
                    choiceGroupId = getChoiceGroupId(targetId);
                }
                List<String> rawValues = Utils.tokenizeString(value, "\\|", true);
                if (fieldTypeId == 1) {
                    ruleValues.put(ruleId, getIdBasedValueForSingleChoice(choiceGroupId, rawValues));
                } else if (fieldTypeId == 3) {
                    ruleValues.put(ruleId, getIdBasedValueForMultipleChoice(choiceGroupId, rawValues));
                }
            }
            saveRuleIdValues(ruleValues);
        } catch (SQLiteException ex) {
            throw ex;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private Integer getChoiceId(Integer choiceGroupId, String code) {
        Integer choiceId = null;
        String select = "SELECT choice_id "
                + "FROM choice_group_choice "
                + "INNER JOIN choice ON choice_group_choice.choice_id = choice.id "
                + "WHERE choice_group_choice.choice_group_id = ? AND choice.code = ?";
        Map<String, Object> params = se.createParameterMap();
        params.put("choice_group_id", choiceGroupId);
        params.put("code", code);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            if (cursor.moveToNext()) {
                choiceId = cursor.getInt(cursor.getColumnIndex("choice_id"));
            }
        } catch (SQLiteException ex) {
            throw ex;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return choiceId;
    }

    private Integer getFieldTypeId(Integer fieldId) {
        Integer fieldTypeId = null;
        Map<String, Object> params = se.createParameterMap("id", fieldId);
        String select = "SELECT field_type_id FROM field WHERE id = ?";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            if (cursor.moveToNext()) {
                fieldTypeId = cursor.getInt(cursor.getColumnIndex("field_type_id"));
            }
        } catch (SQLiteException ex) {
            throw ex;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return fieldTypeId;
    }

    private Integer getChoiceGroupId(Integer fieldId) {
        Integer choiceGroupId = null;
        Map<String, Object> params = se.createParameterMap("id", fieldId);
        String select = "SELECT choice_group_id FROM field WHERE id = ?";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            if (cursor.moveToNext()) {
                choiceGroupId = cursor.getInt(cursor.getColumnIndex("choice_group_id"));
            }
        } catch (SQLiteException ex) {
            throw ex;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return choiceGroupId;
    }

    private String getIdBasedValueForSingleChoice(Integer choiceGroupId, List<String> rawValues) {
        List<String> idBasedValues = new ArrayList<>();
        for (String rawValue : rawValues) {
            if (ruleValueIsCode(rawValue)) {
                idBasedValues.add(rawValue);
            } else {
                idBasedValues.add(String.valueOf(getChoiceId(choiceGroupId, rawValue)));
            }
        }
        String idBasedStringValue = Utils.string(idBasedValues, "|", null, false);
        idBasedStringValue = idBasedStringValue.replace(" ", "");
        return idBasedStringValue;
    }

    private String getIdBasedValueForMultipleChoice(Integer choiceGroupId, List<String> rawValues) {
        List<String> idBasedValues = new ArrayList<>();
        String binaryString = rawValues.get(0);
        String[] bin = binaryString.split("(?!^)");
        Integer choiceId = null;
        Map<String, Object> params = se.createParameterMap("choice_group_id", choiceGroupId);
        String select = "SELECT choice_id, ordinal FROM choice_group_choice WHERE choice_group_id = ? ORDER BY ordinal DESC";
        Cursor cursor = null;
        int i = bin.length - 1;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                choiceId = cursor.getInt(cursor.getColumnIndex("choice_id"));
                String binVal;
                if (i >= 0) {
                    binVal = bin[i];
                } else {
                    binVal = "0";
                }
                idBasedValues.add((choiceId + "#" + binVal).trim());
                i--;
            }
        } catch (SQLiteException ex) {
            throw ex;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Collections.reverse(idBasedValues);
        String idBasedStringValue = Utils.string(idBasedValues, "|", null, false);
        idBasedStringValue = idBasedStringValue.replace(" ", "");
        return idBasedStringValue;
    }

    private boolean ruleValueIsCode(String ruleValue) {
        return ruleValue.equals(Constants.Command.EMPTY)
                || ruleValue.equals(Constants.Command.TODAY)
                || ruleValue.equals(Constants.Command.NOW);
    }

    private void saveChoiceIdValues(Map<String, Integer> liveFieldValues) {
        se.beginTransaction();
        try {
            for (String liveFieldUuid : liveFieldValues.keySet()) {
                Map<String, Object> whereParams = se.createParameterMap("uuid", liveFieldUuid);
                Map<String, Object> params = se.createParameterMap("value", liveFieldValues.get(liveFieldUuid));
                se.update("live_field", params, "uuid = ?", whereParams);
            }
            se.setTransactionSuccessful();
        } catch (SQLiteException ex) {
            throw ex;
        } finally {
            se.endTransaction();
        }
    }

    private void saveRuleIdValues(Map<Integer, String> ruleValues) {
        se.beginTransaction();
        try {
            for (Integer ruleId : ruleValues.keySet()) {
                Map<String, Object> whereParams = se.createParameterMap("id", ruleId);
                Map<String, Object> params = se.createParameterMap("value", ruleValues.get(ruleId));
                se.update("rule", params, "id = ?", whereParams);
            }
            se.setTransactionSuccessful();
        } catch (SQLiteException ex) {
            throw ex;
        } finally {
            se.endTransaction();
        }
    }

    private class RecordRowMapper {

        Record mapRow(Cursor cursor, int fetchMode) {
            Integer formId = cursor.getInt(cursor.getColumnIndex("form_id"));
            Form form = se.get(formId, Form.class);
            if (form == null) {
                form = formDao.getFormById(formId);
            }
            boolean test = cursor.getInt(cursor.getColumnIndex("test")) > 1;
            Record record = new Record(form, true, test);
            record.setUuid(cursor.getString(cursor.getColumnIndex("record_uuid")));
            record.setCaption(cursor.getString(cursor.getColumnIndex("caption")));
            record.setSearchTerms(cursor.getString(cursor.getColumnIndex("search_terms")));
            record.setUniqueValue(cursor.getString(cursor.getColumnIndex("unique_value")));
            record.setDateCreated(cursor.getLong(cursor.getColumnIndex("date_created")));
            record.setLastField(se.get(getLastFieldId(record), Field.class));
            MainRecord mainRecord = new MainRecord(record);
            mainRecord.setStartAddress(cursor.getString(cursor.getColumnIndex("start_address")));
            mainRecord.setUserId(cursor.getInt(cursor.getColumnIndex("user_id")));
            mainRecord.setStage(cursor.getInt(cursor.getColumnIndex("stage")));
            record.setDetail(mainRecord);
            if (fetchMode >= FetchMode.LITE) {
                mainRecord.setDateCompleted(cursor.getLong(cursor.getColumnIndex("date_completed")));
                mainRecord.setDateUploaded(cursor.getLong(cursor.getColumnIndex("date_uploaded")));
                mainRecord.setFormVersion(cursor.getString(cursor.getColumnIndex("form_version")));
                mainRecord.setAppVersion(cursor.getString(cursor.getColumnIndex("app_version")));
                mainRecord.setStartLatitude(cursor.getDouble(cursor.getColumnIndex("start_latitude")));
                mainRecord.setStartLongitude(cursor.getDouble(cursor.getColumnIndex("start_longitude")));
                mainRecord.setEndLatitude(cursor.getDouble(cursor.getColumnIndex("end_latitude")));
                mainRecord.setEndLongitude(cursor.getDouble(cursor.getColumnIndex("end_longitude")));
            }
            if (fetchMode >= FetchMode.FULL) {
                mainRecord.setDeviceId(cursor.getString(cursor.getColumnIndex("device_id")));
                mainRecord.setStartAccuracy(cursor.getDouble(cursor.getColumnIndex("start_accuracy")));
                mainRecord.setStartAge(cursor.getLong(cursor.getColumnIndex("start_age")));
                mainRecord.setEndAccuracy(cursor.getDouble(cursor.getColumnIndex("end_accuracy")));
                mainRecord.setEndAge(cursor.getLong(cursor.getColumnIndex("end_age")));
                setLiveFields(record);
            }
            return record;
        }

    }

    private class WhereClauseBuilder {

        private final Map<String, Object> parameters;

        private final String whereClause;

        WhereClauseBuilder(RecordSearch recordSearch) {
            parameters = new LinkedHashMap<>();
            StringBuilder whereClause = new StringBuilder();
            if (recordSearch.getUuid() != null) {
                parameters.put("uuid", recordSearch.getUuid());
                whereClause.append("uuid = ? ");
            }
            if (recordSearch.isTestMode() != null) {
                parameters.put("test", recordSearch.isTestMode() ? 1 : 0);
                if (whereClause.length() > 0) {
                    whereClause.append("AND ");
                }
                whereClause.append("test = ? ");
            }
            if (recordSearch.getStage() != null && recordSearch.getStage() != -1) {
                parameters.put("stage", recordSearch.getStage());
                if (whereClause.length() > 0) {
                    whereClause.append("AND ");
                }
                whereClause.append("stage = ? ");
            }
            if (recordSearch.getCaption() != null && !"".equals(recordSearch.getCaption())) {
                parameters.put("search_terms", "%" + recordSearch.getCaption() + "%");
                if (whereClause.length() > 0) {
                    whereClause.append("AND ");
                }
                whereClause.append("search_terms LIKE ? ");
            }
            if (recordSearch.getTimeStamp() != null && recordSearch.getTimeStamp() != 0L) {
                parameters.put("date_created", recordSearch.getTimeStamp());
                if (whereClause.length() > 0) {
                    whereClause.append("AND ");
                }
                if (recordSearch.isReverse()) {
                    whereClause.append("date_created > ? ");
                } else {
                    whereClause.append("date_created < ? ");
                }
            }
            if (recordSearch.getForm() != null && recordSearch.getForm().getId() != -1) {
                parameters.put("form_id", recordSearch.getForm().getId());
                if (whereClause.length() > 0) {
                    whereClause.append("AND ");
                }
                whereClause.append("form_id = ? ");
            }
            if (recordSearch.getFrom() != null) {
                parameters.put("from", recordSearch.getFrom().getTime());
                if (whereClause.length() > 0) {
                    whereClause.append("AND ");
                }
                whereClause.append("date_created >= ? ");
            }
            if (recordSearch.getTo() != null) {
                parameters.put("to", recordSearch.getTo().getTime());
                if (whereClause.length() > 0) {
                    whereClause.append("AND ");
                }
                whereClause.append("date_created <= ? ");
            }
            this.whereClause = whereClause.toString();
        }

        public Map<String, Object> getParameters() {
            return parameters;
        }

        public String getWhereClause() {
            return whereClause.length() > 0 ? "WHERE " + whereClause : " ";
        }
    }

}
