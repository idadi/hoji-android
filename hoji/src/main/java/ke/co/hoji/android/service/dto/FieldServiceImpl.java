package ke.co.hoji.android.service.dto;

import java.util.List;

import ke.co.hoji.android.dao.dto.SqliteFieldDao;
import ke.co.hoji.core.data.dto.Field;
import ke.co.hoji.core.service.dto.FieldService;

public class FieldServiceImpl implements FieldService {

    private final SqliteFieldDao fieldDao;

    public FieldServiceImpl(SqliteFieldDao fieldDao) {
        this.fieldDao = fieldDao;
    }

    @Override
    public List<Field> saveFields(List<Field> fields) {
        return fieldDao.saveFields(fields);
    }

}
