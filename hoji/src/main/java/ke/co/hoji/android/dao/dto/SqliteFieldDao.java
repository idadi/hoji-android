package ke.co.hoji.android.dao.dto;

import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.android.dao.model.SqliteChoiceGroupDao;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.dto.Field;
import ke.co.hoji.core.data.dto.Rule;

import java.util.*;

/**
 * Created by gitahi on 27/08/15.
 */
public class SqliteFieldDao {

    private final SqliteSqlExecutor se;

    private final SqliteChoiceGroupDao choiceGroupDao;

    /**
     * A cache for @{@link ke.co.hoji.core.data.ChoiceGroup} ids so we only process each ChoiceGroup
     * once for efficiency.
     */
    private Set<Integer> choiceGroupIds = new HashSet<>();

    public SqliteFieldDao(SqliteSqlExecutor se, SqliteChoiceGroupDao choiceGroupDao) {
        this.se = se;
        this.choiceGroupDao = choiceGroupDao;
    }

    public List<Field> saveFields(List<Field> fields) {
        se.beginTransaction();
        try {
            {
                // Disable all fields - fields that still exist on the server will be re-enabled.
                Map<String, Object> params = se.createParameterMap();
                params.put("enabled", false);
                se.update("field", params, null, null);
            }
            List<Field> fieldsWithRules = new ArrayList<>();
            for (Field field : fields) {
                ChoiceGroup choiceGroup = field.getChoiceGroup();
                if (choiceGroup != null) {
                    Integer choiceGroupId = choiceGroup.getId();
                    if (!choiceGroupIds.contains(choiceGroupId)) {
                        choiceGroupDao.saveChoiceGroup(field.getChoiceGroup());
                        choiceGroupIds.add(choiceGroupId);
                    }
                }
                if (!fieldExists(field.getId())) {
                    insertField(field);
                } else {
                    updateField(field);
                }
                saveFieldRecordFormIds(field);
                if (field.getRules().size() > 0) {
                    fieldsWithRules.add(field);
                } else {
                    // Delete any rules owned by this field, if any - rules that still exist on the
                    // server will be recreated.
                    Map<String, Object> whereParams = se.createParameterMap("owner_id", field.getId());
                    se.delete("rule", "owner_id = ?", whereParams);
                }
            }
            for (Field fieldWithRules : fieldsWithRules) {
                saveRules(fieldWithRules);
            }
            se.setTransactionSuccessful();
        } finally {
            se.endTransaction();
            choiceGroupIds.clear();
        }
        return fields;
    }

    private void insertField(Field field) {
        Map<String, Object> params = se.createParameterMap();
        params.put("id", field.getId());
        params.putAll(generateParameters(field));
        se.insert("field", params);
    }

    private void updateField(Field field) {
        Map<String, Object> params = generateParameters(field);
        Map<String, Object> whereParams = se.createParameterMap("id", field.getId());
        se.update("field", params, "id = ?", whereParams);
    }

    private Map<String, Object> generateParameters(Field field) {
        Map<String, Object> parameters = new LinkedHashMap<>();
        parameters.put("name", field.getName());
        parameters.put("description", field.getDescription());
        parameters.put("column", field.getColumn());
        parameters.put("ordinal", field.getOrdinal());
        parameters.put("tag", field.getTag());
        parameters.put("calculated", field.getCalculated());
        parameters.put("value_script", field.getValueScript());
        parameters.put("instructions", field.getInstructions());
        parameters.put("field_type_id", field.getTypeId());
        parameters.put("missing_value", field.getMissingValue());
        parameters.put("missing_action", field.getMissingAction());
        parameters.put("min_value", field.getMinValue());
        parameters.put("max_value", field.getMaxValue());
        parameters.put("regex_value", field.getRegexValue());
        parameters.put("uniqueness", field.getUniqueness());
        parameters.put("default_value", field.getDefaultValue());
        parameters.put("captioning", field.isCaptioning());
        parameters.put("searchable", field.isSearchable());
        parameters.put("filterable", field.isFilterable());
        parameters.put("output_type", field.getOutputType());
        parameters.put("choice_group_id", field.getChoiceGroup() != null ? field.getChoiceGroup().getId() : null);
        parameters.put("parent_id", field.getParentId());
        parameters.put("pipe_source_id", field.getPipeSourceId());
        parameters.put("choice_filter_field_id", field.getChoiceFilterFieldId());
        parameters.put("reference_field_id", field.getReferenceFieldId());
        parameters.put("main_record_field_id", field.getMainRecordFieldId());
        parameters.put("matrix_record_field_id", field.getMatrixRecordFieldId());
        parameters.put("response_inheriting", field.isResponseInheriting());
        parameters.put("form_id", field.getFormId());
        parameters.put("enabled", field.isEnabled());
        return parameters;
    }

    private boolean fieldExists(int fieldId) {
        String select = "SELECT COUNT(id) FROM field WHERE id = ?";
        return se.exists(select, fieldId);
    }

    private void saveFieldRecordFormIds(Field field) {
        Map<String, Object> whereParams = se.createParameterMap("field_id", field.getId());
        se.delete("field_record_form", "field_id = ?", whereParams);
        List<Integer> formIds = field.getRecordFormIds();
        if (formIds != null && !formIds.isEmpty()) {
            for (Integer formId : formIds) {
                insertFieldRecordForm(field, formId);
            }
        }
    }

    private void saveRules(Field owner) {
        deleteRules(owner);
        for (Rule rule : owner.getRules()) {
            insertRule(rule);
        }
    }

    private void insertFieldRecordForm(Field field, Integer formId) {
        Map<String, Object> params = se.createParameterMap();
        params.put("field_id", field.getId());
        params.put("form_id", formId);
        se.insert("field_record_form", params);
    }

    private void insertRule(Rule rule) {
        Map<String, Object> params = se.createParameterMap();
        params.put("id", rule.getId());
        params.put("type", rule.getType());
        params.put("ordinal", rule.getOrdinal());
        params.put("owner_id", rule.getOwnerId());
        params.put("operator_descriptor_id", rule.getOperatorDescriptorId());
        params.put("value", rule.getValue());
        params.put("target_id", rule.getTargetId());
        params.put("combiner", rule.getCombiner());
        params.put("grouper", rule.getGrouper());
        se.insert("rule", params);
    }

    private void deleteRules(Field owner) {
        Map<String, Object> whereParams = se.createParameterMap("owner_id", owner.getId());
        se.delete("rule", "owner_id = ?", whereParams);
    }
}
