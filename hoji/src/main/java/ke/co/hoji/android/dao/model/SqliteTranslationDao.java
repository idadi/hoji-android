package ke.co.hoji.android.dao.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.Translatable;
import ke.co.hoji.core.data.model.Translation;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SqliteTranslationDao {

    private final SqliteSqlExecutor se;

    public SqliteTranslationDao(SqliteSqlExecutor se) {
        this.se = se;
    }

    public Translation findTranslation(Translatable translatable, int languageId, String component) {
        Translation translation = null;
        String select = "SELECT id, component, `value` "
            + "FROM translation "
            + "WHERE translatable = ? AND translatable_id = ? AND language_id = ? AND component = ?";
        Map<String, Object> params = se.createParameterMap();
        params.put("translatable", translatable.getTranslatableName());
        params.put("translatable_id", translatable.getId());
        params.put("language_id", languageId);
        params.put("component", component);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                translation = new Translation
                    (
                        cursor.getInt(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("component")),
                        cursor.getString(cursor.getColumnIndex("value"))
                    );
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteTranslationDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return translation;
    }
}
