package ke.co.hoji.android.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.listener.OnLiteRecordsFoundListener;
import ke.co.hoji.core.data.dto.LiteRecord;
import ke.co.hoji.core.data.model.Form;

public class ImportActivity extends AppCompatActivity implements OnLiteRecordsFoundListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.import_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    @Override
    public void onLiteRecordsFound(List<LiteRecord> liteRecords, Form form) {
        final int LIMIT = 500;
        List<LiteRecord> listOfRecords = new ArrayList<>();
        if (liteRecords.size() > LIMIT) {
            listOfRecords.addAll(liteRecords.subList(0, LIMIT));
        } else {
            listOfRecords.addAll(liteRecords);
        }
        Intent intent = new Intent(ImportActivity.this, LiteRecordActivity.class);
        intent.putExtra(Constants.DEFLATED_FORM, ((AndroidHojiContext) getApplication()).deflateForm(form));
        intent.putExtra(Constants.LITE_RECORDS, (Serializable) listOfRecords);
        startActivity(intent);
    }
}
