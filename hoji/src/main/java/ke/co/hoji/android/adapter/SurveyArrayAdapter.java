package ke.co.hoji.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.helper.LetterIconCreator;
import ke.co.hoji.core.data.http.SurveyResponse;
import ke.co.hoji.core.data.model.Survey;

/**
 * Created by gitahi on 12/05/15.
 */
public class SurveyArrayAdapter extends ArrayAdapter<SurveyResponse> {

    private final LetterIconCreator letterIconCreator = new LetterIconCreator();

    public SurveyArrayAdapter(Context context, List<SurveyResponse> surveyResponses) {
        super(context, 0, surveyResponses);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SurveyResponse surveyResponse = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.survey_row, parent, false);
        }
        ImageView surveyIconImageView = convertView.findViewById(R.id.surveyIconImageView);
        TextView surveyNameTextView = convertView.findViewById(R.id.surveyNameTextView);
        TextView surveyOwnerTextView = convertView.findViewById(R.id.surveyOwnerTextView);
        TextView surveyStatusTextView = convertView.findViewById(R.id.surveyStatusTextView);
        TextView surveyAccessTextView = convertView.findViewById(R.id.surveyAccessTextView);

        surveyIconImageView.setImageDrawable(
                letterIconCreator.createIcon(
                        getContext(),
                        surveyResponse.getSurvey().getName(),
                        String.valueOf(surveyResponse.getSurvey().getId())
                )
        );

        surveyNameTextView.setText(surveyResponse.getSurvey().getName());
        surveyOwnerTextView.setText(surveyResponse.getOwnerName());
        if (surveyResponse.getSurvey().isEnabled()
                && surveyResponse.getSurvey().getStatus() == Survey.PUBLISHED) {
            surveyStatusTextView.setText(convertView.getResources().getString(R.string.published));
        } else {
            if (!surveyResponse.getSurvey().isEnabled()) {
                surveyStatusTextView.setText(convertView.getResources().getString(R.string.disabled));
            } else if (surveyResponse.getSurvey().getStatus() != Survey.PUBLISHED) {
                surveyStatusTextView.setText(convertView.getResources().getString(R.string.unpublished));
            }
        }
        if (surveyResponse.getSurvey().getAccess() == Survey.Access.PRIVATE) {
            surveyAccessTextView.setText(convertView.getResources().getString(R.string.private_survey));
        } else {
            surveyAccessTextView.setText(convertView.getResources().getString(R.string.public_survey));
        }
        return convertView;
    }
}
