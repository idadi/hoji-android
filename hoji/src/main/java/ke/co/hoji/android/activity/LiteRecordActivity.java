package ke.co.hoji.android.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.fragment.LiteRecordFragment;
import ke.co.hoji.android.listener.OnLiteRecordsFoundListener;
import ke.co.hoji.android.listener.OnMainRecordsImportedListener;
import ke.co.hoji.core.data.dto.LiteRecord;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.dto.RecordService;

public class LiteRecordActivity extends AppCompatActivity
        implements OnLiteRecordsFoundListener, OnMainRecordsImportedListener {

    private LiteRecordFragment liteRecordFragment;

    private List<LiteRecord> liteRecords;
    private Form form;

    private RecordService recordService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lite_record_activity);
        liteRecordFragment = (LiteRecordFragment) getSupportFragmentManager()
                .findFragmentById(R.id.liteRecordFragment);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        List<LiteRecord> liteRecs;
        Form deflatedForm = null;
        if (savedInstanceState != null) {
            liteRecs = (List<LiteRecord>) savedInstanceState.getSerializable(Constants.LITE_RECORDS);
            deflatedForm = (Form) savedInstanceState.getSerializable(Constants.DEFLATED_FORM);
        } else {
            Intent intent = getIntent();
            liteRecs = (List<LiteRecord>) intent.getSerializableExtra(Constants.LITE_RECORDS);
            deflatedForm = (Form) intent.getSerializableExtra(Constants.DEFLATED_FORM);
        }
        Form inflatedForm = null;
        if (deflatedForm != null) {
            inflatedForm = ((AndroidHojiContext) getApplication()).inflateForm(deflatedForm);

        }
        recordService = ((AndroidHojiContext) getApplication()).getDtoServiceManager().getRecordService();
        onLiteRecordsFound(liteRecs, inflatedForm);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(Constants.LITE_RECORDS, (Serializable) liteRecords);
        outState.putSerializable(Constants.DEFLATED_FORM, ((AndroidHojiContext) getApplication()).deflateForm(form));
    }

    @Override
    public void onLiteRecordsFound(List<LiteRecord> liteRecords, Form form) {
        this.liteRecords = liteRecords;
        this.form = form;
        Collections.sort(liteRecords, new Comparator<LiteRecord>() {
            @Override
            public int compare(LiteRecord thisOne, LiteRecord thatOne) {
                return thatOne.getDateCreated().compareTo(thisOne.getDateCreated());
            }
        });
        recordService.flagNonNewLiteRecords(liteRecords);
        refresh();
    }

    @Override
    public void onMainRecordsImported(List<MainRecord> mainRecords) {
        LiteRecord liteRecord = getPrincipalLiteRecord(mainRecords);
        recordService.saveMainRecords(mainRecords);
        liteRecord.setStatus(LiteRecord.Status.IMPORTED);
    }

    private LiteRecord getPrincipalLiteRecord(List<MainRecord> mainRecords) {
        for (MainRecord mainRecord : mainRecords) {
            for (LiteRecord liteRecord : liteRecords) {
                if (liteRecord.getUuid().equals(mainRecord.getUuid())) {
                    return liteRecord;
                }
            }
        }
        return null;
    }

    public void refresh() {
        liteRecordFragment.refresh(liteRecords, form);
    }
}
