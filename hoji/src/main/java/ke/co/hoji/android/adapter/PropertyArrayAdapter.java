package ke.co.hoji.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.core.data.model.Property;

/**
 * Created by gitahi on 12/05/15.
 */
public class PropertyArrayAdapter extends ArrayAdapter<Property> {

    public PropertyArrayAdapter(Context context, List<Property> properties) {
        super(context, 0, properties);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Property property = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.property_row, parent, false);
        }
        TextView propertyNameTextView = convertView.findViewById(R.id.propertyNameTextView);
        TextView propertyValueTextView = convertView.findViewById(R.id.propertyValueTextView);
        propertyNameTextView.setText(property.getKey());
        propertyValueTextView.setText(property.getValue());
        return convertView;
    }
}
