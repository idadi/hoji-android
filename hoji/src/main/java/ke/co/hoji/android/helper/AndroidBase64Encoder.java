package ke.co.hoji.android.helper;

import android.util.Base64;

import ke.co.hoji.core.response.helper.Base64Encoder;

public class AndroidBase64Encoder implements Base64Encoder {

    @Override
    public String encodeToBase64String(byte[] byteArray) {
        return Base64.encodeToString(byteArray, Base64.NO_WRAP);
    }

    @Override
    public byte[] decodeToByteArray(String base64String) {
        return Base64.decode(base64String, Base64.NO_WRAP);
    }
}
