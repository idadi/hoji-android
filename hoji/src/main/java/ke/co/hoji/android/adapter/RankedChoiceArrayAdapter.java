package ke.co.hoji.android.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.service.model.TranslationService;

/**
 * Created by gitahi on 12/05/15.
 */
public class RankedChoiceArrayAdapter extends ArrayAdapter<Choice> {

    private final boolean editable;

    public RankedChoiceArrayAdapter(Context context, List<Choice> choices, boolean editable) {
        super(context, 0, choices);
        this.editable = editable;
    }

    @Override
    public View getView(int position, View rankedChoiceView, ViewGroup parent) {
        AndroidHojiContext hojiContext = (AndroidHojiContext) getContext().getApplicationContext();
        TranslationService translationService = hojiContext.getModelServiceManager().getTranslationService();
        Integer languageId = hojiContext.getLanguageId();
        Choice choice = getItem(position);
        if (rankedChoiceView == null) {
            rankedChoiceView = LayoutInflater.from(getContext()).inflate(R.layout.ranked_choice_row, parent, false);
        }
        TextView rankTextView = rankedChoiceView.findViewById(R.id.rankTextView);
        TextView choiceTextView = rankedChoiceView.findViewById(R.id.choiceTextView);

        if (choice.getRank() == null || choice.getRank() == 0) {
            rankTextView.setText(String.valueOf(0));
        } else {
            rankTextView.setText(String.valueOf(choice.getRank()));
        }
        choiceTextView.setText(translationService.translate(
                choice,
                languageId,
                Choice.TranslatableComponent.NAME,
                choice.getName()
        ));
        if (choice.isAlreadyEntered()) {
            choiceTextView.setPaintFlags(choiceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            choiceTextView.setPaintFlags(choiceTextView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
        return rankedChoiceView;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return editable;
    }

    @Override
    public boolean isEnabled(int position) {
        return editable;
    }
}
