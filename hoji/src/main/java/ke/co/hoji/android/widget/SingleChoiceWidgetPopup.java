package ke.co.hoji.android.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;

import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.adapter.ChoiceArrayAdapter;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.widget.WidgetUtils;

/**
 * Created by gitahi on 05/03/18.
 */
public class SingleChoiceWidgetPopup extends AndroidWidget {

    private EditText editText;
    private List<Choice> choices;
    private Choice choice;
    private int selectedPosition = -1;

    public SingleChoiceWidgetPopup(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.popup_multiple_choice_widget;
    }

    @Override
    public void loadViews() {
        editText = findViewById(R.id.multipleChoiceEditText);
        editText.setKeyListener(null);
        editText.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                showSingleChoicePickerDialog();
            }
        });
    }

    @Override
    public void load() {
        Record record = getNavigable().getNavigator() != null ? getNavigable().getNavigator().getRecord() : null;
        choices = WidgetUtils.getChoices(
                getLiveField().getField(),
                record,
                getHojiContext().getRecord(),
                getHojiContext().getModelServiceManager().getRecordService()
        );
    }

    private void showSingleChoicePickerDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getCurrentContext());
        ChoiceArrayAdapter adapter = new ChoiceArrayAdapter
                (
                        getCurrentContext(),
                        R.layout.simple_list_item_single_choice,
                        choices,
                        editable
                );
        builder.setSingleChoiceItems(adapter, selectedPosition, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int position) {
                selectedPosition = position;
            }
        });
        builder.setPositiveButton(
                SingleChoiceWidgetPopup.this.getResources().getString(R.string.ok_action),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int position) {
                        if (selectedPosition == -1) {
                            choice = null;
                            editText.setText(R.string.tap_to_select);
                        } else {
                            choice = choices.get(selectedPosition);
                            editText.setText(readResponse().displayString());
                        }
                        raiseResponseChangedEvent();
                    }
                });
        builder.setNegativeButton(
                SingleChoiceWidgetPopup.this.getResources().getString(R.string.cancel_action),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null) {
            setChoice((Choice) response.getActualValue(), false);
        }
    }

    @Override
    public Object getValue() {
        return choice;
    }

    @Override
    public void focus() {
        editText.requestFocus();
    }

    @Override
    public void wipe() {
        setChoice(null, false);
    }

    @Override
    public View getPrincipalView() {
        return editText;
    }

    private void setChoice(Choice choice, boolean go) {
        this.choice = choice;
        if (this.choice != null) {
            editText.setText(readResponse().displayString());
            selectedPosition = choices.indexOf(choice);
        } else {
            editText.setText(R.string.tap_to_select);
            selectedPosition = -1;
        }
        focus();
        if (go) {
            raiseResponseChangedEvent();
        }
    }
}
