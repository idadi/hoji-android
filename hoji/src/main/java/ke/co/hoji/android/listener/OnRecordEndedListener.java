package ke.co.hoji.android.listener;

import ke.co.hoji.core.data.model.Record;

/**
 * Defines a callback for reacting to a {@link Record} ended event.
 * <p/>
 * Created by gitahi on 17/05/15.
 */
public interface OnRecordEndedListener {

    /**
     * Called when a {@link Record} reaches the end.
     *
     * @param record the record that just ended.
     */
    void onRecordEnded(Record record);
}
