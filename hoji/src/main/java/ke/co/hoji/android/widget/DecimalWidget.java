package ke.co.hoji.android.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import ke.co.hoji.R;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.TextInputType;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;

/**
 * Created by gitahi on 14/11/14.
 */
public class DecimalWidget extends AndroidWidget {

    private EditText editText;

    public DecimalWidget(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.decimal_widget;
    }

    @Override
    public void loadViews() {
        editText = findViewById(R.id.editText);
        editText.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                raiseResponseChangingEvent();
            }
        });
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT) {
                    raiseResponseChangedEvent();
                }
                return false;
            }
        });
    }

    @Override
    public void wipe() {
        editText.setText("");
        int defaultInputType = TextInputType.DECIMAL;
        Integer requestedInputType = Utils.parseInteger(getLiveField().getField().getTag());
        if (requestedInputType != null
                && requestedInputType == TextInputType.DECIMAL_SIGNED) {
            TextWidgetUtil.setInputType(editText, requestedInputType);
        } else {
            TextWidgetUtil.setInputType(editText, defaultInputType);
        }
    }

    @Override
    public boolean setDefaultValue(String defaultValue) {
        Double defaultDouble = Utils.parseDouble(defaultValue);
        if (defaultDouble != null) {
            editText.setText(String.valueOf(defaultDouble));
            return true;
        }
        return false;
    }

    @Override
    public boolean needsKeyboard() {
        return true;
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null && response.getActualValue() != null) {
            editText.setText(response.getActualValue().toString());
        }
    }

    @Override
    public Object getValue() {
        String text = editText.getText().toString();
        if (text.length() > 0) {
            return Utils.parseDouble(text);
        }
        return null;
    }

    @Override
    public void focus() {
        TextWidgetUtil.setFocus(editText, isEditable());
    }

    @Override
    public Comparable getComparable() {
        return (Double) readResponse().getActualValue();
    }

    @Override
    public Comparable getMissingComparable(String stringValue) {
        return Utils.parseDouble(stringValue);
    }

    @Override
    public Field.ValidationValue getMin(Record record) {
        Field.ValidationValue genericValidationValue = getGenericValidationValue(
                getLiveField().getField().getMinValue(),
                record
        );
        if (genericValidationValue != null) {
            Double min = Utils.parseDouble(String.valueOf(genericValidationValue.getValue()));
            if (min != null) {
                return new Field.ValidationValue(min, genericValidationValue.getAction());
            }
        }
        return null;
    }

    @Override
    public Field.ValidationValue getMax(Record record) {
        Field.ValidationValue genericValidationValue = getGenericValidationValue(
                getLiveField().getField().getMaxValue(),
                record
        );
        if (genericValidationValue != null) {
            Double max = Utils.parseDouble(String.valueOf(genericValidationValue.getValue()));
            if (max != null) {
                return new Field.ValidationValue(max, genericValidationValue.getAction());
            }
        }
        return null;
    }

    @Override
    public View getPrincipalView() {
        return editText;
    }
}
