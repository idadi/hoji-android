package ke.co.hoji.android.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.activity.HomeActivity;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.PropertyService;

/**
 * Processes project updates received from the server to let the user know that they should update
 * their project to get the latest forms and other data.
 */
public class ProjectUpdateService extends FirebaseMessagingService {

    /**
     * The channel id for project updates.
     */
    private static final String PROJECT_UPDATE_CHANNEL_ID = "PROJECT_UPDATE_CHANNEL_ID";

    /**
     * A handle to the {@link NotificationManager}.
     */
    private NotificationManager notificationManager = null;

    /**
     * Called when we receive a push notification message.
     *
     * @param remoteMessage the object containing the data in the  notification.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        AndroidHojiContext hojiContext = (AndroidHojiContext) getApplicationContext();
        Survey survey = hojiContext.getSurvey();
        Integer surveyId = Utils.parseInteger(remoteMessage.getData().get("surveyId"));
        if (survey != null && survey.getId().equals(surveyId)) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                setupChannelsForOreo();
            }
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            final PendingIntent pendingIntent = PendingIntent.getActivity(
                    this,
                    0,
                    intent,
                    PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE
            );
            PropertyService propertyService = hojiContext.getModelServiceManager().getPropertyService();
            propertyService.setProperty(new Property(Constants.FIRE_UPDATE, Boolean.TRUE.toString()));
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, PROJECT_UPDATE_CHANNEL_ID)
                    .setSmallIcon(R.drawable.notification_icon)
                    .setContentTitle(survey.getName())
                    .setContentText(getText(R.string.project_update_notification_message))
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setColor(getResources().getColor(R.color.primary_dark))
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(Constants.PROJECT_UPDATE_NOTIFICATION_ID, builder.build());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannelsForOreo() {
        CharSequence channelName = getString(R.string.project_update_channel_name);
        String channelDescription = getString(R.string.project_update_channel_description);
        NotificationChannel channel = new NotificationChannel(
                PROJECT_UPDATE_CHANNEL_ID,
                channelName,
                NotificationManager.IMPORTANCE_HIGH
        );
        channel.setDescription(channelDescription);
        channel.enableLights(true);
        channel.setLightColor(Color.RED);
        channel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(channel);
        }
    }
}
