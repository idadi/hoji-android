package ke.co.hoji.android.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.Hours;
import org.joda.time.LocalDate;
import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.activity.HomeActivity;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.android.helper.LetterIconCreator;
import ke.co.hoji.android.task.SearchTask;
import ke.co.hoji.android.helper.UiUtils;
import ke.co.hoji.android.listener.OnRecordSelectedListener;
import ke.co.hoji.android.listener.OnRecordsLoadedListener;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.NavigableList;
import ke.co.hoji.core.data.PageInfo;
import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.MainRecord;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.core.service.model.RecordService;
import ke.co.hoji.core.service.model.TranslationService;

/**
 * Parent fragment for the 3 record "folders" namely Drafts, Outbox and Sent.
 * <p>
 * Created by gitahi on 15/04/18.
 */
public abstract class RecordFolderFragment extends Fragment implements OnRecordsLoadedListener {

    /**
     * The list of records in the folder.
     */
    List<Record> records;

    /**
     * A @{@link RecordSearch} object representing the criteria used to add records to the folder.
     */
    private RecordSearch recordSearch;

    /**
     * The listener to notify when a record in this folder is selected.
     */
    private OnRecordSelectedListener onRecordSelectedListener;

    /**
     * A progress dialog for showing background tasks in progress.
     */
    ProgressDialog progressDialog;

    /**
     * A set of running @{@link SearchTask} so we don't spawn more than one to do the same work.
     */
    private final Set<String> searchTaskIds = new HashSet<>();

    /**
     * The UI View for this folder.
     */
    private RecyclerView recyclerView;

    /**
     * The {@link LinearLayoutManager} for the recycler view.
     */
    private LinearLayoutManager layoutManager;

    @Override
    public void onResume() {
        super.onResume();
        /*
         * The scroll listener that we use to support "infinite scrolling" within the folder. We
         * use it to detect when we get to the end of the list so that we can load the next page
         * of records.
         *
         * Note that when counting the total number of items in the list, we subtract 1 to account
         * for the extra item added to the bottom of the list to represent an empty folder whenever
         * applicable.
         */
        RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {

            private final LinearLayoutManager layoutManager = RecordFolderFragment.this.layoutManager;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItems = layoutManager.getChildCount();
                    int totalItems = layoutManager.getItemCount() - 1;
                    int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                    if (pastVisibleItems + visibleItems >= totalItems) {
                        loadRecords(getLastTimeStamp());
                    }
                } else if (dy < 0) {
                    requestForFeedback();
                }
            }
        };
        recyclerView.addOnScrollListener(scrollListener);
        records = new ArrayList<>();
        loadRecords(0L);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        recyclerView = (RecyclerView) inflater.inflate(R.layout.home_fragment, container, false);
        layoutManager = new LinearLayoutManager(recyclerView.getContext());
        recyclerView.setLayoutManager(layoutManager);
        return recyclerView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        onRecordSelectedListener = (OnRecordSelectedListener) getActivity();
        progressDialog = new ProgressDialog(this.getActivity());
    }

    /**
     * An adapter for supplying records to the recycler view for the folder.
     */
    public class RecordRecyclerViewAdapter
            extends RecyclerView.Adapter<RecordRecyclerViewAdapter.ViewHolder> {

        private final TypedValue typedValue = new TypedValue();
        private final int backGround;

        /**
         * The records for this adapter.
         */
        private List<Record> recordList;

        private final LetterIconCreator letterIconCreator = new LetterIconCreator();

        public void setRecords(List<Record> records) {
            this.recordList = records;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            Record record;

            /**
             * The view for a single item in the recycler view.
             */
            final View view;

            /**
             * This is where we put the record caption.
             */
            final TextView recordCaption;

            /**
             * This is where we put the record form.
             */
            final TextView recordForm;

            /**
             * This is where we put the record pretty date.
             */
            final TextView recordDate;

            /**
             * This is where we put the record icon.
             */
            final ImageView recordIcon;

            /**
             * This is where we put the record location.
             */
            final TextView recordLocation;

            /**
             * A separate additional view for showing folder empty state, when applicable.
             */
            final TextView noRecords;

            ViewHolder(View view) {
                super(view);
                this.view = view;
                recordCaption = view.findViewById(R.id.recordCaptionTextView);
                recordForm = view.findViewById(R.id.formTextView);
                recordLocation = view.findViewById(R.id.recordLocationTextView);
                recordDate = view.findViewById(R.id.recordDateTextView);
                recordIcon = view.findViewById(R.id.recordIconImageView);
                noRecords = view.findViewById(R.id.noRecordsTextView);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + recordCaption.getText();
            }
        }

        RecordRecyclerViewAdapter(List<Record> items) {
            Activity activity = getActivity();
            if (activity != null) {
                activity.getTheme().resolveAttribute(R.attr.selectableItemBackground, typedValue, true);
            }
            backGround = typedValue.resourceId;
            recordList = items;
        }

        /**
         * Here we need to return 2 different view holders. The regular record view for a record item in
         * the list and an additional view inserted at the end to display the folder empty state i.e.
         * when the folder has no records.
         */
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view;
            if (viewType == R.layout.record_row) {
                view = layoutInflater.inflate(R.layout.record_row, parent, false);
            } else {
                view = layoutInflater.inflate(R.layout.no_records_row, parent, false);
            }
            view.setBackgroundResource(backGround);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            if (recordList.isEmpty()) {
                switch (getStage()) {
                    case MainRecord.Stage.DRAFT:
                        holder.noRecords.setText(getResources().getText(R.string.no_draft_records));
                        break;
                    case MainRecord.Stage.COMPLETE:
                        holder.noRecords.setText(getResources().getText(R.string.no_outbox_records));
                        break;
                    case MainRecord.Stage.UPLOADED:
                        holder.noRecords.setText(getResources().getText(R.string.no_sent_records));
                        break;
                    default:
                        holder.noRecords.setText(getResources().getText(R.string.no_records));
                        break;
                }
                return;
            }
            if (position == recordList.size()) {
                //If the folder is not empty, hide the empty state view.
                holder.view.setVisibility(View.GONE);
                holder.view.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
            } else {
                final AndroidHojiContext androidHojiContext = (AndroidHojiContext) getActivity().getApplication();
                final TranslationService translationService = androidHojiContext.getModelServiceManager().getTranslationService();
                Integer languageId = androidHojiContext.getLanguageId();

                holder.record = recordList.get(position);
                Form form = (Form) holder.record.getFieldParent();
                MainRecord mainRecord = (MainRecord) holder.record.getDetail();
                holder.recordCaption.setText(holder.record.getCaption() != null
                        ? holder.record.getCaption()
                        : getTranslatedFormName(form, languageId, translationService));
                holder.recordForm.setText(Html.fromHtml(getTranslatedFormName(form, languageId, translationService)));
                int dateFormat = Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(
                        RecordFolderFragment.this.getActivity()
                ).getString(SettingsActivity.DATE_TIME_FORMAT, "1"));
                Date dateCreated = new Date(mainRecord.getRecord().getDateCreated());
                String dateCreatedString;
                if (dateFormat == 2) {
                    dateCreatedString = Utils.formatDate(dateCreated);
                } else if (dateFormat == 3) {
                    dateCreatedString = Utils.formatTime(dateCreated);
                } else if (dateFormat == 4) {
                    dateCreatedString = Utils.formatDateTime(dateCreated);
                } else {
                    dateCreatedString = new PrettyTime().format(dateCreated);
                }
                holder.recordDate.setText(dateCreatedString);
                holder.recordLocation.setText(UiUtils.getLocation(mainRecord));
                setRecordIcon(holder.record, holder.recordIcon);
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Record deflatedRecord = holder.record;
                        if (true) {
                            PropertyService propertyService = androidHojiContext.getModelServiceManager().getPropertyService();
                            int stage = getStage();
                            Integer surveyId = androidHojiContext.getSurvey().getId();
                            Integer formId = form.getId();
                            Property property = null;
                            int folderTitleId = -1;
                            if (stage == MainRecord.Stage.DRAFT) {
                                String propKey = Constants.Server.DRAFT_LOCK + "." + formId;
                                property = propertyService.getProperty(propKey, surveyId);
                                if (property == null) {
                                    propKey = Constants.Server.DRAFT_LOCK;
                                    property = propertyService.getProperty(propKey, surveyId);
                                }
                                folderTitleId = R.string.drafts;
                            } else if (stage == MainRecord.Stage.COMPLETE) {
                                String propKey = Constants.Server.OUTOBOX_LOCK + "." + formId;
                                property = propertyService.getProperty(propKey, surveyId);
                                if (property == null) {
                                    propKey = Constants.Server.OUTOBOX_LOCK;
                                    property = propertyService.getProperty(propKey, surveyId);
                                }
                                folderTitleId = R.string.outbox;
                            } else if (stage == MainRecord.Stage.UPLOADED) {
                                String propKey = Constants.Server.SENT_LOCK + "." + formId;
                                property = propertyService.getProperty(propKey, surveyId);
                                if (property == null) {
                                    propKey = Constants.Server.SENT_LOCK;
                                    property = propertyService.getProperty(propKey, surveyId);
                                }
                                folderTitleId = R.string.sent;
                            }
                            if (property != null && !StringUtils.isBlank(property.getValue())) {
                                Integer lockHours = Utils.parseInteger(property.getValue());
                                if (lockHours != null && lockHours >= 0) {
                                    LocalDate dateCreated = new LocalDate(new Date(deflatedRecord.getDateCreated()));
                                    int recordHours = Hours.hoursBetween(dateCreated, new LocalDate()).getHours();
                                    boolean lock = lockHours == 0 || recordHours > lockHours;
                                    if (lock) {
                                        String message;
                                        if (lockHours != 0) {
                                            message = getResources().getString(folderTitleId)
                                                    + " " + getResources().getString(
                                                    R.string.locked_record_timed_message,
                                                    lockHours);
                                        } else {
                                            message = getResources().getString(folderTitleId)
                                                    + " " + getResources().getString(
                                                    R.string.locked_record_immediate_message);
                                        }
                                        new AlertDialog.Builder(getActivity())
                                                .setTitle(R.string.locked_record_title)
                                                .setMessage(message)
                                                .setPositiveButton(R.string.ok_action, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        //Do nothing
                                                    }
                                                })
                                                .create()
                                                .show();
                                        return;
                                    }
                                }
                            }
                        }
                        ((AndroidHojiContext) getActivity().getApplication()).inflateRecord(
                                deflatedRecord,
                                onRecordSelectedListener,
                                progressDialog,
                                getActivity()
                        );
                    }
                });
                holder.view.setOnLongClickListener(new View.OnLongClickListener() {

                    @Override
                    public boolean onLongClick(View v) {
                        final Record selected = holder.record;
                        final String recordName = selected.getCaption() != null && !selected.getCaption().equals("") ?
                                selected.getCaption() : getResources().getString(R.string.this_record);
                        new AlertDialog.Builder(RecordFolderFragment.this.getActivity())
                                .setTitle(getResources().getString(R.string.delete_record_title))
                                .setMessage(getResources().getString(R.string.delete_record_message, recordName))
                                .setPositiveButton(R.string.yes_action, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        AndroidHojiContext hojiContext = (AndroidHojiContext) getActivity().getApplication();
                                        RecordService recordService = hojiContext.getModelServiceManager().getRecordService();
                                        recordService.delete(selected);
                                        RecordFolderFragment.this.records.remove(selected);
                                        refreshRecords();
                                        ((HomeActivity) getActivity()).showBadges();
                                    }
                                })
                                .setNegativeButton(R.string.no_action, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //Do nothing
                                    }
                                })
                                .create()
                                .show();
                        return true;
                    }
                });
            }
        }

        @Override
        public int getItemViewType(int position) {
            return (position == recordList.size()) ? R.layout.no_records_row : R.layout.record_row;
        }

        /**
         * Add 1 to accommodate the additional view added to represent folder empty state.
         */
        @Override
        public int getItemCount() {
            return recordList.size() + 1;
        }

        void setRecordIcon(Record record, ImageView recordIcon) {
            AndroidHojiContext androidHojiContext = (AndroidHojiContext) getActivity().getApplication();
            TranslationService translationService = androidHojiContext.getModelServiceManager().getTranslationService();
            Integer languageId = androidHojiContext.getLanguageId();
            String displayName = record.getCaption();
            if (StringUtils.isBlank(displayName)) {
                Form form = (Form) record.getFieldParent();
                displayName = getTranslatedFormName(form, languageId, translationService);
            }
            recordIcon.setImageDrawable(letterIconCreator.createIcon(RecordFolderFragment.this.getActivity(), displayName, record.getUuid()));
        }
    }

    private Long getLastTimeStamp() {
        long timeStamp = 0L;
        if (records != null && !records.isEmpty()) {
            timeStamp = new NavigableList<>(records).last().getDateCreated();
        }
        return timeStamp;
    }

    public void setRecordSearch(RecordSearch recordSearch) {
        this.recordSearch = recordSearch;
    }

    @Override
    public void onRecordsLoaded(List<Record> records, String searchTaskId) {
        onRecordsLoaded(records, searchTaskId, null);
    }

    @Override
    public void onRecordsLoaded(List<Record> records, String searchTaskId, RecordSearch recordSearch) {
        searchTaskIds.remove(searchTaskId);
        if (
                this.records == null
                        || this.records.isEmpty()
                        || records.isEmpty()
                        || (recordSearch != null
                        && (recordSearch.getPageInfo() == null
                        || recordSearch.getPageInfo().getPage() == 1))) {
            this.records = new ArrayList<>(records);
        } else {
            this.records.addAll(records);
        }
        refreshRecords(recordSearch.getPageInfo());
    }

    /**
     * Re-load records for loading onto the view.
     */
    public void loadRecords(Long fromTimeStamp) {
        boolean testMode = PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getBoolean(SettingsActivity.TEST_MODE, Boolean.FALSE);
        //If loading afresh, rather than paging, do not reuse recordSearch to avoid messing up with
        //pagination information.
        if (recordSearch == null || fromTimeStamp == 0L) {
            recordSearch = new RecordSearch();
        }
        recordSearch.setStage(getStage());
        recordSearch.setTimeStamp(fromTimeStamp);
        recordSearch.setTestMode(testMode);
        String searchTaskId = recordSearch.getStage() + "" + recordSearch.getTimeStamp();
        if (!searchTaskIds.contains(searchTaskId)) {
            searchTaskIds.add(searchTaskId);
            new SearchTask(searchTaskId, recordSearch, this.getActivity(), this, true).execute();
        }
    }

    protected abstract int getStage();

    public void requestForFeedback() {
    }

    /**
     * Re-render records on the recycler view.
     *
     * @param pageInfo a @{@link PageInfo} object to help scroll to the correct page after rendering.
     */
    private void refreshRecords(PageInfo pageInfo) {
        if (this.records != null && recyclerView != null) {
            recyclerView.setAdapter(new RecordRecyclerViewAdapter(this.records));
            if (pageInfo != null) {
                int page = pageInfo.getPage();
                int maxPageSize = pageInfo.getMaxPageSize();
                int scrollPosition = ((page - 1) * maxPageSize);
                recyclerView.scrollToPosition(scrollPosition);
            }
        }
    }

    void refreshRecords() {
        refreshRecords(null);
    }

    private String getTranslatedFormName(Form form, Integer languageId, TranslationService translationService) {
        return translationService.translate(form, languageId, Form.TranslatableComponent.NAME, form.getName());
    }
}