package ke.co.hoji.android.widget;

import android.content.Context;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import ke.co.hoji.R;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.response.Response;

/**
 * Created by gitahi on 14/11/14.
 */
public class ScaleWidget extends AndroidWidget {

    private SeekBar seekBar;
    private TextView textView;

    private static final int DEFAULT_MAX = 5;

    public ScaleWidget(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.scale_widget;
    }

    @Override
    public void loadViews() {
        seekBar = findViewById(R.id.seekBar);
        textView = findViewById(R.id.textView);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                raiseResponseChangingEvent();
                textView.setText(String.valueOf(seekBar.getProgress()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    @Override
    public void prepare() {
        seekBar.setMax(maxValue());
    }

    @Override
    public void wipe() {
        seekBar.setProgress(0);
        textView.setText(String.valueOf(seekBar.getProgress()));
    }

    @Override
    public boolean setDefaultValue(String defaultValue) {
        Long defaultLong = Utils.parseLong(defaultValue);
        if (defaultLong != null && (defaultLong >= 0 && defaultLong <= maxValue())) {
            seekBar.setProgress(defaultLong.intValue());
            textView.setText(String.valueOf(seekBar.getProgress()));
            return true;
        }
        return false;
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null && response.getActualValue() != null) {
            seekBar.setProgress(((Long) response.getActualValue()).intValue());
            textView.setText(String.valueOf(seekBar.getProgress()));
        }
    }

    @Override
    public Object getValue() {
        Integer integer = seekBar.getProgress();
        if (!integer.equals(0)) {
            return integer.longValue();
        }
        return null;
    }

    @Override
    public void focus() {
        seekBar.requestFocus();
    }

    private int maxValue() {
        Long max = Utils.parseLong(getLiveField().getField().getMaxValue());
        if (max != null) {
            return max.intValue();
        } else {
            return DEFAULT_MAX;
        }
    }

    @Override
    public View getPrincipalView() {
        return seekBar;
    }

    @Override
    public boolean isEmpty() {
        return seekBar.getProgress() < 1;
    }
}
