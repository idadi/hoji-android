package ke.co.hoji.android.helper;

import android.graphics.Bitmap;

public class HojiBitmap {

    private final Bitmap bitmap;
    private final String filePath;

    public HojiBitmap(Bitmap bitmap, String filePath) {
        this.bitmap = bitmap;
        this.filePath = filePath;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public String getFilePath() {
        return filePath;
    }
}
