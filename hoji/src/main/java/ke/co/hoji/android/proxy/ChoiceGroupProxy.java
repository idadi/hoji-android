package ke.co.hoji.android.proxy;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChoiceGroupProxy extends ChoiceGroup {

    private ChoiceGroup choiceGroup;

    public ChoiceGroupProxy(ChoiceGroup choiceGroup) {
        super();
        this.choiceGroup = choiceGroup;
    }

    @Override
    public int getOrderingStrategy() {
        return choiceGroup.getOrderingStrategy();
    }

    @Override
    public int getExcludeLast() {
        return choiceGroup.getExcludeLast();
    }

    @Override
    public List<Choice> getChoices() {
        if (choiceGroup.getChoices() == null) {
            choiceGroup.setChoices(loadChoices());
        }
        return choiceGroup.getChoices();
    }

    private List<Choice> loadChoices() {
        List<Choice> choices = new ArrayList<>();
        SqliteSqlExecutor se = SqliteSqlExecutor.getInstance();
        Map<String, Object> params = se.createParameterMap("choice_group_id", choiceGroup.getId());
        String select = "SELECT "
            + "c.id, "
            + "c.name, "
            + "cgc.description, "
            + "cgc.code, "
            + "cgc.scale, "
            + "cgc.loner, "
            + "cgc.ordinal, "
            + "cgc.choice_parent_id, "
            + "cp.id AS id_p, "
            + "cp.name AS name_p, "
            + "cp.description AS description_p, "
            + "cp.code AS code_p, "
            + "cp.scale AS scale_p, "
            + "cp.loner AS loner_p "
            + "FROM choice c "
            + "INNER JOIN choice_group_choice cgc ON c.id = cgc.choice_id "
            + "LEFT JOIN choice cp on cgc.choice_parent_id = cp.id "
            + "WHERE cgc.choice_group_id = ? "
            + "ORDER BY cgc.ordinal ASC;";
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            Map<Integer, Choice> parents = new HashMap<>();
            while (cursor.moveToNext()) {
                Integer parentId = cursor.getInt(cursor.getColumnIndex("id_p"));
                if (parentId > 0 && !parents.containsKey(parentId)) {
                    String parentName = cursor.getString(cursor.getColumnIndex("name_p"));
                    String parentDescription = cursor.getString(cursor.getColumnIndex("description_p"));
                    String parentCode = cursor.getString(cursor.getColumnIndex("code_p"));
                    Integer parentScale = cursor.getInt(cursor.getColumnIndex("scale_p"));
                    boolean parentLoner = cursor.getInt(cursor.getColumnIndex("loner_p")) > 0;
                    Choice parent = new Choice(parentId, parentName, parentDescription, parentCode, parentScale, parentLoner);
                    parents.put(parentId, parent);
                }
                Integer id = cursor.getInt(cursor.getColumnIndex("id"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String description = cursor.getString(cursor.getColumnIndex("description"));
                String code = cursor.getString(cursor.getColumnIndex("code"));
                Integer scale = cursor.getInt(cursor.getColumnIndex("scale"));
                boolean loner = cursor.getInt(cursor.getColumnIndex("loner")) > 0;
                Choice choice = new Choice(id, name, description, code, scale, loner);
                Double ordinal = cursor.getDouble(cursor.getColumnIndex("ordinal"));
                choice.setOrdinal(BigDecimal.valueOf(ordinal));
                Integer parentChoiceId = cursor.getInt(cursor.getColumnIndex("choice_parent_id"));
                if (parentChoiceId > 0) {
                    choice.setParent(parents.get(parentChoiceId));
                }
                choices.add(choice);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(ChoiceGroupProxy.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return choices;
    }
}
