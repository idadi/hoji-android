package ke.co.hoji.android;

import android.content.Context;

import ke.co.hoji.android.dao.dto.SqliteFieldDao;
import ke.co.hoji.android.dao.dto.SqliteFormDao;
import ke.co.hoji.android.dao.dto.SqliteLanguageDao;
import ke.co.hoji.android.dao.dto.SqliteRecordDao;
import ke.co.hoji.android.dao.dto.SqliteReferenceDao;
import ke.co.hoji.android.dao.dto.SqliteSurveyDao;
import ke.co.hoji.android.dao.dto.SqliteTranslationDao;
import ke.co.hoji.android.dao.model.SqliteChoiceDao;
import ke.co.hoji.android.dao.model.SqliteChoiceGroupDao;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.helper.ImageEncoder;
import ke.co.hoji.android.service.dto.FieldServiceImpl;
import ke.co.hoji.android.service.dto.FormServiceImpl;
import ke.co.hoji.android.service.dto.LanguageServiceImpl;
import ke.co.hoji.android.service.dto.RecordServiceImpl;
import ke.co.hoji.android.service.dto.ReferenceServiceImpl;
import ke.co.hoji.android.service.dto.SurveyServiceImpl;
import ke.co.hoji.android.service.dto.TranslationServiceImpl;
import ke.co.hoji.core.AbstractServiceManager;
import ke.co.hoji.core.DtoServiceManager;
import ke.co.hoji.core.service.dto.FieldService;
import ke.co.hoji.core.service.dto.FormService;
import ke.co.hoji.core.service.dto.LanguageService;
import ke.co.hoji.core.service.dto.RecordService;
import ke.co.hoji.core.service.dto.ReferenceService;
import ke.co.hoji.core.service.dto.SurveyService;
import ke.co.hoji.core.service.dto.TranslationService;

/**
 * Created by gitahi on 15/04/15.
 */
class SqliteDtoServiceManager extends AbstractServiceManager implements DtoServiceManager {

    private final AndroidHojiContext androidHojiContext;
    private final SqliteSqlExecutor sqlExecutor;

    private SurveyService surveyService;
    private FormService formService;
    private FieldService fieldService;
    private ReferenceService referenceService;
    private RecordService recordService;
    private LanguageService languageService;
    private TranslationService translationService;

    public SqliteDtoServiceManager(AndroidHojiContext androidHojiContext) {
        this.androidHojiContext = androidHojiContext;
        this.sqlExecutor = SqliteSqlExecutor.getInstance();
    }

    public SurveyService getSurveyService() {
        if (surveyService == null) {
            setContextFromParameters();
            surveyService = new SurveyServiceImpl(new SqliteSurveyDao(sqlExecutor));
        }
        return surveyService;
    }

    private void setContextFromParameters() {
        Context context = (Context) getParameters().get(Constants.CONTEXT);
        sqlExecutor.setSqliteAssetHelper(new HojiSQLiteAssetHelper(context));
    }

    @Override
    public FormService getFormService() {
        if (formService == null) {
            setContextFromParameters();
            formService = new FormServiceImpl(new SqliteFormDao(sqlExecutor));
        }
        return formService;
    }

    @Override
    public FieldService getFieldService() {
        if (fieldService == null) {
            setContextFromParameters();
            fieldService = new FieldServiceImpl(fieldDao());
        }
        return fieldService;
    }

    @Override
    public ReferenceService getReferenceService() {
        if (referenceService == null) {
            setContextFromParameters();
            referenceService = new ReferenceServiceImpl(new SqliteReferenceDao(sqlExecutor));
        }
        return referenceService;
    }

    @Override
    public RecordService getRecordService() {
        if (recordService == null) {
            setContextFromParameters();
            ke.co.hoji.android.dao.model.SqliteFieldDao fieldDao =
                    new ke.co.hoji.android.dao.model.SqliteFieldDao(sqlExecutor, choiceGroupDao());
            recordService = new RecordServiceImpl(
                    new SqliteRecordDao(sqlExecutor, fieldDao, new ImageEncoder(androidHojiContext))
            );
        }
        return recordService;
    }

    @Override
    public LanguageService getLanguageService() {
        if (languageService == null) {
            setContextFromParameters();
            languageService = new LanguageServiceImpl(new SqliteLanguageDao(sqlExecutor));
        }
        return languageService;
    }

    @Override
    public TranslationService getTranslationService() {
        if (translationService == null) {
            setContextFromParameters();
            translationService = new TranslationServiceImpl(new SqliteTranslationDao(sqlExecutor));
        }
        return translationService;
    }

    @Override
    public void clearCache() {
        sqlExecutor.clearCache();
    }

    private SqliteFieldDao fieldDao() {
        return new SqliteFieldDao(sqlExecutor, choiceGroupDao());
    }

    private SqliteChoiceGroupDao choiceGroupDao() {
        return new SqliteChoiceGroupDao(sqlExecutor, choiceDao());
    }

    private SqliteChoiceDao choiceDao() {
        return new SqliteChoiceDao(sqlExecutor);
    }
}
