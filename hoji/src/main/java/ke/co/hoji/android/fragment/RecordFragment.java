package ke.co.hoji.android.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.ListFragment;

import java.util.ArrayList;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.android.adapter.RecordArrayAdapter2;
import ke.co.hoji.android.task.SearchTask;
import ke.co.hoji.android.listener.OnRecordSearchUpdatedListener;
import ke.co.hoji.android.listener.OnRecordSelectedListener;
import ke.co.hoji.android.listener.OnRecordsLoadedListener;
import ke.co.hoji.core.data.NavigableList;
import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.service.model.RecordService;

/**
 * Created by gitahi on 08/05/15.
 */
public class RecordFragment extends ListFragment {

    private List<Record> records;
    private RecordSearch recordSearch;
    private RecordArrayAdapter2 adapter;
    private OnRecordSelectedListener onRecordSelectedListener;
    private OnRecordSearchUpdatedListener onRecordSearchUpdatedListener;
    private OnRecordsLoadedListener onRecordsLoadedListener;

    private ProgressDialog progressDialog;
    private SearchTask searchTask;

    public void setRecordSearch(RecordSearch recordSearch) {
        this.recordSearch = recordSearch;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        records = new ArrayList<>();
    }

    @Override
    public void onResume() {
        super.onResume();
        progressDialog = new ProgressDialog(this.getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        Record deflatedRecord = records.get(position);
        ((AndroidHojiContext) getActivity().getApplication()).inflateRecord(
                deflatedRecord,
                onRecordSelectedListener,
                progressDialog,
                getActivity()
        );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        adapter = new RecordArrayAdapter2
                (
                        inflater.getContext(),
                        records
                );
        setListAdapter(adapter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final Record selected = records.get(position);
                final String recordName = selected.getCaption() != null && !selected.getCaption().equals("") ?
                        selected.getCaption() : getResources().getString(R.string.this_record);
                new AlertDialog.Builder(RecordFragment.this.getActivity())
                        .setTitle(getResources().getString(R.string.delete_record_title))
                        .setMessage(getResources().getString(R.string.delete_record_message, recordName))
                        .setPositiveButton(R.string.yes_action, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AndroidHojiContext hojiContext = (AndroidHojiContext) getActivity().getApplication();
                                RecordService recordService = hojiContext.getModelServiceManager().getRecordService();
                                recordService.delete(selected);
                                long timeStamp = 0L;
                                if (searchTask == null) {
                                    searchTask = createSearchTask(timeStamp, false);
                                    searchTask.execute();
                                } else {
                                    if (searchTask.getStatus() == AsyncTask.Status.FINISHED) {
                                        searchTask = createSearchTask(timeStamp, false);
                                        searchTask.execute();
                                        onRecordSearchUpdatedListener.onRecordSearchUpdated(recordSearch);
                                    }
                                }
                            }
                        })
                        .setNegativeButton(R.string.no_action, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //Do nothing
                            }
                        })
                        .create()
                        .show();
                return true;
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        onRecordSelectedListener = (OnRecordSelectedListener) getActivity();
        onRecordSearchUpdatedListener = (OnRecordSearchUpdatedListener) getActivity();
        onRecordsLoadedListener = (OnRecordsLoadedListener) getActivity();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.record_fragment_actions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.previousButton:
                if (recordSearch.getPageInfo().hasPrevious() && !records.isEmpty()) {
                    long timeStamp = new NavigableList<>(records).first().getDateCreated();
                    if (searchTask == null) {
                        searchTask = createSearchTask(timeStamp, true);
                        searchTask.execute();
                    } else {
                        if (searchTask.getStatus() == AsyncTask.Status.FINISHED) {
                            searchTask = createSearchTask(timeStamp, true);
                            searchTask.execute();
                            onRecordSearchUpdatedListener.onRecordSearchUpdated(recordSearch);
                        }
                    }
                } else {
                    nothingMore();
                }
                return true;
            case R.id.nextButton:
                if (recordSearch.getPageInfo().hasNext() && !records.isEmpty()) {
                    long timeStamp = new NavigableList<>(records).last().getDateCreated();
                    if (searchTask == null) {
                        searchTask = createSearchTask(timeStamp, false);
                        searchTask.execute();
                    } else {
                        if (searchTask.getStatus() == AsyncTask.Status.FINISHED) {
                            searchTask = createSearchTask(timeStamp, false);
                            searchTask.execute();
                            onRecordSearchUpdatedListener.onRecordSearchUpdated(recordSearch);
                        }
                    }
                } else {
                    nothingMore();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private SearchTask createSearchTask(long timeStamp, boolean reverse) {
        boolean testMode = PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getBoolean(SettingsActivity.TEST_MODE, Boolean.FALSE);
        recordSearch.setTimeStamp(timeStamp);
        recordSearch.setReverse(reverse);
        recordSearch.setTestMode(testMode);
        String searchTaskId = recordSearch.getStage() + "" + recordSearch.getTimeStamp();
        return new SearchTask(searchTaskId, recordSearch, this.getActivity(), onRecordsLoadedListener, false, progressDialog);
    }

    public void refresh(List<Record> records) {
        if (records != null) {
            this.records.clear();
            this.records.addAll(records);
            adapter.notifyDataSetChanged();
        }
    }

    private void nothingMore() {
        Toast toast = Toast.makeText(getActivity(),
                getResources().getText(R.string.nothing_more), Toast.LENGTH_SHORT);
        toast.show();
    }
}