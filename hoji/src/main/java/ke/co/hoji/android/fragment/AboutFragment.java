package ke.co.hoji.android.fragment;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.ListFragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.activity.InitialActivity;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.adapter.PropertyArrayAdapter;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.service.model.TranslationService;

/**
 * Created by gitahi on 08/05/15.
 */
public class AboutFragment extends ListFragment {

    private List<Property> properties;

    private int count = 0;
    private long start;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        properties = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        PropertyArrayAdapter adapter = new PropertyArrayAdapter
                (
                        inflater.getContext(),
                        properties
                );
        setListAdapter(adapter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AndroidHojiContext hojiContext = (AndroidHojiContext) getActivity().getApplication();
        TranslationService translationService = hojiContext.getModelServiceManager().getTranslationService();
        Integer languageId = hojiContext.getLanguageId();
        Survey survey = hojiContext.getSurvey();
        {
            Property property = new Property();
            property.setKey(getString(R.string.about_user));
            property.setValue(hojiContext.getNameOfUser() + " - " + hojiContext.getUserCode());
            properties.add(property);
        }
        {
            Property property = new Property();
            property.setKey(getString(R.string.about_survey));
            property.setValue(
                    translationService.translate(
                            survey,
                            languageId,
                            Survey.TranslatableComponent.NAME,
                            survey.getName()
                    )
            );
            properties.add(property);
        }
        {
            Property property = new Property();
            property.setKey(getString(R.string.about_forms));
            property.setValue(getString(R.string.about_forms_tap));
            properties.add(property);
        }
        {
            Property property = new Property();
            property.setKey(getString(R.string.about_tenant));
            String tenantName = hojiContext.getTenantName();
            String tenantCode = hojiContext.getTenantCode();
            property.setValue(tenantName + " - " + tenantCode);
            properties.add(property);
        }
        {
            Property property = new Property();
            property.setKey(getString(R.string.about_server_title));
            property.setValue(ke.co.hoji.android.data.Constants.SERVER_URL);
            properties.add(property);
        }
        {
            Property property = new Property();
            property.setKey(getString(R.string.about_access));
            property.setValue(getAccessString(survey.getAccess()));
            properties.add(property);
        }
        {
            Property property = new Property();
            property.setKey(getString(R.string.about_website_title));
            property.setValue(getString(R.string.about_website_description));
            properties.add(property);
        }
        {
            Property property = new Property();
            property.setKey(getString(R.string.about_facebook_title));
            property.setValue(getString(R.string.about_facebook_description));
            properties.add(property);
        }
        {
            Property property = new Property();
            property.setKey(getString(R.string.about_twitter_title));
            property.setValue(getString(R.string.about_twitter_description));
            properties.add(property);
        }
        {
            Property property = new Property();
            property.setKey(getString(R.string.about_email_title));
            property.setValue(getString(R.string.about_email_description));
            properties.add(property);
        }
        {
            Property property = new Property();
            property.setKey(getString(R.string.about_developer));
            property.setValue(getString(R.string.about_hoji_ltd));
            properties.add(property);
        }
        {
            try {
                PackageManager manager = hojiContext.getPackageManager();
                PackageInfo info = manager.getPackageInfo(hojiContext.getPackageName(), 0);
                String version = info.versionName;
                Property property = new Property();
                property.setKey(getString(R.string.about_version));
                property.setValue(version);
                properties.add(property);
            } catch (Exception ex) {
                //Do nothing.
            }
        }
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        switch (position) {
            case 2:
                //Forms
                showFormVersions();
                break;
            case 6:
                //Website
                openUrl("https://www.hoji.co.ke/");
                break;
            case 7:
                //Facebook
                String url = "https://www.facebook.com/HojiLtd";
                try {
                    Uri uri = Uri.parse(url);
                    try {
                        ApplicationInfo applicationInfo = getActivity().getPackageManager().getApplicationInfo("com.facebook.katana", 0);
                        if (applicationInfo.enabled) {
                            uri = Uri.parse("fb://facewebmodal/f?href=" + url);
                        }
                    } catch (PackageManager.NameNotFoundException ignored) {
                    }
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } catch (Exception ex) {
                    openUrl(url);
                }
                break;
            case 8:
                //Twitter
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=HojiLtd"));
                    startActivity(intent);
                } catch (Exception ex) {
                    openUrl("https://twitter.com/HojiLtd");
                }
                break;
            case 9:
                //Support
                try {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    String uriText = "mailto:" + Constants.SUPPORT_EMAIL +
                            "?subject=" + Uri.encode(getResources().getString(R.string.support_email_title)) +
                            "&body=" + Uri.encode(getResources().getString(R.string.support_email_message));
                    Uri uri = Uri.parse(uriText);
                    intent.setData(uri);
                    startActivity(Intent.createChooser(
                            intent,
                            getResources().getString(R.string.support_email_prompt))
                    );
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(this.getActivity(), R.string.unable_to_open_email, Toast.LENGTH_SHORT).show();
                }
                break;
            case 11:
                //Version
                undoAllUploads();
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.about_fragment_actions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.signOutButton:
                AndroidHojiContext hojiContext = (AndroidHojiContext) getActivity().getApplication();
                hojiContext.logout();
                Intent intent = new Intent(getActivity(), InitialActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                getActivity().startActivity(intent);
                getActivity().finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private String getAccessString(int access) {
        String accessString = "";
        switch (access) {
            case 0:
                accessString = getString(R.string.about_access_0);
                break;
            case 1:
                accessString = getString(R.string.about_access_1);
                break;
            case 2:
                accessString = getString(R.string.about_access_2);
                break;
            case 3:
                accessString = getString(R.string.about_access_3);
                break;
        }
        return accessString;
    }

    private void showFormVersions() {
        AndroidHojiContext hojiContext = (AndroidHojiContext) getActivity().getApplication();
        TranslationService translationService = hojiContext.getModelServiceManager().getTranslationService();
        Integer languageId = hojiContext.getLanguageId();
        List<Form> forms = hojiContext.getSurvey().getForms();
        int n = forms.size();
        final String[] formVersions = new String[n];
        for (int i = 0; i < n; i++) {
            Form form = forms.get(i);
            formVersions[i] = translationService.translate(
                    form,
                    languageId,
                    Form.TranslatableComponent.NAME,
                    form.getName()
            ) + " - " + form.getFullVersion();
        }
        new AlertDialog.Builder(getActivity())
                .setPositiveButton(R.string.ok_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Do nothing. Just dismiss the dialog.
                    }
                })
                .setTitle(getString(R.string.about_form_versions))
                .setItems(formVersions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do nothing.
                    }
                })
                .create()
                .show();
    }

    private void undoAllUploads() {
        if (count == 0) {
            start = new Date().getTime();
        }
        count++;
        if (count == 7) {
            long end = new Date().getTime();
            long length = (end - start) / 1000;
            if (length <= 10) {
                ((AndroidHojiContext) getActivity().getApplication()).getDtoServiceManager()
                        .getRecordService().undoUpdateStatuses();
                Toast toast = Toast.makeText(getActivity(),
                        getResources().getText(R.string.sent_records_undone), Toast.LENGTH_LONG);
                toast.show();
            } else {
                count = 0;
            }
        } else if (count == 5) {
            Toast toast = Toast.makeText(getActivity(),
                    getResources().getText(R.string.about_to_undo_sent_records), Toast.LENGTH_LONG);
            toast.show();
        }
    }

    private void openUrl(String url) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(this.getActivity(), R.string.unable_to_open_browser, Toast.LENGTH_SHORT).show();
        }
    }
}