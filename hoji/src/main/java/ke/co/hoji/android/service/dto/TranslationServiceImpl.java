package ke.co.hoji.android.service.dto;

import java.util.List;

import ke.co.hoji.android.dao.dto.SqliteTranslationDao;
import ke.co.hoji.core.data.dto.Translation;
import ke.co.hoji.core.service.dto.TranslationService;

public class TranslationServiceImpl implements TranslationService {

    private final SqliteTranslationDao translationDao;

    public TranslationServiceImpl(SqliteTranslationDao translationDao) {
        this.translationDao = translationDao;
    }

    @Override
    public void saveTranslations(List<Translation> translations) {
        translationDao.saveTranslations(translations);
    }

}
