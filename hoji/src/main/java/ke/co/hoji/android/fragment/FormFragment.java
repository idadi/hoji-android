package ke.co.hoji.android.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.helper.LetterIconCreator;
import ke.co.hoji.android.listener.OnFormSelectedListener;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.TranslationService;

/**
 * Created by gitahi on 07/05/15.
 */
public class FormFragment extends Fragment {

    private List<Form> forms;

    private OnFormSelectedListener onFormSelectedListener;

    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        recyclerView = (RecyclerView) inflater.inflate(
                R.layout.home_fragment, container, false);
        setupRecyclerView(recyclerView);
        return recyclerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new FormRecyclerViewAdapter(forms));
    }

    public void setForms(List<Form> forms) {
        if (this.forms == null) {
            this.forms = new ArrayList<>();
        }
        this.forms.clear();
        this.forms.addAll(forms);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        onFormSelectedListener = (OnFormSelectedListener) getActivity();
    }

    public void refresh() {
        if (forms != null && recyclerView != null) {
            setupRecyclerView(recyclerView);
        }
    }

    public class FormRecyclerViewAdapter
            extends RecyclerView.Adapter<FormRecyclerViewAdapter.ViewHolder> {

        private final TypedValue typedValue = new TypedValue();
        private final int backGround;
        private final List<Form> forms;

        private final LetterIconCreator letterIconCreator = new LetterIconCreator();

        public class ViewHolder extends RecyclerView.ViewHolder {

            Form form;
            final View view;
            final ImageView formIconImageView;
            final TextView formNameTextView;
            final TextView formDescriptionTextView;

            ViewHolder(View view) {
                super(view);
                this.view = view;
                formIconImageView = view.findViewById(R.id.formIconImageView);
                formNameTextView = view.findViewById(R.id.formNameTextView);
                formDescriptionTextView = view.findViewById(R.id.formDescriptionTextView);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + formNameTextView.getText();
            }
        }

        FormRecyclerViewAdapter(List<Form> items) {
            getActivity().getTheme().resolveAttribute(R.attr.selectableItemBackground, typedValue, true);
            backGround = typedValue.resourceId;
            forms = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.form_row, parent, false);
            view.setBackgroundResource(backGround);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            AndroidHojiContext androidHojiContext = (AndroidHojiContext) getActivity().getApplication();
            TranslationService translationService = androidHojiContext.getModelServiceManager().getTranslationService();
            Integer languageId = androidHojiContext.getLanguageId();
            holder.form = forms.get(position);
            String formName = translationService.translate(
                    holder.form,
                    languageId,
                    Form.TranslatableComponent.NAME,
                    holder.form.getName()
            );
            holder.formNameTextView.setText(Html.fromHtml(formName));
            holder.formDescriptionTextView.setText(getText(R.string.tap_to_open));
            holder.formIconImageView.setImageDrawable(
                    letterIconCreator.createIcon(
                            FormFragment.this.getActivity(),
                            formName,
                            String.valueOf(holder.form.getId())
                    )
            );
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Form selected = holder.form;
                    FormFragment.this.onFormSelectedListener.onFormSelected(selected);
                }
            });
        }

        @Override
        public int getItemCount() {
            return forms.size();
        }
    }
}