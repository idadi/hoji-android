package ke.co.hoji.android.widget;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.fragment.app.DialogFragment;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by gitahi on 15/05/15.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private DateComponent dateComponent;

    private final Date min;
    private final Date max;

    public DatePickerFragment() {
        this(null, null);
    }

    public DatePickerFragment(Date min, Date max) {
        this.min = min;
        this.max = max;
    }

    public void setDateComponent(DateComponent dateComponent) {
        this.dateComponent = dateComponent;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        if (dateComponent != null && dateComponent.getDate() != null) {
            calendar.setTime(dateComponent.getDate());
        }
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        setMinAndMaxDates(dialog.getDatePicker());
        return dialog;
    }

    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        if (dateComponent == null) {
            return;
        }
        Date date = getDateFromDatePicker(datePicker);
        if (dateComponent instanceof DateWidget) {
            DateWidget dateWidget = (DateWidget) dateComponent;
            dateWidget.setDate(date, true);
        } else {
            dateComponent.setDate(date);
        }
    }

    private Date getDateFromDatePicker(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return calendar.getTime();
    }

    private void setMinAndMaxDates(DatePicker datePicker) {
        if (min != null) {
            datePicker.setMinDate(min.getTime());
        }
        if (max != null) {
            datePicker.setMaxDate(max.getTime());
        }
    }
}
