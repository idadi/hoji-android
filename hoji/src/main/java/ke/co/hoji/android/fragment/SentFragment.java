package ke.co.hoji.android.fragment;

import ke.co.hoji.android.manager.FeedbackManager;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.data.model.MainRecord;

/**
 * Created by gitahi on 15/04/18.
 */
public class SentFragment extends RecordFolderFragment {

    private FeedbackManager feedbackManager;

    private HojiContext hojiContext;

    /**
     * Track when we have requested feedback so we don't ask again every time the user scrolls up.
     */
    private boolean feedbackRequested = false;

    @Override
    public int getStage() {
        return MainRecord.Stage.UPLOADED;
    }

    @Override
    public void requestForFeedback() {
        if (!feedbackRequested) {
            FeedbackManager fm = getFeedbackManager();
            if (fm.isFeedbackEligible(false)) {
                fm.requestFeedback(getActivity(), getNameOfUser());
                feedbackRequested = true;
            }
        }
    }

    private FeedbackManager getFeedbackManager() {
        if (feedbackManager == null) {
            if (hojiContext == null) {
                hojiContext = (HojiContext) getActivity().getApplication();
            }
            ModelServiceManager modelServiceManager = hojiContext.getModelServiceManager();
            feedbackManager = new FeedbackManager(
                    modelServiceManager.getPropertyService(),
                    modelServiceManager.getRecordService()
            );
        }
        return feedbackManager;
    }

    private String getNameOfUser() {
        if (hojiContext == null) {
            hojiContext = (HojiContext) getActivity().getApplication();
        }
        return hojiContext.getNameOfUser();
    }
}