package ke.co.hoji.android.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

import java.util.List;

import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.service.model.TranslationService;

public class ChoiceArrayAdapter extends ArrayAdapter<Choice> {

    private final boolean editable;

    public ChoiceArrayAdapter(Context context, int resource, List<Choice> choices, boolean editable) {
        super(context, resource, choices);
        this.editable = editable;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AndroidHojiContext androidHojiContext = (AndroidHojiContext) getContext().getApplicationContext();
        TranslationService translationService = androidHojiContext.getModelServiceManager().getTranslationService();
        Integer languageId = androidHojiContext.getLanguageId();
        Choice choice = getItem(position);
        CheckedTextView checkedTextView = (CheckedTextView) super.getView(position, convertView, parent);
        checkedTextView.setText(
                translationService.translate(
                        choice,
                        languageId,
                        Choice.TranslatableComponent.NAME,
                        choice.getName()
                )
        );
        if (choice.isAlreadyEntered()) {
            checkedTextView.setPaintFlags(checkedTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            checkedTextView.setPaintFlags(checkedTextView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
        return checkedTextView;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return editable;
    }

    @Override
    public boolean isEnabled(int position) {
        return editable;
    }
}
