package ke.co.hoji.android.helper;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.activity.HomeActivity;
import ke.co.hoji.android.fragment.DraftsFragment;
import ke.co.hoji.android.fragment.FormFragment;
import ke.co.hoji.android.fragment.OutboxFragment;
import ke.co.hoji.android.fragment.SentFragment;

/**
 * Created by gitahi on 01/09/15.
 */
public class HomePagerAdapter extends FragmentPagerAdapter {

    private final HomeActivity homeActivity;

    private final List<Fragment> fragments = new ArrayList<>();
    private final List<String> titles = new ArrayList<>();

    public HomePagerAdapter(FragmentManager fragmentManager, HomeActivity homeActivity) {
        super(fragmentManager);
        this.homeActivity = homeActivity;
        init();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object object = super.instantiateItem(container, position);
        if (object instanceof Fragment) {
            Fragment fragment = (Fragment) object;
            if (position == 0) {
                FormFragment ff = (FormFragment) fragment;
                ff.setForms(homeActivity.getForms());
                ff.refresh();
            }
        }
        return object;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    private void init() {
        {
            FormFragment fragment = new FormFragment();
            fragments.add(fragment);
            titles.add(homeActivity.getResources().getString(R.string.forms_tab_title));
        }
        {
            DraftsFragment fragment = new DraftsFragment();
            fragments.add(fragment);
            titles.add(homeActivity.getResources().getString(R.string.drafts_tab_title));
        }
        {
            OutboxFragment fragment = new OutboxFragment();
            fragments.add(fragment);
            titles.add(homeActivity.getResources().getString(R.string.outbox_tab_title));
        }
        {
            SentFragment fragment = new SentFragment();
            fragments.add(fragment);
            titles.add(homeActivity.getResources().getString(R.string.sent_tab_title));
        }
    }
}
