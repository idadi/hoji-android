package ke.co.hoji.android.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import ke.co.hoji.BuildConfig;
import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.data.LaunchType;
import ke.co.hoji.android.data.LocationWrapper;
import ke.co.hoji.android.fragment.WizardFragment;
import ke.co.hoji.android.helper.HojiBitmap;
import ke.co.hoji.android.helper.ImageCapturer;
import ke.co.hoji.android.helper.ImageEncoder;
import ke.co.hoji.android.helper.ImageLocation;
import ke.co.hoji.android.helper.ImageReceiver;
import ke.co.hoji.android.listener.OnGpsStatusChangedListener;
import ke.co.hoji.android.listener.OnWidgetRenderedListener;
import ke.co.hoji.android.listener.OnLiveFieldSavedListener;
import ke.co.hoji.android.listener.OnLiveFieldSelectedListener;
import ke.co.hoji.android.listener.OnRecordEndedListener;
import ke.co.hoji.android.service.UploadService;
import ke.co.hoji.android.widget.LocationWidget;
import ke.co.hoji.android.widget.helper.LocationComponent;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.GpsLocation;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.response.ImageResponse;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.core.service.model.TranslationService;
import ke.co.hoji.core.widget.Widget;

/**
 * Created by gitahi on 08/05/15.
 */
public class DataEntryActivity
        extends
        SwipableActivity
        implements
        OnLiveFieldSelectedListener,
        OnLiveFieldSavedListener,
        OnRecordEndedListener,
        OnWidgetRenderedListener,
        OnGpsStatusChangedListener,
        ImageCapturer,
        RecordContainer,
        LocationComponent {

    private static final int CAMERA_REQUEST = 1;
    private ImageReceiver imageReceiver;
    private ImageLocation imageLocation = null;

    private static final int LOCATION_REQUEST = 1;

    private int launchType;
    private Form form;
    private Record record;
    private LiveField liveField;

    private AndroidHojiContext androidHojiContext;

    private TextView testModeTextView;
    private ProgressBar formProgressBar;
    private ProgressDialog gpsProgressDialog;
    private ProgressDialog mandatoryLocationProgressDialog;
    private ProgressDialog optionalLocationProgressDialog;

    private boolean readyToUse = true;

    public TextView getTestModeTextView() {
        return testModeTextView;
    }

    public ProgressBar getFormProgressBar() {
        return formProgressBar;
    }

    public Form getForm() {
        return form;
    }

    public int getLaunchType() {
        return launchType;
    }

    public LiveField getLiveField() {
        return liveField;
    }

    @Override
    public Record getRecord() {
        return record;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_entry_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        androidHojiContext = (AndroidHojiContext) getApplication();
        if (savedInstanceState != null) {
            launchType = savedInstanceState.getInt(Constants.LAUNCH_TYPE);
            if (launchType == LaunchType.NEW) {
                Form deflatedForm = (Form) savedInstanceState.getSerializable(Constants.DEFLATED_FORM);
                form = androidHojiContext.inflateForm(deflatedForm);
            } else if (launchType == LaunchType.OPEN || launchType == LaunchType.RESUME) {
                Record deflatedRecord = (Record) savedInstanceState.getSerializable(Constants.DEFLATED_RECORD);
                LiveField deflatedLiveField = (LiveField) savedInstanceState.getSerializable(Constants.DEFLATED_LIVE_FIELD);
                record = androidHojiContext.inflateRecord(deflatedRecord);
                liveField = androidHojiContext.inflateLiveField(deflatedLiveField, record);
            }
            imageLocation = androidHojiContext.getImageLocation();
            imageReceiver = androidHojiContext.getImageReceiver();
        } else {
            launchType = getIntent().getExtras().getInt(Constants.LAUNCH_TYPE);
            if (launchType == LaunchType.NEW) {
                Form deflatedForm = (Form) getIntent().getSerializableExtra(Constants.DEFLATED_FORM);
                form = androidHojiContext.inflateForm(deflatedForm);
            } else if (launchType == LaunchType.OPEN || launchType == LaunchType.RESUME) {
                Intent intent = getIntent();
                Record deflatedRecord = (Record) intent.getSerializableExtra(Constants.DEFLATED_RECORD);
                LiveField deflatedLiveField = (LiveField) intent.getSerializableExtra(Constants.DEFLATED_LIVE_FIELD);
                record = androidHojiContext.inflateRecord(deflatedRecord);
                liveField = androidHojiContext.inflateLiveField(deflatedLiveField, record);
            }
        }
        refreshResponseHojiContext(record);
        testModeTextView = findViewById(R.id.testModeTextView);
        formProgressBar = findViewById(R.id.formProgressBar);
        testModeTextView.setPadding(20, 10, 20, 0);
        boolean testMode = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(SettingsActivity.TEST_MODE, Boolean.FALSE);
        if (testMode) {
            testModeTextView.setVisibility(View.VISIBLE);
            formProgressBar.setPadding(20, 0, 20, 0);
        } else {
            testModeTextView.setVisibility(View.GONE);
            formProgressBar.setPadding(20, 20, 20, 0);
        }
        View fragmentContainer = findViewById(R.id.fragment_container);
        if (fragmentContainer != null) {
            if (savedInstanceState != null) {
                return;
            }
            WizardFragment wizardFragment = new WizardFragment();
            wizardFragment.setArguments(getIntent().getExtras());
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.fragment_container, wizardFragment);
            transaction.commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestLocationAccess();
        androidHojiContext.connectLocationService(this);
    }

    @Override
    protected void onStop() {
        androidHojiContext.disconnectLocationService();
        boolean autoUpload = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(SettingsActivity.AUTO_UPLOAD, Boolean.TRUE);
        if (autoUpload) {
            Intent intent = new Intent(this, UploadService.class);
            intent.putExtra(Constants.SHOW_UPLOAD_PROGRESS, false);
            this.startService(intent);
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissGpsProgressDialog();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Form deflatedForm = form != null ? androidHojiContext.deflateForm(form) : null;
        Record deflatedRecord = androidHojiContext.deflateRecord(record);
        LiveField deflatedLiveField = androidHojiContext.deflateLiveField(liveField);
        outState.putInt(Constants.LAUNCH_TYPE, launchType);
        outState.putSerializable(Constants.DEFLATED_FORM, deflatedForm);
        outState.putSerializable(Constants.DEFLATED_RECORD, deflatedRecord);
        outState.putSerializable(Constants.DEFLATED_LIVE_FIELD, deflatedLiveField);
        androidHojiContext.setImageLocation(imageLocation);
        androidHojiContext.setImageReceiver(imageReceiver);
    }

    @Nullable
    @Override
    public Intent getParentActivityIntent() {
        Intent intent;
        int launchType = getIntent().getExtras().getInt(Constants.LAUNCH_TYPE);
        if (launchType == LaunchType.NEW) {
            intent = new Intent(this, HomeActivity.class);
        } else if (launchType == LaunchType.OPEN) {
            intent = new Intent(this, LiveFieldActivity.class);
            intent.putExtra(Constants.DEFLATED_RECORD, record);
        } else {
            intent = super.getParentActivityIntent();
        }
        return intent;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onLiveFieldSelected(LiveField liveField) {
        this.liveField = liveField;
        this.launchType = LaunchType.OPEN;
        getSupportFragmentManager().popBackStack();
    }

    public void onLiveFieldSaved(LiveField liveField, Record record) {
        this.liveField = liveField;
        this.record = record;
        this.launchType = LaunchType.RESUME;
        if (liveField.getField().getType().isImage()) {
            // Deflate image to an empty array to avoid overrunning 1 MiB bundle limit.
            liveField.setResponse(((ImageResponse) liveField.getResponse()).prune());
        }
    }

    public void onRecordEnded(Record record) {
        Toast toast = Toast.makeText(this,
                getResources().getString(R.string.completed, record.getCaption()), Toast.LENGTH_LONG);
        toast.show();
        Intent intent = new Intent(DataEntryActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    /**
     * A callback invoked when a {@link Widget} has been rendered. We also use this method to
     * start or pause the location service. If the current form has less than 10 fields in total, we
     * always listen for location information. If more than that, we listen for location information
     * only if we are at 0% or greater than 90% completion to save battery - since we only need
     * location information at the beginning and at the end of the record.
     *
     * @param widget the {@link Widget} rendered.
     * @param record the {@link Record} associated with the rendered liveField.
     */
    @Override
    public void onWidgetRendered(Widget widget, Record record) {
        int recordProgress = 0;
        if (record != null && !record.getLiveFields().isEmpty()) {
            this.liveField = widget.getLiveField();
            this.launchType = LaunchType.RESUME;
            recordProgress = record.computeProgress();
        }
        formProgressBar.setProgress(recordProgress);
        if (recordProgress == 0 || recordProgress >= 90 || record.getFieldParent().getChildren().size() < 10) {
            if (androidHojiContext.isLocationEnabled()) {
                this.setReadyToUse(false);
                int minAccuracy = getMinGpsAccuracy();
                int priority = LocationRequest.PRIORITY_HIGH_ACCURACY;
                if (minAccuracy == -1 || minAccuracy > Constants.HIGH_ACCURACY_THRESHOLD) {
                    priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
                }
                androidHojiContext.startLocationService(this, priority);
            }
        } else {
            androidHojiContext.pauseLocationService(this);
        }
        setTitle(createTitle());
    }

    @Override
    public void startCapture(ImageReceiver imageReceiver, String directoryPath, String fileName) {
        this.imageReceiver = imageReceiver;
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            File imageFile = null;
            try {
                ImageEncoder imageEncoder = new ImageEncoder(androidHojiContext);
                imageFile = imageEncoder.createImageFile(directoryPath, fileName);
            } catch (IOException ex) {
                Toast.makeText(
                        this,
                        getResources().getString(R.string.picture_error), Toast.LENGTH_LONG
                ).show();
            }
            if (imageFile != null) {
                String authority = BuildConfig.APPLICATION_ID + ".provider";
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    imageLocation = new ImageLocation(
                            FileProvider.getUriForFile(this, authority, imageFile),
                            imageFile.getAbsolutePath());
                } else {
                    imageLocation = new ImageLocation(
                            Uri.fromFile(imageFile),
                            imageFile.getAbsolutePath()
                    );
                }
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageLocation.getUri());
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        }
    }

    @Override
    public void finishCapture(HojiBitmap hojiBitmap) {
        if (imageReceiver != null) {
            imageReceiver.receiveImage(hojiBitmap);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap image = this.getImage();
            if (image != null) {
                HojiBitmap hojiBitmap = new HojiBitmap(image, imageLocation.getPath());
                finishCapture(hojiBitmap);
            }
        }
    }

    @Override
    protected void swipeDetected(int direction) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (currentFragment instanceof WizardFragment) {
            WizardFragment wizardFragment = (WizardFragment) currentFragment;
            if (direction == RIGHT) {
                wizardFragment.previous();
            } else if (direction == LEFT) {
                wizardFragment.next();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                androidHojiContext.permitLocationService(true);
            } else {
                androidHojiContext.permitLocationService(false);
            }
        }
    }

    @Override
    public boolean isReadyToUse() {
        return readyToUse;
    }

    @Override
    public void setReadyToUse(boolean readyToUse) {
        this.readyToUse = readyToUse;
    }

    @Override
    public boolean acceptsLastKnownLocation() {
        return true;
    }

    @Override
    public void updateLocation(LocationWrapper locationWrapper, boolean lastKnown) {
        androidHojiContext.setLocationWrapper(locationWrapper);
        if (!isReadyToUse()) {
            return;
        }
        Form f = form != null ? form : (Form) record.getFieldParent();
        boolean mandatory = f.getGpsCapture() == Form.GpsCapture.ALWAYS;
        int gpsQuality = checkGpsQuality(locationWrapper);
        if (gpsQuality == GpsQuality.GOOD) {
            useLocation(locationWrapper);
        } else {
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (currentFragment instanceof WizardFragment) {
                WizardFragment wizardFragment = (WizardFragment) currentFragment;
                if (wizardFragment.isAtRecordEnd()) {
                    useLocation(locationWrapper);
                } else {
                    showGpsProgressDialog(locationWrapper, mandatory);
                }
            }
        }
    }

    @Override
    public void onGpsReady(LocationWrapper locationWrapper) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (currentFragment instanceof WizardFragment) {
            WizardFragment wizardFragment = (WizardFragment) currentFragment;
            if (!wizardFragment.isAtRecordEnd()) {
                setGpsLocation(locationWrapper);
                wizardFragment.proceedWithNavigation();
            } else {
                onRecordEnded(androidHojiContext.getRecord());
            }
        }
    }

    @Override
    public void onGpsAborted(LocationWrapper locationWrapper) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (currentFragment instanceof WizardFragment) {
            WizardFragment wizardFragment = (WizardFragment) currentFragment;
            if (!wizardFragment.isAtRecordEnd()) {
                setGpsLocation(locationWrapper);
                wizardFragment.abortNavigation();
            } else {
                onRecordEnded(androidHojiContext.getRecord());
            }
        }
    }

    public void enableLocationMandatory(String fieldOrForm) {
        final Context context = this;
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(context.getResources().getString(R.string.enable_location_title));
        dialog.setMessage(context.getResources().getString(R.string.enable_location_mandatory, fieldOrForm));
        dialog.setPositiveButton(context.getResources().getString(R.string.yes_action), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });
        dialog.setNegativeButton(context.getString(R.string.no_action), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                //do nothing
            }
        });
        dialog.show();
    }

    public void enableLocationNonMandatory(String fieldOrForm) {
        final Context context = this;
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        Boolean showLocationDialog = sharedPreferences.getBoolean(Constants.SHOW_LOCATION_DIALOG, true);
        if (showLocationDialog) {
            View checkBoxView = View.inflate(context, R.layout.checkbox, null);
            CheckBox checkBox = checkBoxView.findViewById(R.id.checkbox);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    SharedPreferences sharedPreferences = context.getSharedPreferences
                            (
                                    Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE
                            );
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean(Constants.SHOW_LOCATION_DIALOG, !isChecked);
                    editor.commit();
                }
            });
            checkBox.setText(context.getString(R.string.never_ask_again));
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setTitle(context.getResources().getString(R.string.enable_location_title)).setView(checkBoxView);
            dialog.setMessage(context.getResources()
                    .getString(R.string.enable_location_non_mandatory, fieldOrForm)).setView(checkBoxView);
            dialog.setPositiveButton(context.getResources().getString(R.string.yes_action), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(intent);
                }
            });
            dialog.setNegativeButton(context.getString(R.string.no_action), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    onGpsReady(null);
                }
            });
            dialog.show();
        } else {
            onGpsReady(null);
        }
    }

    // Create the ProgressDialog for location here so that it is always based on the current Activity,
    // given that the current Activity in the context of a Widget morphs due to Widget caching.
    public ProgressDialog getLocationWidgetProgressDialog(boolean mandatory, final LocationWrapper locationWrapper, final LocationWidget locationWidget) {
        if (mandatory) {
            if (mandatoryLocationProgressDialog == null) {
                mandatoryLocationProgressDialog = new ProgressDialog(this);
                mandatoryLocationProgressDialog.setCancelable(false);
                mandatoryLocationProgressDialog.setButton(
                        DialogInterface.BUTTON_NEGATIVE, getResources().getText(R.string.cancel_action),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                locationWidget.setReadyToUse(false);
                                dismissGpsProgressDialog();
                            }
                        });
            }
            return mandatoryLocationProgressDialog;
        } else {
            if (optionalLocationProgressDialog == null) {
                optionalLocationProgressDialog = new ProgressDialog(this);
                optionalLocationProgressDialog.setCancelable(false);
                optionalLocationProgressDialog.setButton(
                        DialogInterface.BUTTON_POSITIVE, getResources().getText(R.string.continue_anyway_gps),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                locationWidget.useLocation(locationWrapper);
                            }
                        });
                optionalLocationProgressDialog.setButton(
                        DialogInterface.BUTTON_NEGATIVE, getResources().getText(R.string.cancel_action),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                locationWidget.setReadyToUse(false);
                                dismissGpsProgressDialog();
                            }
                        });
            }
            return optionalLocationProgressDialog;
        }
    }

    private int checkGpsQuality(LocationWrapper locationWrapper) {
        if (locationWrapper == null) {
            return GpsQuality.NULL;
        }
        Integer minGpsAccuracy = getMinGpsAccuracy();
        int gpsAccuracy = (int) Math.ceil(locationWrapper.getAccuracy());
        if (minGpsAccuracy >= 0 && gpsAccuracy > minGpsAccuracy) {
            return GpsQuality.INACCURATE;
        }
        if (!BuildConfig.DEBUG) {
            Long gpsShelfLife = getMaxGpsShelfLife();
            if (gpsShelfLife == null || gpsShelfLife < 1) {
                gpsShelfLife = DEFAULT_GPS_SHELF_LIFE;
            }
            gpsShelfLife = gpsShelfLife * 60 * 1000;
            long gpsAge = new Date().getTime() - locationWrapper.getTime();
            if (gpsAge > gpsShelfLife) {
                return GpsQuality.STALE;
            }
        }
        return GpsQuality.GOOD;
    }

    /*
     * Locations with an accuracy value more than returned here are considered unusable.
     */
    private int getMinGpsAccuracy() {
        Integer minGpsAccuracy = -1;
        int formId = form != null ? form.getId() : record.getFieldParent().getId();
        PropertyService propertyService = androidHojiContext.getModelServiceManager().getPropertyService();
        Survey survey = androidHojiContext.getSurvey();
        String propKey = ke.co.hoji.core.data.Constants.Server.GPS_ACCURACY + "." + formId;
        Property property = propertyService.getProperty(propKey, survey.getId());
        if (property == null) {
            propKey = ke.co.hoji.core.data.Constants.Server.GPS_ACCURACY;
            property = propertyService.getProperty(propKey, survey.getId());
        }
        if (property != null && !StringUtils.isBlank(property.getValue())) {
            minGpsAccuracy = Utils.parseInteger(property.getValue());
            if (minGpsAccuracy == null || minGpsAccuracy < 1) {
                minGpsAccuracy = -1;
            }
        }
        return minGpsAccuracy;
    }

    /*
     * Locations older than the number of minutes returned here are considered stale and unusable.
     */
    private long getMaxGpsShelfLife() {
        Long maxGpsShelfLife = 15L;
        int formId = form != null ? form.getId() : record.getFieldParent().getId();
        PropertyService propertyService = androidHojiContext.getModelServiceManager().getPropertyService();
        Survey survey = androidHojiContext.getSurvey();
        String propKey = ke.co.hoji.core.data.Constants.Server.GPS_SHELF_LIFE + "." + formId;
        Property property = propertyService.getProperty(propKey, survey.getId());
        if (property == null) {
            propKey = ke.co.hoji.core.data.Constants.Server.GPS_SHELF_LIFE;
            property = propertyService.getProperty(propKey, survey.getId());
        }
        if (property != null && !StringUtils.isBlank(property.getValue())) {
            maxGpsShelfLife = Utils.parseLong(property.getValue());
            if (maxGpsShelfLife == null || maxGpsShelfLife < 1) {
                maxGpsShelfLife = 15L;
            }
        }
        return maxGpsShelfLife;
    }

    private void setGpsLocation(LocationWrapper locationWrapper) {
        GpsLocation gpsLocation = GpsLocation.getInstance();
        if (locationWrapper != null) {
            gpsLocation.setLatitude(locationWrapper.getLatitude());
            gpsLocation.setLongitude(locationWrapper.getLongitude());
            gpsLocation.setAccuracy(locationWrapper.getAccuracy());
            gpsLocation.setAddress(locationWrapper.getAddress());
        }
    }

    private void useLocation(LocationWrapper locationWrapper) {
        dismissGpsProgressDialog();
        androidHojiContext.pauseLocationService(this);
        onGpsReady(locationWrapper);
    }

    private void showGpsProgressDialog(
            final LocationWrapper locationWrapper,
            boolean mandatory
    ) {
        if (gpsProgressDialog == null) {
            gpsProgressDialog = new ProgressDialog(this);
            gpsProgressDialog.setCancelable(false);
            if (!mandatory) {
                gpsProgressDialog.setButton(
                        DialogInterface.BUTTON_POSITIVE, getResources().getText(R.string.continue_anyway_gps),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                useLocation(locationWrapper);
                            }
                        });
            }
            gpsProgressDialog.setButton(
                    DialogInterface.BUTTON_NEGATIVE, getResources().getText(R.string.cancel_action),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DataEntryActivity.this.setReadyToUse(false);
                            dismissGpsProgressDialog();
                        }
                    });
        }
        String title;
        String message = null;
        int gpsQuality = checkGpsQuality(locationWrapper);
        if (gpsQuality == GpsQuality.NULL) {
            title = getResources().getString(R.string.acquiring_location_title);
            message = getResources().getString(R.string.please_wait);
        } else {
            String locationAddress = locationWrapper.getAddress();
            int locationAccuracy = (int) Math.ceil(locationWrapper.getAccuracy());
            title = !StringUtils.isBlank(locationAddress)
                    ? locationAddress
                    : getResources().getString(R.string.improving_location_accuracy_title);
            if (gpsQuality == GpsQuality.STALE) {
                message = getResources().getString(R.string.stale_location_message);
            } else if (gpsQuality == GpsQuality.INACCURATE) {
                int minAccuracy = getMinGpsAccuracy();
                message = getResources().getString(
                        R.string.improving_location_accuracy_message,
                        locationAccuracy,
                        minAccuracy
                );
            }
        }
        gpsProgressDialog.setTitle(title);
        gpsProgressDialog.setMessage(message);
        if (!isFinishing()) {
            if (!gpsProgressDialog.isShowing()) {
                gpsProgressDialog.show();
            }
        }
    }

    private void dismissGpsProgressDialog() {
        if (!isFinishing()) {
            if (gpsProgressDialog != null && gpsProgressDialog.isShowing()) {
                // Catch IAE just in case the activity was destroyed. This seems to be necessary
                // in spite checking isFinishing() above.
                try {
                    gpsProgressDialog.dismiss();
                } catch (IllegalArgumentException ex) {
                    // Do nothing
                }
            }
        }
    }

    private Bitmap getImage() {
        Bitmap bitmap = null;
        if (imageLocation != null) {
            ContentResolver contentResolver = this.getContentResolver();
            contentResolver.notifyChange(imageLocation.getUri(), null);
            try {
                bitmap = android.provider.MediaStore.Images.Media.getBitmap(contentResolver, imageLocation.getUri());
            } catch (Exception ex) {
                Toast.makeText(this, getResources().getText(R.string.failed_to_load_image), Toast.LENGTH_SHORT).show();
            } catch (OutOfMemoryError e) {
                Toast.makeText(this, getResources().getText(R.string.insufficient_memory), Toast.LENGTH_SHORT).show();
            }
        }
        return bitmap;
    }

    private String createTitle() {
        String title;
        if (this.getRecord() != null
                && this.record.getCaption() != null) {
            title = this.record.getCaption();
        } else {
            TranslationService translationService = androidHojiContext.getModelServiceManager().getTranslationService();
            title = translationService.translate(
                    androidHojiContext.getForm(),
                    androidHojiContext.getLanguageId(),
                    Form.TranslatableComponent.NAME,
                    androidHojiContext.getForm().getName()
            );
        }
        return title;
    }

    /*
     * Reset HojiContext on responses because it is not serialized when records are passed between
     * activities.
     */
    private void refreshResponseHojiContext(Record record) {
        if (record != null && record.getLiveFields() != null && !record.getLiveFields().isEmpty()) {
            for (LiveField liveField : record.getLiveFields()) {
                if (liveField.getResponse() != null) {
                    liveField.getResponse().setHojiContext(androidHojiContext);
                }
            }
        }
    }

    private void requestLocationAccess() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
            } else {
                androidHojiContext.permitLocationService(true);
            }
        }
    }
}