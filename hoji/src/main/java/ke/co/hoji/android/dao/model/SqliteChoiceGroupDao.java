package ke.co.hoji.android.dao.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.android.proxy.ChoiceGroupProxy;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by gitahi on 15/04/15.
 */
public class SqliteChoiceGroupDao {

    private final SqliteSqlExecutor se;

    private final SqliteChoiceDao choiceDao;

    public SqliteChoiceGroupDao(SqliteSqlExecutor se, SqliteChoiceDao choiceDao) {
        this.se = se;
        this.choiceDao = choiceDao;
    }

    public ChoiceGroup getChoiceGroupById(int choiceGroupId) {
        Map<String, Object> params = se.createParameterMap("id", choiceGroupId);
        String select = "SELECT id, name, ordering_strategy, exclude_last, user_id FROM choice_group WHERE id = ?";
        Cursor cursor = null;
        ChoiceGroup choiceGroup = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                ChoiceGroup cg = new ChoiceGroup();
                cg.setId(cursor.getInt(cursor.getColumnIndex("id")));
                cg.setName(cursor.getString(cursor.getColumnIndex("name")));
                cg.setOrderingStrategy(cursor.getInt(cursor.getColumnIndex("ordering_strategy")));
                cg.setExcludeLast(cursor.getInt(cursor.getColumnIndex("exclude_last")));
                choiceGroup = new ChoiceGroupProxy(cg);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteChoiceGroupDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return choiceGroup;
    }

    public ChoiceGroup saveChoiceGroup(ChoiceGroup choiceGroup) {
        saveChoiceGroups(Collections.singleton(choiceGroup));
        return choiceGroup;
    }

    public Set<ChoiceGroup> saveChoiceGroups(Collection<ChoiceGroup> choiceGroups) {
        se.beginTransaction();
        try {
            for (ChoiceGroup choiceGroup : choiceGroups) {
                boolean exists = choiceGroupExists(choiceGroup.getId());
                Map<String, Object> params = se.createParameterMap();
                if (!exists) {
                    params.put("id", choiceGroup.getId());
                }
                params.put("name", choiceGroup.getName());
                params.put("ordering_strategy", choiceGroup.getOrderingStrategy());
                params.put("exclude_last", choiceGroup.getExcludeLast());
                if (!exists) {
                    se.insert("choice_group", params);
                } else {
                    Map<String, Object> whereParams = se.createParameterMap("id", choiceGroup.getId());
                    se.update("choice_group", params,  "id = ?", whereParams);
                }
                saveChoices(choiceGroup);
            }
            se.setTransactionSuccessful();
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteChoiceGroupDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex.getMessage());
        } finally {
            se.endTransaction();
        }
        return new HashSet<>(choiceGroups);
    }

    private void saveChoices(ChoiceGroup choiceGroup) {
        List<Choice> choices = choiceGroup.getChoices();
        if (choices != null && !choices.isEmpty()) {
            deleteChoiceGroupChoices(choiceGroup);
            for (Choice choice : choices) {
                choiceDao.saveChoice(choice);
                insertChoiceGroupChoice(choiceGroup, choice);
            }
        }
    }

    private boolean choiceGroupExists(int choiceGroupId) {
        String select = "SELECT COUNT(id) FROM choice_group WHERE id = ?";
        return se.exists(select, choiceGroupId);
    }

    private void insertChoiceGroupChoice(ChoiceGroup choiceGroup, Choice choice) {
        Map<String, Object> params = se.createParameterMap();
        params.put("choice_group_id", choiceGroup.getId());
        params.put("choice_id", choice.getId());
        params.put("ordinal", choice.getOrdinal());
        params.put("description", choice.getDescription());
        params.put("code", choice.getCode());
        params.put("scale", choice.getScale());
        params.put("loner", choice.isLoner());
        params.put("choice_parent_id", choice.getParent() != null ? choice.getParent().getId() : null);
        se.insert("choice_group_choice", params);
    }

    private void deleteChoiceGroupChoices(ChoiceGroup choiceGroup) {
        Map<String, Object> whereParams = se.createParameterMap("choice_group_id", choiceGroup.getId());
        se.delete("choice_group_choice", "choice_group_id = ?", whereParams);
    }
}
