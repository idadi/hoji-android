package ke.co.hoji.android.activity;

import ke.co.hoji.core.data.model.Record;

/**
 * An container that may contain a @{@link ke.co.hoji.core.data.model.Record}. This interface is a
 * means to allow both the @{@link DataEntryActivity} and {@link LiveFieldActivity} to be referred
 * commonly.
 * <p>
 * Created by gitahi on 6/23/17.
 */

public interface RecordContainer {

    /**
     * Get the @{@link Record} associated with this RecordContainer.
     *
     * @return the Record.
     */
    Record getRecord();
}
