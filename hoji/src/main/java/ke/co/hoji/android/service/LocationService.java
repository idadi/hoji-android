package ke.co.hoji.android.service;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.HashMap;
import java.util.Map;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.data.LocationWrapper;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.widget.helper.LocationComponent;

/**
 * Created by gitahi on 20/08/15.
 */
public class LocationService implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    /**
     * A handle to the AndroidHojiContext so we can send location updates to a central place.
     */
    private final AndroidHojiContext androidHojiContext;

    /**
     * A handle to the GoogleApiClient to provide access to the Location API.
     */
    private final GoogleApiClient googleApiClient;

    /**
     * The callback for handling location updates.
     */
    private final LocationCallback locationCallback;

    /**
     * The FusedLocationProviderClient to unify access to location across GPS, WiFi and Cellular.
     */
    private FusedLocationProviderClient fusedLocationProviderClient;

    /**
     * The accuracy priority to use.
     */
    private int priority = LocationRequest.PRIORITY_HIGH_ACCURACY;

    /**
     * On versions of Android lower than M (Marshmallow), permission to access location is granted
     * during installation and thus can be taken for granted. For M and up, permission has to
     * requested explicitly upon first use.
     */
    private boolean permitted = Build.VERSION.SDK_INT < Build.VERSION_CODES.M;

    /**
     * The Activity under which this service is running.
     */
    private Activity activity;

    /**
     * A Map of all @{@link LocationComponent}s interested in receiving location updates. We use
     * a map here to ensure that multiple instances of LocationComponents are not added more than
     * once.
     */
    private final Map<String, LocationComponent> locationComponents = new HashMap<>();

    public LocationService(AndroidHojiContext androidHojiContext) {
        this.androidHojiContext = androidHojiContext;
        this.googleApiClient = new GoogleApiClient.Builder(androidHojiContext)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        this.locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                for (Location location : locationResult.getLocations()) {
                    updateLocation(location);
                }
            }
        };
    }

    public void setPermitted(boolean permitted) {
        this.permitted = permitted;
    }

    public void addLocationComponent(LocationComponent locationComponent) {
        locationComponents.put(locationComponent.getClass().getName(), locationComponent);
    }

    /**
     * Connects the GoogleApiClient.
     */
    public void connect(Activity activity) {
        this.activity = activity;
        if (!googleApiClient.isConnected()) {
            googleApiClient.connect();
        }
    }

    /**
     * Starts listening for location updates.
     */
    @SuppressLint("MissingPermission")
    public void start(int priority) {
        this.priority = priority;
        if (permitted) {
            if (googleApiClient.isConnected()) {
                LocationRequest locationRequest = new LocationRequest();
                locationRequest.setInterval(Constants.LOCATION_REQUEST_INTERVAL);
                locationRequest.setFastestInterval(Constants.LOCATION_FASTEST_REQUEST_INTERVAL);
                locationRequest.setPriority(this.priority);
                fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
                fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null /* Looper */);
            }
        }
        //Immediately update location components with the cached location
        for (LocationComponent locationComponent : locationComponents.values()) {
            if (locationComponent.acceptsLastKnownLocation()) {
                locationComponent.updateLocation(androidHojiContext.getLocationWrapper(), true);
            } else {
                locationComponent.updateLocation(null, true);
            }
        }
    }

    /**
     * Stops listening for location updates.
     */
    public void pause() {
        if (permitted) {
            if (googleApiClient.isConnected()) {
                if (fusedLocationProviderClient != null) {
                    fusedLocationProviderClient.removeLocationUpdates(locationCallback);
                }
            }
        }
    }

    /**
     * Disconnects from the GoogleApiClient.
     */
    public void disconnect() {
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    /**
     * Called if connecting the GoogleApiClient fails.
     *
     * @param connectionResult the {@link ConnectionResult} saying what went wrong.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (!connectionResult.hasResolution()) {
            GoogleApiAvailability.getInstance().getErrorDialog(activity,
                    connectionResult.getErrorCode(), 0).show();
            return;
        }
        try {
            connectionResult.startResolutionForResult(activity, 100);
        } catch (IntentSender.SendIntentException ex) {
            Toast toast = Toast.makeText
                    (
                            androidHojiContext,
                            androidHojiContext.getResources()
                                    .getString(R.string.location_service_unavailable,
                                            connectionResult.getErrorCode()), Toast.LENGTH_SHORT
                    );
            toast.show();
        }
    }

    /**
     * {@see ConnectionCallbacks#onConnected(Bundle)} )}
     */
    @Override
    public void onConnected(Bundle bundle) {
        start(this.priority);
    }

    /**
     * Checks whether location is enabled.
     *
     * @return true if location is enabled and false otherwise.
     */
    public boolean isLocationEnabled() {
        if (permitted) {
            LocationManager locationManager = (LocationManager) androidHojiContext.getSystemService(Context.LOCATION_SERVICE);
            boolean gpsEnabled = false;
            boolean networkEnabled = false;
            try {
                gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch (Exception ex) {
                //do nothing
            }
            try {
                networkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } catch (Exception ex) {
                //do nothing
            }
            return gpsEnabled || networkEnabled;
        } else {
            return false;
        }
    }


    /**
     * {@see ConnectionCallbacks#onConnectionSuspended(int)}
     */
    @Override
    public void onConnectionSuspended(int i) {
        //do nothing
    }

    /*
     * React to new location data being available. This method calls
     * {@link #startAddressService(Location)} to reverse geocode it.
     *
     * @param location the new/updated Location.
     */
    private void updateLocation(Location location) {
        if (googleApiClient.isConnected()) {
            startAddressService(location);
        }
    }

    /*
     * Start the {@link AddressService} to reverse-geo-code the location and give us an actual
     * address, like a street address.
     *
     * @param location the Location to reverse-geo-code.
     */
    private void startAddressService(Location location) {
        AddressResultReceiver resultReceiver = new AddressResultReceiver(new Handler());
        Intent intent = new Intent(androidHojiContext, AddressService.class);
        intent.putExtra(Constants.RESULT_RECEIVER, resultReceiver);
        intent.putExtra(Constants.LOCATION_DATA, location);
        androidHojiContext.startService(intent);
    }

    /**
     * A {@link ResultReceiver} that receives the address once a location is reverse-geo-coded
     * by the {@link AddressService}.
     */
    @SuppressLint("ParcelCreator")
    private class AddressResultReceiver extends ResultReceiver {

        private AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * {@see ResultReceiver#onReceiveResult(int, Bundle)}
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            String address = resultData.getString(Constants.RESULT_DATA);
            Location location = resultData.getParcelable(Constants.LOCATION_DATA);
            for (LocationComponent locationComponent : locationComponents.values()) {
                locationComponent.updateLocation(new LocationWrapper(location, address), false);
            }
            androidHojiContext.setUpAcraLocationData();
        }
    }
}
