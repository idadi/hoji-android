package ke.co.hoji.android.service.dto;

import java.util.List;

import ke.co.hoji.android.dao.dto.SqliteFormDao;
import ke.co.hoji.core.data.dto.Form;
import ke.co.hoji.core.service.dto.FormService;

public class FormServiceImpl implements FormService {

    private final SqliteFormDao formDao;

    public FormServiceImpl(SqliteFormDao formDao) {
        this.formDao = formDao;
    }

    @Override
    public List<Form> saveForms(List<Form> forms) {
        return formDao.saveForms(forms);
    }

    @Override
    public List<Form> getAllForms() {
        return null;
    }

    @Override
    public Form getFormById(Integer formId) {
        return formDao.getFormById(formId);
    }

}
