package ke.co.hoji.android.helper;

import ke.co.hoji.core.data.model.Field;


/**
 * Interface to be implemented by the class responsible for taking an image.
 * <p>
 * Created by gitahi on 12/06/15.
 */
public interface ImageCapturer {

    /**
     * Start the process of taking an image.
     *
     * @param imageReceiver the {@link ImageReceiver} that will receive the image once captured.
     * @param directoryPath the path to the directory in which to store the image, stated relative
     *                      to the root image directory
     * @param fileName      the name of the image file
     */
    void startCapture(ImageReceiver imageReceiver, String directoryPath, String fileName);

    /**
     * Complete the process of taking an image.
     *
     * @param hojiBitmap the {@link HojiBitmap} representing the image.
     */
    void finishCapture(HojiBitmap hojiBitmap);
}
