package ke.co.hoji.android.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;

import ke.co.hoji.R;

/**
 * Created by gitahi on 12/06/15.
 */
public class DeleteSurveyButton extends LinearLayout {

    private final Button button;

    public DeleteSurveyButton(Context context, int layout) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(layout, this);
        button = findViewById(R.id.deleteButton);
    }

    public Button getButton() {
        return button;
    }
}
