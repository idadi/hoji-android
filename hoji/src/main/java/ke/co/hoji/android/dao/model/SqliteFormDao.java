package ke.co.hoji.android.dao.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.model.Form;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by gitahi on 15/04/15.
 */
public class SqliteFormDao {

    private final SqliteSqlExecutor se;

    public SqliteFormDao(SqliteSqlExecutor se) {
        this.se = se;
    }

    public List<Form> getFormBySurvey(int surveyId) {
        List<Form> forms = new ArrayList<>();
        String select = "SELECT "
            + "id, name, ordinal, survey_id, gps_capture, min_gps_accuracy, "
            + "no_of_gps_attempts, enabled, minor_version, major_version "
            + "FROM form "
            + "WHERE survey_id = ? "
            + "ORDER BY ordinal";
        Map<String, Object> params = se.createParameterMap("survey_id", surveyId);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                Form form = new FormRowMapper().mapRow(cursor);
                forms.add(form);
                se.put(form);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteFormDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return forms;
    }

    public List<Form> getFormBySurvey(int surveyId, boolean enabled) {
        List<Form> forms = new ArrayList<>();
        String select = "SELECT "
            + "id, name, ordinal, survey_id, gps_capture, min_gps_accuracy, "
            + "no_of_gps_attempts, enabled, minor_version, major_version "
            + "FROM form "
            + "WHERE survey_id = ? AND enabled = ? "
            + "ORDER BY ordinal";
        Map<String, Object> params =  se.createParameterMap();
        params.put("survey_id", surveyId);
        params.put("enabled", enabled);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                Form form = new FormRowMapper().mapRow(cursor);
                se.put(form);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteFormDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return forms;
    }

    public Form getFormById(int formId) {
        Form form = null;
        String select = "SELECT "
            + "id, name, ordinal, survey_id, gps_capture, min_gps_accuracy, "
            + "no_of_gps_attempts, enabled, minor_version, major_version "
            + "FROM form "
            + "WHERE id = ? ";
        Map<String, Object> params = se.createParameterMap("id", formId);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                form = new FormRowMapper().mapRow(cursor);
                se.put(form);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteFormDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return form;
    }

    private class FormRowMapper {

        Form mapRow(Cursor cursor) {
            Form form = new Form
                    (
                            cursor.getInt(cursor.getColumnIndex("id")),
                            cursor.getString(cursor.getColumnIndex("name")),
                            cursor.getInt(cursor.getColumnIndex("enabled")) > 0,
                            new BigDecimal(cursor.getDouble(cursor.getColumnIndex("ordinal"))),
                            cursor.getInt(cursor.getColumnIndex("minor_version")),
                            cursor.getInt(cursor.getColumnIndex("major_version"))
                    );
            form.setGpsCapture(cursor.getInt(cursor.getColumnIndex("gps_capture")));
            return form;
        }

    }
}
