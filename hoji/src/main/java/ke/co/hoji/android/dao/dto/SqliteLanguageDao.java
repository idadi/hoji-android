package ke.co.hoji.android.dao.dto;

import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.dto.Language;

import java.util.List;
import java.util.Map;

public class SqliteLanguageDao {

    private final SqliteSqlExecutor se;

    public SqliteLanguageDao(SqliteSqlExecutor se) {
        this.se = se;
    }

    public List<Language> saveLanguages(List<Language> languages) {
        se.beginTransaction();
        try {
            se.delete("language", null, null);
            for (Language language : languages) {
                Map<String, Object> params = se.createParameterMap();
                params.put("name", language.getName());
                params.put("survey_id", language.getSurveyId());
                if (!languageExists(language.getId())) {
                    params.put("id", language.getId());
                    se.insert("language", params);
                } else {
                    Map<String, Object> whereParams = se.createParameterMap("id", language.getId());
                    se.update("language", params, "id = ?", whereParams);
                }
            }
            se.setTransactionSuccessful();
        } finally {
            se.endTransaction();
        }
        return languages;
    }

    private boolean languageExists(int languageId) {
        String select = "SELECT COUNT(id) FROM language WHERE id = ?";
        return se.exists(select, languageId);
    }
}
