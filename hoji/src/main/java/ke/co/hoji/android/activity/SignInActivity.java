package ke.co.hoji.android.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.service.HttpService;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.http.AuthenticationRequest;
import ke.co.hoji.core.data.http.AuthenticationResponse;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.core.service.model.HttpException;
import ke.co.hoji.core.service.model.PropertyService;

public class SignInActivity extends AppCompatActivity {

    private EditText emailEditText;
    private EditText passwordEditText;
    private Button signInButton;
    private ProgressDialog progressDialog;

    private AndroidHojiContext androidHojiContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in_activity);
        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        signInButton = findViewById(R.id.signInButton);
        TextView signUpTextView = findViewById(R.id.signUpTextView);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
        signUpTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setResult(InitialActivity.RESULT_SWITCH, null);
                finish();
            }
        });
        androidHojiContext = (AndroidHojiContext) getApplication();
        PropertyService propertyService = androidHojiContext.getModelServiceManager().getPropertyService();
        String username = propertyService.getValue(Constants.Server.USERNAME);
        if (username != null) {
            emailEditText.setText(username);
            passwordEditText.requestFocus();
        } else {
            emailEditText.requestFocus();
        }

        TextView forgotPasswordText = findViewById(R.id.forgotPasswordText);
        forgotPasswordText.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    protected void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        setResult(InitialActivity.RESULT_CANCEL, null);
        finish();
    }

    private void signIn() {
        if (!validate()) {
            return;
        }
        signInButton.setEnabled(false);
        progressDialog = new ProgressDialog(SignInActivity.this);
        SignInTask sighInTask = new SignInTask
                (
                        androidHojiContext,
                        emailEditText.getText().toString(),
                        passwordEditText.getText().toString(),
                        progressDialog
                );
        sighInTask.execute();
    }

    private boolean validate() {
        boolean valid = true;
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailEditText.setError(getResources().getString(R.string.invalid_email));
            emailEditText.requestFocus();
            valid = false;
        } else {
            emailEditText.setError(null);
        }
        if (password.isEmpty()) {
            passwordEditText.setError(getResources().getString(R.string.enter_password));
            passwordEditText.requestFocus();
            valid = false;
        } else {
            passwordEditText.setError(null);
        }
        return valid;
    }

    private void signInSucceeded(final boolean signUp) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                signInButton.setEnabled(true);
                setResult(InitialActivity.RESULT_SUCCESS, null);
                finish();
            }
        });
    }

    private void signInFailed(final String message, final boolean unauthenticated) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText
                        (
                                getBaseContext(),
                                message,
                                Toast.LENGTH_LONG
                        ).show();
                if (unauthenticated) {
                    passwordEditText.setText("");
                }
                passwordEditText.requestFocus();
                signInButton.setEnabled(true);
            }
        });
    }

    private void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private class SignInTask extends AsyncTask<Void, Void, Void> {

        private final AndroidHojiContext androidHojiContext;
        private final String username;
        private final String password;
        private final ProgressDialog progressDialog;

        private SignInTask(AndroidHojiContext androidHojiContext, String username, String password,
                           ProgressDialog progressDialog) {
            this.androidHojiContext = androidHojiContext;
            this.username = username;
            this.password = password;
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            if (progressDialog != null) {
                progressDialog.setMessage(getResources().getText(R.string.signing_in));
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            AuthenticationRequest request = new AuthenticationRequest(username, password);
            ConfigService configService = androidHojiContext.getModelServiceManager().getConfigService();
            HttpService httpService = androidHojiContext.getHttpService();
            Survey survey = configService.getSurvey();
            if (survey != null) {
                request.setSurveyId(survey.getId());
            }
            AuthenticationResponse response;
            try {
                response = httpService.sendAsJson
                        (
                                ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.AUTHENTICATE,
                                null,
                                null,
                                request,
                                AuthenticationResponse.class
                        );
                if (response.isAuthenticated()) {
                    if (response.isAuthorized()) {
                        androidHojiContext.getModelServiceManager().getPropertyService().setProperties
                                (
                                        new Property(Constants.Server.USER_ID, response.getUserId().toString()),
                                        new Property(Constants.Server.USER_CODE, response.getUserCode()),
                                        new Property(Constants.Server.NAME, response.getName()),
                                        new Property(Constants.Server.USERNAME, username),
                                        new Property(Constants.Server.PASSWORD, password),
                                        new Property(Constants.Server.TENANT_CODE, response.getTenantCode()),
                                        new Property(Constants.Server.TENANT_NAME, response.getTenantName())
                                );
                        signInSucceeded(false);
                    } else {
                        signInFailed(getResources().getString(R.string.unauthorized), false);
                    }
                } else {
                    signInFailed(getResources().getString(R.string.unauthenticated), true);
                }
            } catch (HttpException ex) {
                signInFailed(httpService.interpretHttpExceptionCode(ex), false);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (SignInActivity.this.isFinishing()) {
                return;
            }
            dismissProgressDialog();
        }
    }
}
