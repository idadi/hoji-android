package ke.co.hoji.android.listener;

import ke.co.hoji.core.data.model.Record;

/**
 * Defines a callback for reacting to a {@link Record} selection event.
 * <p/>
 * Created by gitahi on 17/05/15.
 */
public interface OnRecordSelectedListener {

    /**
     * Called when a {@link Record} is selected.
     *
     * @param record the selected {@link Record}.
     */
    void onRecordSelected(Record record);
}
