package ke.co.hoji.android.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.data.MatchSensitivity;
import ke.co.hoji.android.data.UserForSearch;
import ke.co.hoji.android.adapter.FormArrayAdapter;
import ke.co.hoji.android.listener.OnLiteRecordsFoundListener;
import ke.co.hoji.android.service.HttpService;
import ke.co.hoji.android.widget.DateComponent;
import ke.co.hoji.android.widget.DatePickerFragment;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.Period;
import ke.co.hoji.core.data.dto.LiteRecord;
import ke.co.hoji.core.data.http.ResultCode;
import ke.co.hoji.core.data.http.SearchRequest;
import ke.co.hoji.core.data.http.SearchResponse;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.HttpException;
import ke.co.hoji.core.service.model.PropertyService;

/**
 * Created by gitahi on 16/11/16.
 */
public class ImportFragment extends Fragment implements DateComponent {

    private boolean setFrom = true;
    private Date fromDate;
    private Date toDate;

    private MaterialSpinner formSpinner;
    private MaterialSpinner periodSpinner;
    private EditText fromEditText;
    private EditText toEditText;
    private MaterialSpinner createdBySpinner;
    private EditText matchingEditText;
    private MaterialSpinner sensitivitySpinner;
    private Button searchButton;
    private LinearLayout dateLinearLayout;

    private DatePickerFragment datePickerFragment;

    private ProgressDialog progressDialog;

    private void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
        if (this.fromDate != null) {
            fromEditText.setText(Utils.formatDate(this.fromDate));
        } else {
            fromEditText.setText("");
        }
    }

    private void setToDate(Date toDate) {
        this.toDate = toDate;
        if (this.toDate != null) {
            toEditText.setText(Utils.formatDate(this.toDate));
        } else {
            toEditText.setText("");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        formSpinner = getView().findViewById(R.id.formSpinner);
        periodSpinner = getView().findViewById(R.id.periodSpinner);
        fromEditText = getView().findViewById(R.id.fromEditText);
        toEditText = getView().findViewById(R.id.toEditText);
        setFromDate(new Date());
        setToDate(new Date());
        createdBySpinner = getView().findViewById(R.id.createdBySpinner);
        matchingEditText = getView().findViewById(R.id.matchingEditText);
        sensitivitySpinner = getView().findViewById(R.id.sensitivitySpinner);
        searchButton = getView().findViewById(R.id.searchButton);
        dateLinearLayout = getView().findViewById(R.id.dateLinearLayout);
        dateLinearLayout.setVisibility(LinearLayout.GONE);
        addListeners();
        loadForms();
        loadPeriods();
        loadUsersForSearch();
        loadSensitivities();
        formSpinner.requestFocus();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.import_fragment, container, false);
    }


    @Override
    public void onResume() {
        super.onResume();
        progressDialog = new ProgressDialog(this.getActivity());
    }


    @Override
    public void onPause() {
        super.onPause();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        if (datePickerFragment != null) {
            datePickerFragment.dismiss();
        }
    }

    private void addListeners() {
        periodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Object selected = periodSpinner.getSelectedItem();
                if (selected instanceof Period && ((Period) selected).getName().equals(Period.BETWEEN)) {
                    dateLinearLayout.setVisibility(LinearLayout.VISIBLE);
                    fromEditText.requestFocus();
                } else {
                    dateLinearLayout.setVisibility(LinearLayout.GONE);
                }
            }

            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
        searchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                search();
            }
        });
        fromEditText.setKeyListener(null);
        fromEditText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                setFrom = true;
                showDatePickerDialog();
            }
        });
        toEditText.setKeyListener(null);
        toEditText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                setFrom = false;
                showDatePickerDialog();
            }
        });
    }

    private void loadForms() {
        List<Form> searchableForms = ((HojiContext) getActivity().getApplication()).getSurvey().getForms();
        FormArrayAdapter adapter = new FormArrayAdapter
                (
                        getActivity(),
                        android.R.layout.simple_spinner_item,
                        searchableForms
                );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        formSpinner.setAdapter(adapter);
    }

    private void loadPeriods() {
        List<Period> periods = new ArrayList<>();
        periods.add(new Period(Period.TODAY));
        periods.add(new Period(Period.YESTERDAY));
        periods.add(new Period(Period.THIS_WEEK));
        periods.add(new Period(Period.LAST_WEEK));
        periods.add(new Period(Period.THIS_MONTH));
        periods.add(new Period(Period.LAST_MONTH));
        periods.add(new Period(Period.THIS_YEAR));
        periods.add(new Period(Period.LAST_YEAR));
        periods.add(new Period(Period.BETWEEN));
        ArrayAdapter<Period> adapter = new ArrayAdapter<>
                (
                        getActivity(),
                        android.R.layout.simple_spinner_item,
                        periods
                );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        periodSpinner.setAdapter(adapter);
    }

    private void loadUsersForSearch() {
        List<UserForSearch> usersForSearch = new ArrayList<>();
        usersForSearch.add(UserForSearch.getByValue(1));
        usersForSearch.add(UserForSearch.getByValue(2));
        ArrayAdapter<UserForSearch> adapter = new ArrayAdapter<>
                (
                        getActivity(),
                        android.R.layout.simple_spinner_item,
                        usersForSearch
                );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        createdBySpinner.setAdapter(adapter);
    }

    private void loadSensitivities() {
        List<MatchSensitivity> sensitivities = new ArrayList<>();
        sensitivities.add(MatchSensitivity.getByValue(1));
        sensitivities.add(MatchSensitivity.getByValue(2));
        sensitivities.add(MatchSensitivity.getByValue(3));
        ArrayAdapter<MatchSensitivity> adapter = new ArrayAdapter<>
                (
                        getActivity(),
                        android.R.layout.simple_spinner_item,
                        sensitivities
                );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sensitivitySpinner.setAdapter(adapter);
    }

    private void search() {
        if (!validate()) {
            return;
        }
        final AndroidHojiContext androidHojiContext = (AndroidHojiContext) getActivity().getApplication();
        final HttpService httpService = androidHojiContext.getHttpService();
        PropertyService propertyService = androidHojiContext.getModelServiceManager().getPropertyService();
        Form form = formSpinner.getSelectedItem() instanceof Form ?
                (Form) formSpinner.getSelectedItem() : null;
        Period period = periodSpinner.getSelectedItem() instanceof Period ?
                (Period) periodSpinner.getSelectedItem() : null;
        String searchTerms = matchingEditText.getText().toString();
        UserForSearch userForSearch = createdBySpinner.getSelectedItem() instanceof UserForSearch ?
                (UserForSearch) createdBySpinner.getSelectedItem() : null;
        MatchSensitivity sensitivity = sensitivitySpinner.getSelectedItem() instanceof MatchSensitivity ?
                (MatchSensitivity) sensitivitySpinner.getSelectedItem() : null;
        Integer userId = userForSearch.getValue() == UserForSearch.ME.getValue() ?
                Utils.parseInteger(propertyService.getValue(Constants.Server.USER_ID)) : null;
        Long from = null;
        Long to = null;
        if (period != null) {
            from = (period.getFrom() != null ? period.getFrom().getTime() : null);
            to = (period.getTo() != null ? period.getTo().getTime() : null);
        }
        SearchRequest searchRequest = new SearchRequest(
                form.getSurvey().getId(),
                form.getId(),
                form.getMajorVersion(),
                from,
                to,
                userId,
                searchTerms,
                sensitivity.getValue()
        );
        ImportSearchTask importSearchTask = new ImportSearchTask
                (
                        getActivity(),
                        httpService,
                        propertyService.getValue(Constants.Server.USERNAME),
                        propertyService.getValue(Constants.Server.PASSWORD),
                        searchRequest,
                        form,
                        (OnLiteRecordsFoundListener) getActivity(),
                        progressDialog
                );
        importSearchTask.execute();
    }

    private boolean validate() {
        boolean valid = true;
        Form form = formSpinner.getSelectedItem() instanceof Form ?
                (Form) formSpinner.getSelectedItem() : null;
        String searchTerms = matchingEditText.getText().toString();
        UserForSearch userForSearch = createdBySpinner.getSelectedItem() instanceof UserForSearch ?
                (UserForSearch) createdBySpinner.getSelectedItem() : null;
        MatchSensitivity sensitivity = sensitivitySpinner.getSelectedItem() instanceof MatchSensitivity ?
                (MatchSensitivity) sensitivitySpinner.getSelectedItem() : null;
        if (form == null) {
            formSpinner.setError(getResources().getString(R.string.cannot_be_empty));
            formSpinner.requestFocus();
            valid = false;
        }
        final int MAX_CHARS = 3;
        int searchTermsLength = searchTerms.trim().length();
        if (searchTermsLength < 1) {
            matchingEditText.setError(getResources().getString(R.string.cannot_be_empty));
            matchingEditText.requestFocus();
            valid = false;
        } else if (!"-".equals(searchTerms) && !"*".equals(searchTerms)) {
            if (searchTerms.trim().length() < MAX_CHARS) {
                matchingEditText.setError(getResources().getString(R.string.must_be_at_least_n_characters, MAX_CHARS));
                matchingEditText.requestFocus();
                valid = false;
            }
        }
        if (userForSearch == null) {
            createdBySpinner.setError(getResources().getString(R.string.cannot_be_empty));
            createdBySpinner.requestFocus();
            valid = false;
        }
        if (sensitivity == null) {
            sensitivitySpinner.setError(getResources().getString(R.string.cannot_be_empty));
            sensitivitySpinner.requestFocus();
            valid = false;
        }
        return valid;
    }

    private void showDatePickerDialog() {
        datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDateComponent(this);
        datePickerFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }

    public void setDate(Date date) {
        if (setFrom) {
            setFromDate(date);
        } else {
            setToDate(date);
        }
    }

    public Date getDate() {
        if (setFrom) {
            return fromDate;
        } else {
            return toDate;
        }
    }

    private class ImportSearchTask extends AsyncTask<Void, Void, Integer> {

        static final int NOTHING_FOUND = -2;
        static final int ERROR_OCCURRED = -1;

        private final Activity activity;
        private final HttpService httpService;
        private final String username;
        private final String password;
        private final SearchRequest searchRequest;
        private final Form form;
        private final OnLiteRecordsFoundListener onLiteRecordsFoundListener;
        private final ProgressDialog progressDialog;

        private ImportSearchTask
                (
                        Activity activity,
                        HttpService httpService,
                        String username,
                        String password,
                        SearchRequest searchRequest,
                        Form form,
                        OnLiteRecordsFoundListener onLiteRecordsFoundListener,
                        ProgressDialog progressDialog
                ) {
            this.activity = activity;
            this.httpService = httpService;
            this.username = username;
            this.password = password;
            this.searchRequest = searchRequest;
            this.form = form;
            this.onLiteRecordsFoundListener = onLiteRecordsFoundListener;
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            if (progressDialog != null) {
                progressDialog.setMessage(activity.getResources().getText(R.string.searching));
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                SearchResponse searchResponse = httpService.sendAsJson
                        (
                                ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.IMPORT_SEARCH,
                                username,
                                password,
                                searchRequest,
                                SearchResponse.class
                        );
                int result = searchResponse.getResultCode();
                if (result == ResultCode.OKAY) {
                    List<LiteRecord> liteRecords = searchResponse.getLiteRecords();
                    if (liteRecords == null || liteRecords.isEmpty()) {
                        result = NOTHING_FOUND;
                    } else {
                        onLiteRecordsFoundListener.onLiteRecordsFound(liteRecords, form);
                    }
                }
                return result;
            } catch (final HttpException ex) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast toast = Toast.makeText(
                                activity,
                                httpService.interpretHttpExceptionCode(ex),
                                Toast.LENGTH_SHORT
                        );
                        toast.show();
                    }
                });
                return ERROR_OCCURRED;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            String message = getMessage(result);
            if (message != null) {
                Toast toast = Toast.makeText(
                        activity,
                        message,
                        Toast.LENGTH_SHORT
                );
                toast.show();
            }
        }

        private String getMessage(int resultCode) {
            String message = null;
            if (resultCode == ResultCode.FORM_NOT_FOUND) {
                message = activity.getResources().getText(R.string.nothing_found).toString();
            } else if (resultCode == ResultCode.SURVEY_NOT_ENABLED) {
                message = activity.getResources().getText(R.string.import_survey_not_enabled).toString();
            } else if (resultCode == ResultCode.SURVEY_NOT_PUBLISHED) {
                message = activity.getResources().getText(R.string.import_survey_not_published).toString();
            } else if (resultCode == ResultCode.SURVEY_OUT_OF_DATE) {
                message = activity.getResources().getText(R.string.survey_out_of_date).toString();
            } else if (resultCode == NOTHING_FOUND) {
                message = activity.getResources().getText(R.string.nothing_found).toString();
            }
            return message;
        }
    }
}