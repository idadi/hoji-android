package ke.co.hoji.android.manager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ke.co.hoji.R;
import ke.co.hoji.android.activity.HomeActivity;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.core.service.model.RecordService;

/**
 * This class manages how we ask for feedback from users.
 * <p>
 * We ask for feedback using one of two methods. The first is is by interrupting the user with a
 * popup when they load records onto the Records activity. This is completely unsolicited. The
 * second is by providing a Feedback menu option on the Home Activity that the user may actively
 * choose to use if he decides to provide feedback at his convenience.
 * <p>
 * To prevent annoying the user, we run an "eligibility test" before asking. The test also ensures
 * we only ask users who have used the app enough to provide meaningful feedback.
 * <p>
 * We ask for feedback in 2 steps. First we ask if the user is enjoying using the app. If he says
 * he is, we ask him to rate the app on Play Store. If he says he is not enjoying using
 * the app, we offer to have him send us his feedback via email. He may decline to do so.
 * <p>
 * We never offer the feedback menu option on the Home Activity unless the user has previously been
 * asked for feedback by interruption and declined to give it. This option is simply there to allow
 * users who decline to give us feedback to do so at their convenience if and when they change their
 * minds.
 * <p>
 * Before soliciting for feedback, we ensure that the user:
 * <p>
 * 1. Hasn't previously been asked to provide feedback before AND
 * 2. Has successfully created and uploaded at least 1 record on the day we ask for feedback AND
 * 3 (a). Has successfully created at least 5 records across the span of any 3 different days OR
 * 3 (b). Has successfully created at least 20 records, irrespective of when he created them.
 * <p>
 * Created by gitahi on 2/22/18.
 */

public final class FeedbackManager {

    private static final int FEEDBACK_NOT_ASKED = 0;
    private static final int FEEDBACK_GIVEN = 1;
    private static final int FEEDBACK_DENIED = 2;

    private static final int DAYS_LIMIT = 3;
    private static final int LOWER_RECORD_LIMIT = 5;
    private static final int HIGHER_RECORD_LIMIT = 20;

    private final PropertyService propertyService;

    private final RecordService recordService;

    public FeedbackManager(
            PropertyService propertyService,
            RecordService recordService
    ) {
        this.propertyService = propertyService;
        this.recordService = recordService;
    }

    /**
     * Check if the user is eligible to be asked for feedback.
     *
     * @param viaMenuItem true if feedback is to be sought via the menu item rather than by
     *                    interrupting the user to solicit it.
     * @return true if the user is eligible and false otherwise.
     */
    public boolean isFeedbackEligible(boolean viaMenuItem) {
        boolean evaluateEligibility = false;
        int feedbackStatus = getFeedbackStatus();
        if (feedbackStatus == FEEDBACK_NOT_ASKED) {
            evaluateEligibility = !viaMenuItem;
        } else if (feedbackStatus == FEEDBACK_DENIED) {
            evaluateEligibility = viaMenuItem;
        }
        if (evaluateEligibility) {
            List<Record> records = fetchLast20Records();
            Set<String> dates = new HashSet<>();
            String today = Utils.formatIsoDate(new Date());
            boolean recordFromToday = false;
            int numberOfRecords = records.size();
            for (Record record : records) {
                String date = Utils.formatIsoDate(new Date(record.getDateCreated()));
                dates.add(date);
                if (today.equals(date)) {
                    recordFromToday = true;
                }
            }
            if (recordFromToday) {
                if (numberOfRecords >= HIGHER_RECORD_LIMIT) {
                    return true;
                } else if (numberOfRecords >= LOWER_RECORD_LIMIT) {
                    int noOfDays = dates.size();
                    return noOfDays >= DAYS_LIMIT;
                }
            }
        }
        return false;
    }

    /**
     * Request the user for feedback.
     *
     * @param activity   the Activity from which we are requesting feedback.
     * @param nameOfUser the name of the user from whom we are requesting feedback.
     */
    public void requestFeedback(final Activity activity, String nameOfUser) {
        String firstName = Utils.tokenizeString(
                nameOfUser,
                " ",
                true,
                true
        ).get(0);
        Dialog dialog = new AlertDialog.Builder(activity)
                .setTitle(activity.getResources().getString(R.string.enjoying_hoji_title, firstName))
                .setMessage(R.string.enjoying_hoji_message)
                .setPositiveButton(R.string.emphatic_yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        askToRate(activity);
                    }
                })
                .setNegativeButton(R.string.not_really, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        askToComplain(activity);
                    }
                })
                .create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void askToRate(final Activity activity) {
        new AlertDialog.Builder(activity)
                .setTitle(R.string.rate_hoji_title)
                .setMessage(R.string.rate_hoji_message)
                .setPositiveButton(R.string.sure, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(ke.co.hoji.android.data.Constants.PLAY_STORE_URL));
                        activity.startActivity(intent);
                        setFeedbackStatus(activity, FEEDBACK_GIVEN);
                    }
                })
                .setNegativeButton(R.string.no_thanks, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        setFeedbackStatus(activity, FEEDBACK_DENIED);
                    }
                })
                .create()
                .show();
    }

    private void askToComplain(final Activity activity) {
        Dialog dialog = new AlertDialog.Builder(activity)
                .setTitle(R.string.feedback_hoji_title)
                .setMessage(R.string.feedback_hoji_message)
                .setPositiveButton(R.string.sure, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        String uriText = "mailto:" + Constants.SUPPORT_EMAIL +
                                "?subject=" + Uri.encode(activity.getResources().getString(R.string.feedback_email_title)) +
                                "&body=" + Uri.encode(activity.getResources().getString(R.string.feedback_email_message));
                        Uri uri = Uri.parse(uriText);
                        intent.setData(uri);
                        activity.startActivity(Intent.createChooser(
                                intent,
                                activity.getResources().getString(R.string.feedback_email_prompt))
                        );
                        setFeedbackStatus(activity, FEEDBACK_GIVEN);
                    }
                })
                .setNegativeButton(R.string.no_thanks, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        setFeedbackStatus(activity, FEEDBACK_DENIED);
                    }
                })
                .create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private int getFeedbackStatus() {
        String value = propertyService.getValue(Constants.FEEDBACK_STATUS);
        if (value != null) {
            Integer feedbackStatus = Utils.parseInteger(value);
            if (feedbackStatus != null) {
                return feedbackStatus;
            }
        }
        return FEEDBACK_NOT_ASKED;
    }

    private List<Record> fetchLast20Records() {
        RecordSearch recordSearch = new RecordSearch();
        recordSearch.setStage(ke.co.hoji.core.data.model.MainRecord.Stage.UPLOADED);
        return recordService.search(recordSearch, HIGHER_RECORD_LIMIT);
    }

    private void setFeedbackStatus(Activity activity, int feedbackResult) {
        Property property = new Property(
                Constants.FEEDBACK_STATUS,
                String.valueOf(feedbackResult)
        );
        propertyService.setProperty(property);
        if (activity instanceof HomeActivity) {
            HomeActivity homeActivity = (HomeActivity) activity;
            if (feedbackResult == FEEDBACK_GIVEN) {
                homeActivity.hideFeedbackMenuItem();
            } else {
                homeActivity.showFeedbackMenuItem();
            }
        }
    }
}
