package ke.co.hoji.android.listener;

import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.RecordSearch;

import java.util.List;

/**
 * Defines a callback for reacting to a {@link Record}s loaded event.
 * <p/>
 * Created by gitahi on 17/05/15.
 */
public interface OnRecordsLoadedListener {

    /**
     * Called when a {@link Record}s are loaded.
     *
     * @param records the loaded {@link Record}s.
     */
    void onRecordsLoaded(List<Record> records, String searchTaskId);

    /**
     * Called when a {@link Record}s are loaded.
     *
     * @param records      the loaded {@link Record}s.
     * @param recordSearch the {@link RecordSearch} object representing parameters for this
     *                     search.
     */
    void onRecordsLoaded(List<Record> records, String searchTaskId, RecordSearch recordSearch);
}
