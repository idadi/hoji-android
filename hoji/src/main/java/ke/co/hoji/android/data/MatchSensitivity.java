package ke.co.hoji.android.data;

/**
 * Created by gitahi on 11/17/16.
 */
public enum MatchSensitivity {

    HIGH(1, "High"),
    MEDIUM(2, "Medium"),
    LOW(3, "Low");

    private final int value;
    private final String label;

    MatchSensitivity(int value, String label) {
        this.value = value;
        this.label = label;
    }

    public int getValue() {
        return value;
    }

    private String getLabel() {
        return label;
    }

    public static MatchSensitivity getByValue(int value) {
        switch (value) {
            case 1:
                return MatchSensitivity.HIGH;
            case 2:
                return MatchSensitivity.MEDIUM;
            case 3:
                return MatchSensitivity.LOW;
            default:
                return null;
        }
    }

    @Override
    public String toString() {
        return getLabel();
    }
}
