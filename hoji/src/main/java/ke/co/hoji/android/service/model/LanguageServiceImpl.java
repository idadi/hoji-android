package ke.co.hoji.android.service.model;

import java.util.List;

import ke.co.hoji.android.dao.model.SqliteLanguageDao;
import ke.co.hoji.core.data.model.Language;
import ke.co.hoji.core.service.model.LanguageService;

public class LanguageServiceImpl implements LanguageService {

    private final SqliteLanguageDao languageDao;

    public LanguageServiceImpl(SqliteLanguageDao languageDao) {
        this.languageDao = languageDao;
    }

    @Override
    public List<Language> findAllLanguages() {
        return languageDao.getAllLanguages();
    }

}
