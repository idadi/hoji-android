package ke.co.hoji.android.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.LinkedHashMap;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.adapter.RankedChoiceArrayAdapter;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.widget.WidgetUtils;

/**
 * Created by gitahi on 17/01/18.
 */
public class RankingWidget extends AndroidWidget {

    private ListView listView;

    private List<Choice> choices;
    private LinkedHashMap<Choice, Integer> choiceMap;

    private Integer currentRank = 1;

    public RankingWidget(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.multiple_choice_widget;
    }

    @Override
    public void loadViews() {
        listView = findViewById(R.id.listView);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Choice clicked = choices.get(position);
                RankedChoiceArrayAdapter adapter = (RankedChoiceArrayAdapter) listView.getAdapter();
                if (clicked.getRank() == null || clicked.getRank() == 0) {
                    clicked.setRank(currentRank);
                    adapter.notifyDataSetChanged();
                    currentRank++;
                } else {
                    if (clicked.getRank() == (currentRank - 1)) {
                        clicked.setRank(0);
                        currentRank--;
                        adapter.notifyDataSetChanged();
                    } else {
                        new AlertDialog.Builder(getCurrentContext())
                                .setMessage(getResources().getString(R.string.confirm_reset_ranks_message, clicked.getName()))
                                .setTitle(R.string.confirm_reset_ranks_title)
                                .setPositiveButton(R.string.yes_action, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        reset();
                                    }
                                })
                                .setNegativeButton(R.string.no_action, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // do nothing
                                    }
                                })
                                .create()
                                .show();

                    }
                }
                raiseResponseChangingEvent();
            }
        });
    }

    @Override
    public void load() {
        Record record = getNavigable().getNavigator() != null ? getNavigable().getNavigator().getRecord() : null;
        choices = WidgetUtils.getChoices(
                getLiveField().getField(),
                record,
                getHojiContext().getRecord(),
                getHojiContext().getModelServiceManager().getRecordService()
        );
        RankedChoiceArrayAdapter adapter = new RankedChoiceArrayAdapter
                (
                        getCurrentContext(),
                        choices,
                        editable
                );
        listView.setAdapter(adapter);
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null) {
            choiceMap = (LinkedHashMap<Choice, Integer>) response.getActualValue();
            if (choiceMap != null) {
                currentRank = 0;
                for (Choice choice : choices) {
                    Integer rank = choiceMap.get(choice);
                    choice.setRank(rank != null ? rank : 0);
                    if (rank > currentRank) {
                        currentRank = rank;
                    }
                }
                RankedChoiceArrayAdapter adapter = (RankedChoiceArrayAdapter) listView.getAdapter();
                adapter.notifyDataSetChanged();
                currentRank++;
            }
        }
    }

    @Override
    public Object getValue() {
        choiceMap = new LinkedHashMap<>();
        for (int position = 0; position < choices.size(); position++) {
            Choice choice = (Choice) listView.getItemAtPosition(position);
            choiceMap.put(choice, choice.getRank() != null ? choice.getRank() : 0);
        }
        return choiceMap;
    }

    @Override
    public void focus() {
        listView.requestFocus();
    }

    @Override
    public void prepare() {
        listView.setAdapter(null);
    }

    @Override
    public void wipe() {
        choiceMap = null;
        for (Choice choice : choices) {
            choice.setRank(null);
        }
        RankedChoiceArrayAdapter adapter = (RankedChoiceArrayAdapter) listView.getAdapter();
        adapter.notifyDataSetChanged();
        currentRank = 1;
    }

    @Override
    public boolean isEmpty() {
        Response response = readResponse();
        if (response != null) {
            Long comparableFromActualValue = (Long) response.comparableFromActualValue();
            if (comparableFromActualValue != null) {
                return comparableFromActualValue == 0;
            }
        }
        return super.isEmpty();
    }


    @Override
    public Comparable getComparable() {
        long comparable = 0;
        if (choiceMap != null) {
            for (Integer rank : choiceMap.values()) {
                if (rank != null && rank != 0) {
                    comparable++;
                }
            }
        }
        return comparable;
    }

    @Override
    public Field.ValidationValue getMin(Record record) {
        String minString = getLiveField().getField().getMinValue();
        if (minString == null) {
//             If no min value is supplied, assume that all choices must be ranked.
            return new Field.ValidationValue((long) choices.size(), Field.ValidationAction.STOP);
        }
        return super.getMin(record);
    }

    @Override
    public View getPrincipalView() {
        return listView;
    }
}
