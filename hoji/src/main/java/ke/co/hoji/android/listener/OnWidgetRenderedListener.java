package ke.co.hoji.android.listener;

import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.widget.Widget;

/**
 * Defines a callback for reacting to a {@link Widget} rendered event.
 * <p/>
 * Created by gitahi on 26/05/15.
 */
public interface OnWidgetRenderedListener {

    /**
     * Called when a {@link Widget} is rendered.
     *
     * @param widget the {@link Widget} rendered.
     * @param record the {@link Record} associated with the rendered liveField.
     */
    void onWidgetRendered(Widget widget, Record record);
}
