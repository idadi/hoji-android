package ke.co.hoji.android.widget;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.LinkedHashMap;
import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.adapter.ChoiceArrayAdapter;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.widget.WidgetUtils;

/**
 * Created by gitahi on 14/11/14.
 */
public class MultipleChoiceWidgetStatic extends AndroidWidget {

    private ListView listView;

    private List<Choice> choices;
    private LinkedHashMap<Choice, Boolean> choiceMap;

    public MultipleChoiceWidgetStatic(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.multiple_choice_widget;
    }

    @Override
    public void loadViews() {
        listView = findViewById(R.id.listView);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                raiseResponseChangingEvent();
            }
        });
    }

    @Override
    public void load() {
        Record record = getNavigable().getNavigator() != null ? getNavigable().getNavigator().getRecord() : null;
        choices = WidgetUtils.getChoices(
                getLiveField().getField(),
                record,
                getHojiContext().getRecord(),
                getHojiContext().getModelServiceManager().getRecordService()
        );
        ChoiceArrayAdapter adapter = new ChoiceArrayAdapter
                (
                        getCurrentContext(),
                        R.layout.simple_list_item_multiple_choice,
                        choices,
                        editable
                );
        listView.setAdapter(adapter);
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null) {
            choiceMap = (LinkedHashMap<Choice, Boolean>) response.getActualValue();
            if (choiceMap != null) {
                int firstChoicePosition = -1;
                for (Choice choice : choiceMap.keySet()) {
                    int choicePosition = ((ArrayAdapter) listView.getAdapter()).getPosition(choice);
                    if (firstChoicePosition == -1) {
                        firstChoicePosition = choicePosition;
                    }
                    Boolean checked = choiceMap.get(choice);
                    listView.setItemChecked(choicePosition, checked != null ? checked : false);
                }
                listView.smoothScrollToPosition(firstChoicePosition);
            }
        }
    }

    @Override
    public Object getValue() {
        choiceMap = new LinkedHashMap<>();
        SparseBooleanArray sparseBooleanArray = listView.getCheckedItemPositions();
        for (int position = 0; position < choices.size(); position++) {
            choiceMap.put((Choice) listView.getItemAtPosition(position), sparseBooleanArray.get(position));
        }
        return choiceMap;
    }

    @Override
    public void focus() {
        listView.requestFocus();
    }

    @Override
    public void prepare() {
        listView.setAdapter(null);
    }

    @Override
    public void wipe() {
        choiceMap = null;
        listView.clearChoices();
        listView.requestLayout();
    }

    @Override
    public boolean isEmpty() {
        Response response = readResponse();
        if (response != null) {
            Long comparableFromActualValue = (Long) response.comparableFromActualValue();
            if (comparableFromActualValue != null) {
                return comparableFromActualValue == 0;
            }
        }
        return super.isEmpty();
    }

    @Override
    public Comparable getComparable() {
        return MultipleChoiceWidgetUtil.getComparable(
                (LinkedHashMap<Choice, Boolean>) readResponse().getActualValue()
        );
    }

    @Override
    public View getPrincipalView() {
        return listView;
    }

    @Override
    public ValidationResult validate(Record record) {
        Choice accompaniedLoner = MultipleChoiceWidgetUtil.getAccompaniedLoner(
                (LinkedHashMap<Choice, Boolean>) readResponse().getActualValue()
        );
        if (accompaniedLoner != null) {
            return ValidationResult.stop(getCurrentContext().getResources()
                    .getString(R.string.loner_not_alone, accompaniedLoner.getName()));
        }
        return super.validate(record);
    }
}
