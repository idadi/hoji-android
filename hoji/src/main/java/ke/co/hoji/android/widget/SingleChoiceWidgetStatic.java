package ke.co.hoji.android.widget;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import ke.co.hoji.R;
import ke.co.hoji.android.adapter.ChoiceArrayAdapter;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.widget.WidgetUtils;

/**
 * Created by gitahi on 14/11/14.
 */
public class SingleChoiceWidgetStatic extends AndroidWidget {

    private ListView listView;

    private List<Choice> choices;
    private Choice choice;

    public SingleChoiceWidgetStatic(Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.single_choice_widget;
    }

    @Override
    public void loadViews() {
        listView = findViewById(R.id.listView);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                choice = choices.get(position);
                raiseResponseChangedEvent();
            }
        });
    }

    @Override
    public void load() {
        Record record = getNavigable().getNavigator() != null ? getNavigable().getNavigator().getRecord() : null;
        choices = WidgetUtils.getChoices(
                getLiveField().getField(),
                record,
                getHojiContext().getRecord(),
                getHojiContext().getModelServiceManager().getRecordService()
        );
        ChoiceArrayAdapter adapter = new ChoiceArrayAdapter
                (
                        getCurrentContext(),
                        R.layout.simple_list_item_single_choice,
                        choices,
                        editable
                );
        listView.setAdapter(adapter);
    }

    @Override
    public void writeResponse() {
        Response response = getLiveField().getResponse();
        if (response != null) {
            choice = (Choice) response.getActualValue();
            int position = ((ArrayAdapter) listView.getAdapter()).getPosition(choice);
            listView.setItemChecked(position, true);
            listView.smoothScrollToPosition(position);
        }
    }

    @Override
    public Object getValue() {
        return choice;
    }

    @Override
    public void focus() {
        listView.requestFocus();
    }

    @Override
    public void prepare() {
        listView.setAdapter(null);
    }

    @Override
    public void wipe() {
        choice = null;
        listView.clearChoices();
        listView.requestLayout();
    }

    @Override
    public View getPrincipalView() {
        return listView;
    }
}
