package ke.co.hoji.android.dao.dto;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.dto.Form;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by gitahi on 27/08/15.
 */
public class SqliteFormDao {

    private final SqliteSqlExecutor se;

    public SqliteFormDao(SqliteSqlExecutor se) {
        this.se = se;
    }

    public Form getFormById(int formId) {
        Form form = null;
        String select = "SELECT "
                + "id, name, ordinal, survey_id, gps_capture, min_gps_accuracy, "
                + "no_of_gps_attempts, enabled, minor_version, major_version "
                + "FROM form "
                + "WHERE id = ? "
                + "ORDER BY ordinal";
        Map<String, Object> params = se.createParameterMap("id", formId);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                form = new Form();
                form.setId(cursor.getInt(cursor.getColumnIndex("id")));
                form.setName(cursor.getString(cursor.getColumnIndex("name")));
                form.setEnabled(cursor.getInt(cursor.getColumnIndex("enabled")) > 0);
                form.setOrdinal(new BigDecimal(cursor.getDouble(cursor.getColumnIndex("ordinal"))));
                form.setMinorVersion(cursor.getInt(cursor.getColumnIndex("minor_version")));
                form.setMajorVersion(cursor.getInt(cursor.getColumnIndex("major_version")));
                form.setGpsCapture(cursor.getInt(cursor.getColumnIndex("gps_capture")));
                form.setSurveyId(cursor.getInt(cursor.getColumnIndex("survey_id")));
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteFormDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return form;
    }

    public List<Form> saveForms(List<Form> forms) {
        se.beginTransaction();
        try {
            {
                Map<String, Object> params = se.createParameterMap();
                params.put("enabled", false);
                se.update("form", params, null, null);
            }
            for (Form form : forms) {
                Map<String, Object> params = se.createParameterMap();
                params.put("name", form.getName());
                params.put("ordinal", form.getOrdinal());
                params.put("survey_id", form.getSurveyId());
                params.put("gps_capture", form.getGpsCapture());
                params.put("enabled", form.isEnabled());
                params.put("minor_version", form.getMinorVersion());
                params.put("major_version", form.getMajorVersion());
                if (!formExists(form.getId())) {
                    params.put("id", form.getId());
                    se.insert("form", params);
                } else {
                    Map<String, Object> whereParams = se.createParameterMap("id", form.getId());
                    se.update("form", params, "id = ?", whereParams);
                }
            }
            se.setTransactionSuccessful();
        } finally {
            se.endTransaction();
        }
        return forms;
    }

    private boolean formExists(int formId) {
        String select = "SELECT COUNT(id) FROM form WHERE id = ?";
        return se.exists(select, formId);
    }
}
