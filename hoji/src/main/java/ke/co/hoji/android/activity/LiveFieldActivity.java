package ke.co.hoji.android.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.widget.ProgressBar;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.data.Constants;
import ke.co.hoji.android.data.LaunchType;
import ke.co.hoji.android.listener.OnLiveFieldSelectedListener;
import ke.co.hoji.android.listener.OnRecordSelectedListener;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.MainRecord;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.widget.Navigator;

/**
 * Created by gitahi on 12/05/15.
 */
public class LiveFieldActivity extends AppCompatActivity
        implements OnRecordSelectedListener, OnLiveFieldSelectedListener, RecordContainer {

    private Record record;

    private ProgressBar formProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_field_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        formProgressBar = findViewById(R.id.formProgressBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        Record deflatedRecord;
        if (savedInstanceState != null) {
            deflatedRecord = (Record) savedInstanceState.getSerializable(Constants.DEFLATED_RECORD);
        } else {
            deflatedRecord = (Record) getIntent().getSerializableExtra(Constants.DEFLATED_RECORD);

        }
        Record record = ((AndroidHojiContext) getApplication()).inflateRecord(deflatedRecord);
        onRecordSelected(record);
        refresh();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(Constants.DEFLATED_RECORD, ((AndroidHojiContext) getApplication()).deflateRecord(record));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        refresh();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private void refresh() {
        Record record = ((HojiContext) getApplication()).getRecord();
        if (record != null) {
            if (this.record.equals(record)) {
                onRecordSelected(record);
            }
        }
    }

    public void onLiveFieldSelected(LiveField liveField) {
        openLiveField(liveField, Navigator.Interruption.CHECK);
    }

    public void onRecordSelected(Record record) {
        this.record = record;
        formProgressBar.setProgress(record.computeProgress());
    }

    @Override
    public Record getRecord() {
        return this.record;
    }

    private void openLiveField(LiveField liveField, int interruption) {
        if (interruption == Navigator.Interruption.CHECK) {
            processInterruption(liveField);
        } else if (interruption == Navigator.Interruption.EXECUTE) {
            AndroidHojiContext androidHojiContext = (AndroidHojiContext) getApplication();
            Record deflatedRecord = androidHojiContext.deflateRecord(record);
            LiveField deflatedLiveField = androidHojiContext.deflateLiveField(liveField);
            Intent intent = new Intent(LiveFieldActivity.this, DataEntryActivity.class);
            intent.putExtra(Constants.LAUNCH_TYPE, LaunchType.OPEN);
            intent.putExtra(Constants.DEFLATED_RECORD, deflatedRecord);
            intent.putExtra(Constants.DEFLATED_LIVE_FIELD, deflatedLiveField);
            startActivity(intent);
        }
    }

    private void processInterruption(final LiveField liveField) {
        final MainRecord mainRecord = (MainRecord) record.getDetail();
        if (mainRecord.getStage() == MainRecord.Stage.UPLOADED) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.confirm_edit_message)
                    .setTitle(R.string.confirm_edit_title)
                    .setPositiveButton(R.string.yes_action, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            openLiveField(liveField, Navigator.Interruption.EXECUTE);
                        }
                    })
                    .setNegativeButton(R.string.no_action, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            openLiveField(liveField, Navigator.Interruption.ABORT);
                        }
                    })
                    .create()
                    .show();

        } else {
            openLiveField(liveField, Navigator.Interruption.EXECUTE);
        }
    }
}