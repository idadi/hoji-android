package ke.co.hoji.android.service.dto;

import java.util.List;

import ke.co.hoji.android.dao.dto.SqliteLanguageDao;
import ke.co.hoji.core.data.dto.Language;
import ke.co.hoji.core.service.dto.LanguageService;

public class LanguageServiceImpl implements LanguageService {

    private final SqliteLanguageDao languageDao;

    public LanguageServiceImpl(SqliteLanguageDao languageDao) {
        this.languageDao = languageDao;
    }

    @Override
    public void saveLanguages(List<Language> languages) {
        languageDao.saveLanguages(languages);
    }

}
