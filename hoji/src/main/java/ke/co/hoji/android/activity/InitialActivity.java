package ke.co.hoji.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ke.co.hoji.R;

/**
 * The initial app screen to guide the user to the sign up or sign in process.
 * <p/>
 * Created by gitahi on 23/06/19.
 */
public class InitialActivity extends Activity {

    /**
     * Flag for requesting sign up result.
     */
    private static final int REQUEST_SIGN_UP = 0;

    /**
     * Flag for requesting sign in result.
     */
    private static final int REQUEST_SIGN_IN = 1;

    /**
     * This result indicates that the user decided to switch from signing up to signing in or
     * vice versa.
     */
    public static final int RESULT_SWITCH = -1;

    /**
     * This result indicates that the user either signed up or signed in successfully.
     */
    public static final int RESULT_SUCCESS = 0;

    /**
     * This result indicates that the user left the sign up or sign in screen by clicking on the
     * back button.
     */
    public static final int RESULT_CANCEL = 1;

    private Button signUpButton;
    private Button signInButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.initial_activity);
        signUpButton = findViewById(R.id.signUpButton);
        signInButton = findViewById(R.id.signInButton);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestSignUp();
            }
        });
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestSignIn();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGN_UP) {
            if (resultCode == RESULT_SUCCESS) {
                signUpOrInSucceeded(true);
            } else if (resultCode == RESULT_SWITCH) {
                requestSignIn();
            }
        } else if (requestCode == REQUEST_SIGN_IN) {
            if (resultCode == RESULT_SUCCESS) {
                signUpOrInSucceeded(false);
            } else if (resultCode == RESULT_SWITCH) {
                requestSignUp();
            }
        }
    }

    private void requestSignUp() {
        Intent intent = new Intent(InitialActivity.this, SignUpActivity.class);
        startActivityForResult(intent, REQUEST_SIGN_UP);
    }

    private void requestSignIn() {
        Intent intent = new Intent(InitialActivity.this, SignInActivity.class);
        startActivityForResult(intent, REQUEST_SIGN_IN);
    }

    private void signUpOrInSucceeded(final boolean signUp) {
        Intent intent = new Intent(InitialActivity.this, SurveysActivity.class);
        intent.putExtra(ke.co.hoji.android.data.Constants.SIGN_UP, signUp);
        startActivity(intent);
        finish();
    }
}