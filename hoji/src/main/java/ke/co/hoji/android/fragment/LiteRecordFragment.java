package ke.co.hoji.android.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.ListFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.activity.LiteRecordActivity;
import ke.co.hoji.android.adapter.LiteRecordArrayAdapter;
import ke.co.hoji.android.listener.OnMainRecordsImportedListener;
import ke.co.hoji.android.service.HttpService;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.dto.LiteRecord;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.http.ImportRequest;
import ke.co.hoji.core.data.http.ImportResponse;
import ke.co.hoji.core.data.http.ResultCode;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.HttpException;
import ke.co.hoji.core.service.model.PropertyService;

/**
 * Created by gitahi on 17/11/16.
 */
public class LiteRecordFragment extends ListFragment {

    private List<LiteRecord> liteRecords;
    private Form form;

    private LiteRecordArrayAdapter adapter;

    private ProgressDialog progressDialog;

    private AndroidHojiContext androidHojiContext;
    private PropertyService propertyService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        liteRecords = new ArrayList<>();
    }

    @Override
    public void onResume() {
        super.onResume();
        progressDialog = new ProgressDialog(this.getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.lite_record_fragment_actions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.importButton:
                importAll();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        importOne(liteRecords.get(position));
    }

    private void importOne(LiteRecord liteRecord) {
        if (liteRecord.getStatus() == LiteRecord.Status.IMPORTED) {
            Toast toast = Toast.makeText
                    (
                            getActivity(),
                            getResources().getText(R.string.already_imported),
                            Toast.LENGTH_SHORT
                    );
            toast.show();
        } else {
            importOneRecord(liteRecord);
        }
    }

    private void importAll() {
        List<LiteRecord> toImport = new ArrayList<>();
        for (LiteRecord liteRecord : liteRecords) {
            if (liteRecord.getStatus() != LiteRecord.Status.IMPORTED) {
                toImport.add(liteRecord);
            }
        }
        if (!toImport.isEmpty()) {
            importAllRecords(toImport);
        } else {
            Toast toast = Toast.makeText
                    (
                            getActivity(),
                            getResources().getText(R.string.nothing_to_import),
                            Toast.LENGTH_SHORT
                    );
            toast.show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        adapter = new LiteRecordArrayAdapter
                (
                        inflater.getContext(),
                        liteRecords
                );
        setListAdapter(adapter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return true;
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        androidHojiContext = (AndroidHojiContext) getActivity().getApplication();
        propertyService = androidHojiContext.getModelServiceManager().getPropertyService();
    }

    public void refresh(List<LiteRecord> liteRecords, Form form) {
        this.form = form;
        if (liteRecords != null) {
            this.liteRecords.clear();
            this.liteRecords.addAll(liteRecords);
            adapter.notifyDataSetChanged();
        }
    }

    private void importOneRecord(LiteRecord liteRecord) {
        List<LiteRecord> liteRecords = new ArrayList<>();
        liteRecords.add(liteRecord);
        importRecords(liteRecords);
    }

    private void importAllRecords(final List<LiteRecord> liteRecords) {
        if (liteRecords.size() > 1) {
            new AlertDialog.Builder(LiteRecordFragment.this.getActivity())
                    .setTitle(getResources().getString(R.string.import_all_title))
                    .setMessage(getResources().getString(R.string.import_all_message, liteRecords.size()))
                    .setPositiveButton(R.string.yes_action, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            importRecords(liteRecords);
                        }
                    })
                    .setNegativeButton(R.string.no_action, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Do nothing
                        }
                    })
                    .create()
                    .show();
        } else {
            importRecords(liteRecords);
        }
    }

    private void importRecords(List<LiteRecord> liteRecords) {
        ImportRecordTask importRecordTask = new ImportRecordTask
                (
                        getActivity(),
                        androidHojiContext.getHttpService(),
                        propertyService.getValue(Constants.Server.USERNAME),
                        propertyService.getValue(Constants.Server.PASSWORD),
                        liteRecords,
                        form,
                        (OnMainRecordsImportedListener) getActivity(),
                        progressDialog
                );
        importRecordTask.execute();
    }

    private void scrollToPosition(int position) {
        if (this.isAdded()) {
            ListView listView = getListView();
            listView.setSelection(position);
            listView.smoothScrollToPosition(position);
        }
    }

    private class ImportRecordTask extends AsyncTask<Void, String, Integer> {

        static final int NOTHING_FOUND = -2;
        static final int ERROR_OCCURRED = -1;

        private final Activity activity;
        private final HttpService httpService;
        private final String username;
        private final String password;
        private final List<LiteRecord> liteRecords;
        private final Form form;
        private final OnMainRecordsImportedListener onMainRecordsImportedListener;
        private final ProgressDialog progressDialog;

        private int noOfRecordsImported = 0;

        private ImportRecordTask
                (
                        Activity activity,
                        HttpService httpService,
                        String username,
                        String password,
                        List<LiteRecord> liteRecords,
                        Form form,
                        OnMainRecordsImportedListener onMainRecordsImportedListener,
                        ProgressDialog progressDialog
                ) {
            this.activity = activity;
            this.httpService = httpService;
            this.username = username;
            this.password = password;
            this.liteRecords = liteRecords;
            this.form = form;
            this.onMainRecordsImportedListener = onMainRecordsImportedListener;
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            if (progressDialog != null) {
                progressDialog.setMessage(activity.getResources().getString(
                        R.string.saving_imported_records,
                        1,
                        liteRecords.size(),
                        liteRecords.get(0).getCaption()
                        )
                );
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }

        @Override
        protected Integer doInBackground(Void... params) {
            List<String> uuids = new ArrayList<>();
            Map<String, LiteRecord> liteRecordMap = new HashMap<>();
            for (LiteRecord liteRecord : liteRecords) {
                uuids.add(liteRecord.getUuid());
                liteRecordMap.put(liteRecord.getUuid(), liteRecord);
            }
            try {
                publishProgress(activity.getResources().getQuantityString(
                        R.plurals.importing_records,
                        liteRecords.size(),
                        liteRecords.size()
                ));
                ImportRequest importRequest = new ImportRequest();
                importRequest.setUuids(uuids);
                importRequest.setFormId(form.getId());
                importRequest.setFormVersion(form.getMajorVersion());
                importRequest.setSurveyId(form.getSurvey().getId());
                ImportResponse importResponse = httpService.sendAsJson
                        (
                                ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.IMPORT_RECORD,
                                username,
                                password,
                                importRequest,
                                ImportResponse.class
                        );
                if (importResponse.getResultCode() == ResultCode.OKAY) {
                    final Map<String, List<MainRecord>> mainRecordMap = importResponse.getMainRecordMap();
                    if (mainRecordMap != null && !mainRecordMap.isEmpty()) {
                        int count = 1;
                        for (final String uuid : mainRecordMap.keySet()) {
                            final LiteRecord liteRecord = liteRecordMap.get(uuid);
                            final int liteRecordIndex = LiteRecordFragment.this.liteRecords.indexOf(liteRecord) + 1;
                            publishProgress(activity.getResources().getString(
                                    R.string.saving_imported_records,
                                    count,
                                    liteRecords.size(),
                                    liteRecordMap.get(uuid).getCaption()
                            ));
                            count++;
                            onMainRecordsImportedListener.onMainRecordsImported(mainRecordMap.get(uuid));
                            noOfRecordsImported++;
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    scrollToPosition(liteRecordIndex);
                                    LiteRecordActivity liteRecordActivity = (LiteRecordActivity) getActivity();
                                    if (liteRecordActivity != null) {
                                        liteRecordActivity.refresh();
                                    }
                                }
                            });
                        }
                    }
                }
            } catch (final HttpException ex) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast toast = Toast.makeText(
                                activity,
                                httpService.interpretHttpExceptionCode(ex),
                                Toast.LENGTH_SHORT
                        );
                        toast.show();
                    }
                });
                return ERROR_OCCURRED;
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            String message = values[0];
            progressDialog.setMessage(message);
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            String message;
            if (result == null) {
                message = activity.getResources().getString(
                        R.string.imported_n_out_of_m_records_1,
                        noOfRecordsImported)
                        + " " +
                        activity.getResources().getQuantityString(
                                R.plurals.imported_n_out_of_m_records_2,
                                liteRecords.size(),
                                liteRecords.size()
                        );

            } else {
                message = getMessage(result);
            }
            if (message != null) {
                Toast toast = Toast.makeText(
                        activity,
                        message,
                        Toast.LENGTH_SHORT
                );
                toast.show();
            }
        }

        private String getMessage(int resultCode) {
            String message = null;
            if (resultCode == ResultCode.FORM_NOT_FOUND) {
                message = activity.getResources().getText(R.string.nothing_found).toString();
            } else if (resultCode == ResultCode.SURVEY_NOT_ENABLED) {
                message = activity.getResources().getText(R.string.import_survey_not_enabled).toString();
            } else if (resultCode == ResultCode.SURVEY_NOT_PUBLISHED) {
                message = activity.getResources().getText(R.string.import_survey_not_published).toString();
            } else if (resultCode == ResultCode.SURVEY_OUT_OF_DATE) {
                message = activity.getResources().getText(R.string.survey_out_of_date).toString();
            } else if (resultCode == NOTHING_FOUND) {
                message = activity.getResources().getText(R.string.nothing_found).toString();
            }
            return message;
        }
    }
}