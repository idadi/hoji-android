package ke.co.hoji.android.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.Hours;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.task.DownloadTask;
import ke.co.hoji.android.helper.DownloadingComponent;
import ke.co.hoji.android.adapter.SurveyArrayAdapter;
import ke.co.hoji.android.service.HttpService;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.PublicSurveyAccess;
import ke.co.hoji.core.data.dto.Survey;
import ke.co.hoji.core.data.http.ChangeTenantRequest;
import ke.co.hoji.core.data.http.ChangeTenantResponse;
import ke.co.hoji.core.data.http.SurveyResponse;
import ke.co.hoji.core.data.http.SurveysRequest;
import ke.co.hoji.core.data.http.SurveysResponse;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.service.model.HttpException;
import ke.co.hoji.core.service.model.PropertyService;

/**
 * Created by gitahi on 10/01/16.
 */
public class SurveysActivity extends AppCompatActivity implements DownloadingComponent {

    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout surveyListLayout;
    private LinearLayout emptyStateLayout;
    private ListView listView;
    private TextView emptyStateMessage;
    private Button emptyStatePrimaryAction;
    private TextView emptyStateSecondaryAction;
    private final List<SurveyResponse> surveyResponses = new ArrayList<>();

    private SurveyArrayAdapter adapter;

    private AndroidHojiContext hojiContext;

    private HttpService httpService;
    private PropertyService propertyService;

    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.surveys_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        setTitle(getString(R.string.surveys_activity_title));
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setProgressViewOffset(false, 100, 150);
        surveyListLayout = findViewById(R.id.surveyListLayout);
        emptyStateLayout = findViewById(R.id.emptyStateLayout);
        listView = findViewById(R.id.listView);
        emptyStateMessage = findViewById(R.id.emptyStateMessage);
        emptyStatePrimaryAction = findViewById(R.id.emptyStatePrimaryAction);
        emptyStateSecondaryAction = findViewById(R.id.emptyStateSecondaryAction);
        emptyStateLayout.setVisibility(View.GONE);
        hojiContext = (AndroidHojiContext) getApplication();
        hojiContext.clearContext();
        adapter = new SurveyArrayAdapter(this, surveyResponses);
        listView.setAdapter(adapter);
        if (getIntent().getExtras() != null) {
            Boolean signUp = getIntent().getExtras().getBoolean(ke.co.hoji.android.data.Constants.SIGN_UP);
            if (signUp != null && signUp) {
                new AlertDialog.Builder(SurveysActivity.this)
                        .setTitle(getString(R.string.welcome_to_hoji))
                        .setMessage(getResources()
                                .getString(R.string.verification_email_notification, Constants.Server.VERIFICATION_EMAIL_EXPIRY))
                        .setPositiveButton(R.string.ok_action, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        })
                        .create()
                        .show();
            }
        }
        httpService = hojiContext.getHttpService();
        propertyService = hojiContext.getModelServiceManager().getPropertyService();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh(new SurveysRequest());
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Survey survey = surveyResponses.get(position).getSurvey();
                DownloadTask downloadTask = new DownloadTask
                        (
                                SurveysActivity.this,
                                survey,
                                false,
                                hojiContext.getHttpService(),
                                hojiContext.getModelServiceManager().getPropertyService(),
                                hojiContext.getDtoServiceManager().getSurveyService(),
                                hojiContext.getDtoServiceManager().getFormService(),
                                hojiContext.getDtoServiceManager().getFieldService(),
                                hojiContext.getDtoServiceManager().getReferenceService(),
                                hojiContext.getDtoServiceManager().getLanguageService(),
                                hojiContext.getDtoServiceManager().getTranslationService(),
                                progressDialog
                        );
                downloadTask.execute();
                hojiContext.clearContext();
            }
        });
        emptyStateSecondaryAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openChangeTenantDialog();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressDialog = new ProgressDialog(this);
        ke.co.hoji.core.data.model.Survey survey = hojiContext.getSurvey();
        if (survey != null) {
            Intent intent = new Intent(SurveysActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        } else {
            refresh(new SurveysRequest());
        }
    }

    @Override
    protected void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.surveys_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.changeTenantMenuItem:
                openChangeTenantDialog();
                return true;
            case R.id.signOutButton:
                signOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void downloadSucceeded() {
        Intent intent = new Intent(SurveysActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void downloadFailed(final String message) {
        downloadFailed(message, false);
    }

    @Override
    public void downloadFailed(final String message, boolean nothingNewFound) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText
                        (
                                SurveysActivity.this, message,
                                Toast.LENGTH_SHORT
                        );
                toast.show();
            }
        });
    }

    @Override
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public boolean isAlive() {
        return !this.isFinishing();
    }

    private void signOut() {
        hojiContext.logout();
        Intent intent = new Intent(SurveysActivity.this, InitialActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void openChangeTenantDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SurveysActivity.this);
        LayoutInflater inflater = SurveysActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.change_tenant_dialog, null);
        final TextView currentTenantEditText = dialogView.findViewById(R.id.currentTenantTextView);
        final EditText tenantCodeEditText = dialogView.findViewById(R.id.tenantCodeEditText);
        final CheckBox ownTenantCodeCheckBox = dialogView.findViewById(R.id.ownTenantCodeCheckBox);
        currentTenantEditText.setText(getString(R.string.current_tenant,
                hojiContext.getTenantName() + " - " + hojiContext.getTenantCode()));
        tenantCodeEditText.requestFocus();
        ownTenantCodeCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    tenantCodeEditText.setText(hojiContext.getUserCode());
                    tenantCodeEditText.setFocusable(false);
                    tenantCodeEditText.setFocusableInTouchMode(false);
                    tenantCodeEditText.setClickable(false);
                } else {
                    tenantCodeEditText.setText("");
                    tenantCodeEditText.setFocusable(true);
                    tenantCodeEditText.setFocusableInTouchMode(true);
                    tenantCodeEditText.setClickable(true);
                }
            }
        });
        builder.setView(dialogView)
                .setPositiveButton(R.string.ok_action, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //do nothing since this is overridden further down to prevent it
                        //from automatically dismissing the dialog
                    }
                })
                .setNegativeButton(R.string.cancel_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean closeDialog = true;
                String tenantCode = tenantCodeEditText.getText().toString();
                Matcher matcher = Pattern.compile("[A-Z0-9]").matcher(tenantCode);
                if (tenantCode.length() != 5 || !matcher.find()) {
                    tenantCodeEditText.setError("Invalid Tenant Code");
                    closeDialog = false;
                } else {
                    ChangeTenantTask changeTenantTask = new ChangeTenantTask(
                            httpService,
                            hojiContext.getUserId(),
                            tenantCode,
                            propertyService.getValue(Constants.Server.USERNAME),
                            propertyService.getValue(Constants.Server.PASSWORD)
                    );
                    changeTenantTask.execute();
                }
                refresh(new SurveysRequest());
                if (closeDialog) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void refresh(SurveysRequest request) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                RefreshTask refreshTask = new RefreshTask(
                        httpService,
                        propertyService.getValue(Constants.Server.USERNAME),
                        propertyService.getValue(Constants.Server.PASSWORD),
                        request
                );
                refreshTask.execute();
            }
        });
    }

    private void showMessage(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText
                        (
                                getBaseContext(),
                                message,
                                Toast.LENGTH_LONG
                        ).show();
            }
        });
    }

    private int hoursSinceLastAccessRequest(String tenantCode) {
        int hoursDifference = Integer.MAX_VALUE;
        Property property = propertyService.getProperty(constructAccessRequestKey(), null);
        if (property != null && property.getValue() != null) {
            Date then = Utils.parseIsoDateTime(property.getValue());
            if (then != null) {
                Date now = new Date();
                hoursDifference = Hours.hoursBetween(new DateTime(then), new DateTime(now)).getHours();
            }
        }
        return hoursDifference;
    }

    private String constructAccessRequestKey() {
        return ke.co.hoji.android.data.Constants.ACCESS_REQUEST + hojiContext.getUserCode()
                + "." + hojiContext.getTenantCode();
    }

    private class RefreshTask extends AsyncTask<Void, Void, Void> {

        private final HttpService httpService;
        private final String username;
        private final String password;
        private final SurveysRequest request;

        private RefreshTask(HttpService httpService, String username, String password, SurveysRequest request) {
            this.httpService = httpService;
            this.username = username;
            this.password = password;
            this.request = request;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                final SurveysResponse response = httpService.sendAsJson
                        (
                                ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.SURVEYS_DOWNLOAD,
                                username,
                                password,
                                request,
                                SurveysResponse.class
                        );
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        propertyService.setProperties(
                                new Property(Constants.Server.TENANT_CODE, response.getTenantCode()),
                                new Property(Constants.Server.TENANT_NAME, response.getTenantName())
                        );
                        new ResultsProcessor().processResults(200, response.getSurveyResponses());
                    }
                });
            } catch (final HttpException ex) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new ResultsProcessor().processResults(ex.getCode(), null);
                    }
                });
                if (ex.getCode() != HttpException.Code.UNAUTHORIZED
                        && ex.getCode() != HttpException.Code.FORBIDDEN
                        && ex.getCode() != HttpException.Code.METHOD_NOT_ALLOWED) {
                    showMessage(httpService.interpretHttpExceptionCode(ex));
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            swipeRefreshLayout.setRefreshing(false);
            if (request.getPublicSurveyAccess() != null) {
                new AlertDialog.Builder(SurveysActivity.this)
                        .setTitle(R.string.sample_project_activated_title)
                        .setMessage(getString(R.string.sample_project_activated_message)
                        ).setPositiveButton(R.string.ok_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        refresh(new SurveysRequest());
                    }
                }).create().show();
            }
        }
    }

    private class ChangeTenantTask extends AsyncTask<Void, Void, ChangeTenantResponse> {

        private final HttpService httpService;
        private final Integer userId;
        private final String tenantCode;
        private final String username;
        private final String password;

        ChangeTenantTask(HttpService httpService, Integer userId, String tenantCode, String username, String password) {
            this.httpService = httpService;
            this.userId = userId;
            this.tenantCode = tenantCode;
            this.username = username;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            if (progressDialog != null) {
                progressDialog.setMessage(SurveysActivity.this.getText(R.string.please_wait));
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }

        @Override
        protected ChangeTenantResponse doInBackground(Void... voids) {
            ChangeTenantResponse response = null;
            try {
                ChangeTenantRequest request = new ChangeTenantRequest(userId, tenantCode);
                response = httpService.sendAsJson
                        (
                                ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.CHANGE_TENANT,
                                username,
                                password,
                                request,
                                ChangeTenantResponse.class
                        );
            } catch (HttpException ex) {
                showMessage(httpService.interpretHttpExceptionCode(ex));
            }
            return response;
        }

        @Override
        protected void onPostExecute(ChangeTenantResponse response) {
            if (response != null) {
                if (response.getCode() == ChangeTenantResponse.Code.TENANT_FOUND) {
                    hojiContext.getModelServiceManager().getPropertyService().setProperties
                            (
                                    new Property(Constants.Server.TENANT_CODE, response.getTenantCode()),
                                    new Property(Constants.Server.TENANT_NAME, response.getTenantName())
                            );
                    hojiContext.clearContext();
                    refresh(new SurveysRequest());
                } else {
                    showMessage("Unknown Tenant Code: " + tenantCode);
                }
            }
            dismissProgressDialog();
        }
    }

    private class RequestAccessTask extends AsyncTask<Void, Void, Boolean> {

        private final HttpService httpService;
        private final String username;
        private final String password;

        RequestAccessTask(HttpService httpService, String username, String password) {
            this.httpService = httpService;
            this.username = username;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            if (progressDialog != null) {
                progressDialog.setMessage(SurveysActivity.this.getText(R.string.please_wait));
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            Boolean result = Boolean.FALSE;
            try {
                result = httpService.sendAsJson(
                        ke.co.hoji.android.data.Constants.SERVER_URL + Constants.Api.REQUEST_ACCESS,
                        username,
                        password,
                        new Object(),
                        Boolean.class
                );
            } catch (HttpException ex) {
                showMessage(httpService.interpretHttpExceptionCode(ex));
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            dismissProgressDialog();
            if (result != null && result) {
                propertyService.setProperty(new Property(constructAccessRequestKey(), Utils.formatIsoDateTime(new Date())));
                refresh(new SurveysRequest());
                new AlertDialog.Builder(SurveysActivity.this)
                        .setTitle(R.string.access_requested_title)
                        .setMessage(
                                getString(
                                        R.string.access_requested__message, hojiContext.getTenantName(),
                                        ke.co.hoji.android.data.Constants.ACCESS_REQUEST_WAITING_PERIOD
                                )
                        ).setPositiveButton(R.string.ok_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Do nothing.
                    }
                }).create().show();
            }
        }
    }

    /**
     * Processes the results when we query for all the Surveys that a user has access to. This
     * class encapsulates both the logic for showing the list and the logic for managing the
     * empty state when no items are available to list.
     */
    private class ResultsProcessor {

        private void processResults(final int resultCode, final List<SurveyResponse> newResponses) {
            surveyResponses.clear();
            if (resultCode == 200) {
                if (newResponses != null && !newResponses.isEmpty()) {
                    showSurveysList(newResponses);
                } else {
                    showEmptyStateNoSurveys();
                }
            } else if (resultCode == HttpException.Code.FORBIDDEN
                    || resultCode == HttpException.Code.METHOD_NOT_ALLOWED) {
                showEmptyStateAccessDenied();
            } else if (resultCode == HttpException.Code.UNAUTHORIZED) {
                signOut();
            } else {
                showEmptyStateError();
            }
        }

        private void showSurveysList(final List<SurveyResponse> newResponses) {
            surveyResponses.addAll(newResponses);
            adapter = new SurveyArrayAdapter(SurveysActivity.this, surveyResponses);
            listView.setAdapter(adapter);
            showAndHideUiComponents(true);
        }

        private void showEmptyStateNoSurveys() {
            if (hojiContext.getUserCode().equals(hojiContext.getTenantCode())) {
                emptyStateMessage.setText(getString(R.string.no_projects_under_user));
                emptyStatePrimaryAction.setText(getString(R.string.activate_sample_action));
                emptyStateSecondaryAction.setText(Html.fromHtml(getString(R.string.working_with_someone_else)));
                emptyStatePrimaryAction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        refresh(new SurveysRequest(PublicSurveyAccess.VERIFIED));
                    }
                });
            } else {
                emptyStateMessage.setText(getResources()
                        .getString(R.string.no_projects_under_tenant, hojiContext.getTenantName()));
                emptyStatePrimaryAction.setText(getString(R.string.go_to_my_account_action));
                emptyStateSecondaryAction.setText(Html.fromHtml(getString(R.string.no_longer_with_tenant, hojiContext.getTenantName())));
                emptyStatePrimaryAction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ChangeTenantTask changeTenantTask = new ChangeTenantTask(
                                httpService,
                                hojiContext.getUserId(),
                                hojiContext.getUserCode(),
                                propertyService.getValue(Constants.Server.USERNAME),
                                propertyService.getValue(Constants.Server.PASSWORD)
                        );
                        changeTenantTask.execute();
                    }
                });
            }
            showAndHideUiComponents(false);
        }

        private void showEmptyStateAccessDenied() {
            emptyStateMessage.setText(getResources()
                    .getString(R.string.no_projects_access_denied, hojiContext.getTenantName()));
            int hours = hoursSinceLastAccessRequest(propertyService.getValue(Constants.Server.TENANT_CODE));
            emptyStatePrimaryAction.setText(getString(R.string.request_access_action));
            if (hours > ke.co.hoji.android.data.Constants.ACCESS_REQUEST_WAITING_PERIOD) {
                emptyStatePrimaryAction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        RequestAccessTask requestAccessTask = new RequestAccessTask(
                                httpService,
                                propertyService.getValue(Constants.Server.USERNAME),
                                propertyService.getValue(Constants.Server.PASSWORD)
                        );
                        requestAccessTask.execute();
                    }
                });
            } else {
                emptyStatePrimaryAction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(SurveysActivity.this)
                                .setTitle(R.string.access_already_requested_title)
                                .setMessage(
                                        getString(
                                                R.string.access_already_requested_message, hojiContext.getTenantName(),
                                                (ke.co.hoji.android.data.Constants.ACCESS_REQUEST_WAITING_PERIOD - hours)
                                        )
                                ).setPositiveButton(R.string.ok_action, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                refresh(new SurveysRequest());
                            }
                        }).create().show();
                    }
                });
            }
            emptyStateSecondaryAction.setText(Html.fromHtml(getString(R.string.no_longer_with_tenant, hojiContext.getTenantName())));
            showAndHideUiComponents(false);
        }

        private void showEmptyStateError() {
            showAndHideUiComponents(true);
        }

        private void showAndHideUiComponents(boolean showList) {
            ActionMenuItemView changeTenantMenuItem = findViewById(R.id.changeTenantMenuItem);
            changeTenantMenuItem.setVisibility(showList ? View.VISIBLE : View.GONE);
            surveyListLayout.setVisibility(showList ? View.VISIBLE : View.GONE);
            emptyStateLayout.setVisibility(showList ? View.GONE : View.VISIBLE);
        }
    }
}