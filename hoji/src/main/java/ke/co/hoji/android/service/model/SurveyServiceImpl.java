package ke.co.hoji.android.service.model;

import java.util.List;

import ke.co.hoji.android.dao.model.SqliteSurveyDao;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.SurveyService;

public class SurveyServiceImpl implements SurveyService {

    private final SqliteSurveyDao surveyDao;

    public SurveyServiceImpl(SqliteSurveyDao surveyDao) {
        this.surveyDao = surveyDao;
    }

    @Override
    public Survey getSurveyById(Integer surveyId) {
        return surveyDao.getSurveyById(surveyId);
    }

    @Override
    public List<Survey> getAllSurveys() {
        return surveyDao.getAllSurveys();
    }

    @Override
    public List<Survey> getSurveysByTenantUserId(Integer integer, int i) {
        return null;
    }

    @Override
    public Survey saveSurvey(Survey survey) {
        return null;
    }

    @Override
    public void deleteSurvey(Integer surveyId) {
        surveyDao.deleteSurvey(surveyId);
    }

    @Override
    public void enableSurvey(Survey survey, boolean b) {

    }

    @Override
    public boolean surveyExists(String s) {
        return false;
    }

    @Override
    public void setPublishedStatus(Survey survey, Integer integer) {

    }

}
