package ke.co.hoji.android.data;

/**
 * Created by gitahi on 20/05/15.
 */
public class LaunchType {

    public static final int NEW = 1;
    public static final int OPEN = 2;
    public static final int RESUME = 3;
}
