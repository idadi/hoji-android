package ke.co.hoji.android.data;

/**
 * Created by gitahi on 08/05/15.
 */
public class Constants {

    /**
     * How frequently we want location updates, in milliseconds.
     */
    public static final long LOCATION_REQUEST_INTERVAL = 2000;

    /**
     * The fastest we want location updates, assuming some other app is asking for location
     * more frequently than we are.
     */
    public static final long LOCATION_FASTEST_REQUEST_INTERVAL = 1000;

    /**
     * A level of accuracy between 0 and this amount is considered high and fires a location
     * request in high accuracy mode. Otherwise the request is made with a more conservative
     * priority setting.
     */
    public static final long HIGH_ACCURACY_THRESHOLD = 600;

    /**
     * The number of hours a user must wait before being able to make an access request to the
     * same tenant.
     */
    public static final long ACCESS_REQUEST_WAITING_PERIOD = 48;

    private static final String PACKAGE_NAME = "ke.co.hoji.android";

    public static final String CONTEXT = "context";

    public static final int SUCCESS = 0;
    public static final int FAILURE = 1;

    public static final String SIGN_UP = PACKAGE_NAME + ".SIGN_UP";

    public static final String SHOW_LOCATION_DIALOG = PACKAGE_NAME + ".SHOW_LOCATION_DIALOG";

    public static final String LAUNCH_TYPE = PACKAGE_NAME + ".LAUNCH_TYPE";
    public static final String DEFLATED_FORM = PACKAGE_NAME + ".FORM";
    public static final String DEFLATED_RECORD = PACKAGE_NAME + ".RECORD";
    public static final String DEFLATED_RECORDS = PACKAGE_NAME + ".RECORDS";
    public static final String DEFLATED_LIVE_FIELD = PACKAGE_NAME + ".LIVE_FIELD";
    public static final String RECORD_SEARCH = PACKAGE_NAME + ".RECORD_SEARCH";

    public static final String LITE_RECORDS = PACKAGE_NAME + ".LITE_RECORDS";

    public static final String BROADCAST_ACTION = PACKAGE_NAME + ".BROADCAST";
    public static final String EXTENDED_DATA_STATUS = PACKAGE_NAME + ".STATUS";
    public static final String SHOW_UPLOAD_PROGRESS = PACKAGE_NAME + ".SHOW_UPLOAD_PROGRESS";

    public static final String RESULT_RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA = PACKAGE_NAME + ".RESULT_DATA";
    public static final String LOCATION_DATA = PACKAGE_NAME + ".LOCATION_DATA";

    public static final String SHARED_PREFERENCES = PACKAGE_NAME + ".SHARED_PREFERENCES";

    /**
     * The link to the Hoji app on Google Play Store.
     */
    public static final String PLAY_STORE_URL = "https://play.google.com/store/apps/details?id=ke.co.hoji.android";

    /**
     * The link to the Hoji server.
     */
    public static final String SERVER_URL = ke.co.hoji.core.data.Constants.Server.URL;

    /**
     * The support email address.
     */
    public static final String SUPPORT_EMAIL = "support@hoji.co.ke";

    /**
     * The FCM push notification topic for project updates i.e. new forms or project data available.
     */
    public static final String PROJECT_UPDATE_TOPIC = "project_updates";

    /**
     * The ID for project update notifications. This is constant so that old updates are overwritten
     * by new ones.
     */
    public static final int PROJECT_UPDATE_NOTIFICATION_ID = 1;

    /**
     * The property key for storing feedback request/result status.
     */
    public static final String FEEDBACK_STATUS = "user.feedback";

    /**
     * The property key for storing whether or not to fire a project update based on a push
     * notification from the server..
     */
    public static final String FIRE_UPDATE = "fire.update";

    /**
     * The property key for storing the date when an access request was made. In order for this
     * to be unique per user per tenant, the actual key must be concatenated with the user code
     * and the tenant code, hence the trailing dot.
     */
    public static final String ACCESS_REQUEST = "access.request.";

    /**
     * The id of the default language of the project.
     */
    public static final String DEFAULT_LANGUAGE_ID = "-1";
}
