package ke.co.hoji.android.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import ke.co.hoji.R;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.core.widget.WidgetManager;

/**
 * Created by gitahi on 22/05/15.
 */
public class DeleteProjectTask extends AsyncTask<Void, Void, Void> {

    private final Survey survey;
    private final ConfigService configService;
    private final WidgetManager widgetManager;
    private final SettingsActivity settingsActivity;

    private final ProgressDialog progressDialog;

    public DeleteProjectTask(
            Survey survey,
            ConfigService configService,
            WidgetManager widgetManager,
            SettingsActivity settingsActivity,
            ProgressDialog progressDialog
    ) {
        this.survey = survey;
        this.configService = configService;
        this.widgetManager = widgetManager;
        this.settingsActivity = settingsActivity;
        this.progressDialog = progressDialog;
    }

    @Override
    protected void onPreExecute() {
        if (progressDialog != null) {
            progressDialog.setMessage(settingsActivity.getResources()
                    .getString(R.string.deleting_survey, survey.getName()));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        configService.deleteSurvey(survey);
        widgetManager.clearResponseCache(null);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (!settingsActivity.isFinishing()
                && progressDialog != null
                && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        settingsActivity.projectDeleteCompleted();
        Toast.makeText(settingsActivity, settingsActivity.getResources()
                        .getString(R.string.survey_deleted, survey.getName()),
                Toast.LENGTH_SHORT).show();
    }
}
