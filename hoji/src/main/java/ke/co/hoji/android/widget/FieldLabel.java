package ke.co.hoji.android.widget;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import ke.co.hoji.R;
import ke.co.hoji.android.AndroidHojiContext;
import ke.co.hoji.android.activity.SettingsActivity;
import ke.co.hoji.android.helper.UiUtils;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.service.model.TranslationService;

/**
 * Created by gitahi on 13/05/15.
 */
public class FieldLabel extends LinearLayout {

    private TextView fieldDescriptionTextView;
    private TextView fieldInstructionsTextView;

    private final AndroidHojiContext androidHojiContext;

    public FieldLabel(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.field_label, this);
        androidHojiContext = (AndroidHojiContext) context.getApplicationContext();
        loadViews();
    }

    private void loadViews() {
        fieldDescriptionTextView = findViewById(R.id.fieldDescriptionTextView);
        fieldInstructionsTextView = findViewById(R.id.fieldInstructionsTextView);
        fieldInstructionsTextView.setMovementMethod(new ScrollingMovementMethod());
    }

    public void setField(Field field, Record record, String nameOfUser) {
        TranslationService translationService = androidHojiContext.getModelServiceManager().getTranslationService();
        Integer languageId = androidHojiContext.getLanguageId();
        String fieldText = translationService.translate(
                field,
                languageId,
                Field.TranslatableComponent.DESCRIPTION,
                field.getDescription()
        );
        if (field.getType().getDataType() != FieldType.DataType.NULL) {
            if (showFieldNumbersInEditMode(fieldDescriptionTextView.getContext())) {
                fieldText = "<b>" + field.getName() + ". </b>" + fieldText;
            }
        }
        fieldDescriptionTextView.setText(UiUtils.getText(fieldText, record, nameOfUser));
        fieldInstructionsTextView.setText(
                UiUtils.getText(
                        translationService.translate(
                                field,
                                languageId,
                                Field.TranslatableComponent.INSTRUCTIONS,
                                field.getInstructions()
                        ),
                        record,
                        nameOfUser
                )
        );
        UiUtils.format(field, fieldDescriptionTextView);
        UiUtils.hideOrShow(fieldDescriptionTextView, fieldInstructionsTextView);
        UiUtils.scrollToTop(fieldDescriptionTextView, fieldInstructionsTextView);
        if (fieldInstructionsTextView.getText().toString().isEmpty()) {
            fieldDescriptionTextView.setPadding(
                    fieldDescriptionTextView.getPaddingLeft(),
                    fieldDescriptionTextView.getPaddingTop(),
                    fieldDescriptionTextView.getPaddingRight(),
                    fieldInstructionsTextView.getPaddingBottom()
            );
        } else {
            fieldDescriptionTextView.setPadding(
                    fieldDescriptionTextView.getPaddingLeft(),
                    fieldDescriptionTextView.getPaddingTop(),
                    fieldDescriptionTextView.getPaddingRight(),
                    0
            );
        }
    }

    public void clear() {
        UiUtils.clear(fieldDescriptionTextView, fieldInstructionsTextView);
    }

    private boolean showFieldNumbersInEditMode(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsActivity.SHOW_FIELD_NUMBERS_IN_EDIT_MODE, Boolean.TRUE);
    }
}
