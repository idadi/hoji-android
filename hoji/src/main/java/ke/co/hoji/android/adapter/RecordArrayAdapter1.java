package ke.co.hoji.android.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

import java.util.List;

import ke.co.hoji.core.data.model.Record;

public class RecordArrayAdapter1 extends ArrayAdapter<Record> {

    private final boolean editable;

    public RecordArrayAdapter1(Context context, int resource, List<Record> records, boolean editable) {
        super(context, resource, records);
        this.editable = editable;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Record record = getItem(position);
        CheckedTextView checkedTextView = (CheckedTextView) super.getView(position, convertView, parent);
        checkedTextView.setText(record.getCaption());
        return checkedTextView;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return editable;
    }

    @Override
    public boolean isEnabled(int position) {
        return editable;
    }
}
