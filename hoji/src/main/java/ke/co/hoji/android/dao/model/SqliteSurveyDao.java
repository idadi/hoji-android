package ke.co.hoji.android.dao.model;


import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import ke.co.hoji.android.DaoException;
import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.model.Survey;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by gitahi on 15/04/15.
 */
public class SqliteSurveyDao {

    private final SqliteSqlExecutor se;

    public SqliteSurveyDao(SqliteSqlExecutor se) {
        this.se = se;
    }

    public List<Survey> getAllSurveys() {
        List<Survey> surveys = new ArrayList<>();
        String select = "SELECT id, code, name, user_id, enabled, status, access, free FROM survey";
        Map<String, Object> params = se.createParameterMap();
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                Survey survey = new Survey
                    (
                        cursor.getInt(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("name")),
                        cursor.getInt(cursor.getColumnIndex("enabled")) > 0,
                        cursor.getString(cursor.getColumnIndex("code"))
                    );
                survey.setAccess(cursor.getInt(cursor.getColumnIndex("access")));
                survey.setFree(cursor.getInt(cursor.getColumnIndex("free")) > 0);
                surveys.add(survey);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteSurveyDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return surveys;
    }

    public Survey getSurveyById(int surveyId) {
        Survey survey = null;
        String select = "SELECT id, code, name, user_id, enabled, status, access, free"
            + "FROM survey "
            + "WHERE id = ?";
        Map<String, Object> params = se.createParameterMap("id", surveyId);
        Cursor cursor = null;
        try {
            cursor = se.rawQuery(select, params);
            while (cursor.moveToNext()) {
                survey = new Survey
                    (
                        cursor.getInt(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("name")),
                        cursor.getInt(cursor.getColumnIndex("enabled")) > 0,
                        cursor.getString(cursor.getColumnIndex("code"))
                    );
                survey.setAccess(cursor.getInt(cursor.getColumnIndex("access")));
                survey.setFree(cursor.getInt(cursor.getColumnIndex("free")) > 0);
            }
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteSurveyDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex, select, params);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return survey;
    }

    public void deleteSurvey(Integer surveyId) {
        se.beginTransaction();
        try {
            se.delete("survey", null, null);
            se.delete("choice", null, null);
            se.delete("choice_group", null, null);
            se.delete("reference", null, null);
            se.delete("reference_level", null, null);
            se.delete("record", null, null);
            se.setTransactionSuccessful();
        } catch (SQLiteException ex) {
            Logger.getLogger(SqliteRecordDao.class.getName()).log(Level.SEVERE,
                null, ex);
            throw new DaoException(ex.getMessage());
        } finally {
            se.endTransaction();
        }
    }
}
