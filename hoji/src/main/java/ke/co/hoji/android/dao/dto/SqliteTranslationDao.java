package ke.co.hoji.android.dao.dto;

import ke.co.hoji.android.SqliteSqlExecutor;
import ke.co.hoji.core.data.dto.Translation;

import java.util.List;
import java.util.Map;

public class SqliteTranslationDao {

    private final SqliteSqlExecutor se;

    public SqliteTranslationDao(SqliteSqlExecutor se) {
        this.se = se;
    }

    public List<Translation> saveTranslations(List<Translation> translations) {
        se.beginTransaction();
        try {
            for (Translation translation : translations) {
                Map<String, Object> params = se.createParameterMap();
                params.put("translatable", translation.getTranslatableName());
                params.put("translatable_id", translation.getTranslatableId());
                params.put("language_id", translation.getLanguageId());
                params.put("component", translation.getComponent());
                params.put("value", translation.getValue());
                if (!translationExists(translation.getId())) {
                    params.put("id", translation.getId());
                    se.insert("translation", params);
                } else {
                    Map<String, Object> whereParams = se.createParameterMap("id", translation.getId());
                    se.update("translation", params, "id = ?", whereParams);
                }
            }
            se.setTransactionSuccessful();
        } finally {
            se.endTransaction();
        }
        return translations;
    }

    private boolean translationExists(int translationId) {
        String select = "SELECT COUNT(id) FROM translation WHERE id = ?";
        return se.exists(select, translationId);
    }
}
