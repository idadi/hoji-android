UPDATE field_type SET ordinal=3.5 WHERE rowid=17;
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,response_class) VALUES(18,18,'RNK','Ranked choices (Static)',3.6,3,0,'ke.co.hoji.android.widget.RankingWidget','ke.co.hoji.core.response.RankingResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,response_class) VALUES(19,19,'RNP','Ranked choices (Pop-up)',3.7,3,0,'ke.co.hoji.android.widget.PopupRankingWidget','ke.co.hoji.core.response.RankingResponse');
