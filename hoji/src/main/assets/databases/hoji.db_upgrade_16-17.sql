DROP TABLE form; -- due to schema mismatch
CREATE TABLE "form" (
  "id" int(11) NOT NULL ,
  "name" varchar(45) NOT NULL,
  "description" varchar(255) DEFAULT NULL,
  "ordinal" decimal(6,2) NOT NULL,
  "survey_id" int(11) NOT NULL,
  "gps_capture" int(11) NOT NULL DEFAULT '1',
  "min_gps_accuracy" int(11) NOT NULL DEFAULT '-1',
  "no_of_gps_attempts" int(11) NOT NULL DEFAULT '3',
  "enabled" tinyint(1) NOT NULL DEFAULT '1',
  "minor_version" int(11) NOT NULL DEFAULT '0',
  "major_version" int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY ("id")
  CONSTRAINT "fk_form_survey" FOREIGN KEY ("survey_id") REFERENCES "survey" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX "form_fk_form_survey_idx" ON "form" ("survey_id");
