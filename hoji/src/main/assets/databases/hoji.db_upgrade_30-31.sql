PRAGMA foreign_keys=off;

BEGIN TRANSACTION;

ALTER TABLE choice RENAME TO choice_old;

CREATE TABLE `choice` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `scale` int(11) NOT NULL DEFAULT '0',
  `loner` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_choice_parent` FOREIGN KEY (`parent_id`) REFERENCES `choice` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
);

INSERT INTO choice (`id`, `name`, `description`, `scale`, `loner`, `parent_id`)
  SELECT `id`, `name`, `description`, `scale`, `loner`, `parent_id`
  FROM choice_old;

COMMIT;

PRAGMA foreign_keys=on;


DROP TABLE choice_old;

