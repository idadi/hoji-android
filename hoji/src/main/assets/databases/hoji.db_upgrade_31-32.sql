PRAGMA foreign_keys=off;

BEGIN TRANSACTION;

ALTER TABLE choice_group_choice RENAME TO choice_group_choice_old;

CREATE TABLE `choice_group_choice`
(
	`choice_group_id` int NOT NULL,
	`choice_id` int NOT NULL,
	`ordinal` decimal(10,2) NOT NULL,
	`code` varchar(45) NULL,
	`description` varchar(255) NULL,
	`scale` int NOT NULL DEFAULT '0',
	`loner` tinyint(1) NOT NULL DEFAULT '0',
	`choice_parent_id` int NULL,
	PRIMARY KEY (`choice_group_id`, `choice_id`),
	CONSTRAINT `fk_choice_group_choice_choice` FOREIGN KEY (`choice_id`) references `choice` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `fk_choice_group_choice_choice_group` FOREIGN KEY (`choice_group_id`) REFERENCES `choice_group` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `fk_choice_group_choice_choice_parent` FOREIGN KEY (`choice_parent_id`) REFERENCES `choice` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE index `fk_choice_group_choice_choice_idx` ON `choice_group_choice` (`choice_id`);

INSERT INTO choice_group_choice (`choice_group_id`, `choice_id`, `ordinal`)
  SELECT `choice_group_id`, `choice_id`, `ordinal`
  FROM choice_group_choice_old;

/* Decrement minor form version in order to successfully trigger a project update. */
  
UPDATE form SET minor_version = minor_version - 1;

COMMIT;

PRAGMA foreign_keys=on;


DROP TABLE choice_group_choice_old;

