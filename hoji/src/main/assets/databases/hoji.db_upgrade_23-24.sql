PRAGMA foreign_keys=off;

BEGIN TRANSACTION;

ALTER TABLE survey RENAME TO survey_old;

CREATE TABLE "survey" (
	`id`	int ( 11 ) NOT NULL,
	`code`	varchar ( 45 ) NOT NULL,
	`name`	varchar ( 45 ) NOT NULL,
	`user_id`	int ( 11 ) DEFAULT NULL,
	`enabled`	tinyint ( 1 ) NOT NULL DEFAULT '1',
	`status`	int ( 11 ) NOT NULL DEFAULT '0',
	`access`	int ( 11 ) NOT NULL DEFAULT '0',
	`free`	tinyint ( 1 ) NOT NULL DEFAULT '0',
	PRIMARY KEY(`id`)
);

INSERT INTO survey (id, code, name, user_id, enabled, status, access, free)
  SELECT id, code, name, user_id, enabled, status, access, free
  FROM survey_old;

COMMIT;

PRAGMA foreign_keys=on;

DROP TABLE survey_old;
