PRAGMA foreign_keys=off;

BEGIN TRANSACTION;

ALTER TABLE main_record
RENAME TO main_record_old;

CREATE TABLE "main_record" (
  `record_uuid`     char(36)    NOT NULL,
  `form_id`         int(11)     NOT NULL,
  `stage`           int(11)     NOT NULL,
  `version`         int(11)     NOT NULL DEFAULT '0',
  `start_latitude`  double               DEFAULT NULL,
  `start_longitude` double               DEFAULT NULL,
  `start_altitude`  double               DEFAULT NULL,
  `start_accuracy`  double               DEFAULT NULL,
  `start_age`       bigint(20)           DEFAULT NULL,
  `start_address`   varchar(255)         DEFAULT NULL,
  `end_latitude`    double               DEFAULT NULL,
  `end_longitude`   double               DEFAULT NULL,
  `end_altitude`    double               DEFAULT NULL,
  `end_accuracy`    double               DEFAULT NULL,
  `end_age`         bigint(20)           DEFAULT NULL,
  `end_address`     varchar(255)         DEFAULT NULL,
  `date_completed`  bigint(20)           DEFAULT NULL,
  `date_uploaded`   bigint(20)           DEFAULT NULL,
  `user_id`         int(11)              DEFAULT NULL,
  `device_id`       char(36)    NOT NULL,
  `form_version`    varchar(50) NOT NULL DEFAULT '0',
  `app_version`     varchar(50)          DEFAULT NULL,
  `id`              char(24)             DEFAULT NULL,
  PRIMARY KEY (`record_uuid`),
  CONSTRAINT `fk_main_record_form` FOREIGN KEY (`form_id`) REFERENCES `form` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_main_record_record` FOREIGN KEY (`record_uuid`) REFERENCES `record` (`uuid`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

INSERT INTO main_record (`record_uuid`,
                         `form_id`,
                         `stage`,
                         `version`,
                         `start_latitude`,
                         `start_longitude`,
                         `start_accuracy`,
                         `start_age`,
                         `start_address`,
                         `end_latitude`,
                         `end_longitude`,
                         `end_accuracy`,
                         `end_age`,
                         `end_address`,
                         `date_completed`,
                         `date_uploaded`,
                         `user_id`,
                         `device_id`,
                         `form_version`,
                         `id`)
SELECT `record_uuid`,
       `form_id`,
       `stage`,
       `version`,
       `start_latitude`,
       `start_longitude`,
       `start_accuracy`,
       `start_age`,
       `start_address`,
       `end_latitude`,
       `end_longitude`,
       `end_accuracy`,
       `end_age`,
       `end_address`,
       `date_completed`,
       `date_uploaded`,
       `user_id`,
       `device_id`,
       `form_version`,
       `id`
FROM main_record_old;

COMMIT;

PRAGMA foreign_keys=on;

DROP TABLE main_record_old;