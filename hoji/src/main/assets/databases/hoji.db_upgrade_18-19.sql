DROP TABLE field_type; -- due to schema mismatch
CREATE TABLE "field_type" (
	`id`	int ( 11 ) NOT NULL,
	`code`	char ( 3 ) NOT NULL,
	`name`	varchar ( 45 ) NOT NULL,
	`ordinal`	decimal ( 6 , 2 ) NOT NULL DEFAULT '0.00',
	`data_type`	int ( 11 ) NOT NULL,
	`group`	int ( 11 ) NOT NULL DEFAULT '0',
	`widget_class`	varchar ( 255 ) NOT NULL,
	`matrix_widget_class`	varchar ( 255 ) NOT NULL,
	`response_class`	varchar ( 255 ) NOT NULL DEFAULT NULL,
	PRIMARY KEY(`id`)
);
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(1,1,'SCH','Single choice',1,3,0,'ke.co.hoji.android.widget.RadioWidget','ke.co.hoji.android.widget.DropdownWidget','ke.co.hoji.core.response.SingleChoiceResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(3,3,'MCH','Multiple choices',3,3,0,'ke.co.hoji.android.widget.MultipleChoiceWidget','ke.co.hoji.android.widget.PopupMultipleChoiceWidget','ke.co.hoji.core.response.MultipleChoiceResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(4,4,'DNM','Decimal',5,2,1,'ke.co.hoji.android.widget.DecimalWidget','ke.co.hoji.android.widget.DecimalWidget','ke.co.hoji.core.response.DecimalResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(5,5,'WNM','Integer',4,1,1,'ke.co.hoji.android.widget.NumberWidget','ke.co.hoji.android.widget.NumberWidget','ke.co.hoji.core.response.NumberResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(6,6,'SCL','Slinding scale',6,1,1,'ke.co.hoji.android.widget.ScaleWidget','ke.co.hoji.android.widget.ScaleWidget','ke.co.hoji.core.response.NumberResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(7,7,'ETX','Single line (Encrypted)',9,4,2,'ke.co.hoji.android.widget.TextWidget','ke.co.hoji.android.widget.TextWidget','ke.co.hoji.core.response.EncryptedTextResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(8,8,'MTX','Multiple lines',8,3,2,'ke.co.hoji.android.widget.MultiTextWidget','ke.co.hoji.android.widget.MultiTextWidget','ke.co.hoji.core.response.TextResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(9,9,'STX','Single line',7,3,2,'ke.co.hoji.android.widget.TextWidget','ke.co.hoji.android.widget.TextWidget','ke.co.hoji.core.response.TextResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(10,10,'DAT','Date',10,1,3,'ke.co.hoji.android.widget.DateWidget','ke.co.hoji.android.widget.DateWidget','ke.co.hoji.core.response.DateResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(11,11,'IMG','Image',13,4,4,'ke.co.hoji.android.widget.ImageWidget','ke.co.hoji.android.widget.ImageWidget','ke.co.hoji.core.response.ImageResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(12,12,'LBL','Label',12,0,4,'ke.co.hoji.android.widget.LabelWidget','ke.co.hoji.android.widget.LabelWidget','ke.co.hoji.core.response.LabelResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(13,13,'TIM','Time',11,1,3,'ke.co.hoji.android.widget.TimeWidget','ke.co.hoji.android.widget.TimeWidget','ke.co.hoji.core.response.TimeResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(14,14,'MRX','Matrix (Sub-form)',14,1,5,'ke.co.hoji.android.widget.MatrixWidget','ke.co.hoji.android.widget.MatrixWidget','ke.co.hoji.core.response.MatrixResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(15,15,'RCD','Link (Relate forms)',15,3,5,'ke.co.hoji.android.widget.RecordWidget','ke.co.hoji.android.widget.RecordWidget','ke.co.hoji.core.response.RecordResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(16,16,'REF','Sampling frame',16,1,5,'ke.co.hoji.android.widget.ReferenceWidget','ke.co.hoji.android.widget.ReferenceWidget','ke.co.hoji.core.response.ReferenceResponse');
INSERT INTO field_type(rowid,id,code,name,ordinal,data_type,"group",widget_class,matrix_widget_class,response_class) VALUES(18,18,'RNK','Ranked choices',3.6,3,0,'ke.co.hoji.android.widget.RankingWidget','ke.co.hoji.android.widget.RankingWidget','ke.co.hoji.core.response.RankingResponse');
CREATE INDEX `field_type_fk_field_type_1_idx` ON `field_type` (
	`response_class`
);
