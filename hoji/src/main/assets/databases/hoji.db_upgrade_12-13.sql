UPDATE field_type SET name='Single choice (Option buttons)' WHERE rowid=1;
UPDATE field_type SET name='Single choice (Drop-down list)' WHERE rowid=2;
UPDATE field_type SET name='Multiple choice (Checkboxes)' WHERE rowid=3;
UPDATE field_type SET name='Decimal', ordinal=5 WHERE rowid=4;
UPDATE field_type SET name='Integer', ordinal=4 WHERE rowid=5;
UPDATE field_type SET name='Slinding scale' WHERE rowid=6;
UPDATE field_type SET name='Single line (Encrypted)', ordinal=9 WHERE rowid=7;
UPDATE field_type SET name='Multiple lines' WHERE rowid=8;
UPDATE field_type SET name='Single line', ordinal=7 WHERE rowid=9;
UPDATE field_type SET name='Date' WHERE rowid=10;
UPDATE field_type SET name='Image', ordinal=13 WHERE rowid=11;
UPDATE field_type SET name='Label' WHERE rowid=12;
UPDATE field_type SET name='Time', ordinal=11 WHERE rowid=13;
UPDATE field_type SET name='Matrix (Sub-form)' WHERE rowid=14;
UPDATE field_type SET name='Link (Relate forms)' WHERE rowid=15;
UPDATE field_type SET name='Sampling frame' WHERE rowid=16;
DROP TABLE form; -- due to schema mismatch
CREATE TABLE "form" (
  "id" int(11) NOT NULL ,
  "name" varchar(45) NOT NULL,
  "description" varchar(255) DEFAULT NULL,
  "ordinal" decimal(6,2) NOT NULL,
  "survey_id" int(11) NOT NULL,
  "gps_capture" int(11) NOT NULL DEFAULT '1',
  "min_gps_accuracy" int(11) NOT NULL DEFAULT '-1',
  "no_of_gps_attempts" int(11) NOT NULL DEFAULT '3',
  "enabled" tinyint(1) NOT NULL DEFAULT '1',
  "minor_modified" tinyint(1) NOT NULL DEFAULT '0',
  "major_modified" tinyint(1) NOT NULL DEFAULT '0',
  "minor_version" int(11) NOT NULL DEFAULT '0',
  "major_version" int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY ("id")
  CONSTRAINT "fk_form_survey" FOREIGN KEY ("survey_id") REFERENCES "survey" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX "form_fk_form_survey_idx" ON "form" ("survey_id");
