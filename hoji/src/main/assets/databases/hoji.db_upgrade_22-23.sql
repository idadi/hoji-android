UPDATE operator_descriptor SET name='Includes' WHERE rowid=8;
INSERT INTO operator_descriptor(rowid,id,name,class) VALUES(9,9,'Does not contain','ke.co.hoji.core.rule.NotContains');
INSERT INTO operator_descriptor(rowid,id,name,class) VALUES(10,10,'Does not include','ke.co.hoji.core.rule.NotBitwiseAnd');
