PRAGMA foreign_keys=off;

BEGIN TRANSACTION;

ALTER TABLE field RENAME TO field_old;

CREATE TABLE "field" (
	`id`	int ( 11 ) NOT NULL,
	`name`	varchar ( 45 ) NOT NULL,
	`column`	varchar ( 45 ) DEFAULT NULL,
	`description`	varchar ( 1000 ) DEFAULT NULL,
	`ordinal`	decimal ( 6 , 2 ) NOT NULL,
	`instructions`	varchar ( 1000 ) DEFAULT NULL,
	`field_type_id`	int ( 11 ) NOT NULL,
	`form_id`	int ( 11 ) NOT NULL,
	`enabled`	tinyint ( 1 ) NOT NULL DEFAULT '1',
	`captioning`	tinyint ( 1 ) NOT NULL DEFAULT '0',
	`searchable`	tinyint ( 1 ) NOT NULL DEFAULT '0',
	`filterable`	tinyint ( 1 ) NOT NULL DEFAULT '0',
	`output_type`	tinyint ( 1 ) DEFAULT NULL,
	`tag`	varchar ( 45 ) DEFAULT NULL,
	`default_value`	varchar ( 45 ) DEFAULT NULL,
	`missing_value`	TEXT,
	`missing_action`	int ( 11 ) NOT NULL DEFAULT '0',
	`min_value`	varchar ( 45 ) DEFAULT NULL,
	`min_action`	int ( 11 ) NOT NULL DEFAULT '2',
	`max_value`	varchar ( 45 ) DEFAULT NULL,
	`max_action`	int ( 11 ) NOT NULL DEFAULT '2',
	`regex_value`	varchar ( 255 ) DEFAULT NULL,
	`regex_action`	int ( 11 ) NOT NULL DEFAULT '2',
	`unique_value`	tinyint ( 1 ) NOT NULL DEFAULT '0',
	`unique_action`	int ( 11 ) DEFAULT '2',
	`choice_group_id`	int ( 11 ) DEFAULT NULL,
	`choice_filter_field_id`	int ( 11 ) DEFAULT NULL,
	`parent_id`	int ( 11 ) NOT NULL,
	`pipe_source_id`	int ( 11 ) DEFAULT NULL,
	`reference_field_id`	int ( 11 ) DEFAULT NULL,
	`main_record_field_id`	int ( 11 ) DEFAULT NULL,
	`matrix_record_field_id`	int ( 11 ) DEFAULT NULL,
	`base`	tinyint ( 1 ) DEFAULT '0',
	PRIMARY KEY(`id`),
	CONSTRAINT `fk_field_choice_group` FOREIGN KEY(`choice_group_id`) REFERENCES `choice_group`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `fk_field_form` FOREIGN KEY(`form_id`) REFERENCES `form`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `fk_field_type` FOREIGN KEY(`field_type_id`) REFERENCES `field_type`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO field (`id`, `name`, `column`, `description`, `ordinal`, `instructions`, `field_type_id`, `form_id`, `enabled`, `captioning`, `searchable`, `filterable`, `output_type`, `tag`, `default_value`, `missing_action`, `min_value`, `min_action`, `max_value`, `max_action`, `regex_value`, `regex_action`, `unique_value`, `unique_action`, `choice_group_id`, `choice_filter_field_id`, `parent_id`, `pipe_source_id`, `reference_field_id`, `main_record_field_id`, `matrix_record_field_id`, `base`)
  SELECT `id`, `name`, `column`, `description`, `ordinal`, `instructions`, `field_type_id`, `form_id`, `enabled`, `captioning`, `searchable`, `filterable`, `output_type`, `tag`, `default_value`, `missing_action`, `min_value`, `min_action`, `max_value`, `max_action`, `regex_value`, `regex_action`, `unique_value`, `unique_action`, `choice_group_id`, `choice_filter_field_id`, `parent_id`, `pipe_source_id`, `reference_field_id`, `main_record_field_id`, `matrix_record_field_id`, `base`
  FROM field_old;

COMMIT;

PRAGMA foreign_keys=on;


DROP TABLE field_old;

