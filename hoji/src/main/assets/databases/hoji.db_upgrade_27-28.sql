PRAGMA foreign_keys=off;

BEGIN TRANSACTION;

ALTER TABLE property RENAME TO property_old;

CREATE TABLE "property" (
  "key" varchar(255) NOT NULL,
  "value" text NOT NULL,
  "survey_id" int(11) DEFAULT NULL,
  PRIMARY KEY ("key")
  CONSTRAINT "fk_property_survey" FOREIGN KEY ("survey_id") REFERENCES "survey" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX "property_fk_property_survey" ON "property" ("survey_id");

INSERT INTO property (`key`, `value`)
  SELECT `key`, `value`
  FROM property_old WHERE value IS NOT NULL;

COMMIT;

PRAGMA foreign_keys=on;


DROP TABLE property_old;

